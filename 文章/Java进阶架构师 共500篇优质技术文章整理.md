# 合集 | Java进阶架构师 共500篇优质技术文章整理





## **SpringBoot 源码分析 专题** 

1. [【原创】001 | 搭上 SpringBoot 自动注入源码分析专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888374&idx=1&sn=0fc793e497332b1b9b5e807a6c38cedb&chksm=8fb5481bb8c2c10dd984b7d863feee8929bc3f7a7ad327bb86b331c37e6ddefa0456b017ca99&scene=21#wechat_redirect)
2. [【原创】002 | 搭上 SpringBoot 事务源码分析专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888418&idx=2&sn=9e640f17889d410d7f0c20967e5ab191&chksm=8fb5484fb8c2c159b09065e437ca46e89d7d133b36f9dfeba37c550f76dacab8d296b82080aa&scene=21#wechat_redirect)
3. [【原创】003 | 搭上基于 SpringBoot 事务思想实战专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888505&idx=1&sn=b92bfd425cf50f93210be028e008ba3a&chksm=8fb54894b8c2c1824e251ff88127fca0725a8186db660aea30361b0ae9c87a132c828d5dd7d4&scene=21#wechat_redirect)
4. [【原创】004 | 搭上 SpringBoot 事务诡异事件分析专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888540&idx=2&sn=638239681c7c5d84ae5e16580ffa5f92&chksm=8fb548f1b8c2c1e7ad8fc94dfab856f951d0802af35f896f19a2801e48b0098a4e39ab2edde0&scene=21#wechat_redirect)
5. [【原创】005 | 搭上 SpringBoot 请求处理源码分析专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888575&idx=2&sn=da8fe312cbb820da90da7ba32a1b11a0&chksm=8fb548d2b8c2c1c458416572e5568c502188f3dc09de5b9eee77ce65e2534f82ab4052f6e604&scene=21#wechat_redirect)
6. [【原创】006 | 搭上 SpringBoot 参数解析返回值处理源码分析专车](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888820&idx=2&sn=6263b6287ee429212eda0f8db826aa90&chksm=8fb549d9b8c2c0cfc0c1e2195d8d337411fa51d913f177dfdba6b6881fedf9357a8d7d4987d9&scene=21#wechat_redirect)
7. [【原创】007 | 搭上 SpringBoot 拦截器源码分析专车](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888903&idx=2&sn=513de7b30b29bbaf26c8dc237d166b74&scene=21#wechat_redirect)
8. [【原创】008 | SpringBoot 源码专车总结（共8篇）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889289&idx=1&sn=ee039b1255d2c74f40704f72d49693a4&chksm=8fb54fe4b8c2c6f2d964e77133a93344b55ae964ddeaf594c1e049f735840d3fa26ee8dfb6ec&scene=21#wechat_redirect)



**springboot 进阶 专题**

1. [干货 | SpringBoot注解大全，值得收藏](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888082&idx=3&sn=6d5d71fa2f0d3294681eb92e8dabec6b&chksm=8fb54b3fb8c2c229f69da0535c8b497ac5aa52f40cbb55330ab103689e4859a5e7ac8752e5a5&scene=21#wechat_redirect)
2. [实战 | 手把手带你用数据库中间件Mycat+SpringBoot完成分库分表](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888064&idx=2&sn=1bec3f9b2a815730e0bb65b09f5084e4&chksm=8fb54b2db8c2c23bc0930cd4b565e61e0be3db0a45d66d7098af83231791b2f0cc55afd1593e&scene=21#wechat_redirect)
3. [实战 | springboot+redis+拦截器 实现接口幂等性校验](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888174&idx=2&sn=1166eaeade64fb87238a38958746b1cb&chksm=8fb54b43b8c2c255eaec19252a2043a9146254e22c98a5e8ea45354d70c5d22fe13d2984be6f&scene=21#wechat_redirect)
4. [实战 | SpringBoot微信点餐系统（附源码）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888225&idx=2&sn=f0103e2bba7e2247ac9165d098a9e158&chksm=8fb54b8cb8c2c29ab2f2374b48d2f24d4a4d4e34d74d40e4598fd4638f7396b1bb7f594ae3d8&scene=21#wechat_redirect)
5. [实战 | SpringBoot+Redis分布式锁：模拟抢单](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889477&idx=1&sn=59eaec62ffa3fd705b64bbc4d2d0ec70&chksm=8fb54ca8b8c2c5be250c9706f493f5a1f81f9ca07aa3dafbed91041763a9a93457a39237e1c4&scene=21#wechat_redirect)
6. [实战 | SpringBoot实现过滤器、拦截器与切片](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889458&idx=2&sn=2945aeb98aa9fa56aaf7182c7f9af5e5&chksm=8fb54c5fb8c2c5493a9e36bea6766ee5ad95079ae71a06fe8b48c0e69d2691e034b3fcb8766a&scene=21#wechat_redirect)
7. [深度 | 面试官：能说下 SpringBoot 启动原理吗？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888082&idx=2&sn=74deda431829aa0d18ba3d01ee46aba9&chksm=8fb54b3fb8c2c2291796d3225548c387b0615266e16e5af94263163792826eb167c1bb6901a2&scene=21#wechat_redirect)
8. [源码探秘：Tomcat 在 SpringBoot 中是如何启动的？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889470&idx=2&sn=f3d1ec9776372e62e3c494a938f4d6ea&chksm=8fb54c53b8c2c54551480e074558e785e6056cf1d30d57a487f7ce85142eb14cf8662a2be66b&scene=21#wechat_redirect)



**并发编程 专题** 

1. [【原创】01 | 开篇感言](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888443&idx=2&sn=149cae18a13e89e5e23991b6761f9048&scene=21#wechat_redirect)
2. [【原创】02 | 并发编程三大核心问题](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888446&idx=2&sn=2d341878615e238eb572e855199b9a7e&scene=21#wechat_redirect)
3. [【原创】03 | 重排序-可见性和有序性问题根源](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888476&idx=1&sn=16d2bc7d2298f5c2853e467420c04ab8&scene=21#wechat_redirect)
4. [【原创】04 | Java 内存模型详解](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888495&idx=2&sn=581ccb6a6afb18e31a41bf8988cf28b0&scene=21#wechat_redirect)
5. [【原创】05 | 深入理解 volatile](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888521&idx=1&sn=c880331ef37b5f111f553ad0b4064bab&scene=21#wechat_redirect)
6. [【原创】06 | 你不知道的 final](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888688&idx=2&sn=d12fb2a961049429650723ce5a395f78&scene=21#wechat_redirect)
7. [【原创】07 | synchronized原理](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888833&idx=2&sn=1874f88dd96ee2102148f372ca5a544a&scene=21#wechat_redirect)
8. [【原创】08 | synchronized锁优化](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888881&idx=1&sn=75a4b57b369ff3c79ac372a6ba891101&scene=21#wechat_redirect)
9. [【原创】09 | 基础干货](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889025&idx=2&sn=5022220da1111ce0b7a654f98db07318&scene=21#wechat_redirect)
10. [【原创】10 | 线程状态](https://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889232&idx=2&sn=87c19ffa63cb7244d18c542b45ad445c&scene=21#wechat_redirect)
11. [【原创】11 | 线程调度](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889377&idx=2&sn=7f71200b1e0401b672201e285ec50640&chksm=8fb54c0cb8c2c51a21e9c16fea87509fcd8992af06a80e65bbe091fdcf4131f23fa2654b9702&scene=21#wechat_redirect)
12. [【原创】12 | 揭秘CAS](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889535&idx=2&sn=cb4ccac78e227ca1ba1381b1e3271a00&chksm=8fb54c92b8c2c584265a43ef071d9b39f044b603d2e9a9892d14bfad220e6a4bbd22c4d77d85&scene=21#wechat_redirect)
13. [【原创】13 | LookSupport](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889549&idx=2&sn=9aa5c65aba21960a7f8a0d5a4ea9e989&chksm=8fb54ce0b8c2c5f603498595e3012b41e6c546096aeee1bb31d5e73bc70d69a3707546822178&scene=21#wechat_redirect)
14. [【原创】14 | AQS源码分析](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889563&idx=2&sn=9f3c58d883a0775072ef9a061d35555b&chksm=8fb54cf6b8c2c5e0dd0dc0588cef43f5799c37837bf90b3cd81baa702412a7729fdb4ec2e903&scene=21#wechat_redirect)
15. [【并发编程】一文带你读懂深入理解Java内存模型（面试版本）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886602&idx=1&sn=0ecac49a4572030d481d304e28b16315&chksm=8fb55167b8c2d8715f74c8fbc95df17e8cb9aaf3b36d7a82ccff595ea08ea61c254220682ac9&scene=21#wechat_redirect)
16. [来，带你鸟瞰 Java 中4款常用的并发框架！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887017&idx=1&sn=c5b5317cab2f4dc3760f6771856ac47b&chksm=8fb556c4b8c2dfd2a9d50198b8cc0c4f9a3a6b707b2931747907b481317ab9667d871d89c447&scene=21#wechat_redirect)
17. [高并发的核心技术 - 幂等的实现方案](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887213&idx=1&sn=10d505374f466086fc452fcdcd13ee94&chksm=8fb55780b8c2de96fc3009f5ee76709fd965866e9ca446958d0df32c4832515b7b650eed6d1e&scene=21#wechat_redirect)

**连载中，每周一到两篇**



**面试 专题**

1. [天真！这简历一看就是包装过的](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887555&idx=1&sn=af5f6b83aaabc71e47524c88ee9e0459&chksm=8fb5552eb8c2dc384dbbea2f58426bbb9c6394ae4e3b7a4836b4459a3b3e1eaa7b91065f53a5&scene=21#wechat_redirect)

2. [五步准备一份漂亮的Java简历！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886889&idx=1&sn=46d2c1cbf0ab8e3e65131910fcfefe39&chksm=8fb55644b8c2df52c84f8f3c3ce74bc13145fb869df610dbbcfa128b67e9af1f2034fe9117a8&scene=21#wechat_redirect)

3. [如何在面试中介绍自己的项目经验？如果有以下几点，就悬了](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886606&idx=1&sn=d3492190a79106f020ec241f57939750&chksm=8fb55163b8c2d87582bc8c746db2f18934099b73c0a12bbabd90a96cc4844bd5b9d8a5159474&scene=21#wechat_redirect)

4. [在做技术面试官时，我是这样甄别大忽悠的——如果面试时你有这样的表现，估计悬](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886083&idx=1&sn=21838a3dfd3e754d29fd92389931670c&chksm=8fb5536eb8c2da783e52953e9aa534f7568d859ab35147193c70da2caf38280322c3d2ffc50a&scene=21#wechat_redirect)

   

**BAT 等大厂面经 专题**

1. [【面经】通过五轮面试的阿里实习生亲述！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889415&idx=1&sn=61b430d6118c4fc876f869019cce5f63&chksm=8fb54c6ab8c2c57ca9b28cfefa46e3c776ed715cec09d92b0a40ffca4b23d9f05f6db73cee84&scene=21#wechat_redirect)
2. [【面经】阿里三面被挂，幸获内推，历经5轮终于拿到口碑offer](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889004&idx=1&sn=4ceae10457d543a4810fd917abb6438a&chksm=8fb54e81b8c2c797b1fa23bbea8270bfb786ebf32c63565394b49b77c69a686b4325c86b6bff&scene=21#wechat_redirect)
3. [【面经】面试面试鹅厂，我三面被虐的体无完肤。](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888342&idx=1&sn=7a0fa99d7fdb4056ca5a0a196d1272e7&chksm=8fb5483bb8c2c12d697dbba5d71355f5d7be9d7f9172047734314b1b86af27b366afa899a21d&scene=21#wechat_redirect)[。](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888342&idx=1&sn=7a0fa99d7fdb4056ca5a0a196d1272e7&chksm=8fb5483bb8c2c12d697dbba5d71355f5d7be9d7f9172047734314b1b86af27b366afa899a21d&scene=21#wechat_redirect)[。](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888342&idx=1&sn=7a0fa99d7fdb4056ca5a0a196d1272e7&chksm=8fb5483bb8c2c12d697dbba5d71355f5d7be9d7f9172047734314b1b86af27b366afa899a21d&scene=21#wechat_redirect)
4. [【面经】面试字节跳动，我被面试官狂怼全过程！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889269&idx=1&sn=0cd419514db33b648eb40bfd1b8812a8&chksm=8fb54f98b8c2c68ebcc0f6ec2e3f1a1126f16d3af1634ea9868d00c01318b9700341b5336766&scene=21#wechat_redirect)
5. [【面经】：为了拿到字节跳动offer，鬼知道我经历了啥...](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888935&idx=1&sn=cd4b77add9c305906cc8fc01e2687498&chksm=8fb54e4ab8c2c75c599ebecc7f41f3d2733f28f1e00040685ffd25bc27ce71c70929e1c632a1&scene=21#wechat_redirect)
6. [【面经】我三年开发经验，从字节跳动抖音组离职后，一口气拿到15家公司Offer，薪资再一次直线上涨~](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889426&idx=1&sn=49f1a44888e3f16d46ee9265b6d8889b&chksm=8fb54c7fb8c2c569a24190340d970d4c988fc02ec1939430d0cbaebda9c7805831a005b74799&scene=21#wechat_redirect)
7. [【面经】PDD面试官竟然问我new一个对象背后发生了什么？这太难了...](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889423&idx=2&sn=88e5926574bbecd1afceba44fb8acb53&chksm=8fb54c62b8c2c574298a3353d44277ebdd3569718ce975b0a9d616697f0ba571102222910b00&scene=21#wechat_redirect)
8. [【面经】一个妹子的后台面试经验总结（蚂蚁金服+美团+携程+滴滴+....）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889233&idx=1&sn=a5ae3cd7f47500f656a21f62293836b2&chksm=8fb54fbcb8c2c6aac3980500db61fd797709b5dd384d985b64aa36e5ae5a51cb0779840c2243&scene=21#wechat_redirect)
9. [【面经】GitHub 上四万 Star 大佬一举最终拿下一堆Offer：百度、阿里、腾讯、头条、网易游戏、华为](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887139&idx=1&sn=52bf5aeddf525dd282ebb3030f6f99d3&chksm=8fb5574eb8c2de58054d8e1e6c129dd8d62329d7ba4514c8dfaf349625b1c42e8601094d9d2f&scene=21#wechat_redirect)
10. [【面经】美团技术四面经历，女粉丝已拿到Offer！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887223&idx=1&sn=3d231935e25961f63680b204bdb76586&chksm=8fb5579ab8c2de8ce2ec70779591923e4d47813b5ff8a75398122577dbf90396f86a2f7a7f4b&scene=21#wechat_redirect)



**常见面试题 专题**

1. [10 道关于 Java 泛型的面试题](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886904&idx=1&sn=c86009c7575e45b93158bb31fea177bb&chksm=8fb55655b8c2df4392a49249f3a716e46b60f589e339dce8b2694c06a9b40fdcb008eb2cb7b1&scene=21#wechat_redirect)
2. [dubbo 面试18问（含答案）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887352&idx=2&sn=4f91cb206362006dc3226fadd1b4b75e&chksm=8fb55415b8c2dd0326b6362e2cc42841d0f7f8e0738e384a7f251aade4b4a210acc70d73c66a&scene=21#wechat_redirect)
3. [Java 程序员必须掌握的 8 道数据结构面试题（附答案），你会几道？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886876&idx=1&sn=4d8f6fbce2b05a725eb7a6f8e6324db3&chksm=8fb55671b8c2df67bc7bc1375095ba1e59435936cb69b00029d62fa4bfbec5d1f875dbc3a006&scene=21#wechat_redirect)
4. [面试问烂的 MySQL 四种隔离级别，看完吊打面试官！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887853&idx=1&sn=203bb6e366d0ca7898a13ccd8da311d6&chksm=8fb54a00b8c2c3169588baa15d64b228472878eeeaca62b7d4be15d4359028811674517b5e22&scene=21#wechat_redirect)
5. [SPI面试必问：Dubbo和JDK的SPI究竟有何区别?](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887635&idx=1&sn=0bbbc0df83cb08c8c81652896b06fe4a&chksm=8fb5557eb8c2dc6890bd314a3ef581ca626081a2f011637928a0f03486fd43f6217eed20eb26&scene=21#wechat_redirect)
6. [面试官问我：一个 TCP 连接可以发多少个 HTTP 请求？我竟然回答不上来...](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888022&idx=3&sn=cef92b5871ee4edbf4161e1be989a50b&chksm=8fb54afbb8c2c3ed3fcdd754a21e54585dc5d6cf16c325f7bfd88e8e648a6293d9412f44a9f3&scene=21#wechat_redirect)
7. [高频面试题：Spring 如何解决循环依赖？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888415&idx=2&sn=9a54c5de02bc2428411abde271b01c5d&chksm=8fb54872b8c2c164d3233b246a6c01d8969bb10f0e174d5ba226fe478824f38c08a117947ddc&scene=21#wechat_redirect)
8. [面试官：Mybatis 使用了哪些设计模式？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888460&idx=2&sn=e49a1aa79943f14057589d7d64410e45&chksm=8fb548a1b8c2c1b7eee8fcf461b9ee2fcaea0d15203613dc79b9b1150943702acd3557f76fdc&scene=21#wechat_redirect)
9. [面试官问：Redis 内存满了怎么办？我想不到！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888602&idx=1&sn=db0d9c28817d29a7cdab3df4b2960703&chksm=8fb54937b8c2c021a6f214c59440c091ab84a49f9857e68c016958a9445e5682e5de54e381fb&scene=21#wechat_redirect)
10. [【面经】高频面试题：如何保证缓存与数据库的双写一致性？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889436&idx=1&sn=d4653ef8fdd454688fc27012d5f9e348&chksm=8fb54c71b8c2c56772b32b6377557229a57d6022f54beedeb16447feab490c8378b9e81545e7&scene=21#wechat_redirect)
11. [【面经】面试官问：线程池除了常见的4种拒绝策略，你还知道哪些？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889413&idx=1&sn=1c1377f15b5c06af40a7b363816a1ba1&chksm=8fb54c68b8c2c57ef215ff2a24495ee6b881a73128bb96e5c4e4ed835dfed6e8689d909cbd4a&scene=21#wechat_redirect)
12. [【面经】慌了，面试居然被问到怎么做高并发系统的限流？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889411&idx=1&sn=932b98d4c0a7afd5187aafc93b5894aa&chksm=8fb54c6eb8c2c578d5039a6a9af9d53460541e4ffd9ffdf4692d217bffffa72ba42f5624c8ff&scene=21#wechat_redirect)
13. [我挂树上了：一道树的面试题](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889212&idx=1&sn=21fa3e4ede050b47dcf242d19b145a63&chksm=8fb54f51b8c2c6478256007ff5390853a063ea38e53f8f7b83210e1b7ac818e502a0041ec584&scene=21#wechat_redirect)



**Java进阶架构师**

![图片](https://mmbiz.qpic.cn/mmbiz_gif/XaklVibwUKn49x6AtibreZEicRCwKHPL3oiamQcj4tV0eUsocv5SEou4oiaSDMTjRs6ia6jFJLB78l0ic4NBnBda3mT6g/640?tp=webp&wxfrom=5&wx_lazy=1)

都看到这里了，还不关注下？！





**造个轮子 专题**

1. [写出我的第一个框架：迷你版Spring MVC](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885772&idx=1&sn=c3c08faca40e6949800898892f7c8f1e&chksm=8fb55221b8c2db372f2a7ece48104b2c0140126f35216447ea5f433d476eae41aaa73e939d30&scene=21#wechat_redirect)
2. [手把手带你实现JDK动态代理](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885835&idx=2&sn=afebc4040c33fec234d60e0b394c4677&chksm=8fb55266b8c2db701b29d6d94cf3e23aa6e6829423af6d16fb63ad01cccdd4d4e48ed1612502&scene=21#wechat_redirect)
3. [透彻理解MyBatis设计思想之手写实现](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885877&idx=1&sn=bdecd88b89bec4b932a85efacdded624&chksm=8fb55258b8c2db4e75f1e873e34230375035bfa404d7dda5831e8cbc999e843297d06f9f3d91&scene=21#wechat_redirect)
4. [理解数据库连接池底层原理之手写实现](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885907&idx=2&sn=c45700652dd58b5ab21b410e6b192bbd&chksm=8fb552beb8c2dba8a44203a3ac8b5bf7abbd6017ed76c13ab902afd589082e8c77be59540316&scene=21#wechat_redirect)
5. [对HashMap的思考及手写实现](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885937&idx=1&sn=5bec01af5834338812a1032096bf2c99&chksm=8fb5529cb8c2db8ae5366ea59b3fe910b70a6ebc102896ac09e2c963017a9d40d585907921e4&scene=21#wechat_redirect)
6. [透彻理解Spring事务设计思想之手写实现](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885967&idx=2&sn=a8634cafe753e1c6c18673917a5bfe92&chksm=8fb552e2b8c2dbf4223538769af6e40a363b3903849213cb67cef206d361e32aabbcecd58889&scene=21#wechat_redirect)
7. [手写实现一个迷你版的Tomcat](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885982&idx=1&sn=cb7f8bca3ec7057e66499df09ac5c7c7&chksm=8fb552f3b8c2dbe507b584633ef1d907225a8c0d0feb5d47ff0ee59600a91e0a5726e9253330&scene=21#wechat_redirect)
8. [纯手写实现一个RPC](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886008&idx=1&sn=6136ce25d0ec5f276dd71d9a7b225bf3&chksm=8fb552d5b8c2dbc34539968c586131d2e1272ae3e1d0850eec48fc5a828ca2433b29c5a89148&scene=21#wechat_redirect)
9. [自己动手写一个服务网关](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886060&idx=1&sn=8d52b7ea55257717145684e842f55a9c&chksm=8fb55301b8c2da178c0859307e6c8570e38ee979c780f151fc5b46aa5969c8572ad1864431d3&scene=21#wechat_redirect)
10. [解密Dubbo:自己动手编写一个较为完善的RPC框架（两万字干货）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886310&idx=2&sn=2e5e9f9ecf88b04753d1e1c887a693d0&chksm=8fb5500bb8c2d91d7c6da3e3614e08f27d791971a85a6a75a5d8d5467714ad93472f4d520690&scene=21#wechat_redirect)
11. [手把手带你设计一个百万级的消息推送系统](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886353&idx=2&sn=b998ca350b13be66ed11fb7945d8b832&chksm=8fb5507cb8c2d96a43cb64d2022678c423ac827dd5313d987113b3303a8e68a4c9c56c5c4811&scene=21#wechat_redirect)
12. [手把手带你秒杀架构实践（含完整代码）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886465&idx=1&sn=db5b5c92cd0c7bb740a9fa55540bae9a&chksm=8fb550ecb8c2d9fa221a5dca977bdbb7799257c64e7646eda730ab231713e13030c9ed6d096a&scene=21#wechat_redirect)
13. [为自己搭建一个分布式 IM(即时通讯) 系统](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886716&idx=1&sn=eb6da7119c3a11656f3f194b0bce0c7c&chksm=8fb55191b8c2d887e700dcc225144a2d27130bb35fdec1b680ea96923d59ea4fae4dff90c96e&scene=21#wechat_redirect)
14. [膨胀了！我要手写QQ底层！(附源码)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887628&idx=1&sn=a7600a931bc233f9c9c50d4b70d9c483&chksm=8fb55561b8c2dc7746b06c057c5db088fde6eb59c88f8d61010a77323e5c07a5372418816e6a&scene=21#wechat_redirect)
15. [手把手教你撸一个JSON解析器](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887022&idx=1&sn=cd12a10da3064f5f809e6adb42a9c643&chksm=8fb556c3b8c2dfd52e4468cf66129bab428825a2b964a9d4c1f2e80f638a02ea30982077f7ed&scene=21#wechat_redirect)
16. [从 0 开始手写一个 Mybatis 框架，三步搞定！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886903&idx=1&sn=c1e61cfdad5988d6b5b2d9d7f03962e2&chksm=8fb5565ab8c2df4cce617c2b35793a920f0373f6f21f46a458dd688b21dbe28bd10732113e5e&scene=21#wechat_redirect)
17. [徒手撸一个简单的IOC](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888276&idx=2&sn=54f03374e9a83fedbb8004adfa43c814&chksm=8fb54bf9b8c2c2ef4233f8f4ee934df815c53f878e0b529204a53abdb034c5375144ed02c236&scene=21#wechat_redirect)
18. [还背啥redis面试题？手把手教你写个简版Redis！(附源码)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888974&idx=2&sn=0de51675aeaae5c42f7c7f9a817ed44e&chksm=8fb54ea3b8c2c7b5b8d5861181c4071f2db240d92d53a0951247fcb56408966813f2f8e5e6cb&scene=21#wechat_redirect)



**架构 专题**

1. [【架构技术专题】网站架构的演化（1）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885537&idx=1&sn=fbc2cefce56d9617b80dbb1ba10c3937&chksm=8fb55d0cb8c2d41a46acf9e7a09935186512cda649d94364523a6485b4c42d87c59ff49e2d62&scene=21#wechat_redirect)
2. [「架构技术专题」作为java程序员的你还不知道网站架构的演化(2)?](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885541&idx=1&sn=4f0233c9f746685c852ea08cde7e4c1a&chksm=8fb55d08b8c2d41e2bcc461be43a6ba0ee5e97999c1c7d3347d7fef59ac74bb6f6a793c2c060&scene=21#wechat_redirect)
3. [「架构技术专题」什么是架构设计的五个核心要素？（3）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885545&idx=1&sn=4bb1317fc25938f6d22d7f7c8bca6b91&chksm=8fb55d04b8c2d4127d466f3aa8bca5055784e8a588394cf02c1414142c08ff5a334aef569cc0&scene=21#wechat_redirect)
4. [「架构技术专题」架构核心指标之可扩展架构设计的三要素（4）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885561&idx=1&sn=f893a785195c9f6643eedfa7d8953a6f&chksm=8fb55d14b8c2d402a1e2cd10b37e170617a54924664322fdfd69ba6b473ddec4a6211aaabea5&scene=21#wechat_redirect)
5. [「架构技术专题」9种高性能高可用高并发的技术架构（5）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885603&idx=1&sn=ad2cc3a34dea1329debc2cd27bb3bff7&chksm=8fb55d4eb8c2d45837145a514a7e205e6f867d9746816982aac426c1e95644190225470d0217&scene=21#wechat_redirect)
6. [「架构技术专题」构建网站高可用架构（详细分析篇）（6）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885611&idx=1&sn=5268d135d44408f71c05422d21ab61bf&chksm=8fb55d46b8c2d45067e815e29c15107d8948e59fb5990c01754e1a478dfb5c18cc8c702bc91e&scene=21#wechat_redirect)
7. [「架构技术专题」超详细网站伸缩性架构的设计（7）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885612&idx=1&sn=3245532af5e5ee7915d8c7ba8c223318&chksm=8fb55d41b8c2d457823c4b804905eebab8b5c2fecde80cc33fe418aa5f32f2a5ec3c0cb134b6&scene=21#wechat_redirect)
8. [「架构技术专题」总结：共计7篇阐述架构技术之美](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885780&idx=1&sn=371142de25c4fde05f161cb42565252b&chksm=8fb55239b8c2db2f7d30a9f3029a5964358d9e0b8bff74eba724889703cf2fa48edd8c82cdad&scene=21#wechat_redirect)
9. ---------------分割线---------------------
10. [对呀，我就是认定架构师都是不干活，只会画PPT的！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887592&idx=1&sn=ff54bb2dd7008dd45a2c4342b2e1f919&chksm=8fb55505b8c2dc13b120072504425e09eb82b868789c632ceb9a77bd5330d3cefbfdeda46c06&scene=21#wechat_redirect)
11. [好文 | 架构师更多的是和人打交道，说说我见到和听说到的架构师升级步骤和平时的工作内容](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886132&idx=1&sn=681ece0850d79a2451395823928066ca&chksm=8fb55359b8c2da4f968f682b75e1ea777380054e9bc3966cc8535f047aaeb2f5f61914a784f3&scene=21#wechat_redirect)
12. [架构师成长之路之限流漫谈](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887240&idx=1&sn=b939909e7c122156ea9cb50bf43d3d7b&chksm=8fb557e5b8c2def37d9dea808fd7a25ece221bec81037ea4769e9329012b5b021365a46a34dd&scene=21#wechat_redirect)
13. [支付宝架构师眼中的高并发架构](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886992&idx=1&sn=5c8cf0419e5e930718d5ebe870337c4b&chksm=8fb556fdb8c2dfeb11dd2a04b5516329416e7e8e826fdc34596098fd6d0a7eb78620498e0e5b&scene=21#wechat_redirect)
14. [今日头条 Go 建千亿级微服务的实践](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887125&idx=1&sn=6f46758bb2d93fe7219296938c0cfc49&chksm=8fb55778b8c2de6e429e150eeeec6e31f0a379533dcc3660a42d65020d16f64d2a7cb872f0ea&scene=21#wechat_redirect)
15. [阿里云技术专家分享：现代 IM 系统中消息推送和存储架构的实现](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887163&idx=1&sn=3cbec2793a74609b4856a191ba86ec17&chksm=8fb55756b8c2de40163396956a76e69571529840ba5d5caf9c0317ee1a3ca23a809da5283566&scene=21#wechat_redirect)
16. [程序员必备技能——如何画好架构图](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888625&idx=1&sn=888286ae63a5f237873f6593cd7597df&chksm=8fb5491cb8c2c00ac9d89944a629e485c5ef502754cc5d20070ddaad6b70d0fc1006150de627&scene=21#wechat_redirect)
17. [如何扛住100亿次请求？后端架构应该这样设计！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888161&idx=2&sn=70290d3f590a2ff3338e16bff7d57a41&chksm=8fb54b4cb8c2c25ac466675932d5c2bb1ab8f77ce0460a976cbba9fd0897bb83689ae705be02&scene=21#wechat_redirect)
18. [干货 | 优秀架构师必须了解的6大方面30条设计原则](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888992&idx=1&sn=7bc502dafaaa11165707705ecb1673d1&chksm=8fb54e8db8c2c79b0e5074f69feb94000bfcdc89e129a2727e7fdc2c5846a645abebe8048bf9&scene=21#wechat_redirect)



**支付 专题**

1. [一文快速实现微信公众号支付功能（详细版，建议收藏备用）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886590&idx=1&sn=25500065ac1c16e9b507193df8b8af2b&chksm=8fb55113b8c2d805d51fed4dc661fe94882de6db23669af0a7f540f115940c4c313cae0f364b&scene=21#wechat_redirect)
2. [微信小程序-登录+支付（后台Java）Demo实战（环境搭建+源码）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888329&idx=3&sn=12b87862b1194ec1b77bb1f6f6a48b18&chksm=8fb54824b8c2c1322d80a0bab0851ce1090379687938e04d16dd98c81b5901f88f720afa49bd&scene=21#wechat_redirect)
3. [spring+mvc+mybatis实现微信支付宝等多平台合一的二维码支付](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888191&idx=1&sn=657fbe0bce8bcead3473f01982515bf7&chksm=8fb54b52b8c2c2449f1bec050575b131bd2c54ed5830e67bdb529c84739bb3df9c70504fb6b2&scene=21#wechat_redirect)



**数据结构与算法 专题**

1. [一文读懂JDK7,8,JD9的hashmap，hashtable，concurrenthashmap及他们的区别](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886238&idx=1&sn=9d5980b501aa2125d79214137b963e03&chksm=8fb553f3b8c2dae5b257974d7505cd1d2a1d3f141ad0d0447344b2dcb78aa7b905e079bf2b01&scene=21#wechat_redirect)
2. [几张动态图捋清Java常用数据结构及其设计原理](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886359&idx=1&sn=6e3e1528f806457b11d9fc9583130051&chksm=8fb5507ab8c2d96c0d2cfc07413ec1e17c4948a1b80c45cb69480db210ebca5e26130b9af010&scene=21#wechat_redirect)
3. [买什么数据结构与算法，这里有：**动态图解**十大经典排序算法（含JAVA代码实现）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886426&idx=1&sn=cdb10fbc7ebf4c49ca39613c612d366d&chksm=8fb550b7b8c2d9a176536c73cd9fdea90e94a48bf3990a29ad5004f1762ef53b8768412fe362&scene=21#wechat_redirect)
4. [面试还在被红-黑树虐？看完这篇动图文章轻松反虐面试官](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886239&idx=1&sn=934610d55d9d0b3af9124122ec119ba1&chksm=8fb553f2b8c2dae440f76f661a603f59c0c6e3c246be761e0ad1aec50ee65da9e6f76e48bf80&scene=21#wechat_redirect)
5. [刷了一个月算法，终于拿到了double的offer](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887662&idx=1&sn=7baef65bdf83cb15a6879155a8cd39fb&chksm=8fb55543b8c2dc55f6d80d66e8e3ea14bbe3e6754e345b354be94ebe054c381cbf390652d909&scene=21#wechat_redirect)
6. [GitHub标星15K，这个开源项目让算法动起来](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887651&idx=1&sn=e5e7fe3deda9aae724811d8e5256ceec&chksm=8fb5554eb8c2dc5884eb939177f00eb818a08b1c4b6533187d1aefa664aef10ea072e980cce2&scene=21#wechat_redirect)
7. [【基础不牢地动山摇】一遍记住 Java 面试中常用的八种排序算法与代码实现！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889488&idx=2&sn=028984889ba9a29edb2aa874d463be75&chksm=8fb54cbdb8c2c5ab2175d8bd9d67027562622905040d38865e04fe40b04340bf86462552e98c&scene=21#wechat_redirect)



**redis 专题**

1. [付磊：一份完整的阿里云 Redis 开发规范，值得收藏！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887179&idx=1&sn=66845e273366a0a07eacf8aa04f89731&chksm=8fb557a6b8c2deb09bc9ce8f2db36d4c654fd81e4c1ea9037c4e2f4da84548962640e807caea&scene=21#wechat_redirect)
2. [百亿数据量下，掌握这些Redis技巧你就能Hold全场](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887554&idx=1&sn=a3e5f668bf0e9501540827c9f1f308ea&chksm=8fb5552fb8c2dc3922860068a6da9ca02e20d169bc041a8cd73e6ec5dbfb3543e4ee3a96bb78&scene=21#wechat_redirect)
3. [分布式系统关注点——先写DB还是「缓存」？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887539&idx=1&sn=c49993be9a34b8513036f5a090cd0ef8&chksm=8fb554deb8c2ddc8b99be44ec5111bb86fe728a368081d4a3c90f242cf53584b6e86173ad143&scene=21#wechat_redirect)
4. [付磊：一份完整的阿里云 Redis 开发规范，值得收藏！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887179&idx=1&sn=66845e273366a0a07eacf8aa04f89731&chksm=8fb557a6b8c2deb09bc9ce8f2db36d4c654fd81e4c1ea9037c4e2f4da84548962640e807caea&scene=21#wechat_redirect)
5. [一个牛逼的多级缓存实现方案](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887354&idx=1&sn=5ec7ea90e993f4035b624160ae8c267c&chksm=8fb55417b8c2dd01498427c471d36716e1cfa7bcbf0510599552fc61c6cec7662ded8247de32&scene=21#wechat_redirect)
6. [百亿数据量下，掌握这些Redis技巧你就能Hold全场](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887554&idx=1&sn=a3e5f668bf0e9501540827c9f1f308ea&chksm=8fb5552fb8c2dc3922860068a6da9ca02e20d169bc041a8cd73e6ec5dbfb3543e4ee3a96bb78&scene=21#wechat_redirect)
7. [如何正确访问Redis中的海量数据？服务才不会挂掉](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888017&idx=1&sn=786ce4e100695419e052de176990d602&chksm=8fb54afcb8c2c3eae7a16f73d22846a795915f981a1eface8f00dcef06fbbf05732ef45a931f&scene=21#wechat_redirect)
8. [利用 Redis 实现“附近的人”功能！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889288&idx=3&sn=414747b660741bcbbeb6dd629bc8b8a2&chksm=8fb54fe5b8c2c6f36a6f5051393d6baa7c80e9c94107a81805697a5fbddc910d84f33141b2c8&scene=21#wechat_redirect)
9. [私藏！我是如何用redis做实时订阅推送的？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889315&idx=1&sn=8555c3eb50ebb1546854f96a7fe8815e&chksm=8fb54fceb8c2c6d80980acc315199d290018cd963c4ed271551cfe045fb2d294638874e5251e&scene=21#wechat_redirect)
10. [Redis是如何实现点赞、取消点赞的？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889394&idx=1&sn=b5bf704b8002a5867e0eb9fa6da92133&chksm=8fb54c1fb8c2c509280ef19203b36776f268e8407be2fe4a03e6f551a68fee11ea4e8b87ee13&scene=21#wechat_redirect)
11. [讲给小白的大白话布隆过滤器](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889154&idx=2&sn=cb31a5768de94d8795b1ab8128a258ef&chksm=8fb54f6fb8c2c6799194f9a46eeb03ccd6a1f98117716003b88e40e8dbd00c93183c0d7dcc58&scene=21#wechat_redirect)



**消息队列 专题**

1. [17 个方面，综合对比 Kafka、RabbitMQ、RocketMQ、ActiveMQ 四个分布式消息队列](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888786&idx=2&sn=8a2d113b82390ef16fb4c4b3698eecc9&chksm=8fb549ffb8c2c0e904af83dc6840743ff545d03ea0e85016a331ba2a3c655a696db50c241a7c&scene=21#wechat_redirect)
2. [RabbitMQ和Kafka到底怎么选？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888763&idx=3&sn=0f40d707dfafcd847c5e6068b279620e&chksm=8fb54996b8c2c080a939e3b92b54934b2f49a1b86b631f7c3e722a2fe0b4b289bffa39f72ae4&scene=21#wechat_redirect)
3. [大白话+13张图解 Kafka](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889170&idx=2&sn=8aaca146f9fd694536c831c8f9369c13&chksm=8fb54f7fb8c2c669f8cc4741d32bb02b1b3df3ed06b98a123002c498b703fc38618434795a67&scene=21#wechat_redirect)
4. [Kafka 基本原理（8000 字小结）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889265&idx=2&sn=fff3f9cc3c9c962fc7425ae64c27c3d8&chksm=8fb54f9cb8c2c68ad228fc1471bf575f9492b499de280db1999926957a5d1a8223a5f05cc229&scene=21#wechat_redirect)
5. [Kafka为什么速度那么快？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889297&idx=2&sn=8a26dc14cc44d13fdcceb10d5fbf6f55&chksm=8fb54ffcb8c2c6ea49eaa83d439b0306c8de1fdbbb019107ba41daab7dea8c8ad1dc3b347207&scene=21#wechat_redirect)

**
**

**RPC 专题**

1. [牛逼哄哄的 RPC 框架，底层到底什么原理？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886857&idx=1&sn=1462a08c497c1e3a3b1347511cfb5fa2&chksm=8fb55664b8c2df7236e4b03a3c7874b36b4713ac36a3f15ec4fff7eed028bfb940015fa55de3&scene=21#wechat_redirect)
2. [分布式事务不理解？五种方案一次给你讲清楚！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886948&idx=1&sn=7703c456664d81ef2a60e633b68b6908&chksm=8fb55689b8c2df9fd7a4b25a39c2b38a01bd65e4e6e6f30a4a0d2e572de811499f9a5184f00a&scene=21#wechat_redirect)
3. [分库分表后，如何部署上线？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886684&idx=1&sn=06f87d6179c87348084ba697425d421d&chksm=8fb551b1b8c2d8a75cabc44ab768dfa8ec6c8737335d556294f2584ce5be1a891474099873ea&scene=21#wechat_redirect)



**Mybatis 专题**

1. [推荐一款 MyBatis 开发神器，为简化而生！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887812&idx=1&sn=318f59e7a7d1c32495893fe4fb3bdaf9&chksm=8fb54a29b8c2c33f58d3fd53b09e610b6f5855521e29b5df14314a34e4ed5faf697e35fbbcd8&scene=21#wechat_redirect)

2. [你以为把Mybatis里的#直接替换成$，就能解决sql注入的问题吗？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887772&idx=1&sn=fbe6f0e7e2027a830fee2158d74dba84&chksm=8fb555f1b8c2dce7e77ef9ff2e89b6a1a6798979d7193264f482d7736799651cbec60176fcdd&scene=21#wechat_redirect)

3. [图解源码 | 两张图彻底搞懂MyBatis的Mapper原理！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886954&idx=1&sn=ae37b79e77bd1df4f0cb1d4e41a3ab9a&chksm=8fb55687b8c2df9183de9f306e68a0014d429890f9f23b41b41da4a44deb07d9191fda308435&scene=21#wechat_redirect)

   

**spring cloud 专题**

1. [拜托！面试请不要再问我Spring Cloud底层原理！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887563&idx=2&sn=30c92cd62a25f292d47d99bf85a07165&chksm=8fb55526b8c2dc30ee141b6e74c946506ad99c650ea7b66c1293511dcf110e4111406e226e8c&scene=21#wechat_redirect)
2. [一文讲清Spring Cloud 微服务架构的五脏六腑！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886949&idx=1&sn=f8cb95229817bfca5de16e6f1c734c57&chksm=8fb55688b8c2df9e74edc0c60b1e39eaaee4b0439098f5e6f1f99f474545c915d9bf7b5f2432&scene=21#wechat_redirect)
3. [冒着挂科的风险也要给你们看的 Spring Cloud 入门总结](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889328&idx=1&sn=facd6b9639ae7618f4ba1d434bb2acff&chksm=8fb54fddb8c2c6cbad520de52b153dff5093965d870856f41ca448a69fba98e908d8fbddaee6&scene=21#wechat_redirect)



**JVM调优 专题**

1. [图解 Java 垃圾回收机制，写得非常好！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888055&idx=2&sn=385852c97bcfdee42572264c3e6a3016&chksm=8fb54adab8c2c3cc82787bcd6796ac2bcbf599a687184116f35970b8d7fc9438b0855241ff49&scene=21#wechat_redirect)
2. [系统缓慢+CPU 100%+频繁Full GC问题的定位排查思路！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888241&idx=2&sn=32cadf7c5567da1eb75522c6e5b1e03c&chksm=8fb54b9cb8c2c28a21f24ff6c21cb55bed2df0b2722d061d9457894165f33520edd3c5799000&scene=21#wechat_redirect)
3. [JVM性能调优监控工具jps、jstack、jmap、jhat、jstat、hprof使用详解](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888385&idx=2&sn=65dc17042bd51cc0f4be81a31b6f6577&chksm=8fb5486cb8c2c17a2357a19329669980a4f1e97cda0d6e6abaec170c5086115085bbf27162fa&scene=21#wechat_redirect)



**dubbo源码 专题**

1. [dubbo源码解析-集群容错架构设计](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885741&idx=1&sn=79d898e06314d839a6f6b09ba0f4228f&chksm=8fb55dc0b8c2d4d6cf8f84ff6da370782b3e78b79be321303867f3b5b29f63066f149d74a30e&scene=21#wechat_redirect)
2. [dubbo源码解析-详解directory](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885768&idx=1&sn=801d98ba5d8c51a50a6c34f42a58e543&chksm=8fb55225b8c2db333c41b22498899f0f32002ddfc0a608b0ed824df54cf08c51c1dacc2c441d&scene=21#wechat_redirect)
3. [dubbo源码解析-详解router](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885775&idx=2&sn=d126f6d5b6a9e09c321d2b055b250b29&chksm=8fb55222b8c2db349a4037a34a5b0223792998681523c3d690598eeb6fd67051a655b552576f&scene=21#wechat_redirect)
4. [dubbo源码解析-详解cluster](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885828&idx=1&sn=c3aa1e981ff7ac3b98d3d1a608427fe4&chksm=8fb55269b8c2db7f16b916d793d70c2b0848eed995553082b6dbbbe18344fac15b3fc98d5344&scene=21#wechat_redirect)
5. [dubbo源码解析-详解LoadBalance](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885836&idx=1&sn=bbd46e5fa6881640668da0a6635ef714&chksm=8fb55261b8c2db77fc75daf1de137edb6550366a7e7b2c284110986e244890d31aba6c4af875&scene=21#wechat_redirect)
6. [dubbo源码解析-服务暴露原理](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885870&idx=1&sn=1a8233ab59588d25775c78e778bc16b0&chksm=8fb55243b8c2db552eda4d6e7d56bc7c241f64edbc785f3a918a6c9f04c5ac584b69fe1b55f6&scene=21#wechat_redirect)
7. [dubbo源码解析-本地暴露](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885878&idx=1&sn=c3dee386dfc7b4493164379cc29a1ad1&chksm=8fb5525bb8c2db4dabb56d79ac1d69d7089f026c7135857c1e635446680baa938cf754107aab&scene=21#wechat_redirect)
8. [dubbo源码解析-远程暴露](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885902&idx=1&sn=06619afc6a8cbc5cce9cc569e6290f1c&chksm=8fb552a3b8c2dbb59d49f97a719f593b02417693b09fbb82d1ebbb46cf5a3bd39e40faa1e4d3&scene=21#wechat_redirect)
9. [dubbo专题-深入分析zookeeper连接原理](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885932&idx=1&sn=ddae17aaacfdfe86213e7039719933d5&chksm=8fb55281b8c2db97f94c7af049454882f189ff7ae6939aeb6c9c7da9bb92da0f5a1fb553d447&scene=21#wechat_redirect)
10. [dubbo专题-深入分析zookeeper创建节点过程(高清大图无水印版)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885959&idx=1&sn=6cd689c779470ccb26ab27366a186177&chksm=8fb552eab8c2dbfc498baab018beb93f3b14d4796a3daa52e85831f9d25c9d3c23266fffdfaf&scene=21#wechat_redirect)
11. [dubbo专题-服务暴露总结(本地暴露+远程暴露时序图)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885998&idx=1&sn=2c217bd865301c1d491adcad9da25002&chksm=8fb552c3b8c2dbd5383929805520f3404fce2a95e28cbec00f32c83cd998a82f6c8bd070f225&scene=21#wechat_redirect)
12. [dubbo专题-深入浅出zookeeper订阅原理](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886177&idx=1&sn=77506c008950a7304d6a05c5166bf6e9&chksm=8fb5538cb8c2da9a1ccc2485a5062a9fe1e1e16415b8b1cd70a781fe664e2a99e6cee83a61d8&scene=21#wechat_redirect)



**Docker**

[教程 | 两小时光速入门 Docker（建议收藏）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888218&idx=2&sn=80b65e497de063c55d7e4be95621e8d3&chksm=8fb54bb7b8c2c2a1ea75fc1d7173151a03294552384ccd61d4b0b1e06fad7b7330124fcd9114&scene=21#wechat_redirect)

[终于有人把 Docker 讲清楚了，万字详解！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888778&idx=2&sn=0272d330590e1177dca897addd5ef0cb&chksm=8fb549e7b8c2c0f1831a7469193792b82f11681cdf526ac566bbe804df3e7a72ece1772cba82&scene=21#wechat_redirect)



**ES 专题**

1. [MySQL用得好好的，为啥非要转ES?](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888913&idx=2&sn=2b703a7c980a89a4ba05ad1b87f12b09&chksm=8fb54e7cb8c2c76a505e7b4629d147b770575af135389c1373b4e50ea3b59e79adaeb8e97231&scene=21#wechat_redirect)
2. [面试官是怎么来考察你对ES搜索引擎的理解？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889213&idx=1&sn=17608d11fd897dea03f7b68795963d6e&chksm=8fb54f50b8c2c64662f182e83bdc36d8bdfa960078b58aa1cecc68fb3e41b59d38dd7cc93e95&scene=21#wechat_redirect)
3. [ElasticSearch 亿级数据检索案例实战！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889417&idx=1&sn=be227d7533ab113311f719a69ab20ade&chksm=8fb54c64b8c2c5723e4ffaf34e031597ea5fb41c51df01b75309405089eb316e14bf88f55215&scene=21#wechat_redirect)



**java实战 专题**

1. [实战 | 单点登录系统原理与实现（全套流程图+源码）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888292&idx=2&sn=540bbd99ba6ffe668a75a4e2dab53276&chksm=8fb54bc9b8c2c2dfe3df37965ff681f7002e0fb9d6a8d556de91401b5d376435070b990fb3fe&scene=21#wechat_redirect)
2. [写代码注意了，打死都不要用 User 这个单词！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888077&idx=2&sn=3b948d5e6bbc80d4169345eebbf63a8d&chksm=8fb54b20b8c2c2363eeb0f19137f135a04684b9fc9a960cfd3854f14f19af003122d97bc5d27&scene=21#wechat_redirect)
3. [Java开发最常犯的10个错误，打死都不要犯！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888161&idx=3&sn=16041ba6ef646659b94b87c8c781d358&chksm=8fb54b4cb8c2c25a7f8bc0450f848ba40157f5e43cdf47e5b730e50549a8e821b76da9a013d1&scene=21#wechat_redirect)
4. [别在 Java 代码里乱打日志了，这才是正确的打日志姿势！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888174&idx=3&sn=b63c2fc0cfc9a777ca6b090f41151eb9&chksm=8fb54b43b8c2c25506df3bf5e355699e16ab009fcdc2d5ab17e9327b7567654121b6aa9f9744&scene=21#wechat_redirect)
5. [实战 | 秀儿,如何用一个字段表示八个状态啊](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888412&idx=1&sn=88e666f625c929c9058f466055bab5f6&chksm=8fb54871b8c2c167be8cb51ef2acaeffb71dd5eaf1e8e71b91838a738144d25ef11cf7d8f5cc&scene=21#wechat_redirect)
6. [对啊，我就是认定你不知道ArrayList为什么要实现RandomAccess接口！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888528&idx=1&sn=e538b5c9c0ac33483446ec74c0322733&chksm=8fb548fdb8c2c1eb0648cd346c03eb16bf16060f24e1845f1a8a60161b16bd11548343fea15d&scene=21#wechat_redirect)
7. [【原创】为什么java中一切都是对象，还要有static？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888535&idx=1&sn=46e8ee43b63689264bec40155446ec45&chksm=8fb548fab8c2c1ecbcf37d0f288323f4802dab2395c23329a6b1eaadd3f750c87cb4aba4089b&scene=21#wechat_redirect)
8. [干货 | Java工程师必备素质：如何设计好的系统接口？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888755&idx=3&sn=7c0ed649e0e5899269d0dd9b151d3ec9&chksm=8fb5499eb8c2c0880a41f5df5bbe3c29e9cffa018088a4878abc7146ac6f7089bd17e5245d82&scene=21#wechat_redirect)
9. [e.printStackTrace()不是打印吗，还能锁死？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888591&idx=2&sn=1ed4cb482b5f8c42bc40565b27b1ded6&chksm=8fb54922b8c2c034e74b65771a8e4022bbd01abb738655abee5034cb6acf09432b273060799d&scene=21#wechat_redirect)
10. [【原创】不重写equals和hashcode难道就不行吗？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888840&idx=2&sn=fc70647f444316a0ae90e4801b6a9596&chksm=8fb54e25b8c2c7333bc3b01c4d93e081c0da91af403bc5ac3ec95f75ef44e9ccfbb14b77cdfc&scene=21#wechat_redirect)
11. [Java:如何更优雅的处理空值？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888931&idx=2&sn=fe16dfb458e5b2b728dbe8076bc0e2b2&chksm=8fb54e4eb8c2c758d1ab94a960fc986d6eac96df1f461ff68a373646b21d7e7e34c50b764460&scene=21#wechat_redirect)
12. [【原创】如何高效管理你的js代码](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889003&idx=2&sn=71e655a5910b6547703a9b4728cd1e8f&chksm=8fb54e86b8c2c7902852718c6fbf59808bb19c9d3925d8fdac0e5ebe269271083bc25e200799&scene=21#wechat_redirect)
13. [【原创】你所不知道的读写锁](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888992&idx=2&sn=620c6aa4dd45ddaf700a60fc507c7230&chksm=8fb54e8db8c2c79bd488b51f63da907454328faeaad17b864600244fe55b1cea1b309f9f7525&scene=21#wechat_redirect)
14. [JDK1.8新特性（超详细）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889050&idx=2&sn=f4d2c6fe6d4408b8489a2215b15ea93f&chksm=8fb54ef7b8c2c7e166d7ebc2b020bb8c3ae0a52b2f6294dc8e194793655215d5a7210257e57c&scene=21#wechat_redirect)
15. [【原创】实战 | 用手写一个骚气的请求合并，演绎底层的真实](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889036&idx=1&sn=2fff376438b3db183fa3110bc9d70bd7&chksm=8fb54ee1b8c2c7f73a9c29e371e494917c3145fe33c2ac41f272bc239e2425d18f64a34e59c2&scene=21#wechat_redirect)
16. [Web登录其实没那么简单](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889227&idx=2&sn=4aeda615f39a93ae3fea7f7357963c60&chksm=8fb54fa6b8c2c6b0d9ab0deee5da71113755fa16326988945b3b61fa5dde33d62cc96c7e5148&scene=21#wechat_redirect)
17. 一些不错 Java 实践推荐！建议阅读3遍以上！
18. [巧用Java8中的Stream，让集合操作飞起来！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889400&idx=2&sn=0bccd996bab2f885145db3ba5ba98ba0&chksm=8fb54c15b8c2c503a9f3f818fc09d491a5fd77170c350e95de66731d7794f672b5e9b4ce5912&scene=21#wechat_redirect)
19. [Java8 中用法优雅的 Stream，性能也"优雅"吗？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887634&idx=1&sn=8c5a6180902ce8bab208e74e2e192a58&chksm=8fb5557fb8c2dc69ca90afc31d1660df5df0ae66dd311e21c289fc36dc070ef879a9e94e26a0&scene=21#wechat_redirect)
20. [【原创】我还是很建议你用DelayQueue搞定超时订单的(1)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888534&idx=2&sn=ba0739e843556d655eb479fdd349a4e3&chksm=8fb548fbb8c2c1ed97fabfe43cf6ac7fd9788399ff305002e0439c347c514771550c2c87410e&scene=21#wechat_redirect)
21. [【原创】实战 | 当然我还是更建议你用MQ搞定超时订单的-(2)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888555&idx=2&sn=7f9c95e12ddd5e29fbf7c229760f697c&chksm=8fb548c6b8c2c1d07e513fed05f3ade3a19cab0bbf0bc79143adb8eb69eb594dd4e21b1f7003&scene=21#wechat_redirect)
22. [干货 | 新手也能看懂的源码阅读技巧](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889378&idx=1&sn=bacb4527289729914f898f09658aadc7&chksm=8fb54c0fb8c2c519b21ced2030c61586306adde3725010916fbceeea35449ebdcf487d9fed78&scene=21#wechat_redirect)
23. [【原创】浅谈java事务及隔离级别](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888506&idx=2&sn=97320f26fb59a33dbfb5c04bf0404241&chksm=8fb54897b8c2c1816f6020d1fd7a61cad121307c1293e8e291a6f8cd3665122e80edfc455178&scene=21#wechat_redirect)
24. [三种主流的微服务配置中心深度对比！你怎么看！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888022&idx=2&sn=9f851e288e5e421699ace5b7bedf22d3&chksm=8fb54afbb8c2c3ed3e2e94a3988ac1980d4c9821ab85e97baca255c2fcc805e3a4e4bf17f72c&scene=21#wechat_redirect)
25. [Spring异步编程 | 你的@Async就真的异步吗?异步历险奇遇记](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448888397&idx=2&sn=37aa7fc0448ea1b745c4e39d287a6c82&chksm=8fb54860b8c2c1762a2d7e03192ca9f7376667ffa038e479d90960d90833abac7f0dbef31cb2&scene=21#wechat_redirect)
26. [分库分表就能无限扩容吗，解释得太好了！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889275&idx=2&sn=14cdde8c7ee2d75f65e70ac264b38726&chksm=8fb54f96b8c2c6806a6aa07273bb45b0404dbfe97014363a49d51418876ca9e7a77904acf237&scene=21#wechat_redirect)
27. [为什么微服务一定要有网关？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889163&idx=1&sn=3e3dca33473e5a82a8af6dc798a19eab&chksm=8fb54f66b8c2c67092d5eb62cf9339c618502e68e95fa3c1a0ef08a67c560ea2da349772cff5&scene=21#wechat_redirect)
28. [京东到家订单订单查询服务演进](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889337&idx=2&sn=e5e45de34ae2c7b6cd7065aa7bc16b74&chksm=8fb54fd4b8c2c6c2fcd0d2eb3ae03c9eedaf17926c575ab47f42560d7e363555241fe68b8d17&scene=21#wechat_redirect)
29. [基于Nginx实现访问控制、连接限制](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448889343&idx=2&sn=3103bec0f5da7690aa1da6e80915efef&chksm=8fb54fd2b8c2c6c4a05e2f9f996f0d42f6adfd8625afcf500d89da53c04d0071cccbcc739b3c&scene=21#wechat_redirect)
30. [Maven 虐我千百遍，我待 Maven 如初恋](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887893&idx=2&sn=8286488a037ba7b00f086b693e5f1404&chksm=8fb54a78b8c2c36eb482620c92faf1bae617109841d581f7fb474a83bfb20b25cf2183272a01&scene=21#wechat_redirect)
31. [分库分表？如何做到永不迁移数据和避免热点？](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887829&idx=1&sn=b8159a5169785479c6c6bd19e6fbdbce&chksm=8fb54a38b8c2c32ec35a704b6be2f6c9c864695771c2b2789852ba7a7a04bae9c487e1e0a4e0&scene=21#wechat_redirect)
32. [Java高级开发必会的50个性能优化的细节（珍藏版）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887663&idx=1&sn=b2259b9e19a04c6fc7eec8bdd40f85c2&chksm=8fb55542b8c2dc5466423c576aa53c4d7f714928d54ea17fd9d3a33b26e8e6f739d525600515&scene=21#wechat_redirect)
33. [你必须掌握的 21 个 Java 核心技术！（干货）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887269&idx=1&sn=b9d22bf2f3e82dff698d953648305d46&chksm=8fb557c8b8c2dede08ef0bb89256799c40a283cfe337ea1d5f1093e1689efb703e734411f091&scene=21#wechat_redirect)
34. [什么？这40条显著提升Java性能的优化细节你竟然还不知道？！](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448887566&idx=1&sn=54bab48125ed8f6ff7448aa8f3bcd91d&chksm=8fb55523b8c2dc352f2496edfbb0eca37aa8b80b6691d6693e8daf56757f66b9e695e5588602&scene=21#wechat_redirect)
35. 你们要的动图来了：2张动图快速理解高内聚与低耦合



**网络协议 专题**

1. [一篇文章带你详解 HTTP 协议（上）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885682&idx=1&sn=eba1354453f4ddd1e3b26033a88cd183&chksm=8fb55d9fb8c2d489db6ec88f93e8a09d70805d6bb426ed24f7a38d5b22b3f06d1a9e1e281f22&scene=21#wechat_redirect)
2. [一篇文章带你详解 HTTP 协议之报文首部及字段详解（中）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885709&idx=1&sn=74017ce34ed27bcffb4df18a049106cc&chksm=8fb55de0b8c2d4f6ff4817b2beee119de5068fcceb003341b8edc7553559c36067d766ad82b3&scene=21#wechat_redirect)
3. [一篇文章带你详解 HTTP 协议（下）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885740&idx=1&sn=4c8ea8f944a7af0c1caacfcd4a1fb67e&chksm=8fb55dc1b8c2d4d729bfe92b0d6072052e62029c0b7777326c4f4d0562ca39462b7acfa51645&scene=21#wechat_redirect)
4. [一篇文章带你详解 TCP/IP 协议](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885757&idx=1&sn=0b234017a3312359c2da2cfcedb56f19&chksm=8fb55dd0b8c2d4c6e75e34f9a3701e0fd0aa99de447d6568281bffda42c9a1faf597b5a066e7&scene=21#wechat_redirect)
5. [一篇文章带你详解 TCP/IP 协议（完结）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885826&idx=2&sn=993d50965f992bc8a7ca8d37d88daf0c&chksm=8fb5526fb8c2db795da821ca0bf9b797c8502dcbbedba11dfa22c2258d6efe20437de9746bce&scene=21#wechat_redirect)
6. [动图详解TCP的三次握手与四次挥手](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885901&idx=2&sn=894f009591edad7ffbca6cb6f7635fed&chksm=8fb552a0b8c2dbb6f0ef8ca02367c3b61757991073305252d9bdeaed0c7c72d2f34ee008522b&scene=21#wechat_redirect)
7. [十五道java开发常遇到的计算机网络协议高频面试题](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885867&idx=2&sn=43033c4c4ab31ff2850cf8dbc83a9188&chksm=8fb55246b8c2db5083b948a2b803ba4f61b4187ba08a6a54e42660b6a9f67359680dfdb11d93&scene=21#wechat_redirect)
8. [**【网络协议】专题总结以及网络协议高频面试题汇总(8篇)**](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885957&idx=1&sn=2cf91e991542ca9aa83a180f06a83ebe&chksm=8fb552e8b8c2dbfe66f56f79635ca52d5935192b92cb120081451eeb9dc9ecb20a77ddcb422f&scene=21#wechat_redirect)



**Mysql优化 专题**

1. [「mysql优化专题」这大概是一篇最好的mysql优化入门文章（1）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885479&idx=1&sn=75ad7d668500a2b9cca8b7fa6b3961c8&chksm=8fb55ccab8c2d5dc34355b57b7de4ce259edeba028121b5130080e391f8906cb58f9f8a8bd91&scene=21#wechat_redirect)
2. [「mysql优化专题」90%程序员都会忽略的增删改优化（2）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885482&idx=1&sn=6c6bb84c6a3e579d45484bfa10e16905&chksm=8fb55cc7b8c2d5d134ad217e8c704a7f0b3106552495bc8c70ef15bb54d9ef5f6f0f463dd5ad&scene=21#wechat_redirect)
3. [「mysql优化专题」你们要的多表查询优化来啦！请查收（4）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885496&idx=1&sn=a051714b9cf39c9fc3836de2a2fbea44&chksm=8fb55cd5b8c2d5c3b6a995eb7e6c5d898229617bcfea86fe3ea384e8e52298b612e5f63202c7&scene=21#wechat_redirect)
4. [「mysql优化专题」90%程序员面试都用得上的索引优化手册(5)【面试重点】](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885500&idx=1&sn=749c8c9185f42a6c4e5eacde33613ef0&chksm=8fb55cd1b8c2d5c74feb1bdf2b0d22a36df574d41ce1679492e5b215ab36aedaf90d9bb5f2e0&scene=21#wechat_redirect)
5. [「mysql优化专题」优化之路高级进阶——表的设计及优化（6）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885503&idx=1&sn=366821aead06fd0c3d154015511d76aa&chksm=8fb55cd2b8c2d5c45be5715a8d6402b0bb3bdf437e3f6bc0a1a262bc4743e4362a2c462b665c&scene=21#wechat_redirect)
6. [「mysql优化专题」90%程序员没听过的存储过程和存储函数教学(7)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885506&idx=1&sn=9d505e440d20a513f5cb0921c4aec414&chksm=8fb55d2fb8c2d4397622474834d3d4487bd1f11f7c92bb0d699f40743acdfaa29850b76b98dc&scene=21#wechat_redirect)
7. [「mysql优化专题」视图应用竟然还可以这么优化？不得不收藏（8）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885509&idx=1&sn=00b5bae24e04526a821fad4593609bb7&chksm=8fb55d28b8c2d43e5f2aa8e9efbad4274f7be38c9aa2fd14d0c691cb262751d9251e760c6809&scene=21#wechat_redirect)
8. [「mysql优化专题」详解引擎(InnoDB,MyISAM)的内存优化攻略？（9）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885514&idx=1&sn=47a3173bfa24647a7c9b1ce308bcecb3&chksm=8fb55d27b8c2d4310469a279150a3a8a6954330d2cdf104d78300aa6dafe0736aa8b7ce4632c&scene=21#wechat_redirect)
9. [「mysql优化专题」什么是慢查询？如何通过慢查询日志优化？（10）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885525&idx=1&sn=9a74780855454ccbe253cc5d6675659c&chksm=8fb55d38b8c2d42e1d1eeebed31bd2b09c17d5eb8dd088619009a4e3aed36d1a03daaeb0cb69&scene=21#wechat_redirect)
10. [「mysql优化专题」主从复制面试宝典！面试官都没你懂得多！(11)](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885529&idx=1&sn=0418da541f473426dab0d3073443d79c&chksm=8fb55d34b8c2d422a910d5b8c55af7853188e103d921aeaff29cc0875ef035427170f4614c77&scene=21#wechat_redirect)
11. [「mysql优化专题」高可用性、负载均衡的mysql集群解决方案（12）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885532&idx=1&sn=0f096b806f7ce765eddfcd2d285df353&chksm=8fb55d31b8c2d427bb7c3b5192eabaf7ef46424e72f60b44e888fab73b424a8ca6156e7c902e&scene=21#wechat_redirect)
12. [**【mysql优化专题】本专题终极总结（共13篇）**](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448885667&idx=1&sn=8af6c4ad5acb00e63407d8a00c9191bb&chksm=8fb55d8eb8c2d498f603e6b974cd0a24f2a08be7747c87bee15db53447afcd47d3c05fdcd0da&scene=21#wechat_redirect)
13. [MYSQL优化有理有据全分析（面试必备）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886669&idx=1&sn=50c8742e88f40a481105e4b3249ca2b8&chksm=8fb551a0b8c2d8b66a9d8e8b77b2e7f4c331270c0aa480d512ee773b819917f6538994ccf5f2&scene=21#wechat_redirect)
14. [MySQL 大表优化方案（长文）](http://mp.weixin.qq.com/s?__biz=MzAxMjEwMzQ5MA==&mid=2448886322&idx=1&sn=5dd2154c07bcecd7e51ff062fb287dbc&chksm=8fb5501fb8c2d9097fcba44f1063d2842f8c3c052d6e5fe0528b2780b7239db6a4c03e4b2e6d&scene=21#wechat_redirect)



**设计模式 专题**

![图片](https://mmbiz.qpic.cn/mmbiz_png/5j8oo6Mib4zL4rumG1a6A6r5IktAosDMTxxLfFPgtgGWibnbaSqEXfia8mox9w358KWMgWVwVw6VOEfWy58OuMbog/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

👉[ 原创 | 让设计模式飞一会儿|①开篇](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|②单例模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=2&sn=e6b22d10080f5744267f85ad588661f1&chksm=fdf7c8a3ca8041b5b8e70765cf918e8b3b0e0ce22376c2ebb1d41290e797a3054f353c3b4d1d&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|③工厂模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=3&sn=4b7f1fd8678ceaec848040966f157656&chksm=fdf7c8a3ca8041b55bbc8e8de2476aa824897f97c7304ac0187e7d28916e6b9d0bc5b1462ef9&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|④原型模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=4&sn=f98061eee6287b4589e628dd3fb2d0b6&chksm=fdf7c8a3ca8041b5b1776de9e48bf37cc6e94ba45a85b3d341e885f27fa64061398a68619d36&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|⑤建造者模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=5&sn=936cb2dc5a606caeab7adbd04b53ae8d&chksm=fdf7c8a3ca8041b5da05eae3d3f17141969579e9a16c6d9ab2b9775f319be2914ab31f4ee2d1&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|⑥代理模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484305&idx=2&sn=543090e4e2d229b9c37df8783cc720f4&chksm=fdf7c8b5ca8041a3faba5e6f6cc9c5b4859c40a80686816c6ce0d773e999249ab44368adf90a&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|⑦适配器模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484305&idx=3&sn=8571dedb0cd45a97dfc0db5cc7c4008a&chksm=fdf7c8b5ca8041a3ff224a897eb249e0a8dc1094d8fcb126d25cb71fe7c07defc236a43946b6&scene=21#wechat_redirect)
👉[ 原创 |](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484295&idx=1&sn=d76f2f1a8b92e761416e61153c6e3fc1&chksm=fdf7c8a3ca8041b50125299d84c11b508ce6315a0c505163c42da0fc592d9551be91da89edbc&scene=21#wechat_redirect) [让设计模式飞一会儿|⑧装饰者模式](http://mp.weixin.qq.com/s?__biz=MzU4Njc1MTU2Mw==&mid=2247484313&idx=1&sn=05edd7b828164436b72f89a155247709&chksm=fdf7c8bdca8041ab395453b6951c23e669a3e74e3c064b34c40c79327289dc1b9c0373ebe105&scene=21#wechat_redirect)