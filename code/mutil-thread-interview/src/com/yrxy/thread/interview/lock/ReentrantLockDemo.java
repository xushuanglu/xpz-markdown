package com.yrxy.thread.interview.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ReentrantLockDemo
 * @description：ReentrantLock基本用法
 * @date 2017/6/3 11:21
 */
public class ReentrantLockDemo extends Thread {
    public static ReentrantLock lock = new ReentrantLock();
    public ReentrantLockDemo(String name){
        super(name);
    }
    @Override
    public void run() {
        try {
            if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
                Thread.sleep(6000);
            } else {
                System.out.println(this.getName() + " get lock failed");
            }
        } catch (Exception e) {
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }
    public static void main(String[] args) {
        ReentrantLockDemo t1 = new ReentrantLockDemo("ReentrantLockDemo1");
        ReentrantLockDemo t2 = new ReentrantLockDemo("ReentrantLockDemo2");
        t1.start();
        t2.start();
    }

}
