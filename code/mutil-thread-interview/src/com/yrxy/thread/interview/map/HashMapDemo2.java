package com.yrxy.thread.interview.map;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className HashMapDemo
 * @description：
 * @date 2017/6/12 10:07
 */
public class HashMapDemo2 {

    public static void main(String[] args) throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException {
        HashMap map=new HashMap();
        map.put("id","1");
        map.put("name","huanglaoxie");

        Class<?> mapType = map.getClass();

        Method capacity = mapType.getDeclaredMethod("capacity");

        capacity.setAccessible(true);

        System.out.println("capacity : " + capacity.invoke(map));



        Field size = mapType.getDeclaredField("size");

        size.setAccessible(true);

        System.out.println("size : " + size.get(map));


//        Method loadFactor = mapType.getDeclaredMethod("loadFactor");
//
//        size.setAccessible(true);
//
//        System.out.println("loadFactor : " + loadFactor.invoke(map));
//        map.getClass().getDeclaredField("loadFactor");
        System.out.println("loadFactor : " + map.getClass().getDeclaredField("loadFactor"));

    }

}
