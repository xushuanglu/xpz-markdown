package com.yrxy.thread.interview.cas;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className CasLockDemo
 * @description：
 * @date 2017/7/2 17:16
 */
public class CasLockDemo {

    private AtomicBoolean locked = new AtomicBoolean(false);

    public boolean lock() {
        return locked.compareAndSet(false, true);
    }

}
