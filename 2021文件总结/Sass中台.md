# Saas中台产品



## 相关技术栈

基础：Springboot+Springcloud

持久化：Mybatis-plus

服务容器：**Undertow**

- maven依赖：

```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <!-- 移除掉默认支持的 Tomcat -->
    <exclusions>
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
        </exclusion>
    </exclusions>
</dependency>

<!-- 添加 Undertow 容器 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-undertow</artifactId>
</dependency>

```

连接池：HiKariCP

数据库：MySql

项目构建：**Maven**

API网关：GateWay

日志：Logback

注册、配置中心：**Nacos**

服务调用：**OpenFeign+Ribbon**

熔断、降级：Sentinel

分布式事务：Seata

分布式锁：Redisson

鉴权：Shiro+jwt-token

缓存：Redis、springCache

文件操作：**FastDFS**、**EasyExcel**、**Itext**、**Minlo**

接口文档：**knife4j**

应用监控：**Spring-boot-admin**

前置负载：Nginx

链路追踪：**Skywalking**

脚本执行：**Flyway**

项目部署：Docker、Docker-compose



**账号相关**



邮箱：xushuanglu@senyint.com

密码：Xushuanglu0314



gitlab账号：xushuanglu@senyint.com

密         码：xushuanglu0314



已部署完毕，发版请登录jenkins进行更新即可：
http://192.168.20.117/jenkins/

账号：cuzhong
密码：1qaz2wsx



gitlab账号

账号：caiyuheng。密码是Cai@177480



**线上数据库**

线上数据库名称：saas

线上数据库密码：Senyint.Saas123!



龙富海
saas@'192.168.99.%' identified by 'Senyint.Saas123!'
MySQL： 192.168.40.21：23306

**Minio数据地址：**

prodminio.cinyi.com
MINIO_ACCESS_KEY=senyint
MINIO_SECRET_KEY=gyqtfqclsgvuxuwum.cmhkn59CHekacb



**线上redis地址：**
redis : 192.168.99.161:6379    sass123





**心医国际IT系统微服务架构转型v0.1技术栈：**



- SpringCloudGateway（推荐）

- Nginx+Lua（需定制化开发）
- Nacos（推荐）
- Zookeeper（需定制化开发）
- Nacos（推荐）可与注册中心合并
- SpringSecurity+OAuth2.0（推荐）建议采用授权码模式
- ELK+FileBeat+MetricBeat（推荐）Kafka（必要时使用，当日志处理速度小于日志生成速度时）其它各种Beats（按需使用）
- 关系型数据：
  - 数据量较少时，选用MySQL；
  - 数据量较大时进行分库分表，并配合数据库中间件，如MyBatis、MyBatis-Plus；
  - 非关系型数据库：缓存集群（常用，Redis），文档数据库MongoDB
  - 大数据处理：Hadoop、HDFS、Spark（批量处理）、SparkStreaming/Flink（流式处理）



**linux fastdfs存储文件位置**

>  /home/fastdfs/data/00/00





**windows**

> F:\>minio.exe server  F:\MinioData





**springboot-admin要想显示日志信息需要加上如下内容：**

> ```java
> <dependency>
>     <groupId>org.springframework.boot</groupId>
>     <artifactId>spring-boot-starter-actuator</artifactId>
> </dependency>
> ```

```yaml
management:
  endpoints:
    web:
      exposure:
        #加载所有的端点，默认只加载了info、health
        include: '*'
```





```java
java -javaagent:F:\apache-skywalking-apm-bin\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=store-service
-Dskywalking.collector.backend_service=localhost:12800
-jar senyint-store-0.0.1-SNAPSHOT.jar
```



**OpenFeign 默认使用 Java 自带的 URLConnection 对象创建 HTTP 请求，但接入生产时，如果能将底层通信组件更换为 Apache HttpClient、OKHttp 这样的专用通信组件，基于这些组件自带的连接池，可以更好地对 HTTP 连接对象进行重用与管理。作为 OpenFeign 目前默认支持 Apache HttpClient 与 OKHttp 两款产品。我以OKHttp配置方式为例，为你展现配置方法。**

1.引入 feign-okhttp 依赖包。
复制代码

```java
<dependency>
    <groupId>io.github.openfeign</groupId>
    <artifactId>feign-okhttp</artifactId>
    <version>11.0</version>
</dependency>
```

2.在应用入口，利用 Java Config 形式初始化 OkHttpClient 对象。
复制代码

```java
@SpringBootApplication
@EnableFeignClients
public class OrderServiceApplication {
    //Spring IOC容器初始化时构建okHttpClient对象
    @Bean
    public okhttp3.OkHttpClient okHttpClient(){
        return new okhttp3.OkHttpClient.Builder()
                //读取超时时间
                .readTimeout(10, TimeUnit.SECONDS)
                //连接超时时间
                .connectTimeout(10, TimeUnit.SECONDS)
                //写超时时间
                .writeTimeout(10, TimeUnit.SECONDS)
                //设置连接池
                .connectionPool(new ConnectionPool())
                .build();
    }
    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApplication.class, args);
    }
}
```

3.在 application.yml 中启用 OkHttp。
复制代码

```yaml
feign:
  okhttp:
    enabled: true
```




医生相关

- 医院表

- 数据字典
- 医生表



## 图片存储位置

- 个人头像 ：/sass/account/head-portrait
- 营业执照
- 服务图像
- 产品导航图标
- 产品导航默认图标

  



## 默认初始化的表

- ~~用户使用表~~
- 功能模块配置（**医生/患者多条数据，通过type进行区分**）
- 角色授权表





功能模块配置操作：

初始化数据，分为不同类型，type标识，给前端进行反显的时候根据不同类型返回多个数组ids

```
{
   data:
   医生:{
       注册id，
       登录id
   }
   患者:{
       注册id，
       登录id
   }
}
```



考虑每张表设置一个type标识，做日志记录，方便排查问题





账户模块：

- 短信平台集成
- 记住密码，shiro权限认证
- 账户登录、退出登录
- 账户列表、模糊查询
- 账户头像存在Minio服务器中
- 



**接口访问地址：**

account-web：http://172.16.230.81:8081/doc.html













## 开发组件清单

- account-web   运营端账户服务
- user-web         商户端(医生/患者)用户服务
- 





**代码分支情况：**

> 目前

主分支：master

开发分支：dev







**sass架构**

要想访问接口请求值，**需要在header中添加jwt-token参数**

![image-20210401145531609](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210401145531609.png)





## 微服务架构



### 应用网关



### 业务员服务层



### 基础服务层





## 为什么要做系统拆分







## 微服务拆分通用原则

​           在微服务拆分过程中，我们通常会从**业务场景、团队能力、组织架构等多种因素综合考虑，**这特别考验架构师的业务能力。一般来说，我们总结出几点通用原则：

- **单一职责原则。** 每一个微服务只做好一件事，体现出“高内聚、低耦合”，尽量减少对外界环境的依赖。比如，在公司创业之初，完全可将订单与仓储服务进行合并。因为订单与仓储在业务与数据上紧密相关，如果强行拆分会导致出现跨进程通信带来的数据一致性难题。随着业务的发展，仓储的业务职责扩展，派生出许多与订单无紧密联系的功能，到时再将其剥离形成独立的“仓储服务”。
- **服务依赖原则。** 避免服务间的循环引用，在设计时就要对服务进行分级，区分核心服务与非核心服务。比如订单服务与短信服务，显然短信服务是非核心服务，服务间调用要遵循“核心服务”到“非核心服务”的方向，不允许出现反向调用。同时，对于核心服务要做好保护，避免非核心服务出现问题影响核心服务的正常运行。
- **Two Pizza Team原则。** 就是说让团队保持在两个比萨能让队员吃饱的小规模的概念。团队要小到让每个成员都能做出显著的贡献，并且相互依赖，有共同目标，以及统一的成功标准。一个微服务团队应涵盖从设计到发布运维的完整生命周期，使团队内部便可以解决大部分任务，从人数上4~6人是比较理想的规模。





## 微服务架构的适用场景

本节最后一部分，我总结了适合微服务使用场景，首先咱们梳理下适合做微服务的场景：

- **新规划的大型业务系统，** 这肯定是最适合引入微服务架构的情况了， 微服务强调“高内聚，低耦合”，每一个团队负责一个服务，这就意味着从根本上和传统的整体性应用有本质不同，从规划阶段采用微服务架构是再好不过的。
  
- **敏捷的小团队系统，**公司在大型项目微服务实践前，往往这类边缘化的小项目会起到“试验田”的作用， 引入快速迭代、持续交付等模式，积累适合本公司特点的微服务实践经验，再将这些经验扩大到其他大型项目中。
  
- **历史的大型留存业务系统，**之前多年我一直在金融软件领域工作，在银行内部许多系统已经使用超过10年时间，成百上千个模块错综复杂维护愈发困难，无论架构、框架乃至技术人员都需要更新迭代，但都不可能一次大动手术，这时微服务的“微”就体现出来，重构时可以将某一个部分剥离为微服务独立运行，确保无误后再继续剥离出下一个服务，通过抽丝剥茧一般的剥离，逐步将原有大系统剥离为若干子服务，虽然过程十分痛苦，但这是必须做的事情。



## 微服务架构的不适用场景：

- **微型项目，**前面提到的工单系统，系统压力很小，需求变化也不大，利用单体架构便可以很好解决，使用分布式架构反而增加了架构复杂度，让系统更容易不稳定。
  
- **对数据响应要求高的系统，**就像前面文中提到的“导弹打飞机”的科研项目，对实时性要求极高，那显然是不合适的。



## 通用的微服务架构应包含哪些组件

​         相对于单体式架构的简单粗暴，微服务架构将应用打散，形成多个微服务进行独立开发、测试、部署与运维。虽然从管理与逻辑上更符合业务需要，但微服务架构也带来了诸多急需解决的核心问题：

    1、如何发现新服务节点以及检查服务节点的状态？
    
    2、如何发现服务及负载均衡如何实现？
    
    3、服务间如何进行消息通信？
    
    4、如何对使用者暴露服务 API？
    
    5、如何集中管理众多服务节点的配置文件？
    
    6、如何收集服务节点的日志并统一管理？
    
    7、如何实现服务间调用链路追踪？
    
    8、如何对系统进行链路保护，避免微服务雪崩？
微服务架构需要的标准组件

![图片1.png](https://s0.lgstatic.com/i/image6/M00/13/40/Cgp9HWBB7dmAMXFxAAFTzOxiKrU853.png)



- 注册中心（Service Registry）解决了问题1

  > ​        微服务节点在启动时会将**自身的服务名称、IP、端口等信息在注册中心中进行登记，注册中心会定时检查该节点的运行状态。**注册中心通常会采用**心跳机制**最大程度保证其持有的服务节点列表都是可用的。

  服务注册流程

  ![图片2.png](https://s0.lgstatic.com/i/image6/M00/13/3C/CioPOWBB7eqAPUlxAAEh8rtRqRA639.png)

- 负载均衡（Load Balance）：负载均衡器解决了问题 2

  > ​       通常在微服务彼此调用时并不是直接通过IP、端口直接访问，而是首先通过服务名在注册中心查询该服务拥有哪些可用节点，然后注册中心将可用节点列表返回给服务调用者，这个过程称为“服务发现”。因服务高可用的要求，服务调用者会接收到多个可用节点，必须要从中进行选择，因此在服务调用者一端必须内置负载均衡器，通过负载均衡策略选择适合的节点发起实质的通信请求。

  服务发现与负载均衡

  ![图片3.png](https://s0.lgstatic.com/i/image6/M00/13/40/Cgp9HWBB7fyAA0CzAAEc4ADEq64041.png)

- 





## 如何有效避免雪崩效应？

刚才我们分析了雪崩效应是**因为出现瞬间大流量+微服务响应慢造成的。**针对这两点在架构设计时要采用不同方案。

- **采用限流方式进行预防：**可以采用限流方案，控制请求的流入，让流量有序的进入应用，保证流量在一个可控的范围内。
  
- **采用服务降级与熔断进行补救：**针对响应慢问题，可以采用服务降级与熔断进行补救。服务降级是指当应用处理时间超过规定上限后，无论服务是否处理完成，便立即触发服务降级，响应返回预先设置的异常信息。



通过服务降级减少阻塞时间

![图片2.png](https://s0.lgstatic.com/i/image6/M00/24/6B/CioPOWBYa4yAHas6AAH74uOpAws537.png)





## 微服务 CPU 被打满如何排查

> ​     一台机器上会部署一至多个进程，它们可能是一个或多个业务应用进程和多个其他工具类进程（比如日志收集进程、监控数据收集进程等）。大概率导致机器 CPU 飙升的是业务应用进程，但仍需准确定位才可得出结论。



- 在 Linux 系统里，可以使用**top 命令进行定位，** top 命令可以按进程维度输出各个进程占用的 CPU 的资源并支持排序，同时还会显示对应的进程名称、进程 ID 等信息。
  
- 根据排序，便可以确定占用 CPU 资源最高的进程，但此时仍然不知道是哪段代码导致的 CPU 飙升。所以可以在此进程基础之上，做进一步的定位。top 命令支持查看指定进程里的各线程的 CPU 占用量，命令格式为：**top -Hp 进程号。**通过此方式便可以获得指定进程中最消耗 CPU 资源的线程编号、线程名称等信息。
  
- 假设导致 CPU 飙升的应用是基于 Java 开发的，此时，便可以通过 Java 里自带的 jstack 导出该进程的详细**线程栈**（包含每一个线程名、编号、当前代码运行位置等）信息，具体见下方示例代码：

```java
"thread name" prio=0 tid=0x0 nid=0x0 runnable
at java.net.SocketInputStream.socketRead0(Native Method)
at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
at java.net.SocketInputStream.read(SocketInputStream.java:171)
```

​        通过第三步定位的**线程号**和此步骤生成的**线程栈，**便可以精准确定是哪行代码写的有 Bug，进而导致进程的 CPU 飙升。上述分析是以** Java 应用**进行举例，关于其他语言的应用如何导出进程堆栈信息，你可以到对应官网查看。



## 如何预防微服务 CPU 被打满故障(未整理完)

从**微服务的部署**和**代码编写**两个层面介绍一些准则，以便构建一个更加稳固的微服务，前置减少出现故障的概率。

### 部署层面

首先，微服务及存储需要双机房部署。双机房部署能够进一步提升服务的容灾能力。双机房部署的架构如下图 1 所示：

![图片1.png](https://s0.lgstatic.com/i/image6/M01/13/61/Cgp9HWBCEGOAY71-AAGWcwIKajo733.png)



​                                                                                           双机房部署架构图







### 代码层面

- 第一，不要基于 JSON 格式打印太多、太复杂的日志。
- 第二，需要具有日志级别的动态降级功能。
- 第三，for 循环不要嵌套太深，原则上不要超过三层嵌套。
- 第四，多层嵌套需要有动态跳出的降级手段。
- 第五，如果使用应用内的本地缓存，需要配置容量上限。





## 多线程

**你知不知道如何正确停止线程”这样的问题，我想你一定可以完美地回答了，**

> 首先，从原理上讲应该用 interrupt 来请求中断，而不是强制停止，因为这样可以避免数据错乱，也可以让线程有时间结束收尾工作。

**如果我们是子方法的编写者，遇到了 interruptedException，应该如何处理呢？**

​      我们可以把异常声明在方法中，以便顶层方法可以感知捕获到异常，或者也可以在 catch 中再次声明中断，这样下次循环也可以感知中断，所以要想正确停止线程就要求我们停止方，被停止方，子方法的编写者相互配合，大家都按照一定的规范来编写代码，就可以正确地停止线程了。



**多线程额6种状态：**

New（新创建）
Runnable（可运行）
Blocked（被阻塞）
Waiting（等待）
Timed Waiting（计时等待）
Terminated（被终止）



**我们再看看 Waiting 状态，线程进入 Waiting 状态有三种可能性。**

    1、没有设置 Timeout 参数的 Object.wait() 方法。
    2、没有设置 Timeout 参数的 Thread.join() 方法。
    3、LockSupport.park() 方法。



> ​           Blocked 与 Waiting 的区别是 Blocked 在等待其他线程释放 monitor 锁，而 Waiting 则是在等待某个条件，比如 join 的线程执行完毕，或者是 notify()/notifyAll() 。



**以下情况会让线程进入 Timed Waiting 状态。**

    1、设置了时间参数的 Thread.sleep(long millis) 方法；
    2、设置了时间参数的 Object.wait(long timeout) 方法；
    3、设置了时间参数的 Thread.join(long millis) 方法；
    4、设置了时间参数的 LockSupport.parkNanos(long nanos) 方法和 LockSupport.parkUntil(long deadline) 方法。





## 分布式锁

**单台redis那么如何合理设置超时时间呢？ **

​       你可以基于**续约**的方式设置超时时间：**先给锁设置一个超时时间，然后启动一个守护线程，让守护线程在一段时间后，重新设置这个锁的超时时间。**实现方式就是：写一个守护线程，然后去判断锁的情况，当锁快失效的时候，再次进行续约加锁，当主线程执行完成后，销毁续约锁即可。



**集群下的redis如何实现分布式锁的？**

​        为了避免 Redis 实例故障导致锁无法工作的问题，Redis 的开发者 Antirez 设计了**分布式锁算法 Redlock。**Redlock 算法的基本思路，**是让客户端和多个独立的 Redis 实例依次请求申请加锁，如果客户端能够和半数以上的实例成功地完成加锁操作，那么我们就认为，客户端成功地获得分布式锁，否则加锁失败。**



**那 Redlock 算法是如何做到的呢？**

​       我们假设目前有 N 个独立的 Redis 实例，** 客户端先按顺序依次向 N 个 Redis 实例执行加锁操作。**这里的加锁操作和在单实例上执行的加锁操作一样，但是需要注意的是，**Redlock 算法设置了加锁的超时时间，为了避免因为某个 Redis 实例发生故障而一直等待的情况。**

当客户端完成了和所有 Redis 实例的加锁操作之后，**如果有超过半数的 Redis 实例成功的获取到了锁，并且总耗时没有超过锁的有效时间，那么就是加锁成功。**





​        对于分布式锁，你要从**“解决可用性、死锁、脑裂”**等问题为出发点来展开回答各分布式锁的实现方案的优缺点和适用场景。 另外，在设计分布式锁的时候，**为了解决可用性、死锁、脑裂等问题，一般还会再考虑一下锁的四种设计原则。**

    1、互斥性：即在分布式系统环境下，对于某一共享资源，需要保证在同一时间只能一个线程或进程对该资源进行操作。
    
    2、高可用：也就是可靠性，锁服务不能有单点风险，要保证分布式锁系统是集群的，并且某一台机器锁不能提供服务了，其他机器仍然可以提供锁服务。
    
    3、锁释放：具备锁失效机制，防止死锁。即使出现进程在持有锁的期间崩溃或者解锁失败的情况，也能被动解锁，保证后续其他进程可以获得锁。
    
    4、可重入：一个节点获取了锁之后，还可以再次获取整个锁资源。



**zookeeper实现分布式锁？**

​      zookeeper分布式锁是通过**临时顺序节点**来实现的，由**zab协议保证锁的互斥性和高可用，客户端异常退出后临时顺序节点会失删除，相当于释放锁，并会通知下一个临时节点对应的客户端可以加锁。**基于zookeeper的分布式锁需要通过**共识算法**保证一致性，所以锁的效率不高。







### 分布式锁技术选型

常见的分布式锁，有基于Redis实现，有基于Zookeeper实现，有基于etcd实现。那么，到底哪种更适合用于分布式锁呢？我们做一个对比：

![img](https://pics2.baidu.com/feed/0b46f21fbe096b6382c430cdbef04242e9f8ac9c.jpeg?token=9641d0615a2bf1b8a92c9c2e0c3bf0e5)

**Redis用户分布式锁时，实现简单，市面上也有许多的开源框架。**但是从根本上来说，它并不适合于分布式锁。因为分布式锁从业务场景上来说，是CP的，但Redis是AP的。

**Zookeeper在实现分布式锁时，依靠的是创建临时节点和watch机制，**它的效率和扩展能力都是比较低的，因此，也较少人使用。

**etcd是一个Key/Value存储系统，**但是它不同于Redis，在一致性和集群方面，借鉴了Zookeeper，使得它的集群能力和一致性能力都是比较强的。在使用方面，又采用restful API这种比较简单的使用方式，有点像ES。因此，我们发现，其实etcd是最适合用来做分布式锁的。

表格中，对etcd的接口类型提供，它可以使用http或grpc。grpc是一个基于http/2，面向移动端的RPC框架。它具备以下一些优势：

1. 双向流，多路复用，高吞吐量。

2. 头部压缩。

3. 跨平台，多语言。

后续，我们会有专门的文章来聊http，grpc等通信协议。

**分布式锁业务需求**

确定了etcd是比较适合于分布式锁的一种技术，接下来，我们来思考一下，在设计分布式锁时，有哪些需求是需要满足的。可以参考一些常见的分布式锁框架来整理。

1. **强一致性。**从业务场景来说，分布式锁一定应该是CP的。

2. **服务高可用，系统稳健。**有稳定可靠的集群能力。

3. **锁自动续约和释放。**锁可以自动释放或续约，避免死锁或丢锁。

4. **封装性好，接口简单。**具备极强的可封装性，对业务代码侵入极小。

5. **可重入。**需要实现可重入锁。

6. **统一管理。**需要一个统一的管理后台。可以对当前锁的使用进行统一的监控和管理。

**基于etcd的分布式锁的基本框架和关键技术**

根据上面的业务需求，我们可以描绘出一个基于etcd的分布式锁的基本框架。如下：

![img](https://pics7.baidu.com/feed/7acb0a46f21fbe091794ad9fc2a3c9358544adb9.png?token=f192c47f64d9d40d3434081dc09e3d38)分布式锁框架

​      如图，首先我们需要一个etcd集群和一个监控平台，然后，我们需要封装一个客户端包，供其它微服务方便地接入。客户端中，主要有竞锁，续租，探活，监控等几个主要功能，监控会将运行数据和监控平台进行交互，其它则主要和etcd集群发生交互。

etcd的锁结构，和Redis一样，也是一个key/value对。获取锁时，通过http或grpc结构，向etcd中插入key/value键值对，插入成功则获取锁成功，否则获取锁失败。

服务的续航和自动释放，同样也可以通过像Redis一样，对key设置过期时间，并通过独立线程或协程，采用心跳机制，自动续约。





## sass线上服务器申请

 sass平台上线环境申请配置如下：
 1、申请服务器数量：1台
 2、服务器安装软件：docker-compose、docker
 3、申请安装服务如下
	gatewayweb               服务端口号10007  映射端口号8087

​	datadictionaryweb    服务端口号10005  映射端口号8085
​	componentweb         服务端口号10004  映射端口号8084
​	userweb                      服务端口号10002  映射端口号8082
​	packweb                     服务端口号10003  映射端口号8083
​	accountweb               服务端口号10001  映射端口号8081
​	bootadminweb         服务端口号10006  映射端口号8086

​	redis                          

​	mysql                                      

​	grafana/grafana                            

​	minio/minio                                

​	172.16.230.97/source/tomcat_jdk1.8.0_281   

​	nacos/nacos-server                         

​	bladex/sentinel-dashboard








## Sass平台服务拆分

Saaa平台分为**运营端**和**商户端**，所以第一步拆分是两个平台的拆分，然后根据功能再进行细粒度的拆分。



**运营端拆分**

- 运营端的登录，忘记密码账户管理还有客户管理主要存储的数据是本身平台的数据，和客户的数据，主要由我们这边来操作，所以这几块可以拆分成一个微服务，进行基本的数据查询。senyint-operation-account

- 用户管理模块主要是存储医生，患者的数据信息。前期数据量不是很大的时候可以使用单张表，等数据量多的时候可以进行分库分表操作，根据不同的维度，例如医生/患者分库分表，客户下的用户管理等等不同维度。所以可以把用户管理单独拆分成一个微服务. senyint-operation-user

- 服务包管理属于单独的一块业务，供客户后续购买服务包等相关的操作。而且业务流程涉及的比较复杂，功能较多，更适合低耦合。故拆分成一个微服务来进行集中开发。senyint-operation-servicepack

- 数据集管理(表单)，主要功能是从新建表单--》表单预览的流程。里面可以进行表单设置、表单分组、题目关联设置等功能，目的是可以实现表单动态配置，通过不同表单配置生成不同表单页面来进行数据收集统计。可以单独拆分成一个微服务，来兼容不同的表单配置。senyint-operation-dataset

- 数据字典管理：数据字典就像一个字典库一样。其他服务需要使用字典的数据直接可以使用就可以。可以单独拆分成一个公共的服务模块。

  senyint-operation-datadictionary

- 组件库管理：组件库主要提供一些我们常用的比如说单选、多选按钮等表单常用的选项。里面包含基础组件、组件模块、组件模板三个大的功能。主要为了实现一些组件可以灵活的进行动态的配置，供其他模块使用。所以可以拆分成一个单独的服务。senyint-operation-componentlibrary





**商户端拆分**

- 商户端的用户管理主要包含医生端和患者端和**产品关联的数据**，主要对医生、患者的新增，查询操作，还包含**机构管理、部门科室管理**功能，这些可以单独拆分成一个服务。(商户端的用户管理服务)   senyint-merchants-user

- 购买服务涉及到支付等操作，所以需要单独拆分成一个支模服务，进行支付操作。 senyint-merchants-pay

- 购买服务也涉及到订单操作，包含后台的订单分页，前台用户订购的服务列表显示。故拆分出一个订单服务模块。 senyint-merchants-order

- 产品管理：商户端会创建产品，产品下关联服务、产品配置、产品导航等功能，目的是为了最终生成可以访问的小程序，需要单独拆分成一个服务模块。

  senyint-merchants-product

- 





## Saas中台产品未来的可能情况：



**数据库方面：**

- 每一个客户有单独的一套数据库，客户code做表名的标识
- 可能会存在多个库，不同的数据库存储不同的客户数据信息，避免数据量单点很大
- 不同客户的数据查询可以通过唯一标识查询不同的数据库，达到隔离数据的目的
- 



**代码方面：**

- 可能会根据不同的客户功能的不同有多个分支版本，进行部署
- 





**卒中网站index.html需要修改的请求url：**

var url = "https://biaodan.senyint.com/homepage/reginster";





学号：211742422400001





## 开发过程中的思考

- 实体是否可以单独弄一个组件，供其他项目引入，实现实体统一管理（**实体由在每个服务中=====》统一管理**）
- 是否再单独拆出来一个工具类(和技术点不相关的工具类)







## 【腾讯文档】saas项目进度表
https://docs.qq.com/sheet/DTldpakt4enhLUGp4



## 【周报地址】 OKR

https://rxe250217u.feishu.cn/sheets/shtcnrVmJHP7cZqvgMcsvbnOECF





## 【接口文档访问地址】



### ==>account-web

```http
http://172.16.230.81:8081/doc.html
```

### ==>user-web

```http
http://172.16.230.81:8082/doc.html
```

### ==>servicepack-web

```http
http://172.16.230.81:8083/doc.html
```

### ==>component-web

```http
http://172.16.230.81:8084/doc.html
```

### ==>datadictionary-web

```http
http://172.16.230.81:8085/doc.html
```

### ==>boot-admin

```http
http://172.16.230.81:8086/doc.html
```

### ==>gateway

```http
http://172.16.230.81:8087/doc.html
```







## 【开发遇到的问题】



- mybatis-plus时间默认不填充

```java
 @TableField(fill = FieldFill.INSERT)
 private LocalDateTime createTime;
```

- knife4j在定义tags的时候不要用**斜杠（/）**,否则接口文档访问不了，并且出现多个相同tag
- 不需要鉴权的路径

```yaml
# 不需要鉴权的路径
ignores:
  - "/"
  - "/notice/**"
  - "/excel/**"
  - "/file/**"
  - "/document/**"
  - "/csv/**"
  - "/user-es/**"
  - "/quartz/**"
  - "/test/**"
  - "/cloud/**"
  - "/cache/**"
  - "/mq/**"
  - "/kaptcha/**"
  - "/activiti/**"
  - "/openOffice/**"
  - "/auth/register"
  - "/auth/login"
  - "/operating-account/login"
  - "/auth/logout"
  - "/api/**"
  - "/resources/**"
  - "/templates/**"
  - "/websocket.html"
  - "/error.html"
  - "/kaptcha.html"
  - "/imgs/**"
  - "*.css"
  - "*.js"
  - "*.gif"
  - "*.jpg"
  - "*.png"
  - "*.ico"
  - "/favicon.ico"
  - "/public/**"
  - "/actuator/**"
  - "/swagger-ui.html"
  - "/doc.html"
  - "/swagger-resources/**"
  - "/service-worker.js"
  - "/v2/**"
  - "/webjars/**"
```

- RbacAutoConfiguration类处理mybatis启动前的配置信息，比如承租人id哪些表不查询等等。



**登录正确返回的数据：**

![image-20210414113600026](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210414113600026.png)





**数据校验：**

```
ResponseEnum.SMS_VERIFICATION_CODE_ERROR.assertIsTrue(smsCode.equals(redisSmsCode.toString()));
```



问题排查：

**第一步：**

account模块

![image-20210415093418605](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210415093418605.png)



admin模块

![image-20210415094132036](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210415094132036.png)



第二步：

account

![image-20210415094322417](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210415094322417.png)



admin  已经来就加载

![image-20210415094214772](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210415094214772.png)



最终原因是因为jar包代码不一致导致的。



登录成功之后需要调用的功能：

```
查询用户拥有的菜单权限和按钮权限（根据TOKEN）
```

步骤：

- jwt根据token查询用户信息

- 根据用户查询用户权限

  ```java
  <select id="queryByUser" parameterType="Object"  resultMap="SysPermission">
  		   SELECT * FROM (
  			   SELECT p.*
  			   FROM  sys_permission p
  			   WHERE (exists(
  						select a.id from sys_role_permission a
  						join sys_role b on a.role_id = b.id
  						join sys_user_role c on c.role_id = b.id
  						join sys_user d on d.id = c.user_id
  						where p.id = a.permission_id AND d.username = #{username,jdbcType=VARCHAR}
  					)
  					or (p.url like '%:code' and p.url like '/online%' and p.hidden = 1)
  					or p.url = '/online')
  			   and p.del_flag = 0
  			<!--update begin Author:lvdandan  Date:20200213 for：加入部门权限 -->
  			   UNION
  			   SELECT p.*
  			   FROM  sys_permission p
  			   WHERE exists(
  					select a.id from sys_depart_role_permission a
  					join sys_depart_role b on a.role_id = b.id
  					join sys_depart_role_user c on c.drole_id = b.id
  					join sys_user d on d.id = c.user_id
  					where p.id = a.permission_id AND d.username = #{username,jdbcType=VARCHAR}
  			   )
  			   and p.del_flag = 0
  			<!--update end Author:lvdandan  Date:20200213 for：加入部门权限 -->
  		   ) h order by h.sort_no ASC
  	</select>
  ```

  

- 获取权限json数组

- 查询所有的权限

- 



**Mysql数据库存储中文乱码问题**



> 查看数据库编码类型： show variables like 'character_set_%';
>
>
> 设置数据库编码类型：SET character_set_server = utf8 ;



**sentinel配置出现错误日志问题**

```yml
#  sentinel 流控
sentinel:
  # 是否开启sentinel
  enabled: false
  filter:
    enabled: true
  # 取消Sentinel控制台懒加载
  eager: true
  # 控制台地址
  transport:
    dashboard: 172.16.230.81:8858
  #  持久化数据库-nacos
  datasource:
    # 限流
    flow:
      nacos:
        dataId: ${spring.application.name}-${spring.profiles.active}-sentinel-flow.json
        groupId: ${spring.cloud.nacos.config.group}
        data-type: json
        # 规则
        ruleType: FLOW
        server-addr: ${spring.cloud.nacos.config.server-addr}
        namespace: ${spring.cloud.nacos.config.namespace}
    # 熔断降级
    degrade:
      nacos:
        dataId: ${spring.application.name}-${spring.profiles.active}-sentinel-degrade.json
        groupId: ${spring.cloud.nacos.config.group}
        data-type: json
        # 规则
        ruleType: DEGRADE
        server-addr: ${spring.cloud.nacos.config.server-addr}
        namespace: ${spring.cloud.nacos.config.namespace}
    # 系统保护
    system:
      nacos:
        dataId: ${spring.application.name}-${spring.profiles.active}-sentinel-system.json
        groupId: ${spring.cloud.nacos.config.group}
        data-type: json
        # 规则
        ruleType: SYSTEM
        server-addr: ${spring.cloud.nacos.config.server-addr}
        namespace: ${spring.cloud.nacos.config.namespace}
    # 授权
    authority:
      nacos:
        dataId: ${spring.application.name}-${spring.profiles.active}-sentinel-authority.json
        groupId: ${spring.cloud.nacos.config.group}
        data-type: json
        # 规则
        ruleType: AUTHORITY
        server-addr: ${spring.cloud.nacos.config.server-addr}
        namespace: ${spring.cloud.nacos.config.namespace}
    # 热点
    hot:
      nacos:
        dataId: ${spring.application.name}-${spring.profiles.active}-sentinel-hot.json
        groupId: ${spring.cloud.nacos.config.group}
        data-type: json
        # 规则
        ruleType: PARAM_FLOW
        server-addr: ${spring.cloud.nacos.config.server-addr}
        namespace: ${spring.cloud.nacos.config.namespace}
```



## Gitlab拉取代码SSL问题

> git config --global http.sslVerify false



**Git错误fatal: unable to access 'https://github.com/****.git/': Couldn't resolve host 'github.com'**


今天对github上的项目进行git pull的时候出现了错误：

fatal: unable to access 'https://github.com/****.git/: Couldn't resolve host 'github.com'
1
这里的**.git是假设我的github项目地址。我记得前些天git突然提示我重新输入用户名和密码，当时用户名输入错了，连接remote验证失败，这应该就是原因。
百度了一下解决办法，试了都没什么鸡蛋用，但是每个人情况都不一样，我这里贴出来是因为对于你可能是有用的（你就是它的命中注定^^）：
如什么
1.重设账号密码的：

（1）先重新设置本机git配置：git config --global credential.helper store

（2）输入github账号和密码

（3）最后push代码：git push -u origin master

2.重置git配置的http或https代理的：

http version:

> git config --global --unset http.proxy

https version:

> git config --global --unset https.proxy
>
> 最后我想，反正我是pull代码，直接重新添加远程的分支就好了，于是：

（1）移除本地分支：git remote rm origin
（2）添加远程分支：git remote add origin 'https://github.com/***.git'
（3）然后：git pull即可



## Node问题

> 问题：stack Error: `gyp` failed with exit code: 1

stack Error: `gyp` failed with exit code: 1

**解决办法**

删除 .node-gyp/  

执行 npm i -g node-gyp  

删除 项目/node_modules  

执行 npm i -d 




## Docker 启动服务

### Docker启动gitlab

```dockerfile
docker run \
 -itd  \
 -h gitlab.yuzhaopeng.club\
 -p 8022:22 \
 -p 8880:80 \
 -v /usr/local/gitlab/etc:/etc/gitlab  \
 -v /usr/local/gitlab/log:/var/log/gitlab \
 -v /usr/local/gitlab/opt:/var/opt/gitlab \
 --name gitlab \
 gitlab/gitlab-ce
```



### Docker启动elasticsearch

```shell
docker run -d -p 9200:9200 --name='es' monsantoco/elasticsearch
```



## 【自定义注解】

```java
/**
 * @功能描述 防止重复提交标记注解
 * @author jz.Zhang
 * @date 2018-08-26
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NoRepeatSubmit {
    /**
     * 重复统计时长
     * @return
     */
    int value() default 1;
}
```



使用：

```java
/**
 * 接口防重复切面
 * @author jz.Zhang
 * @date 2018-08-26
 */
@Aspect
@Slf4j
public class NoRepeatSubmitAop {

    private static final String JWT_TOKEN_KEY = "jwt-token";
    public static final String GUEST = "guest";

    @Pointcut("execution(* com.senyint.*.controller.*Controller.*(..))")
    public void serviceNoRepeat() {
    }

    @Around("serviceNoRepeat()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {

        SassRequestWrapper requestWrapper = new SassRequestWrapper(RequestHolder.getRequest());
        String jwtToken = StringUtils.isBlank(requestWrapper.getHeader(JWT_TOKEN_KEY)) ? GUEST : requestWrapper.getHeader(JWT_TOKEN_KEY);
        // 需要拦截的方法类型
        List<String> methods = Arrays.asList("POST", "PUT", "DELETE", "PATCH");
        // 方法类型
        String method = requestWrapper.getMethod();
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        // 接口上加的注解
        NoRepeatSubmit noRepeatSubmit = signature.getMethod().getAnnotation(NoRepeatSubmit.class);
        if(methods.contains(method)){
            StringBuilder keyBuilder = new StringBuilder();
            keyBuilder.append(jwtToken).append("-").append(requestWrapper.getRequestURL()).append("-")
                    .append(JSON.toJSONString(requestWrapper.getParameterMap())).append("-")
                    .append(requestWrapper.getBody());
            String key = Md5Util.md5( keyBuilder.toString());
            if (RedisUtil.get(key) == null) {
                // 默认1秒内统一用户同一个地址同一个参数，视为重复提交
                RedisUtil.setExpire(key, "0",noRepeatSubmit == null ? 1 : noRepeatSubmit.value());
            } else {
                throw  ResponseEnum.REPEAT_COMMIT.newException();
            }
        }
        return pjp.proceed();
    }
}
```







## [Java 8 LocalDateTime 初使用](https://www.cnblogs.com/wt20/p/8179118.html)

**LocalTime :**  只包括时间

**LocalDate :** 只包括日期

**LocalDateTime ：** 包括日期和时间

------

 

**JDBC映射**

**LocalTime** 对应 **time**

**LocalDate** 对应 **date**

**LocalDateTime** 对应 **timestamp**

------

 

**以下测试代码**

简直好用到爆

```java
public class TimeTest {
 
    @Test
    public void testTime() {
        LocalDateTime time = LocalDateTime.now();
 
        System.out.println(time.toString()); //字符串表示
        System.out.println(time.toLocalTime()); //获取时间(LocalTime)
        System.out.println(time.toLocalDate()); //获取日期(LocalDate)
        System.out.println(time.getDayOfMonth()); //获取当前时间月份的第几天
        System.out.println(time.getDayOfWeek());  //获取当前周的第几天
        System.out.println(time.getDayOfYear());  //获取当前时间在该年属于第几天
        System.out.println(time.getHour());
        System.out.println(time.getMinute());
        System.out.println(time.getMonthValue());
        System.out.println(time.getMonth());
        System.out.println("-----------------------------------");
        //格式化输出
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY/MM/dd HH:mm:ss");
        System.out.println(time.format(formatter));
        //构造时间
        LocalDateTime startTime = LocalDateTime.of(2018, 1, 1, 20, 31, 20);
        LocalDateTime endTime = LocalDateTime.of(2018, 1, 3, 20, 31, 20);
        //比较时间
        System.out.println(time.isAfter(startTime));
        System.out.println(time.isBefore(endTime));
 
        //时间运算，相加相减
        System.out.println(time.plusYears(2)); //加2年
        System.out.println(time.plusDays(2)); //加两天
        System.out.println(time.minusYears(2)); //减两年
        System.out.println(time.minusDays(2)); //减两天
 
        //获取毫秒数(使用Instant)
        System.out.println(time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        //获取秒数(使用Instant)
        System.out.println(time.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond());
    }
 
}
```



## 【常用的工具类】



功能：缓存存储

类名：RedisUtil



**服务管理下自定义页面配置功能拆分：**

- 功能菜单列表展示
- 关联页面
  - 组件
  - 数据字典
- 页面生成json+左侧基础组件列表id传递做关联
- 



**maven打包文件变小：**



maven 抽取lib



拷贝jar文件

> mvn dependency:copy-dependencies



maven打包

> mvn package



运行jar

> ```
> java -Dloader.path="dependency/" -jar account-web.jar
> ```





## 短信平台条数申请需要确定

> 因为短信的整体数量是固定的，每个项目使用需要确定下来，方便日后分摊成本。



短信平台目前充了50条



**短信模板**

项目名称: 心医卒中系统
信息模板:【心医云SAAS】验证码：**，5分钟内有效，如不是您本人操作请忽略。

说明：【】内为短信签名，**代表变量



短信平台接入：

账号：810328
密码：1CXPW25G8X



请求短信平台返回结果：

```
Connected to the target VM, address: '127.0.0.1:60857', transport: 'socket'
send msg:	{"cust_code":"810328","sp_code":"","content":"【心医云SAAS】验证码:701687,5分钟内有效，如不是您本人操作请忽略。","destMobiles":"18610376820","uid":"null","need_report":"yes","sign":"86b20bb151a11a369ef4fbddac48c14c"}
recv msg:	{"respCode":"0","respMsg":"提交成功！","result":[{"chargeNum":1,"code":"0","mobile":"18610376820","msg":"提交成功.","msgid":"59105104141433047116"}],"status":"success","totalChargeNum":1,"uid":"null"}
Disconnected from the target VM, address: '127.0.0.1:60857', transport: 'socket'
```





# minio存储桶命名规则



## 存储桶命名规则

创建S3存储桶后，无法更改存储桶名称，因此请明智地选择名称。

重要

在2018年3月1日，我们更新了美国东部（弗吉尼亚北部）地区S3存储桶的命名约定，以匹配我们在所有其他全球AWS区域中使用的命名约定。Amazon S3不再支持创建包含大写字母或下划线的存储桶名称。此更改确保可以使用虚拟主机样式寻址来寻址每个存储桶，例如 `https://myawsbucket.s3.amazonaws.com`。我们强烈建议您查看现有的存储桶创建过程，以确保遵循这些符合DNS的命名约定。

以下是在所有AWS区域中命名S3存储桶的规则：

- 存储桶名称在Amazon S3中的所有现有存储桶名称中必须是唯一的。
- 存储桶名称必须符合DNS命名约定。
- 存储桶名称长度必须至少为3且不超过63个字符。
- 存储桶名称不得包含大写字符或下划线。
- 存储桶名称必须以小写字母或数字开头。
- 存储桶名称必须是一系列一个或多个标签。相邻标签由单个句点（。）分隔。存储桶名称可以包含小写字母，数字和连字符。每个标签必须以小写字母或数字开头和结尾。
- 存储桶名称不得格式化为IP地址（例如，192.168.5.4）。
- 使用带有安全套接字层（SSL）的虚拟托管样式存储桶时，SSL通配符证书仅匹配不包含句点的存储桶。要解决此问题，请使用HTTP或编写自己的证书验证逻辑。我们建议您在使用虚拟托管样式存储桶时不要在存储桶名称中使用句点（“。”）。

### 传统的非DNS兼容存储桶名称

从2018年3月1日开始，我们更新了美国东部（弗吉尼亚北部）区域S3存储桶的命名约定，以要求符合DNS的名称。

美国东部（弗吉尼亚北部）区域之前允许更宽松的存储桶命名标准，这可能导致存储桶名称不符合DNS。例如，它  `MyAWSbucket`是一个有效的存储桶名称，即使它包含大写字母。如果您尝试使用虚拟托管样式的请求（`http://MyAWSbucket.s3.amazonaws.com/yourobject`）访问此存储桶，则URL会解析为存储桶`myawsbucket`而不是存储桶  `MyAWSbucket`。作为响应，Amazon S3返回“未找到存储桶”错误。有关对存储桶进行虚拟托管样式访问的更多信息，请参阅[桶的虚拟主机](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.aws.amazon.com%2FAmazonS3%2Flatest%2Fdev%2FVirtualHosting.html)。

美国东部（弗吉尼亚北部）区域中存储桶名称的旧规则允许存储桶名称长度为255个字符，存储桶名称可以包含大写字母，小写字母，数字，句点（。），连字符（ - ）和下划线（_）。

用于Amazon S3传输加速的存储桶名称必须符合DNS，且不得包含句点（“。”）。有关传输加速的更多信息，请参阅[Amazon S3传输加速](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.aws.amazon.com%2FAmazonS3%2Flatest%2Fdev%2Ftransfer-acceleration.html)。





## 【[HTTP错误code大全](https://www.cnblogs.com/suizhikuo/archive/2012/07/01/2571614.html)】

100 - Continue
101 - Switching Protocols

Top

Success Codes

200 - OK
201 - Created
202 - Accepted
203 - Non-Authoritative Information (for DNS)
204 - No Content
205 - Reset Content
206 - Partial Content

Top

Redirection Codes

300 - Multiple Choices
301 - Moved Permanently
302 - Moved Temporarily
303 - See Other
304 - Not Modified
305 - Use Proxy
307 - Redirect Keep Verb

Top

Failure Codes

400 - Bad Request[物流配货网http://wlphw.com/ ](http://wlphw.com/)[QQ在线:471226865![点击这里给我发消息](http://wpa.qq.com/pa?p=2:442518843:41)](http://wpa.qq.com/msgrd?v=3&uin=442518843&site=qq&menu=yes)
401 - Unauthorized
402 - Payment Required
403 - Forbidden
404 - Not Found
405 - Bad Request
406 - Not Acceptable
407 - Proxy Authentication Required
408 - Request Timed-Out
409 - Conflict
410 - Gone
411 - Length Required
412 - Precondition Failed
413 - Request Entity Too Large
414 - Request, URI Too Large
415 - Unsupported Media Type

Top

Server Error Codes

500 - Internal Server Error
501 - Not Implemented
502 - Bad Gateway
503 - Server Unavailable
504 - Gateway Timed-Out
505 - HTTP Version not supported

Top

Internet API Error Codes

12001 - Out of Handles
12002 - Timeout
12003 - Extended Error
12004 - Internal Error
12005 - Invalid URL
12006 - Unrecognized Scheme
12007 - Name Not Resolved
12008 - Protocol Not Found
12009 - Invalid Option
12010 - Bad Option Length
12011 - Option not Settable
12012 - Shutdown
12013 - Incorrect User Name
12014 - Incorrect Password
12015 - Login Failure
12016 - Invalid Option
12017 - Operation Cancelled
12018 - Incorrect Handle Type
12019 - Inccorect Handle State
12020 - Not Proxy Request
12021 - Registry Value Not Found
12022 - Bad Registry Parameter
12023 - No Direct Access
12024 - No Content
12025 - No Callback
12026 - Request Pending
12027 - Incorrect Format
12028 - Item Not Found
12029 - Cannot Connect
12030 - Connection Aborted
12031 - Connection Reset
12032 - Force Retry
12033 - Invalid Proxy Request
12034 - Need UI
12035 - Not Defined in WinInet
12036 - Handle Exists
12037 - See Cert Date Invalid
12038 - See Cert CN Invalid
12039 - HTTP to HTTPS on Redir
12040 - HTTPs to HTTP on Redir
12041 - Mixed Security
12042 - Chg Post is Non Secure
12043 - Post is Non Secure
12044 - Client Auth Cert Needed
12045 - Invalid CA (Cert)
12046 - Client Auth Not Setup
12047 - Async Thread Failed
12048 - Redirect Scheme Changed
12049 - Dialog Pending
12050 - Retry Dialog
12052 - Https Http Submit Redir
12053 - Insert Cdrom
12171 - Failed DueToSecurityCheck

Top

FTP API Error Codes

12110 - Transfer in Progress
12111 - FTP Dropped

Top

Gopher API Error Codes

12130 - Protocol Error
12131 - Not File
12132 - Data Error
12133 - End of Data
12134 - Invalid Locator
12135 - Invalid Locator Type
12136 - Not Gopher Plus
12137 - Attribute Not Found
12138 - Unknown Locator

Top

HTTP API Error Codes

12150 - Header Not Found
12151 - Downlevel Server
12152 - Invalid Server Response
12153 - Invalid Header
12154 - Invalid Query Request
12155 - Header Already Exists
12156 - Redirect Failed
12157 - Security Channel Error
12158 - Unable to Cache File
12159 - TCP/IP not installed
12160 - Not Redirected
12161 - Cookie Needs Confirmation
12162 - Cookie Declined
12168 - Redirect Needs Confirmation

Top

Additional Internet API Error Codes

12157 - Security Channel Error
12158 - Unable To Cache File
12159 - Tcpip Not Installed
12163 - Disconnected
12164 - Server Unreachable
12165 - Proxy Server Unreachable
12166 - Bad Auto Proxy ｓｃｒｉｐｔ
12167 - Unable To Download ｓｃｒｉｐｔ
12169 - Sec Invalid Cert
12170 - Sec Cert Revoked

HTTP常见错误

HTTP 错误 400
400 请求出错
由于语法格式有误，服务器无法理解此请求。不作修改，客户程序就无法重复此请求。

HTTP 错误 401
401.1 未授权：登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.2 未授权：服务器的配置导致登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。此错误通常由未发送正确的 WWW 验证表头字段所致。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.3 未授权：由于资源中的 ACL 而未授权
此错误表明客户所传输的证书没有对服务器中特定资源的访问权限。此资源可能是客户机中的地址行所列出的网页或文件，也可能是处理客户机中的地址行所列出的文件所需服务器上的其他文件。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.4 未授权：授权服务被筛选程序拒绝
此错误表明 Web 服务器已经安装了筛选程序，用以验证连接到服务器的用户。此筛选程序拒绝连接到此服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.5 未授权：ISAPI/CGI 应用程序的授权失败
此错误表明试图使用的 Web服务器中的地址已经安装了 ISAPI 或 CGI程序，在继续之前用以验证用户的证书。此程序拒绝用来连接到服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web服务器的管理员联系以确认您是否具有访问所请求资源的权限

HTTP 错误 403
403.1 禁止：禁止执行访问
如果从并不允许执行程序的目录中执行 CGI、ISAPI或其他执行程序就可能引起此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.2 禁止：禁止读取访问
如果没有可用的默认网页或未启用此目录的目录浏览，或者试图显示驻留在只标记为执行或脚本权限的目录中的HTML 页时就会导致此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.3 禁止：禁止写访问
如果试图上载或修改不允许写访问的目录中的文件，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.4 禁止：需要 SSL
此错误表明试图访问的网页受安全套接字层（SSL）的保护。要查看，必须在试图访问的地址前输入https:// 以启用 SSL。
如果问题依然存在，请与 Web服务器的管理员联系。
403.5 禁止：需要 SSL 128
此错误消息表明您试图访问的资源受 128位的安全套接字层（SSL）保护。要查看此资源，需要有支持此SSL 层的浏览器。
请确认浏览器是否支持 128 位 SSL安全性。如果支持，就与 Web服务器的管理员联系，并报告问题。
403.6 禁止：拒绝 IP 地址
如果服务器含有不允许访问此站点的 IP地址列表，并且您正使用的 IP地址在此列表中，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.7 禁止：需要用户证书
当试图访问的资源要求浏览器具有服务器可识别的用户安全套接字层（SSL）证书时就会导致此问题。可用来验证您是否为此资源的合法用户。
请与 Web服务器的管理员联系以获取有效的用户证书。
403.8 禁止：禁止站点访问
如果 Web服务器不为请求提供服务，或您没有连接到此站点的权限时，就会导致此问题。
请与 Web 服务器的管理员联系。
403.9 禁止访问：所连接的用户太多
如果 Web太忙并且由于流量过大而无法处理您的请求时就会导致此问题。请稍后再次连接。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.10 禁止访问：配置无效
此时 Web 服务器的配置存在问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.11 禁止访问：密码已更改
在身份验证的过程中如果用户输入错误的密码，就会导致此错误。请刷新网页并重试。
如果问题依然存在，请与 Web服务器的管理员联系。
403.12 禁止访问：映射程序拒绝访问
拒绝用户证书试图访问此 Web 站点。
请与站点管理员联系以建立用户证书权限。如果必要，也可以更改用户证书并重试。

HTTP 错误 404
404 找不到
Web 服务器找不到您所请求的文件或脚本。请检查URL 以确保路径正确。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 405
405 不允许此方法
对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 406
406 不可接受
根据此请求中所发送的“接受”标题，此请求所标识的资源只能生成内容特征为“不可接受”的响应实体。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 407
407 需要代理身份验证
在可为此请求提供服务之前，您必须验证此代理服务器。请登录到代理服务器，然后重试。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 412
412 前提条件失败
在服务器上测试前提条件时，部分请求标题字段中所给定的前提条件估计为FALSE。客户机将前提条件放置在当前资源 metainformation（标题字段数据）中，以防止所请求的方法被误用到其他资源。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 414
414 Request-URI 太长
Request-URL太长，服务器拒绝服务此请求。仅在下列条件下才有可能发生此条件：
客户机错误地将 POST 请求转换为具有较长的查询信息的 GET 请求。
客户机遇到了重定向问题（例如，指向自身的后缀的重定向前缀）。
服务器正遭受试图利用某些服务器（将固定长度的缓冲区用于读取或执行 Request-URI）中的安全性漏洞的客户干扰。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 500
500 服务器的内部错误
Web 服务器不能执行此请求。请稍后重试此请求。
如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 501
501 未实现
Web 服务器不支持实现此请求所需的功能。请检查URL 中的错误，如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 502
502 网关出错
当用作网关或代理时，服务器将从试图实现此请求时所访问的upstream 服务器中接收无效的响应。
如果问题依然存在，请与 Web服务器的管理员联HTTP常见错误

HTTP 错误 400
400 请求出错
由于语法格式有误，服务器无法理解此请求。不作修改，客户程序就无法重复此请求。

HTTP 错误 401
401.1 未授权：登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.2 未授权：服务器的配置导致登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。此错误通常由未发送正确的 WWW 验证表头字段所致。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.3 未授权：由于资源中的 ACL 而未授权
此错误表明客户所传输的证书没有对服务器中特定资源的访问权限。此资源可能是客户机中的地址行所列出的网页或文件，也可能是处理客户机中的地址行所列出的文件所需服务器上的其他文件。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.4 未授权：授权服务被筛选程序拒绝
此错误表明 Web 服务器已经安装了筛选程序，用以验证连接到服务器的用户。此筛选程序拒绝连接到此服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.5 未授权：ISAPI/CGI 应用程序的授权失败
此错误表明试图使用的 Web服务器中的地址已经安装了 ISAPI 或 CGI程序，在继续之前用以验证用户的证书。此程序拒绝用来连接到服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web服务器的管理员联系以确认您是否具有访问所请求资源的权限

HTTP 错误 403
403.1 禁止：禁止执行访问
如果从并不允许执行程序的目录中执行 CGI、ISAPI或其他执行程序就可能引起此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.2 禁止：禁止读取访问
如果没有可用的默认网页或未启用此目录的目录浏览，或者试图显示驻留在只标记为执行或脚本权限的目录中的HTML 页时就会导致此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.3 禁止：禁止写访问
如果试图上载或修改不允许写访问的目录中的文件，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.4 禁止：需要 SSL
此错误表明试图访问的网页受安全套接字层（SSL）的保护。要查看，必须在试图访问的地址前输入https:// 以启用 SSL。
如果问题依然存在，请与 Web服务器的管理员联系。
403.5 禁止：需要 SSL 128
此错误消息表明您试图访问的资源受 128位的安全套接字层（SSL）保护。要查看此资源，需要有支持此SSL 层的浏览器。
请确认浏览器是否支持 128 位 SSL安全性。如果支持，就与 Web服务器的管理员联系，并报告问题。
403.6 禁止：拒绝 IP 地址
如果服务器含有不允许访问此站点的 IP地址列表，并且您正使用的 IP地址在此列表中，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.7 禁止：需要用户证书
当试图访问的资源要求浏览器具有服务器可识别的用户安全套接字层（SSL）证书时就会导致此问题。可用来验证您是否为此资源的合法用户。
请与 Web服务器的管理员联系以获取有效的用户证书。
403.8 禁止：禁止站点访问
如果 Web服务器不为请求提供服务，或您没有连接到此站点的权限时，就会导致此问题。
请与 Web 服务器的管理员联系。
403.9 禁止访问：所连接的用户太多
如果 Web太忙并且由于流量过大而无法处理您的请求时就会导致此问题。请稍后再次连接。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.10 禁止访问：配置无效
此时 Web 服务器的配置存在问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.11 禁止访问：密码已更改
在身份验证的过程中如果用户输入错误的密码，就会导致此错误。请刷新网页并重试。
如果问题依然存在，请与 Web服务器的管理员联系。
403.12 禁止访问：映射程序拒绝访问
拒绝用户证书试图访问此 Web 站点。
请与站点管理员联系以建立用户证书权限。如果必要，也可以更改用户证书并重试。

HTTP 错误 404
404 找不到
Web 服务器找不到您所请求的文件或脚本。请检查URL 以确保路径正确。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 405
405 不允许此方法
对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 406
406 不可接受
根据此请求中所发送的“接受”标题，此请求所标识的资源只能生成内容特征为“不可接受”的响应实体。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 407
407 需要代理身份验证
在可为此请求提供服务之前，您必须验证此代理服务器。请登录到代理服务器，然后重试。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 412
412 前提条件失败
在服务器上测试前提条件时，部分请求标题字段中所给定的前提条件估计为FALSE。客户机将前提条件放置在当前资源 metainformation（标题字段数据）中，以防止所请求的方法被误用到其他资源。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 414
414 Request-URI 太长
Request-URL太长，服务器拒绝服务此请求。仅在下列条件下才有可能发生此条件：
客户机错误地将 POST 请求转换为具有较长的查询信息的 GET 请求。
客户机遇到了重定向问题（例如，指向自身的后缀的重定向前缀）。
服务器正遭受试图利用某些服务器（将固定长度的缓冲区用于读取或执行 Request-URI）中的安全性漏洞的客户干扰。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 500
500 服务器的内部错误
Web 服务器不能执行此请求。请稍后重试此请求。
如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 501
501 未实现
Web 服务器不支持实现此请求所需的功能。请检查URL 中的错误，如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 502
502 网关出错
当用作网关或代理时，服务器将从试图实现此请求时所访问的upstream 服务器中接收无效的响应。
如果问题依然存在，请与 Web服务器的管理员联HTTP常见错误

HTTP 错误 400
400 请求出错
由于语法格式有误，服务器无法理解此请求。不作修改，客户程序就无法重复此请求。

HTTP 错误 401
401.1 未授权：登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.2 未授权：服务器的配置导致登录失败
此错误表明传输给服务器的证书与登录服务器所需的证书不匹配。此错误通常由未发送正确的 WWW 验证表头字段所致。
请与 Web 服务器的管理员联系，以确认您是否具有访问所请求资源的权限。
401.3 未授权：由于资源中的 ACL 而未授权
此错误表明客户所传输的证书没有对服务器中特定资源的访问权限。此资源可能是客户机中的地址行所列出的网页或文件，也可能是处理客户机中的地址行所列出的文件所需服务器上的其他文件。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.4 未授权：授权服务被筛选程序拒绝
此错误表明 Web 服务器已经安装了筛选程序，用以验证连接到服务器的用户。此筛选程序拒绝连接到此服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web 服务器的管理员联系以确认您是否具有访问所请求资源的权限。
401.5 未授权：ISAPI/CGI 应用程序的授权失败
此错误表明试图使用的 Web服务器中的地址已经安装了 ISAPI 或 CGI程序，在继续之前用以验证用户的证书。此程序拒绝用来连接到服务器的真品证书的访问。
请记录试图访问的完整地址，并与 Web服务器的管理员联系以确认您是否具有访问所请求资源的权限

HTTP 错误 403
403.1 禁止：禁止执行访问
如果从并不允许执行程序的目录中执行 CGI、ISAPI或其他执行程序就可能引起此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.2 禁止：禁止读取访问
如果没有可用的默认网页或未启用此目录的目录浏览，或者试图显示驻留在只标记为执行或脚本权限的目录中的HTML 页时就会导致此错误。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.3 禁止：禁止写访问
如果试图上载或修改不允许写访问的目录中的文件，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.4 禁止：需要 SSL
此错误表明试图访问的网页受安全套接字层（SSL）的保护。要查看，必须在试图访问的地址前输入https:// 以启用 SSL。
如果问题依然存在，请与 Web服务器的管理员联系。
403.5 禁止：需要 SSL 128
此错误消息表明您试图访问的资源受 128位的安全套接字层（SSL）保护。要查看此资源，需要有支持此SSL 层的浏览器。
请确认浏览器是否支持 128 位 SSL安全性。如果支持，就与 Web服务器的管理员联系，并报告问题。
403.6 禁止：拒绝 IP 地址
如果服务器含有不允许访问此站点的 IP地址列表，并且您正使用的 IP地址在此列表中，就会导致此问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.7 禁止：需要用户证书
当试图访问的资源要求浏览器具有服务器可识别的用户安全套接字层（SSL）证书时就会导致此问题。可用来验证您是否为此资源的合法用户。
请与 Web服务器的管理员联系以获取有效的用户证书。
403.8 禁止：禁止站点访问
如果 Web服务器不为请求提供服务，或您没有连接到此站点的权限时，就会导致此问题。
请与 Web 服务器的管理员联系。
403.9 禁止访问：所连接的用户太多
如果 Web太忙并且由于流量过大而无法处理您的请求时就会导致此问题。请稍后再次连接。
如果问题依然存在，请与 Web 服务器的管理员联系。
403.10 禁止访问：配置无效
此时 Web 服务器的配置存在问题。
如果问题依然存在，请与 Web服务器的管理员联系。
403.11 禁止访问：密码已更改
在身份验证的过程中如果用户输入错误的密码，就会导致此错误。请刷新网页并重试。
如果问题依然存在，请与 Web服务器的管理员联系。
403.12 禁止访问：映射程序拒绝访问
拒绝用户证书试图访问此 Web 站点。
请与站点管理员联系以建立用户证书权限。如果必要，也可以更改用户证书并重试。

HTTP 错误 404
404 找不到
Web 服务器找不到您所请求的文件或脚本。请检查URL 以确保路径正确。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 405
405 不允许此方法
对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 406
406 不可接受
根据此请求中所发送的“接受”标题，此请求所标识的资源只能生成内容特征为“不可接受”的响应实体。
如果问题依然存在，请与服务器的管理员联系。

HTTP 错误 407
407 需要代理身份验证
在可为此请求提供服务之前，您必须验证此代理服务器。请登录到代理服务器，然后重试。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 412
412 前提条件失败
在服务器上测试前提条件时，部分请求标题字段中所给定的前提条件估计为FALSE。客户机将前提条件放置在当前资源 metainformation（标题字段数据）中，以防止所请求的方法被误用到其他资源。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 414
414 Request-URI 太长
Request-URL太长，服务器拒绝服务此请求。仅在下列条件下才有可能发生此条件：
客户机错误地将 POST 请求转换为具有较长的查询信息的 GET 请求。
客户机遇到了重定向问题（例如，指向自身的后缀的重定向前缀）。
服务器正遭受试图利用某些服务器（将固定长度的缓冲区用于读取或执行 Request-URI）中的安全性漏洞的客户干扰。
如果问题依然存在，请与 Web 服务器的管理员联系。

HTTP 错误 500
500 服务器的内部错误
Web 服务器不能执行此请求。请稍后重试此请求。
如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 501
501 未实现
Web 服务器不支持实现此请求所需的功能。请检查URL 中的错误，如果问题依然存在，请与 Web服务器的管理员联系。

HTTP 错误 502
502 网关出错
当用作网关或代理时，服务器将从试图实现此请求时所访问的upstream 服务器中接收无效的响应。
如果问题依然存在，请与 Web服务器的管理员联











# mysql如何将表结构导出到excel



这篇文章主要介绍了mysql如何将表结构导出到excel，帮助大家更好的理解和使用MySQL数据库，感兴趣的朋友可以了解下

**需求如下**

导出表的结构，和字段备注信息，表名等。不需要借用第三方工具即可实现。

```sql
select
 table_name 表名,
 column_name 列名,
 column_type 数据类型,
 data_type 字段类型,
 character_maximum_length 长度,
 is_nullable 是否为空,
 column_default 默认值,
 column_comment 备注
from
 information_schema.columns
where
-- developerclub为数据库名称，到时候只需要修改成你要导出表结构的数据库即可
table_schema ='developerclub'
```

![1G344E27-0](C:\Users\senyint\Desktop\1G344E27-0.png)

**效果如下**

![1G344CN-1](C:\Users\senyint\Desktop\1G344CN-1.jpg)

我这里懒得一个个改了，效果已经达到。

![1G3441I5-2](C:\Users\senyint\Desktop\1G3441I5-2.jpg)

以上就是mysql如何将表结构导出到excel的详细内容，更多关于mysql表结构导出到excel的资料请关注服务器之家其它相关文章！





## Docker进入Mysql容器命令：

1. ```shell
   [root@bogon canal]# docker ps
   CONTAINER ID   IMAGE                                COMMAND                  CREATED       STATUS                             PORTS                              NAMES
   c205bcee1f4c   mysql:5.5                            "docker-entrypoint.s…"   5 weeks ago   Up 14 minutes                      0.0.0.0:3306->3306/tcp             opt_mysql_1
   
   
   [root@bogon canal]# docker exec -it c205bcee1f4c bash
   root@c205bcee1f4c:/# mysql -uroot -p
   Enter password: 
   Welcome to the MySQL monitor.  Commands end with ; or \g.
   Your MySQL connection id is 1
   Server version: 5.5.62 MySQL Community Server (GPL)
   
   Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.
   
   Oracle is a registered trademark of Oracle Corporation and/or its
   affiliates. Other names may be trademarks of their respective
   owners.
   
   Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
   
   mysql> 
   mysql> 
   mysql> 
   
   ```





## 分布式事务Seata

商城销售积分应用案例

![图片1.png](https://s0.lgstatic.com/i/image6/M01/33/EB/Cgp9HWBv-3GAAY44AAHu9ANH08o023.png)



整体架构图：

![图片2.png](https://s0.lgstatic.com/i/image6/M01/33/F3/CioPOWBv-3qATUoAAAJXS6y4ARs672.png)





## 缓存Redis



​       假设应用程序将原始数据存储在 MySQL 数据库中。众所周知 MySQL 数据库会将数据存储在硬盘以防止掉电丢失，但是受制于硬盘的物理设计，即便是目前性能最好的企业级 SSD 硬盘，也比内存的这种高速设备 IO 层面差一个数量级，**而以淘宝、京东这种电商为代表的互联网应用，都是典型的“读多写少”的场景，因此我们需要在设计上进行数据的读写分离，在数据写入时直接落盘处理，而占比超过 90% 的数据读取操作时则从以 Redis 为代表的内存 NoSQL 数据库提取数据，利用内存的高吞吐瞬间完成数据提取，**这里 Redis 的作用就是我们常说的缓存。





​      对于广域的互联网应用，CDN 几乎是必需的基础设施，它有效解决了带宽集中占用以及数据分发的问题。像 Web 页面中的图片、音视频、CSS、JS 这些静态资源，都可以通过 CDN 服务器就近获取。





​       **在缓存架构设计时，一定要按照由近到远、由快到慢的顺序进行逐级访问。**假设在电商进行商品秒杀活动时，如果没有本地缓存，所有商品、订单、物流的热点数据都保存在 Redis 服务器中，每完成一笔订单，都要额外增加若干次网络通信，网络通信本身就可能由于各种原因存在通信失败的问题。即便是你能保证网络 100% 可用，但 Redis 集群承担了来自所有外部应用的访问压力，一旦突发流量超过 Redis 的负载上限，整体架构便面临崩溃的风险。



​      **因此在 Java 的应用端也要设计多级缓存，我们将进程内缓存与分布式缓存服务结合，有效分摊应用压力。**在 Java 应用层面，只有 EhCache 的缓存不存在时，再去 Redis 分布式缓存获取，如果 Redis 也没有此数据再去数据库查询，**数据查询成功后对 Redis 与 EhCahce 同时进行双写更新。**这样 Java 应用下一次再查询相同数据时便直接从本地 EhCache 缓存提取，不再产生新的网络通信，应用查询性能得到显著提高。



保障缓存一致性

但事无完美，当引入多级缓存后，我们又会遇到缓存数据一致性的挑战，以下图为例：

![图片12.png](https://s0.lgstatic.com/i/image6/M01/37/18/Cgp9HWB1uqGANm21AACYJ7OwlSA834.png)

​      **我们都知道作为数据库写操作，是不通过缓存的。**假设商品服务实例 1 将 1 号商品价格调整为 80 元，这会衍生一个新问题：**如何主动向应用程序推送数据变更的消息来保证它们也能同步更新缓存呢？**

​       相信此时你已经有了答案。没错，我们**需要在当前架构中引入 MQ 消息队列，利用 RocketMQ 的主动推送功能来向其他服务实例以及 Redis 缓存服务发起变更通知。**



![图片13.png](https://s0.lgstatic.com/i/image6/M01/37/20/CioPOWB1uqiAZTFFAADvyhhJIy8978.png)

​                                                                                        通过 RocketMQ 解决保证消息一致性

​        如上图所示，**在商品服务实例 1 对商品调价后，主动向 RocketMQ Broker 发送变更消息，Broker 将变更信息推送至其他实例与 Redis 集群，这些服务实例在收到变更消息后，在缓存中先删除过期缓存，再创建新的数据，以此保证各实例数据一致。**





​          看到这里你会发现，对于缓存来说，并没有终极的解决方案。**虽然多级缓存设计带来了更好的应用性能，但也为了缓存一致性必须引入 MQ 增加了架构的复杂度。**那到底多级缓存设计该如何取舍呢？在我看来，有三种情况特别适合引入多级缓存。



- 第一种情况，缓存的数据是稳定的。例如邮政编码、地域区块、归档的历史数据这些信息适合通过多级缓存减小 Redis 与数据库的压力。

- 第二种情况，瞬时可能会产生极高并发的场景。例如春运购票、双 11 零点秒杀、股市开盘交易等，瞬间的流量洪峰可能击穿 Redis 缓存，产生流量雪崩。这时利用预热的进程内缓存分摊流量，减少后端压力是非常有必要的。

- 第三种情况，一定程度上允许数据不一致。例如某博客平台中你修改了自我介绍这样的非关键信息，此时在应用集群中其他节点缓存不一致也并不会带来严重影响，对于这种情况我们采用T+1的方式在日终处理时保证缓存最终一致就可以了。







## 单体改造成为服务注意点：

- ### 严禁 Big Bang（一步到位）；

- 尽早体现价值；

- 优先分离做前后端；

- 新功能构建成微服务；

- 利用 Spring AOP 开发低侵入的胶水代码；

- 基于 MQ 构建反腐层。





### 为什么要把单体拆分成微服务？

​       开始之前，我们先来分析下为什么要进行微服务化改造。**在项目发展初期或者规模不大的时候，架构师和程序员主要关注是如何快速交付商业价值，很多项目开始时并没有经过架构层面的精细打磨，也没有考虑架构的延展性。**在紧迫的工期下，所有人员都在关注如何尽快实现业务代码，这是在中国乃至全世界中小型软件公司的通病。在这种背景下，大量粗糙的单体式、伪分布式的应用程序被开发出来，这些软件模块间的调用关系盘根错节，**在长年累月的更新迭代中，代码变得臃肿不堪，任何一个微小的改动都可能“牵一发动全身”，**甚至公司内没有一个人可以梳理清模块间的调用关系，这种软件产品对于整个公司都是一场噩梦。





### 单体在拆分成微服务遇到的新问题？

- 改造是一步到位还是逐渐迭代？

- 微服务拆分的粒度是什么？

- 如何保证数据一致性？

- 新老交替过程中如何不影响公司业务进展？





### 什么是绞杀者应用程序？

​       **所谓绞杀者应用程序的想法来自绞杀式藤蔓，这些藤蔓在雨林中生长，他们围绕树木生成，甚至有时会杀死树木。绞杀者应用程序是一个由微服务组成的新应用程序，通过将新功能作为服务，并逐渐从单体应用中提取服务来实现。**随着时间的推移，越来越多单体应用内的功能**被逐渐剥离为独立的微服务**，最终达到消灭单体应用的目的。**绞杀者应用程序最大的好处是，升级改造过程中并不需要推翻原有的代码，而是在新老更迭的过程中一步步完成微服务架构的升级改造。**

![图片2.png](https://s0.lgstatic.com/i/image6/M01/37/94/Cgp9HWB39gKAczsuAAGd5m0vEdU064.png)

### 绞杀者应用的优缺点？

​     **绞杀者应用的重构过程往往需要数月乃至数年，**我以前在中国顶级的普惠金融机构从事架构设计工作，这家机构的普惠金融核心业务线从立项重构到完成绞杀历经 28 个月，从基本的用户管理到高级的定价模型都遵循相同的策略一步步处理，过程中也保证了公司业务的正常开展。





### 重要的改造策略有哪些？

- 严禁 Big Bang（一步到位）

  > 正确的做法是逐步重构你的单体应用，采用绞杀者应用策略，将应用变为单体与微服务的混合状态，随着时间增加一点点蚕食掉单体应用。

- 尽早地体现价值

  > 逐步重构微服务的一个重要好处是立即获得投资回报
  >
  > **所以在排期时应按价值的重要性进行排序，优先解决公司的痛点，尽快体现出你们的工作成果。**

- 优先分离做前后端

  > 在实施重构改造时，优先要完成应用与业务逻辑的分离

  ​       在原本单体应用中，基于经典的分层理论将程序分为四层：表示层、控制层、业务逻辑层、数据访问层。前面两层表示层与控制层，我们拆解为应用前端，业务逻辑与数据访问拆解为服务后端。应用前端与服务后端在物理上进行切割，中间采用 RESTful API 进行通信，应用前端的职责就是负责与用户交互，服务后端只暴露细粒度的 RESTful API 提供业务处理接口。

  **好处：**

  - 使得前后端独立部署、扩展与维护，尤其是表示层在快速迭代部署时并不影响后端功能，可以轻松进行 A/B 测试

  - 后端分离后采用 RESTful 方式暴露接口，这与微服务的设计要求是一致的，这位未来的微服务剥离工作打下良好基础。

    ​                                                                                              **前后端分离策略**

    ![图片3.png](https://s0.lgstatic.com/i/image6/M01/37/9C/CioPOWB39g6AY7B5AAG9fyjusXo878.png)

- 新功能构建成微服务

  > 在系统改造的过程中，业务部门也会提出许多全新的需求，对于这些新需求我们首先要做的是将其剥离成新的微服务，以此遏制老系统的野蛮生长。

  **案例**：业务部门提出新要求，希望参考京东商城提供多维度、条件丰富的商品查询系统，来替代原本简陋的关键字查询。

  对于这种新功能，在改造过程中首先要将其构建为新的“产品检索”微服务，而不应再为单体应用添加代码。如图所示：

​       ![图片5.png](https://s0.lgstatic.com/i/image6/M01/37/94/Cgp9HWB39iSASdBQAAKM5Xy28YM076.png)

**重要原则：**数据源不允许混用

​      商品数据保存在 MySQL 数据库，但绝不允许让微服务直接访问 MySQL 的数据，因为在未来的很长时间，单体应用与微服务是混合运行的，如果出现数据源的交叉访问，稍有不注意便会出现数据问题，因此两端的数据源应完全隔离。正确的做法是引入 Alibaba Canal 做数据源同步，Canal 是阿里巴巴旗下的开源项目，纯 Java 开发。基于数据库增量日志解析，提供增量数据订阅&消费，可自动实现从 MySQL 数据源向其他数据源同步数据的任务。

![图片6.png](https://s0.lgstatic.com/i/image6/M01/37/94/Cgp9HWB39i6AX9z9AANA-aFaAl8976.png)

​      前面我们将全新功能单独构建为微服务，在网关层面进行 URL 的转发，但这种情况太过理想，毕竟更多的情况是在原有单体代码中，剥离一部分成为独立的微服务，在这个过程中既要减少对原始代码的修改，又要实现微服务的远程调用。

​	Spring AOP 称为面向切面编程，Spring 框架底层通过 Java 动态代理或者 CGLib 技术，允许 Java 程序在运行时自动生成代理类，实现在不修改源码的前提下对应用进行动态拦截与扩展。

**案例：**以京东的 iPhone11 为例，这个页面的数据其实来自多张数据表，**商品标题来自商品基础信息表，而价格信息则来自活动价格表。**商品的基础信息相对稳定，**而价格数据随着时间在不断变化。**

**单体实现伪代码：**可以看到在单体应用时，所有的调用都在 JVM 进程内完成。

```java
@Service
public class GoodsService{

    @Resource
    private GoodsDao goodsDao;

    @Resource
    private PriceService priceService; //定价服务类
    
    public Map selectGoods(Long skuId){
        Goods goods = goodsDao.findById(skuId);//从本地查询商品基本信息
        Price price = priceService.findByGoodsId(skuId);//在定价服务查询商品定价
        ... //组织Map对象省略
    }
}

```

​       但是随着业务发展，定价表数据量越来越大，业务逻辑也越发复杂，我们希望剥离出独立的“定价服务”，将原本进程内调用变为 RESTful 远程通信，还要对原始代码尽可能少做修改。

![图片8.png](https://s0.lgstatic.com/i/image6/M01/37/94/Cgp9HWB39kaAahWJAAM5QR3NV-I669.png)



如果你了解 **Spring AOP**，便不难想到利用 **Around 环绕通知便可轻松实现从本地调用到远程访问的修改。**这里只需额外定义一个切面类，伪代码如下：

复制代码

```java
@Component("priceServiceAspect") //声明Bean Id
@Aspect //定义切面类
public class PriceServiceAspect{
    @Resource
    private PriceServiceFeignClient priceServiceFeignClient;
    //利用环绕通知实现对PriceService.findByGoodsId的动态代理
    @Around("execution(* com.lagou..PriceService.findByGoodsId(..)")
    public Object selectGoods(ProceedingJoinPoint joinPoint){
        //通过OpenFeign客户端向定价服务发起远程请求，替代JVM本地访问
        return priceServiceFeignClient.selectGoods((Long)joinPoint.getArgs()[0]);
    }
}
```

​       在上面的伪代码片段中，在 selectGoods 调用 PriceService.findByGoodsId 方法时，会自动进入  PriceServiceAspect 切面类，该切面类会拦截 PriceService.findByGoodsId  方法的执行，不再进行本地调用，而是通过 Spring Cloud OpenFeign 客户端向定价服务发起 RESTful  请求并得到定价结果。因为 Spring AOP 是无侵入的，所以对于原本的 GoodsService 代码无须做任何调整就可将  PriceService.findByGoodsId 方法转为远程访问。

**以上数据查询时利用 Spring AOP 实现零侵入改造，对于这种只读的查询操作改造是非常轻松的.**



**弊端：**事务处理问题

**以新增商品为例，在单体应用时利用进程内事务便可保证数据一致性，**例如：

```
开启事务;
新增商品基础数据;
新增商品价格数据;
提交事务;
```

​          当我们将"定价服务"剥离为独立的服务后，**因为跨进程调用会导致原本进程内事务失效，这就强制要求引入分布式事务来保证数据的一致性，**尽管前面我们学习过 Seata 的 AT 模式可以在较少修改的前提下自动实现分布式事务，但这也不可避免的要求额外部署 Seata-Server  集群，这也必然导致架构复杂度的增加。





**如何处理架构复杂度的增加问题？**

其实我们变通一下，在开始切分微服务时，在不确定外界依赖的情况下可以将微服务粒度做的粗一些，极力避免分布式事务的产生就可以了。



​      **如当前案例，商品信息本身是内聚的，价格只是商品实体的一个属性，因此在设计之初我们可以剥离出粗粒度的“商品服务”，将商品管理与定价策略内聚合在一起，就可以避免分布式事务。**



![图片9.png](https://s0.lgstatic.com/i/image6/M01/37/94/Cgp9HWB39liAKIV_AAKjF5cQ3KA462.png)



​                                                                                                                                   **粗粒度切分策略**





- 基于 MQ 构建反腐层

![图片10.png](https://s0.lgstatic.com/i/image6/M01/37/9C/CioPOWB39mCAMrMRAAIqG54rPHQ175.png)

​                                                                                                        **构建反腐层实现应用隔离**



​        随着改造的持续进行，**我们在单体应用中额外增加了大量 Spring AOP 切面类，这样做虽然对原始代码改动较小，但基于 OpenFeign  直接面向微服务调用本身就破坏了单体应用与微服务间的隔离原则，这也是需要极力避免的。**因此我们可以再进一步，**将 OpenFeign 的  RESTful 调用改为利用 MQ 实现消息的“发布/订阅”，让单体应用与微服务持续解耦。这个引入 MQ  中间层的解耦策略，在微服务改造中被称为“反腐层”。通过反腐层，服务后端无须关注具体是哪个微服务实例消费数据，OpenFeign  也不再越界访问微服务，同时因为 MQ 自带特性，还赋予了应用消息追溯、流控等额外特性，可谓一举多得。**



> ​        **微服务重构改造的六条改造策略，**分别是：***严禁 Big Bang、尽早且频繁的体现价值、优先分离做前后端、新功能构建成微服务、利用 Spring AOP 开发低侵入的胶水代码、基于 MQ 构建反腐层***。





## JWT

### JWT加密字符串组成？

#### 第一部分 标头（Header）：

标头通常由两部分组成：令牌的类型（即 JWT）和所使用的签名算法，例如 HMAC SHA256 或 RSA，下面是标头的原文：

```
{
  "alg": "HS256",
  "typ": "JWT"
}
```

然后，此 JSON 被 Base64 编码以形成 JWT 的第一部分。

```json
eyJhbGciOiJIUzI1NiJ9
```

#### 第二部分 载荷（Payload）：

载荷就是实际的用户数据以及其他自定义数据。载荷原文如下所示。

```
{
  "sub": "1234567890",
  "name": "John Doe",
  "admin": true
}
```

然后对原文进行 Base64 编码形成 JWT 的第二部分。

#### 第三部分 签名（Sign）：

签名就是通过前面两部分标头+载荷+私钥再配合指定的算法，生成用于校验 JWT 是否有效的特殊字符串，签名的生成规则如下。

```
HMACSHA256(base64UrlEncode(header) + "." +  base64UrlEncode(payload),  secret)
```

生成的签名字符串为：

```
NT8QBdoK4S-PbnhS0msJAqL0FG2aruvlsBSyG226HiU
```



### JWT 的创建与校验

JJWT 是一个提供端到端的 JWT 创建和验证的 Java 库，它的官网是：[https://github.com/jwtk/jjwt](https://github.com/jwtk/jjwt?fileGuid=xxQTRXtVcqtHK6j8)







## DevOps执行流程

讲到这里，我们理解了基于容器化几个重要的概念。下面咱们来分析一下某知名大厂是如何实施 DevOps。

![图片8.png](https://s0.lgstatic.com/i/image6/M01/3A/BE/Cgp9HWCBERCAZboKAAMeGgoT_3c748.png)

### DevOps 流程

**第一步**，研发工程师将测试验收后的源码上传到 GitLab 服务器，并创建新的版本分支。GitLab  是用于管理代码版本的 Web 开源项目，使用 Git 作为代码管理工具，你可以理解为内网的 GitHub  仓库。公司出于保密原因其源码不允许将其托管到 GitHub 这样的开放平台，因此才需要搭建独立的 GitLab  仓库。当然创业型公司也可以出于成本考虑将代码托管到 GitHub 或者 Gitee 上。

**第二步，**在 GitLab 创建新的版本分支后，研发工程师还需要创建 Dockerfile 来描述 Docker 镜像的构建过程，之后将 Dockerfile 也上传到该分支下。

**第三步，**在 GitLab 中新版本源码与 Dockerfile 都已上传，由软件工程师或者配置管理员发起 Jekins 的自动化脚本完成镜像的自动化构建与仓库推送，这个自动化脚本包含3个步骤：

1. 抽取新版本源码到 Jekins 服务器，利用 Jekins 服务器安装 Maven 自动完成编译、测试、打包的过程，因为微服务采用 SpringBoot 开发，最终产出物为Jar 文件。
2. 抽取新版本 Dockerfile 到 Jekins 服务器，利用 Jekins 服务器安装的 Docker 完成镜像的构建工作，在构建过程中需要将上一步生成的 Jar 文件包含在内，在容器创建时自动执行这个 Jar 文件。
3. 镜像生成后，还是通过 Jekins 服务器上的 Docker 将新版本镜像推送到 HARBOR  仓库。HARBOR 用于创建 Docker 镜像的私有仓库，公司从保密角度出发要求自己搭建内网 Docker 镜像仓库，创业公司可以直接使用  Dockerhub 或国内 Docker 开放镜像平台进行托管。当新版本镜像推送到镜像仓库后，软件工程师的任务已完成，之后便是等待上线发版了。

**第四步，**在上线日运维工程师接入 Kubernetes 管理端，发起 Deploy  部署命令，此时生产环境的 K8S 节点会从 HARBOR 仓库抽取最新版本的应用镜像，并在服务器上自动创建容器，最新版本的 Jar  文件在容器创建时也会随之启动开始对外提供服务。在校验无误后，本次上线宣告成功。

讲到这里我为你勾勒出 DevOps 的执行流程，当然真实环境比本文介绍的过程复杂得多，还要考虑多种异常因素，例如：

- 源码编译、打包时产生异常的快速应对机制
- 上线失败如何快速应用回滚
- 镜像构建失败的异常跟踪与补救措施
- ……









CDN 技术的核心是“智能 DNS”，智能 DNS 会根据用户的 IP 地址自动确定就近访问 CDN 节点，咱们以下图为例：

![图片5.png](https://s0.lgstatic.com/i/image6/M01/37/20/CioPOWB1ukeAFt5mAADBdyaj6Hs257.png)

CDN 执行流程









**GitHub开源项目：**

开源项目名称：GMall

> https://github.com/18391713434/GMall

项目剖析：





开源项目名称：      **[J2EEFAST](https://github.com/zhouhuan751312/J2EEFAST)**  

> https://github.com/zhouhuan751312/J2EEFAST

项目剖析：



开源项目名称：renren-generator

> https://gitee.com/renrenio/renren-generator

项目介绍：java代码自动生成





## 【高效开发】



### java代码生成平台：

> https://java.bejson.com/generator/



## 答题技巧

### 遇到问题思考3步走

#### 1、出现的问题是什么？

#### 2、哪些情况下可能会出现问题

#### 3、如何去解决问题







## 架构设计题

### 你之前是如何设计这个系统（或子系统/模块/功能）的？请介绍你的思路。

#### 案例背景：

​       在电商中，当用户发表一条商品评论，后台的逻辑是点评系统会调用一系列的远程 API 接口，如调用风控系统、广告系统、消息系统……几个甚至十几个系统的接口。

#### 前期解决方案：

​       在业务建设之初，考虑到快速开发与上线，商品评论发布是通过同步 RPC（Remote Procedure Call，远程过程调用）远程调用各系统接口完成的。这种方式在系统少、逻辑简单的阶段很符合实际情况的设计。

#### 前期解决方案存在的问题

> ​    但随着业务快速发展，通过 RPC 同步调用的问题逐渐暴露出来。由于过多地依赖其他系统，导致评论发布的接口性能很低，可用性也容易受到其他系统影响。而且每当点评系统需求上线时，其他系统都需要跟着进行联调测试，导致需求迭代速度缓慢。

![3.png](https://s0.lgstatic.com/i/image2/M01/05/45/CgpVE1_-d-aAaAdUAAE0zFMFmxg960.png)



#### 解决思路：

​      在做系统架构升级改造时，如果你有互联网设计理念，会很容易想到**问题在于系统间的耦合度太高。**解决办法就是**采用异步化解耦，从而通过引入 MQ 消息管道，在架构上进行系统业务逻辑拆分，将原本强依赖的系统间的同步 RPC 调用变成异步消息触发**，如下面图片中的架构所示：

![4.png](https://s0.lgstatic.com/i/image2/M01/05/45/CgpVE1_-fh6AOb3SAAELEzS8dt8480.png)



#### 案例分析：

> 当被问“如何做这个点评系统的改造？”时，你会怎么回答？

**不专业回答**：我引入了 MQ 消息队列，做了系统解耦，采用异步消息通知的方式来触发系统调用

面对此类问题，我总结了如下四个层面的答案：

- 谈复杂来源；

  > **之所以要先分析系统的复杂度，是因为只有正确分析后才能明确设计原则，进而设计架构方案，整体项目才不会找错方向。**
  >
  > ​       如果一个系统本来因业务逻辑复杂导致功能耦合严重，你却设计了一个 TPS（Transactions Per Second，每秒事务处理量）达到 10000/秒 的高性能架构，那么即使架构性能再优秀，也没有现实意义，因为技术设计没有解决主要问题的复杂度。

  那么如何正确评估系统的复杂度呢？

​      ![5.png](https://s0.lgstatic.com/i/image/M00/8D/67/CgqCHl_-eVaAcKLPAAE9AeK1Y8k819.png)

​                                                                                                                复杂度评估



**从功能性复杂度方面来看**

​       你可以从案例中得知，产品业务发展快速、系统越来越多、协作效率越来越低。作为系统负责人，你敏锐地发现问题根源在架构上各业务子系统强耦合。于是你引入消息队列解耦各系统，这是系统业务领域带来的本质上的复杂度，也就是功能性的复杂度，解决的是系统效率的问题。

​       此外，对于互联网系统设计，还需要考虑非功能性的复杂度，例如高性能、高可用和扩展性等的复杂度的设计。



**从非功能性复杂度方面来看**

​       我们假设系统用户每天发送 100 万条点评，那么点评的消息管道一天会产生 100 万条消息，再假设平均一条消息有 10 个子系统读取，那么每秒的处理数据，即点评消息队列系统的 TPS 和 QPS（Queries Per Second，每秒查询次数）就分别是 11（1000000/60*60*24）和 115（10000000/60*60*24）。



​      不过系统的读写不是完全平均的，设计的目标应该以峰值来计算，即取平均值的 4 倍。于是点评消息队列系统的 TPS 变成了 44，QPS 变成了 460，这个量级的数据意味着并不需要设计高性能架构方案。

​      接着还要考虑**业务规模发展。**架构设计的目标**应该满足未来业务增长，**我们把未来业务增长的预估峰值设定为目前峰值的 4 倍，这样最终的性能要求分别是：TPS 为 176，QPS 是 1840。这样的读写指标还达不到系统压测的性能基线，所以可以确定的是点评系统的复杂度并不在高性能问题上。

​      对于点评系统来说，还需要考虑**高可用**的问题。假设点评系统的消息队列挂掉，将导致用户评论发送失败，当然在用户体验层面，解决方式可以在页面端提示用户重新操作，但如果问题影响到了点评消息的读取，导致评论没有走风控策略，就会造成严重的影响。所以高可用性是点评系统的设计复杂度之一，包括点评写入、点评存储，以及点评消息的读取，都需要保证高可用性。

​      为了方便理解非功能性的复杂度，我只分析了“高性能”和“高可用”这两点，在实际应用中，不同的公司或者团队可能还有其他方面的复杂度分析。例如有的公司会考虑安全性，有的公司会考虑成本等。





- 谈解决方案；

> ​       在确定了系统面临的主要复杂度问题后，就有了明确的方案设计目标，这时就可以开始进行架构方案设计了。我同样会结合本文的案例场景，谈谈点评系统消息管道的架构设计解决方案。



**方案:1：采用开源的 MQ 消息管道。**目前 MQ 消息管道有很多开源解决方案，比如 Kafka、RocketMQ、RabbitMQ 等。在实际项目中，你可以根据不同的应用场景选择合适的成熟开源消息队列方案，这是很多公司常用的做法。

**方案2：采用开源的 Redis 实现消息队列。**方案 1 虽然应用了开源 MQ 实现点评消息的通信，但是因为引入一个消息中间件就会带来运维成本，所以方案 2 可以基于轻量级的 Redis 实现，以降低系统的维护成本和实现复杂度。

**方案3：采用内存队列 + MySQL 来实现。**方案 2 中虽然应用了较为轻量级的 Redis 来实现，但是还需要引入一个缓存系统，同样也会带来运维成本，所以方案 3 是直接基于 MySQL 实现，即基于内存队列的方式，异步持久化到数据库，然后通过定时任务读取 MySQL 中的消息并处理。



> 一般情况，你至少要设计两到三套备选方案，考虑通过不同的技术方式来解决问题。方案设计不用过于详细，而是要确定技术的可行性和优缺点。





- 谈评估标准；

> ​        在互联网软件架构中，架构师常常会把一些**通用的设计原则写到设计文档中，比如设计松耦合、系统可监控，**这些原则似乎不常用，但好的架构师会通过设计原则来控制项目的技术风险。**比如系统无单点，限制了系统技术方案不可出现单点服务的设计；再如系统可降级，限制了系统有具备降级的能力，进而约束了开发人员需要设计数据兜底的技术方案。**



​        这些看似不重要的设计原则，其实是评估架构解决方案的重要手段。**做系统架构，需要站在更高的层面考虑系统的全局性关注点，比如性能、可用性、IT 成本、投入资源、实现复杂度、安全性、后续扩展性等。**这在不同场景的不同阶段会起到决定性作用。



**点评系统功能性复杂度**

​      点评系统的功能性复杂度问题，本质上是随着业务发展带来的系统开发效率问题。解决这个问题要试着站得更高一些，以部门负责人的视角，考虑现有研发团队的能力素质、IT 成本、资源投入周期等因素是否匹配上面三种架构解决方案。



**点评系统非功能性复杂度**



>  为了解决系统的高可用，可以参考三个设计原则。

**第一个是系统无单点原则。**首先要保证系统各节点在部署的时候至少是冗余的，没有单点。很显然三种设计方案都支持无单点部署方式，都可以做到高可用。

**第二个是可水平扩展原则。**对于水平扩展，MQ 和 Redis 都具有先天的优势，但内存队列 + MySQL 的方式则需要做分库分表的开发改造，并且还要根据业务提前考虑未来的容量预估。

**第三个是可降级原则。**降级处理是当系统出现故障的时候，为了系统的可用性，选择有损的或者兜底的方式提供服务。

常用手段主要有三种。

    1、限流，即抛弃超出预估流量外的用户。
    
    2、降级，即抛弃部分不重要的功能，让系统提供有损服务，如商品详情页不展示宝贝收藏的数量，以确保核心功能不受影响。
    
    3、熔断，即抛弃对故障系统的调用。一般情况下熔断会伴随着降级处理，比如展示兜底数据。

​        针对案例场景中三个解决方案的降级策略，在一般的情况下，我们默认数据库是不可降级的，MQ 和 Redis 都可以通过降级到数据库的方式做容灾处理。所以案例中的三个解决方案，MQ 和 Redis 要考虑降级到 MySQL 或其他方式，这里就还需要根据情况投入降级的开发成本。





- 说技术实现

​       **在确定了具体的架构解决方案之后，需要进一步说明技术上的落地实现方式和深层原理，如果你最终选择基于 Redis 来实现消息队列，那么可以有几种实现方式？各自的优缺点有哪些？对于这些问题，要做到心里有数。**比如，基于 Redis List 的 LPUSH 和 RPOP 的实现方式、基于 Redis 的订阅或发布模式，或者基于 Redis 的有序集合（Sorted Set）的实现方式。



#### 总结

最后，我把今天的**“四步回答法”**做个总结，加深每一步你需要掌握的注意点。

    1、在回答系统复杂度来源的时候，要注意结合具体的业务场景和业务发展阶段来阐述。业务场景表明了业务的独特性，发展阶段表明了业务的成熟度，因为同一业务场景在不同阶段产生的矛盾也是不同的。
    
    2、在回答解决方案的时候，有价值的解决方案一定是建立在明确复杂度来源基础之上的。所以在设计架构的时候才分主要问题和次要问题，主要问题是必须要解决的点，次要问题可以根据实际情况进行取舍。
    
    3、在回答如何评估架构方案时，至少要从功能性和非功能性两个角度出发判断方案的合理性。对于很难决策的方案，要从更高的视角（比如技术负责人、业务负责人的角度）进行考量。
    
    4、在技术实现的细节上，要尽量讲出技术的实现原理，不要浮于表面的框架组合。



​       到这里，我们已经知道了如何用架构师的视角进行技术面试，那么你现在理解什么是架构师视角了吗？**其实简单一句话，所谓的架构师视角就是全局的视角，这里的全局包括空间全局和时间全局，在空间全局上你要看到整个系统的领域边界，在时间全局上你要看到整个系统的发展周期。**



#### 精选留言：

![image-20210511105350615](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210511105350615.png)



![image-20210511105431509](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210511105431509.png)



![image-20210511105521087](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210511105521087.png)





## 对架构设计的认知

> 关于架构设计的问题，一定要立足于点、连接成线、扩散成面



### 例子：

> ​       我曾面试过一名研发工程师，他在介绍过往经历时，称自己在重构一个负责交易流程的系统时，将其拆分成报价系统、促销系统，以及订单系统，而当时他们只有两个人负责交易系统的开发工作。
>
> 针对他的经历，我的问题是：**你们只有两个人负责这个交易系统，为什么还要做系统架构拆分？而且拆分之后会带来其他的复杂度，你是怎么考虑的？**



1、**从订单系统层面来看**，由于交易流程中的订单系统相对来说业务稳定，不存在很多的迭代需求，如果耦合到整个交易系统中，在其他功能发布上线的时候会影响订单系统，比如订单中心的稳定性。基于这样的考虑，需要拆分出一个独立的子系统。

2、**从促销系统层面来看**，由于促销系统是交易流程中的非核心系统，出于保障交易流程稳定性的考虑，将促销系统单独拆分出来，在发生异常的时候能让促销系统具有可降级的能力。

3、**从报价系统层面来看**，报价是业务交易流程中最为复杂和灵活的系统，出于专业化和快速迭代的考虑，拆分出一个独立的报价系统，目的就是为了快速响应需求的变化。

4、**从复杂度评估层面来看**，系统拆分虽然会导致系统交互更加复杂，但在规范了 API 的格式定义和调用方式后，系统的复杂度可以维持在可控的范围内。



### 案例分析

​        **如果你是这名应聘者，会怎么回答呢？**很多研发同学一提到架构设计就说要做拆分，将一个系统拆分成两个系统，将一个服务拆分成两个服务，甚至觉得架构就是做系统拆分，但其实并不理解拆分背后的深层原因，所以往往只能回答得比较表面，无法深入背后的底层设计逻辑，那这个问题的底层逻辑到底是什么呢？有这样四点。

**为什么做架构拆分？**通常最直接目的就是做系统之间解耦、子系统之间解耦，或模块之间的解耦。

**为什么要做系统解耦？**系统解耦后，使得原本错综复杂的调用逻辑能有序地分布到各个独立的系统中，从而使得拆封后的各个系统职责更单一，功能更为内聚。

**为什么要做职责单一？**因为职责单一的系统功能逻辑的迭代速度会更快，会提高研发团队响应业务需求的速度，也就是提高了团队的开发效率。

**为什么要关注开发效率？**研发迭代效率的提升是任何一家公司在业务发展期间都最为关注的问题，所以从某种程度上看，架构拆分是系统提效最直接的手段。



## 对分析问题的认知

​       在实际工作中，技术人员在做系统设计时需要与公司或部门的战略定位对齐，才能让你的技术有价值。因为对于系统技术架构升级的问题，业务方、管理者和技术人员的关注点是不同的。

1、业务方的诉求是在技术升级后，系统有能力迭代功能来满足市场的要求，所以**关注点在系统能力。**
2、管理者的诉求是在技术升级后，系统研发团队的开发效能得到提升，所以**关注点在人效管理。**
3、作为技术人员的你，需要找到自己做系统设计的立足点，来满足不同人对技术的诉求，而这个立足点通常就是**系统设计原则。**



> 所以你应该认识到，系统的设计原则不是乱提出来的，而是**针对系统现阶段业务发展带来的主要矛盾提出，才会更有价值且被认可。**



### 例子

之前我做过一个对原有老系统进行架构改造的系统设计，当时的背景是这样的。

​      早期，业务发展比较简单，团队规模也不是很大，单体系统可以支撑业务的早期规模，但当业务不断发展，团队规模越来越大时，之前的一个业务团队逐渐发展成了多个业务团队，这时每个业务团队都会提出自己的功能需求。

​      然而，系统现状仍然是单体架构，研发同学都在同一个系统里进行开发，使得系统逻辑复杂，代码耦合，功能迭代和交付变得非常缓慢，牵一发而动全身，研发同学都不敢轻易修改代码。

​      这个时期系统的主要矛盾就变成了：多人协作进行复杂业务，导致速度缓慢，但业务需求又快速迭代。说白了，就是**研发效率不能匹配业务发展的速度，并且单靠加人不能解决问题。**



> 对于这样的一个系统，此阶段的系统架构核心原则就不能随便定义为要保证高性能和高可用

​      那么应该怎么做呢？**针对这样的问题，我们需要对原有系统进行合理的系统边界拆分，让研发人员有能力提速，来快速响应需求变化，这就要求架构师对业务领域和团队人员有足够的了解。**

​      类似这样的情况也是面试中经常出现的考题，比如面试官在问你历史项目经历的时候，**要重点关注你是如何解决系统核心问题的，**所以不要一张口就是高性能、高可用，这会让有经验的面试官觉得你很初级。



### 案例分析

​       面试中，研发人员在回答系统设计问题的时候，要根据系统所处阶段的主要矛盾来回答架构设计问题，在 20 世纪 60 年代，《人月神话》的作者就分析，软件复杂性来源于两点：本质复杂度和偶然复杂度。开发工具、开发框架、开发模式，以及高性能和高可用这些仅是偶然复杂性，**架构最重要的是要解决本质复杂性，这包括人的复杂性和业务的复杂性。**



> ​      技术是静态的，业务和用户是变化的，具体问题要从具体的业务领域出发。这时有人可能会说，我只想做技术，不想做业务，然而你会慢慢发现，在职业生涯中处理的最有价值的事情，一般都是利用技术解决了业务领域的某阶段的主要问题，这也是最复杂的。



## 对能力边界的认知

​       我在做研发的晋升评审时，常常会问候选人一个问题：**你觉得一个高级研发工程师和一个架构师的区别在哪？** 这个问题很多研发同学回答得都不是很好，有些人说需要足够的技术经验，懂得高性能、高可用，也有些人说需要懂得管理，带过团队。



### 例子

我们先来看一下互联网一些大厂的中高级研发工程师晋升架构师的标准，如下图所示：

![Drawing 1.png](https://s0.lgstatic.com/i/image/M00/8C/0F/CgqCHl_kU2uACOFsAABBxkQJo6I393.png)





可以看出，晋升架构师需要掌握架构知识体系以及互联网的设计经验。

​      那么是不是可以这么理解：想要成为架构师，需要在掌握原有技术框架原理与开发基础之上，再懂得分布式高性能、高可用的设计知识，这样就可以了？如果你真是这么认为的，那就存在一个技术认知的问题。



​       其实不然，**掌握互联网架构设计中的高性能、高可用、高扩展这些非功能性设计方案是基础，但还要看你是站在哪个角色上考虑的。**互联网大厂职级体系晋升的一个很重要规则，就是你所做的事情的边界，所能影响到的范围。

比如，研发工程师和架构师能驾驭的边界可以如下概括：

    1、一个中高级研发工程师对系统的驾驭边界至少是模块或者子系统层面；
    
    2、一个架构师对系统的驾驭边界至少是全系统层面；
    
    3、一个高级架构师对系统的驾驭边界至少是某一领域层面。



### 案例分析

​       我们常说，屁股决定脑袋，不在那个位置就不会真正体会到那个位置带来的问题。**没有触达多系统层面的设计，就不会掌握多系统层面带来的复杂度和解决问题的思考逻辑。**但是往往研发同学意识不到这样的问题存在，即便能碰到一个通盘考虑架构设计的机会，但价值、眼界、认知的形成，也不是一朝一夕的事儿。

​       那么，你要怎么做才能让自己更快速地成长呢？**你要在工作中养成归纳总结的习惯，形成自己的知识体系，沉淀自己的方法论，提高自己的认知能力，并且跳出舒适区，多争取扩展自己能驾驭系统的边界的机会。**

​      在接下来的课程中，我会基于架构设计面试考题的角度，在解决方案、原理理解、实践经验，以及知识体系和认知能力等方面，帮你提高应对架构设计问题的能力。



### 总结

我在今天的课程中，通过三个案例为你讲解了研发工程师在面试中如何提高竞争力，可以总结为三点。

    1、首先要提高你对系统架构设计的认知能力，一个好的架构师的架构设计不是仅仅停留在技术解决方案上。
    
    2、其次要提高你分析系统问题的认知能力，做架构设计要具备根据现阶段的主要矛盾来分析问题的能力。
    
    3、最后你要扩大自己能够驾驭系统的边界，因为只有这样才能遇到之前没经历过的问题层次，注意我这里说的是问题层次，而不是问题数量。



### 留言

![image-20210511163144389](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210511163144389.png)









![image-20210511163210920](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210511163210920.png)





## 那么在实际项目中，你如何通过 BASE 理论来指导设计实践呢？

BASE 中的基本可用指的是保障核心功能的基本可用，其实是做了“可用性”方面的妥协，比如：

    1、电商网站在双十一大促等访问压力较大的时候，关闭商品排行榜等次要功能的展示，从而保证商品交易主流程的可用性，这也是我们常说的服务降级；
    
    2、为了错开双十一高峰期，电商网站会将预售商品的支付时间延后十到二十分钟，这就是流量削峰；
    
    3、在你抢购商品的时候，往往会在队列中等待处理，这也是常用的延迟队列。



**软状态和最终一致性指的是允许系统中的数据存在中间状态，这同样是为了系统可用性而牺牲一段时间窗内的数据一致性，从而保证最终的数据一致性的做法。**

​       目前这种处理数据的方式几乎成了互联网的标配设计模式，**最经典的例子是在用户下单的时候不需要真正地扣减库存，而是仅在前台计个数，然后通过异步任务在后台批量处理。**



**分布式系统就像一个网络计算机，它的知识体系包括四个角度：**

    1、存储器，即分布式存储系统，如 NoSQL 数据库存储；
    
    2、运算器，即分布式计算，如分布式并行计算；
    
    3、输入输出，即分布式系统通信，如同步 RPC 调用和异步消息队列；
    
    4、控制器，即调度管理，如流量调度、任务调度与资源调度。





### 总结

CAP 理论看似简单，但在面试中，对它的理解深度可以从侧面反映出你对分布式系统的整体理解能力和驾驭能力。

所以**你不但要掌握如何在面试中回答案例中 CAP 原理的问题，而且还要掌握回答问题的思路，**以后遇到类似的理论性知识的考察，都可以从三个层面回答。

    1、展示理论深度。你可以从一个熟知的知识点出发，深入浅出地回答，比如它的工作原理、优劣势、适用场景等。
    
    2、结合落地经验。你不能仅停留在理论理解，还要结合落地方案的技术实现，这样才能体现你的技术闭环思维。
    
    3、展示知识体系，这是任何一个程序员向上发展的基础能力。理论深度和落地经验体现了作为程序员的基本素质，而知识体系和技术判断力则体现了你是否达到架构师的能力边界。



> **技术人，要有落于地的能力，还要有浮上天的本**





## 





## 如何在面试中展现出“造轮子”的能力？ 

### 例子：

​       在电商 App 商品详情页中，用户每次刷新页面时，App 都会请求业务网关系统，并由网关系统远程调用多个下游服务（比如商品服务、促销服务、广告服务等）。

针对这个场景，面试官会问**“对于整条 RPC 调用链路（从 App 到网关再到各个服务系统），怎么设置 RPC 的超时时间，要考虑哪些问题？”**

![Lark20210115-182949.png](https://s0.lgstatic.com/i/image2/M01/05/F4/CgpVE2ABbtSAerROAADrjM6HgkI724.png)

**中级程序员回答：**

> ​       App 远程调用网关系统的超时时间要大于网关系统调用后端各服务的超时时间之和。这样至少能保证在网关与下游服务的每个 PRC 调用执行完成之前不超时。



**如果你这么回答，从“实践”的角度上看，基本是不合格的。**

因为 PRC 接口的超时设置看似简单，但其中却涉及了很多技术层面的问题。比如 RPC 都有超时重传的机制，如果后端服务触发超时重传，这时对 App 来说，也会存在请求等待超时的风险，就会出现后端服务还没来得及做降级处理，商品详情页就已经等待超时了。



**并且在 RPC 调用的过程中也还会涉及其他的技术点，**比如：

    即使考虑到整个调用链的平均响应时长会受到所有依赖服务的耗时和重传次数影响，那么依据什么来设置 RPC 超时时间和重试次数呢？
    
    如果发生超时重传，怎么区分哪些 RPC 服务可重传，哪些不可重传呢？
    
    如果请求超过了 PRC 的重传次数，一般会触发服务降级，这又会对商品详情页造成什么影响？
    
    ......

总的来说，任何一个微服务出现性能问题，都会影响网关系统的平均响应时长，最终对 App 产生影响。



**那具体要怎么回答呢？我建议你参考以下解题思路。**

- 结合 TP99 请求耗时：首先如果你要回答“超时时间设置和重传次数问题”，需要根据每一个微服务 TP99 的请求耗时，以及业务场景进行综合衡量。
  
- RPC 调用方式：你要站在业务场景下，讲清楚网关调用各下游服务的串并行方式，服务之间是否存在上下服务依赖。
  
- 分析核心服务：分析出哪些是核心服务，哪些是非核心服务，核心服务是否有备用方案，非核心服务是否有降级策略。

​       总的来讲，**解答“实践操作类面试题”，一定要结合理论和落地实践，要做到即有理也有据，有理表示要有分析问题的能力，有据表示具备落地实战的经验。*很多同学的通病是：回答问题只有方案，没有落地细节，这会让面试官认为你技术不扎实。*





## RPC

### 一次完整的 RPC 流程

​       **因为 RPC 是远程调用，首先会涉及网络通信，** 又因为 RPC 用于业务系统之间的数据交互，要保证数据传输的可靠性，所以它一般默认采用 TCP 来实现网络数据传输。

​       网络传输的数据必须是**二进制数据，**可是在 RPC 框架中，调用方请求的出入参数都是对象，对象不能直接在网络中传输，所以需要提前把对象转成可传输的二进制数据，转换算法还要可逆，**这个过程就叫“序列化”和“反序列化”。**

另外，在网络传输中，RPC 不会把请求参数的所有二进制数据一起发送到服务提供方机器上，而是拆分成好几个数据包（或者把好几个数据包封装成一个数据包），所以服务提供方可能一次获取多个或半个数据包，这也就是网络传输中的粘包和半包问题。**为了解决这个问题，需要提前约定传输数据的格式，即“RPC 协议”。 大多数的协议会分成数据头和消息体：**

    数据头一般用于身份识别，包括协议标识、数据大小、请求类型、序列化类型等信息；
    
    消息体主要是请求的业务参数信息和扩展属性等。

在确定好“ RPC 协议”后，一次完整的 RPC 调用会经过这样几个步骤：

    调用方持续把请求参数对象序列化成二进制数据，经过 TCP 传输到服务提供方；
    
    服务提供方从 TCP 通道里面接收到二进制数据；
    
    根据 RPC 协议，服务提供方将二进制数据分割出不同的请求数据，经过反序列化将二进制数据逆向还原出请求对象，找到对应的实现类，完成真正的方法调用；
    
    然后服务提供方再把执行结果序列化后，回写到对应的 TCP 通道里面；
    
    调用方获取到应答的数据包后，再反序列化成应答对象。

![Lark20210115-183000.png](https://s0.lgstatic.com/i/image/M00/8E/05/Ciqc1GABbyeAWysgAAGQtM8Kx4Q574.png)

​      你应该能发现， RPC 通信流程中的核心组成部分包括了协议、序列化与反序列化，以及网络通信。在了解了 RPC 的调用流程后，我们回到“电商 App”的案例中，先来解答序列化的问题。



### 如何选型序列化方式

RPC 的调用过程会涉及网络数据（二进制数据）的传输，从中延伸的问题是：**如何选型序列化和反序列化方式？**

要想回答这一点，你需要先明确序列化方式，常见的方式有以下几种。

    1、JSON：Key-Value 结构的文本序列化框架，易用且应用最广泛，基于 HTTP 协议的 RPC 框架都会选择 JSON 序列化方式，但它的空间开销很大，在通信时需要更多的内存。
    
    2、Hessian：一种紧凑的二进制序列化框架，在性能和体积上表现比较好。
    
    3、Protobuf：Google 公司的序列化标准，序列化后体积相比 JSON、Hessian 还要小，兼容性也做得不错。



明确“常见的序列化方式”后，你就可以组织回答问题的逻辑了：**考虑时间与空间开销，切勿忽略兼容性。**

​        在大量并发请求下，如果序列化的速度慢，势必会增加请求和响应的时间（时间开销）。另外，如果序列化后的传输数据体积较大，也会使网络吞吐量下降（空间开销）。所以，你要先考虑上述两点才能保证 RPC 框架的整体性能。除此之外，在 RPC 迭代中，常常会因为序列化协议的兼容性问题使 RPC 框架不稳定，比如某个类型为集合类的入参服务调用者不能解析，某个类的一个属性不能正常调用......

​        当然还有**安全性、易用性等指标，**不过并不是 RPC 的关键指标。总的来说，在面试时，你要综合考虑上述因素，总结出常用序列化协议的选型标准，比如首选 Hessian 与 Protobuf，因为它们在时间开销、空间开销、兼容性等关键指标上表现良好。



### 如何提升网络通信性能

​       如何提升 RPC 的网络通信性能，这句话翻译一下就是：**一个 RPC 框架如何选择高性能的网络编程 I/O 模型？**这样一来，和 I/O 模型相关的知识点就是你需要掌握的了。

对于 RPC 网络通信问题，你首先要掌握网络编程中的**五个 I/O 模型：**

    1、同步阻塞 I/O（BIO）
    
    2、同步非阻塞 I/O
    
    3、I/O 多路复用（NIO）
    
    4、信号驱动
    
    5、以及异步 I/O（AIO）

但在实际开发工作，最为常用的是 **BIO 和 NIO**（这两个 I/O 模型也是面试中面试官最常考察候选人的）





​       **BIO 的网络模型中，每当客户端发送一个连接请求给服务端，服务端都会启动一个新的线程去处理客户端连接的读写操作，即每个 Socket 都对应一个独立的线程，客户端 Socket 和服务端工作线程的数量是 1 比 1，这会导致服务器的资源不够用，无法实现高并发下的网络开发。所以 BIO 的网络模型只适用于 Socket 连接不多的场景，无法支撑几十甚至上百万的连接场景。**





**另外，BIO 模型有两处阻塞的地方。**

    服务端阻塞等待客户端发起连接。在第 11 行代码中，通过 serverSocket.accept() 方法服务端等待用户发连接请求过来。
    
    连接成功后，工作线程阻塞读取客户端 Socket 发送数据。在第 27 行代码中，通过 in.readLine() 服务端从网络中读客户端发送过来的数据，这个地方也会阻塞。如果客户端已经和服务端建立了一个连接，但客户端迟迟不发送数据，那么服务端的 readLine() 操作会一直阻塞，造成资源浪费。

以上这些就是 BIO 网络模型的问题所在，总结下来就两点：

    Socket 连接数量受限，不适用于高并发场景；
    
    有两处阻塞，分别是等待用户发起连接，和等待用户发送数据。



**那怎么解决这个问题呢？ **

​		答案是 NIO 网络模型，操作上是用一个线程处理多个连接，使得每一个工作线程都可以处理多个客户端的 Socket 请求，这样工作线程的利用率就能得到提升，所需的工作线程数量也随之减少。此时 NIO 的线程模型就变为 1 个工作线程对应多个客户端 Socket 的请求，这就是所谓的 I/O多路复用。

![Lark20210115-183005.png](https://s0.lgstatic.com/i/image/M00/8E/05/Ciqc1GABbzqAPbdOAAIaibzeawc243.png)





顺着这个思路，我们继续深入思考：**既然服务端的工作线程可以服务于多个客户端的连接请求，那么具体由哪个工作线程服务于哪个客户端请求呢？**

​        这时就需要一个调度者去监控所有的客户端连接，比如当图中的客户端 A 的输入已经准备好后，就由这个调度者去通知服务端的工作线程，告诉它们由工作线程 1 去服务于客户端 A 的请求。这种思路就是 NIO 编程模型的基本原理，调度者就是 Selector 选择器。



> 由此可见，**NIO 比 BIO 提高了服务端工作线程的利用率，并增加了一个调度者，来实现 Socket 连接与 Socket 数据读写之间的分离。**
>
> 在目前主流的 RPC 框架中，广泛使用的也是 I/O 多路复用模型，**Linux 系统中的 select、poll、epoll等系统调用都是 I/O 多路复用的机制。**







## MQ消息队列



### 在使用 MQ 的时候，怎么确保消息 100% 不丢失？

#### 案例背景

​        以京东系统为例，用户在购买商品时，通常会选择用京豆抵扣一部分的金额，在这个过程中，交易服务和京豆服务通过 MQ 消息队列进行通信。在下单时，交易服务发送“扣减账户 X 100 个京豆”的消息给 MQ 消息队列，而京豆服务则在消费端消费这条命令，实现真正的扣减操作。

![1.png](https://s0.lgstatic.com/i/image2/M01/07/69/CgpVE2AICf-AIldlAACI4Qo9Nv4750.png)

那在这个过程中你会遇到什么问题呢？

#### 案例分析

要知道，在互联网面试中，引入 MQ 消息中间件最直接的目的是：**做系统解耦合流量控制，追其根源还是为了解决互联网系统的高可用和高性能问题。**

**系统解耦**：用 MQ 消息队列，可以隔离系统上下游环境变化带来的不稳定因素，比如京豆服务的系统需求无论如何变化，交易服务不用做任何改变，即使当京豆服务出现故障，主交易流程也可以将京豆服务降级，实现交易服务和京豆服务的解耦，做到了系统的高可用。

**流量控制**：遇到秒杀等流量突增的场景，通过 MQ 还可以实现流量的“削峰填谷”的作用，可以根据下游的处理能力自动调节流量。



**引入MQ需要考虑的问题：**

1、引入 MQ 消息中间件实现系统解耦，**会影响系统之间数据传输的一致性。** 我们在 04 讲提到过，在分布式系统中，如果两个节点之间存在数据同步，就会带来数据一致性的问题。同理，在这一讲你要解决的就是：**消息生产端和消息消费端的消息数据一致性问题（也就是如何确保消息不丢失）。**

2、**而引入 MQ 消息中间件解决流量控制，** 会使消费端处理能力不足从而导致**消息积压**，这也是你要解决的问题。



那面对“在使用 MQ 消息队列时，**如何确保消息不丢失”这个问题时，你要怎么回答呢？**首先，你要分析其中有几个考点，比如：

    1、如何知道有消息丢失？
    
    2、哪些环节可能丢消息？
    
    3、如何确保消息不丢失？



​         **候选人在回答时，要先让面试官知道你的分析思路，然后再提供解决方案：** 网络中的数据**传输不可靠，想要解决如何不丢消息的问题，首先要知道哪些环节可能丢消息，以及我们如何知道消息是否丢失了，最后才是解决方案（而不是上来就直接说自己的解决方案）。**就好比“架构设计”“架构”体现了架构师的思考过程，而“设计”才是最后的解决方案，两者缺一不可。





#### 案例解答

> 我们首先来看消息丢失的环节，一条消息从生产到消费完成这个过程，可以划分三个阶段，分别为消息生产阶段，消息存储阶段和消息消费阶段。

![2.png](https://s0.lgstatic.com/i/image2/M01/07/68/Cip5yGAICkGAI7vpAAEcjkYXvaA495.png)



​                                                                                                             消息的生产、存储与消费



- **消息生产阶段：** 从消息被生产出来，然后提交给 MQ 的过程中，只要能正常收到 MQ Broker 的 ack 确认响应，就表示发送成功，所以只要处理好返回值和异常，这个阶段是不会出现消息丢失的。

- **消息存储阶段：** 这个阶段一般会直接交给 MQ 消息中间件来保证，但是你要了解它的原理，比如 Broker 会做副本，保证一条消息至少同步两个节点再返回 ack（这里涉及数据一致性原理，我在 04 讲中已经讲过，在面试中，你可以灵活延伸）。

- **消息消费阶段：** 消费端从 Broker 上拉取消息，只要消费端在收到消息后，不立即发送消费确认给 Broker，而是等到执行完业务逻辑后，再发送消费确认，也能保证消息的不丢失。



​       方案看似万无一失，每个阶段都能保证消息的不丢失，但在分布式系统中，故障不可避免，作为消费生产端，你并不能保证 MQ 是不是弄丢了你的消息，消费者是否消费了你的消息，所以，本着 Design for Failure 的设计原则，你还是需要一种机制，来 Check 消息是否丢失了。



 **紧接着，你还可以向面试官阐述怎么进行消息检测？**

 总体方案解决思路为：**在消息生产端，给每个发出的消息都指定一个全局唯一 ID，或者附加一个连续递增的版本号，然后在消费端做对应的版本校验。**



> ​         具体怎么落地实现呢？**你可以利用拦截器机制。 在生产端发送消息之前，通过拦截器将消息版本号注入消息中（版本号可以采用连续递增的 ID 生成，也可以通过分布式全局唯一 ID生成）。然后在消费端收到消息后，再通过拦截器检测版本号的连续性或消费状态，这样实现的好处是消息检测的代码不会侵入到业务代码中，可以通过单独的任务来定位丢失的消息，做进一步的排查。**



​       这里需要你注意：**如果同时存在多个消息生产端和消息消费端，通过版本号递增的方式就很难实现了，因为不能保证版本号的唯一性，此时只能通过全局唯一 ID 的方案来进行消息检测，具体的实现原理和版本号递增的方式一致。**





回答完“如何确保消息不会丢失？”之后，面试官通常会追问**“怎么解决消息被重复消费的问题？” 比如：在消息消费的过程中，如果出现失败的情况，通过补偿的机制发送方会执行重试，重试的过程就有可能产生重复的消息，那么如何解决这个问题？**



> ​       这个问题其实可以换一种说法，就是如何解决消费端幂等性问题（幂等性，就是一条命令，任意多次执行所产生的影响均与一次执行的影响相同），只要消费端具备了幂等性，那么重复消费消息的问题也就解决了。



我们还是来看扣减京豆的例子，将账户 X 的金豆个数扣减 100 个，在这个例子中，我们可以通过改造业务逻辑，让它具备**幂等性。**

![3.png](https://s0.lgstatic.com/i/image2/M01/07/69/CgpVE2AICsCAYHgNAAF3z8OC8eg779.png)





​       **最简单的实现方案，就是在数据库中建一张消息日志表， 这个表有两个字段：消息 ID 和消息执行状态。**这样，我们消费消息的逻辑可以变为：**在消息日志表中增加一条消息记录，然后再根据消息记录，异步操作更新用户京豆余额。**



​       因为我们每次都会在插入之前检查是否消息已存在，所以就不会出现一条消息被执行多次的情况，这样就实现了一个幂等的操作。**当然，基于这个思路，不仅可以使用关系型数据库，也可以通过 Redis 来代替数据库实现唯一约束的方案。**

​       在这里我多说一句，想要解决**“消息丢失”**和**“消息重复消费”**的问题，有一个前提条件就是要实现一个**全局唯一 ID 生成的技术方案。**这也是面试官喜欢考察的问题，你也要掌握。



在分布式系统中，全局唯一 ID 生成的实现方法有**数据库自增主键、UUID、Redis，Twitter-Snowflake 算法，**我总结了几种方案的特点，你可以参考下。

![4.png](https://s0.lgstatic.com/i/image/M00/8F/79/Ciqc1GAIDXuAZ2iUAAIGj0vJThg862.png)

​        我提醒你注意，无论哪种方法，**如果你想同时满足简单、高可用和高性能，就要有取舍，**所以你要站在实际的业务中，说明你的选型所考虑的平衡点是什么。我个人在业务中比较倾向于选择 **Snowflake 算法，**在项目中也进行了一定的改造，主要是让算法中的 ID 生成规则更加符合业务特点，以及优化诸如**时钟回拨**等问题。



#### 消息积压

​        当然，除了“怎么解决消息被重复消费的问题？”之外，面试官还会问到你**“消息积压”**。 原因在于消息积压反映的是性能问题，**解决消息积压问题，可以说明候选者有能力处理高并发场景下的消费能力问题。**

​        你在解答这个问题时，依旧要传递给面试官一个这样的思考过程： **如果出现积压，那一定是性能问题，想要解决消息从生产到消费上的性能问题，就首先要知道哪些环节可能出现消息积压，然后在考虑如何解决。**

​        因为消息发送之后才会出现积压的问题，**所以和消息生产端没有关系，又因为绝大部分的消息队列单节点都能达到每秒钟几万的处理能力，相对于业务逻辑来说，性能不会出现在中间件的消息存储上面。**毫无疑问，**出问题的肯定是消息消费阶段，**那么从消费端入手，如何回答呢？



##### 第一种情况： 线上突发问题

> ​      如果是线上突发问题，要临时扩容，增加消费端的数量，与此同时，降级一些非核心的业务。通过扩容和降级承担流量，这是为了表明你对应急问题的处理能力。



##### 第二种情况：其次，才是排查解决异常问题

其次，才是排查解决异常问题，**如通过监控，日志等手段分析是否消费端的业务逻辑代码出现了问题，优化消费端的业务处理逻辑。**

​         最后，**如果是消费端的处理能力不足**，可以通过**水平扩容**来提供消费端的**并发处理能力，**但这里有一个考点需要特别注意， **那就是在扩容消费者的实例数的同时，必须同步扩容主题 Topic 的分区数量，确保消费者的实例数和分区数相等。**如果**消费者的实例数超过了分区数，由于分区是单线程消费，所以这样的扩容就没有效果。**





> ​       比如在 Kafka 中，一个 Topic 可以配置多个 Partition（分区），数据会被写入到多个分区中，但在消费的时候，Kafka 约定一个分区只能被一个消费者消费，Topic 的分区数量决定了消费的能力，所以，可以通过增加分区来提高消费者的处理能力。



#### 总结

​       至此，我们讲解了 MQ 消息队列的热门问题的解决方案，无论是初中级还是高级研发工程师，本讲都是你需要掌握的，你都可以从这几点出发，与面试官进行友好的交流。我来总结一下今天的重点内容。

    1、如何确保消息不会丢失？ 你要知道一条消息从发送到消费的每个阶段，是否存在丢消息，以及如何监控消息是否丢失，最后才是如何解决问题，方案可以基于“ MQ 的可靠消息投递 ”的方式。
    
    2、如何保证消息不被重复消费？ 在进行消息补偿的时候，一定会存在重复消息的情况，那么如何实现消费端的幂等性就这道题的考点。
    
    3、如何处理消息积压问题？ 这道题的考点就是如何通过 MQ 实现真正的高性能，回答的思路是，本着解决线上异常为最高优先级，然后通过监控和日志进行排查并优化业务逻辑，最后是扩容消费端和分片的数量。

​        在回答问题的时候，你需要特别注意的是，让面试官了解到你的**思维过程**，这种解决问题的能力是面试官更为看中的，比你直接回答一道面试题更有价值。

另外，如果你应聘的部门是基础架构部，那么除了要掌握本讲中的常见问题的主线知识以外，还要掌握消息中间件的其他知识体系，如：

    1、如何选型消息中间件？
    
    2、消息中间件中的队列模型与发布订阅模型的区别？
    
    3、为什么消息队列能实现高吞吐？
    
    4、序列化、传输协议，以及内存管理等问题









## 【Netty学习】



**Netty**

![image-20210422183334814](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422183334814.png)



![image-20210422183416677](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422183416677.png)



![image-20210422183626347](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422183626347.png)





![image-20210422183647951](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422183647951.png)



![image-20210422183821860](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422183821860.png)

![image-20210422184002909](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184002909.png)





![image-20210422184052186](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184052186.png)



![image-20210422184401933](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184401933.png)



![image-20210422184412453](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184412453.png)





![image-20210422184433966](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184433966.png)

![image-20210422184445780](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184445780.png)







![image-20210422184541494](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184541494.png)



![image-20210422184554046](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184554046.png)

![image-20210422184612890](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184612890.png)



![image-20210422184653250](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184653250.png)



![image-20210422184733054](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184733054.png)





![image-20210422184758384](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184758384.png)



![image-20210422184826097](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184826097.png)



![image-20210422184852647](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184852647.png)





![image-20210422184928608](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184928608.png)

![image-20210422184952014](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422184952014.png)

![image-20210422185018843](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185018843.png)



![image-20210422185119482](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185119482.png)





![image-20210422185136615](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185136615.png)



![image-20210422185157289](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185157289.png)



![image-20210422185459570](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185459570.png)



![image-20210422185732373](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185732373.png)





![image-20210422185741380](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422185741380.png)



![image-20210422191659267](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422191659267.png)



![image-20210422191740178](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210422191740178.png)





![image-20210423083126923](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083126923.png)





![image-20210423083149529](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083149529.png)





![image-20210423083220779](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083220779.png)

![image-20210423083243513](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083243513.png)



![image-20210423083256892](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083256892.png)



![image-20210423083345771](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083345771.png)



![image-20210423083530633](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083530633.png)



![image-20210423083643139](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083643139.png)



![image-20210423083734147](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083734147.png)



![image-20210423083843669](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083843669.png)



![image-20210423083903940](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423083903940.png)



![image-20210423084042053](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423084042053.png)



![image-20210423084207231](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423084207231.png)



![image-20210423084230202](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423084230202.png)



![image-20210423084355697](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423084355697.png)



![image-20210423084550666](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423084550666.png)





![image-20210423085026287](C:\Users\senyint\AppData\Roaming\Typora\typora-user-images\image-20210423085026287.png)









**数据结构与算法**

四步分析法：

- 模拟
- 规律
- 匹配
- 









**栈**

![Drawing 65.png](https://s0.lgstatic.com/i/image6/M01/0B/7F/CioPOWA4q6qASB-UAADhj7uzOwg933.png)







# Java类职位合集：方向-中间件/平台/系统，推荐/支付/供应链/评论，短视频/物联网/风控策略

专业IT互联网技术猎头，突破职业瓶颈，规划职业生涯

**方向-中间件/ 广告平台/技术平台/基础架构/消息基础链路/系统**

​       **-推荐系统/商品/用户/出价/营销/订单/支付/商户//供应链/物流/评论方向/**

​       **-业务/短视频/物联网/直播/交易/地理信息服务/风控策略**

**Level-高级专家/负责人/架构师/专家/资深/高级/工程师**



**存储中间件高级技术专家**

工作职责

1、负责超大流量，低延时，易运维的分布式缓存/存储集群的研发与构建;  

2、负责架构设计和系统的演进，主导技术难题攻关，满足业务需求与中间件产品自身快速发展的要求；  

3、负责中间件产品方案落地项目的推进和跟踪，协调各方资源完成目标；

任职资格

1、乐于研究及尝试最新的技术，对业界前沿技术发展有清晰理解，对产品所在领域有预见性； 

2、精通JAVA或C/C++开发，熟悉多种编程语言则更佳；  

3、五年以上大规模分布式存储系统架构设计和研发经验；有构建过分布式数据库，缓存，文件系统等优先；  

4、熟悉计算机网络，精通高性能网络服务编程，如异步通信，事件驱动，性能优化等。理解Linux内核原理优先； 

5、具备体系化思考能力和DevOps思维方式，善于挖掘问题背后的本质，能从技术趋势和思路上能影响团队 

------

**高级后端开发专家-ZLC**

工作职责

1.负责项目的统筹管理、推进和协调，如项目可行性评估，制定项目计划并有效跟进等； 

2.负责根据业务目标编写业务需求、用户测试和生产验证等工作，推动系统项目需求投产； 

3.对相关系统架构方案、设计、代码进行评审,控制产品系统架构质量； 

4.参与项目实施过程中的需求调研与分析。

任职资格

\1. Java基础扎实，熟悉IO、多线程、集合等基础框架，熟悉分布式、缓存、消息等机制。 

\2. 6年以上使用JAVA进行开发的经验；精通AOP、MVC等框架。 

\3. 熟悉常用的设计模式；熟悉高并发、高性能的分布式系统的设计及应用、调优。 

\4. 具备优秀的团队协作沟通能力，熟悉Scrum研发流程。 

5.具备良好的表达和沟通能力，擅长技术文档、解决方案PPT的编写。 

6.对整个产品解决方案有深刻的理解及熟练的应用，优秀的团队精神和丰富的技术团队管理经验。

------

**后端负责人/leader**

职位描述：

1、带领后端团队，对整体产出负责，对成员成长负责；

2、跨团队和跨部门沟通，与多角色沟通，深刻理解教育业务；

3、追求极致，完成技术架构设计和核心技术攻关等工作。

职位要求：

1、本科及以上学历，5年以上后端开发经验，3年以上团队管理经验；

2、负责过较高复杂度的业务后端或基础服务，有比较完整的经验；

3、扎实的编程基本功，有golang经验最好，C/C++或Java也可以；

4、熟悉互联网后端基础技术，优秀的架构设计能力，熟悉微服务架构；

5、优秀的跨团队沟通能力，善于项目管理，能够将技术的价值最大化；

6、有B端或中台经验加分，有机器学习或搜索推荐等算法经验加分。

------

**架构师-广告平台 P7+~P8**

职位描述：

1.负责广告系统的架构、设计、核心研发等；

2.负责系统的代码重构、性能优化、系统改进等；

3.协助构建技术梯队、提升团队技术实力和影响力。

职位要求：

1.5年及以上互联网研发工作经验；全日制统招本科及以上学历；

2.具有扎实的计算机科学功底，扎实的编程基础和数据结构算法基础，极强的编程能力和问题解决能力；

3.熟悉Linux、多线程、并发处理、分布式系统等知识，具有扎实的实践经验；

4.熟悉Java或C++，深入理解面向对象技术和主流设计模式，对复杂系统、复杂问题有化繁为简的能力；

5.善于交流，有良好的团队合作精神和协调沟通能力，有较强的抗压能力；

6.热爱互联网和新技术，具有极强的快速学习能力，对于较大型的系统能快速上手。

------

**架构师-平台技术 P7~P7+**

职位描述：

1.负责核心业务系统的架构设计，进行技术可行性研究及技术选型，指导项目研发；

2.核心架构部分代码实现和技术难题攻关；

3.系统性能优化，保证平台安全、稳定、快速运行；

4.指导并培养团队工程师，协助提升团队整体技术实力。

职位要求：

1.5年及以上互联网研发工作经验；全日制统招本科及以上学历；

2.具有扎实的计算机科学功底，扎实的编程基础和数据结构算法基础，极强的编程能力和问题解决能力；

3.熟悉Linux、多线程、并发处理、分布式系统等知识，具有扎实的实践经验；

4.熟悉Java或C++，深入理解面向对象技术和主流设计模式，对复杂系统、复杂问题有化繁为简的能力；

5.善于交流，有良好的团队合作精神和协调沟通能力，有较强的抗压能力；

6.热爱互联网和新技术，具有极强的快速学习能力，对于较大型的系统能快速上手。

------

**架构师-SFC**

职责描述: 

\1. 主导和参与业界领先水平的四轮出行交易订单中心的系统架构设计和演进，包括行前、行中、行后及逆向交易模块。支撑千万级别的交易高可用，保证业务数据高一致性和业务迭代效率。 

\2. 主导和参与出行策略中心的系统架构设计和演进，合理做好离线/在线数据计算/存储选型，深度参与算法工程化平台能力搭建。支撑千万级行程实时匹配，支撑智能定价、营销及纠纷判责，驱动业务增长，提升用户出行体验。 

\3. 主导和参与四轮出行开放平台的搭建，标准化快速对接外部出行需求平台或运力平台，建立行业服务标准，构建四轮出行生态。 

任职要求： 

1、本科以上学历，计算机相关专业，8年以上工作经验。 

2、敏锐的业务理解能力，以及较强的抽象能力和逻辑分析能力，善于发现问题并解决问题。 

3、具有极强的落地能力，能够推动产品技术在业务上的运用和改善； 

4、有团队精神和自驱能力，有一定的抗压能力，具备良好的沟通能力。

------

**JAVA技术专家-基础架构**

工作职责
1.开发和维护微服务框架、实时消息、缓存等中间件 

2.参与技术中间件需求调研分析，对业务技术部门进行技术支持 

\3. 参与中间件、基础平台方案设计 

4.研究前沿技术，将技术转化应用

任职资格

1.精通Java开发，理解 io、多线程、集合等基础框架，对 JVM 原理有一定的了解 

2.深入理解分布式系统原理 

3.计算机理论基础扎实，例如操作系统原理、TCP/IP等 

4.学习能力强，适应能力好，抗压能力强 

5.深刻理解企业应用设计模式，有大型分布式，高并发，高负载，高可用性系统设计开发经验；

------

**服务端Java技术专家-评论方向**

岗位职责:

负责评论生态整体架构升级，方案设计及落地

任职资格:

\1. 四年以上电商平台评论生态建设经验

\2. 深刻理解微服务架构及组件

\3. 深刻理解I/O，网络，并发

\4. 熟练应用设计模式，领域模型

\5. 理解常用中间件，缓存组件

\6. 深刻理解分布式一致性，分布式事务

\7. 具有常用存储设计及优化能力

\8. 了解常用搜索引擎

\9. 正直诚信，有团队精神、有责任感、对前沿技术有激情

加分项：

\1. 大型电商或互联网背景

\2. 积极参与开源社区

\3. 有数据挖掘和机器学习算法经验

------

**JAVA服务端开发工程师/技术专家**

工作职责
\1. 负责算法平台在线服务引擎的高性能服务端研发； 

\2. 能够根据不同的业务需求，灵活快速地完成具有挑战性的项目； 

任职资格

\1. 具备3年及以上JAVA服务端研发经验，计算机相关专业本科及以上学历； 

\2. 扎实的JAVA编程基础，熟悉各种设计模式； 

\3. 熟练掌握各种主流Web/RPC框架并对实现原理有深入的了解； 

\4. 具有基于数据库、缓存、分布式存储开发高性能、高可用算法应用实际经验； 

\5. 具有搜索、推荐或广告系统服务端研发经验者优先；具有大数据量高并发在线处理工作经验者优先； 

\6. 具有搭建机器学习模型在线服务工作经验者优先； 

\7. 具备良较强的沟通和学习能力，有团队和敬业精神。

------

**资深Java开发/技术专家-DDCZZ**

工作职责

1、主导技术子系统设计、核心代码开发、系统优化等等工作 

\2. 协助业务方梳理业务需求，提供业务规划方案、架构设计方案， 并能根据方案主导研发工作的开展 

\3. 带领团队攻克例如高并发、高稳定性等带来的各种挑战及技术难关

任职资格

1、JAVA基础扎实，熟悉IO、多线程、集合等基础框架，熟悉分布式、缓存、消息等机制 

2、8年以上使用JAVA进行开发的经验；精通AOP、MVC等框架。  

3、熟悉常用的设计模式；有大吞吐量的业务场景中高并发、高性能的分布式系统的设计及应用、调优的经验。 

4、掌握Java技术栈互联网主流技术，对常见框架及组件有自己的理解和技术实践。 

5、具有极强的自我驱动和自我目标管理能力。对技术问题有自己的理解并在应用中实践。 

6、能面对压力，较好的解决问题能力，具备较强的协调能力、团队合作精神和沟通技巧。 

7、有租赁或出行行业研发经验者优先。

------

**资深Java开发/技术专家-DC/SFC**

工作职责

\1. 主导业务系统设计、开发以及系统优化等工作，帮助业务快速成长。 

\2. 协助业务方梳理需求，提供规划方案、架构设计方案， 能独立推进研发工作的开展。 

\3. 参与攻克复杂系统下高并发、高稳定性等带来的各种挑战及技术难关。  

任职资格
\1. Java基础扎实，熟悉IO、多线程、集合等基础框架，熟悉分布式、缓存、消息等机制。 

\2. 5年以上使用JAVA进行开发的经验；精通AOP、MVC等框架。 

\3. 熟悉常用的设计模式；熟悉高并发、高性能的分布式系统的设计及应用、调优。 

\4. 具备优秀的团队协作沟通能力，熟悉Scrum研发流程。

------

**资深Java开发/技术专家-ZLC**

工作职责

1、负责ZLC业务技术研发工作，保障业务实施；  

2、开拓技术视野、更有前瞻性的做技术布局；  

3、规划、实施助力车业务技术战略。

任职资格

\1. 扎实的编程基础，精通java开发语言，熟练掌握jvm，web开发、缓存，分布式架构、消息中间件等核心技术，拥有分布式、大数据量的系统开发经验者优先考虑；  

\2. 对业务能够深度理解，有清晰的业务思路，具备良好的业务建模能力，能够结合业务场景抽象业务模型，有良好的面向对象设计经验； 

\3. 技术思路清晰，结构化思维清晰，善于解决复杂问题； 

\4. 技术思路开拓，知识面广，能够不断的推动技术创新； 

\5. 具备领导和制定整个业务技术体系的技术规划和技术战略。

------

**资深Java开发工程师/专家**

工作职责

\1. 负责公司移动端研发协同平台的架构和开发。 

\2. 挖掘业务需求，进行整体技术方案设计，核心代码开发和系统调优等工作。 

\3. 负责各种高并发、数据处理、大型研发协同解决方案等方面的技术攻关。 

\4. 参与产品的部署运维，保障系统高可用性。 

任职资格

\1. 本科以上学历，计算机或其他相关专业优先。 

\2. 3年及以上Java开发经验， 熟悉Spring、Mybatis等开源框架，熟悉分布式、缓存、消息队列等机制。 

\3. 具有出色的业务建模和抽象设计能力，开发过大型复杂软件系统。 

\4. 拥有项目管理及协作、CI/CD、DevOps、APM、大数据处理等工具开发经验者优先。 

\5. 热爱技术，对业界新技术敏感，有高度自驱力主动完成工作挑战，善于沟通和跨团队协作。

------

**资深推荐系统开发⼯程师**

岗位职责： 

1、推荐系统开发； 

2、系统架构设计与优化； 

任职要求： 

1、三年以上的Java开发经验，在⾄少两个技术领域能够有较深⼊的研究； 

2、熟练掌握JVM内存管理、类加载机制等；熟练掌握Java系统的故障排查和性能调优技能； 

3、熟练掌握MySQL、Mongo、Redis中的⼀种或多种，能够进⾏使⽤优化； 

4、熟悉Linux平台运维及Shell的编写；

5、深⼊研究或参与过⼀些Java开源项⽬的优先考虑； 

6、有⾼并发或者⾼可⽤等相关的架构设计经验者优先考虑

------

**资深Java开发-营销**

职位描述：  

1、业务系统开发，兼顾性能优化工作与技术沉淀，能起到业务开发的主导作用；  

2、按计划完成产品模块的代码编写，产品模块测试，保证代码质量。

任职要求：  

1、5年以上java开发经验；  

2、 熟悉 JavaSE/JavaEE API，IO、集合、多线程、并发控制 等；  

3、 熟悉 Spring Framework，Spring MVC, Ibatis/Mybatis；  

4、 熟悉 Mysql，对于数据库结构、SQL优化有一定经验；  

5、 熟悉 WebService 技术；  

6、 熟悉 JavaScript 、JQuery ， 对HTML，CSS有一定的了解；  

7、 熟悉 Maven，Git，Eclipse等开发、构建工具；  

8、有团队合作意识，愿意接受挑战。

------

**资深研发工程师-消息基础链路方向**

职位描述：

1.优化改进底层消息链路，破除性能瓶颈，持续提升消息发送速度和送达率。

2.参与优化消息底层链路的架构升级，满足业务方海量消息发送需求，确保系统在线稳定运行。

3.完善优化消息底层的监控报警体系。

4.对数据敏感，能够通过分析数据发送当前系统存在的问题。

职位要求：

1.至少5年JAVA开发相关经验，熟悉Spring等常用Java开源框架

2.熟悉微服务架构，有大数据量高并发场景的开发经验，熟悉redis、kafka等常用技术组件。

3.具有良好的编程习惯、对代码质量有追求。

4.本科及以上学历。

5.有消息平台研发经验者优先。

------

**资深开发工程师-物流方向**

职位描述：

1.参与物流平台系统架构演进的设计和开发工作，包括商家后台物流模块的底层架构，物流开放平台以及与物流服务商或第三方ERP软件的对接平台，独立负责各业务模块的需求对接和技术改造。

2.担任重点项目的技术负责人，设计技术方案，协调和对接各方技术资源完成方案落地，并撰写高质量的开发文档。

3.负责提炼物流相关的公共组建和基础服务，为各业务系统提供稳健的基础支撑。

4.带领并辅导新人完成日常开发和系统维护工作，帮助新人成长。

职位要求：

1.本科及以上学历，3年以上的物流行业工程研发经验。

2.技术功底扎实，熟悉业界流行的开源框架和组件。

3.熟悉了解物流模块的基础服务架构，参与研发过大规模物流相关业务系统的架构设计和开发，包括EPR发货系统、物流管理系统等等；对接并开发过物流服务商的物流系统（特别是主流快递公司的相关系统）优先。

4.责任心强，思路清晰，技术视野开阔，对业界新技术敏感，喜欢钻研，具有良好的学习能力并注重团队合作。

------

**高级java开发工程师 — 短视频**

岗位描述：
1、负责抖音、TikTok产品的研发； 

2、深入发掘和分析业务需求，撰写技术方案和系统设计； 

3、根据产品需求，进行系统设计和编码； 

4、持续对系统架构进行改造和优化。

岗位要求：
1、三年以上的Java开发经验； 

2、具备良好的基本功，熟悉多线程编程、IO、垃圾回收； 

3、具备良好的编码习惯，结构清晰，命名规范，逻辑性强，代码冗余率低； 

4、熟练掌握Java常用框架，深入了解框架提供的特性及其实现原理细节； 

5、有大规模分布式系统的设计和开发经验，能独立完成系统的设计及开发； 

6、熟练掌握MySQL数据库，具备MySQL索引优化、查询优化的能力； 

7、熟练掌握一种以上非关系型数据库，如Redis, Cassandra，理解其使用场景及限制； 

8、熟悉分布式系统，熟练掌握一种以上服务框架和消息中间件，了解其实现原理； 

9、熟悉Internet常用协议，如HTTP、TCP/IP、熟悉RESTful规范； 

10、有以下经验者优先： 

① 熟练掌握Golang/Python并能灵活运用； 

② 具有大规模分布式系统的调优经验，如JVM调优、SQL调优、缓存优化、RPC优化等；

 ③ 熟悉大规模分布式系统架构设计，熟悉CAP、Quorum、Consistent Hashing等原理和算法。

------

**资深中间件研发工程师**

职位描述：

负责中间件产品研发工作，主要方向为serverless和service mesh

1、参与设计研发公司下一代serverless计算平台

2、建设公司Cloud Native解决方案和微服务中间件

职位要求：

1.精通Java或go，有良好的设计思想和编码习惯 

2.了解云原生相关技术：Kubernetes、Docker、Istio、Knative、Prometheus等

3.熟悉微服务框架和服务治理相关技术

4.熟悉常见shell脚本，熟悉日常运维

5.动手能力强，紧跟技术前沿动态，具备持续学习能力

------

**高级Java后端工程师-物联网**

职位描述：

1、 负责工业/能源物联网应用开发

2、 负责与项目经理及相关方沟通协调，评估技术可行性与工作量，完成相关技术架构设计与设计文档的编写与更新 

3、 负责核心代码的开发，确保应用的稳定、高质量、高性能、可运维，解决开发过程中遇到的技术难题

4、 与团队协作并能完成跨团队的沟通协调，带领团队成员共同完成开发交付

职位要求：

1、3年以上软件/互联网/IT行业工作经验。

2、精通JAVA后端开发，对Spring等常用框架有深入的理解。

3、有大数据平台软件开发经验，熟悉分布式系统架构与相关的中间件。

4、有IOT领域应用开发经验优先。

5、有良好的沟通力，能够协调部门及跨业务线的工作，并有一定的项目管理经验。

6、具有良好的代码编程习惯及文档编写能力。

7、对新技术有比较强的好奇心，热爱应用开发，有比较强的学习能力，开源项目有比较多的关注，有一定的代码贡献尤佳。

\8.   具备良好的英语读写能力，有流利的英语沟通能力尤佳。

------

**高级java开发工程师-订单/出价/商品/支付/商家/供应链** 

职责描述：

1.负责移动应用的后台系统设计的迭代、开发、重构;

2.编写代码实现产品功能，完成项目开发，撰写相关文档，参与需求分析;

3.负责核心技术问题的攻关，架构设计、系统优化，协助解决项目开发过程中的技术难题;

4.了解互联网的技术发展、评估外部技术与解决方案。

职位要求：

\1. 3年以上Java及Java Web开发经验， 具有扎实的程序设计基本功，有志于从事技术工作，能够独立担当功能模块的设计开发；

\2. 理解Java常用设计模式，并且能够合理运用，基本技术特性，如Java数据结构，多线程编程，Java IO等

\3. 熟练使用Java常用框架和工具，如 Spring boot 、Spring Cloud、SpringMVC、Struts、Hibernate、Mybatis等

\4. 熟悉缓存Redis、Memcache/Couchbase，缓存设计和模式及其应用；

5、熟悉数据库原理并熟练掌握相关优化方案，熟悉ORACLE、MySql数据库开发技术及相关工具，熟悉数据库设计，熟练掌握SQL语句；

6、能在类Unix环境下熟练开发，掌握Java开发，Tomcat 配置、MySQL配置；有shell/python脚本开发能力更佳；

7、好的沟通能力及团队合作精神，具备良好的理解能力和表达能力，能够承受一定工作压力；

8、具备强烈的责任心，思路清晰，较强的学习能力和解决问题的能力；良好的需求归纳分析和文档编写能力

9、有电商、社区、金融、支付、增长、营销、供应链相关经验优先考虑/有移动互联网行业开发经验者优先。

------

**后台核心研发工程师 — 短视频**

岗位描述：

1、负责抖音、TikTok等产品的服务端研发; 

2、深入发掘和分析业务需求, 撰写技术方案和系统设计; 

3、根据产品需求, 进行系统设计和编码; 

4、持续对系统架构进行改造和优化。

岗位要求：

1、三年以上开发经验, 一年以上的Golang/Java/Python开发经验，具有较强的责任心, 自驱力和成长型思维; 

2、具有良好的业务梳理和抽象能力, 掌握重构代码基本功; 

3、熟练使用基本的数据结构和算法, 深入理解多线程、Socket等相关技术; 

4、具备良好的编码习惯, 结构清晰, 命名规范, 逻辑性强, 代码冗余率低; 

5、熟练掌握Golang/Java/Python常用框架, 深入了解框架提供的特性及其实现原理细节; 

6、有大规模分布式系统的设计和开发经验, 能独立完成系统的设计及开发; 

7、熟练掌握MySQL数据库, 具备MySQL索引优化、查询优化的能力; 

8、熟练掌握一种以上非关系型数据库, 如Redis, Cassandra, HBase, 理解其使用场景及限制; 

9、熟悉分布式系统, 熟练掌握一种以上服务框架和消息中间件, 了解其实现原理; 

10、熟悉Internet常用协议, 如HTTP、TCP/IP、熟悉RESTful规范; 

11、有以下经验者优先： 

① 熟练掌握一种以上脚本语言并能灵活运用; 

② 具有大规模分布式系统的调优经验; 

③ 熟悉大规模分布式系统架构设计, 熟悉CAP、Quorum、Consistent Hashing等原理和算法。

------

**Java开发工程师-系统**

工作职责
1、 负责Bss、Bpc等系统整体技术框架、澄清技术细节、解决技术难点； 

2、 完成需求评估、业务建模、系统设计，撰写相关技术文档； 

3、 完成系统核心模块开发、维护，按计划完成各个模块日常开发工作； 

4、 保证产品质量、扩展性、维护性和开发人员的开发效率和开发规范；

任职资格

1、 本科及以上学历，有丰富的Java 开发经验，能独立进行项目开发； 

2、 深入掌握面向对象技术和设计模式，数据结构和算法； 

3、 熟练掌握多线程开发技术，熟悉linux环境及linux下的分布式、负载均衡、缓存、消息队列等方案； 

4、熟悉分布式, 缓存, 消息通信等机制; 能对分布式常用技术进行合理应用, 解决问题； 

5、熟练掌握 MySQL、PG数据库等数据库开发技术以及相关的调优策略； 

6、熟练使用spring系列、mybatis 等框架进行开发 

7、对微服务有深入理解和实践，能针对业务和部门发展需要设计灵活健壮高可用的系统架构

8、乐于并善于沟通，思维敏捷，扛压能力强。

------

**Java研发工程师 -用户/交易/直播**

职位描述： 

1.负责⽤户/交易/直播等核⼼业务开发。 

2.负责业务的架构、设计和优化，关注技术领域的最新成果。 

任职资格： 

1.计算机及相关专业，本科及以上学历，2-5年Java研发经验； 

2.精通Java开发，深⼊理解jvm原理，熟悉常⽤框架（Spring、Spring MVC、Mybatis）并能掌握它的原理和机制；

3.对微服务架构组件有实践经验，关注并理解架构的⾼可⽤性与可扩展性。 

4.了解常⽤RPC框架如SpringCloud、Dubbo等，了解常⽤消息中间件如Kafka、RabbitMQ。 

5.具有较⾼的技术钻研能⼒、技术难点攻关能⼒，分析问题解决问题的能⼒。 

6.对技术有激情，有较强的独⽴开发能⼒、主动的学习能⼒，良好的沟通表达能⼒和团队协作能⼒

------

**地理信息服务端开发工程师**

任职资格

1）2～3年以上地图相关研发工作经验； 

2）地理信息或计算机相关专业； 

3）熟悉开发语言Java、C++或者python之一； 

4）熟悉至少一种导航数据格式，优先考虑（如：高德或者百度等自研导航数据格式、NDS或OpenDriver等通用导航数据格式）； 

5）有数据编译、地图导航引擎研发经验，优先考虑； 

6）有高德、百度、四维、超图、ESRI等公司研发经验，优先考虑；

------

**风控策略研发工程师**

职位描述：

\1. 深入理解业务特性，参与风控特征系统研发，提升系统的性能与稳定性；

\2. 负责在线风控系统设计与实现，参与特征抽取、参数选择、ABtest、效果分析、数据可视化等。

职位要求：

1.三年以上Java开发经验，对常见数据结构和算法原理有较为深入的理解，有大型系统的开发/改进/策略优化经验；

2.具备风控系统开发经验；

3.具备优秀的团队合作意识，良好的沟通能力；优秀的分析问题和解决问题的能力和成长潜力。



**解锁更多详情/职位请联系**

**微信：Cathy-LYY**

**简历请投：cathy.liu@gitalent.cn**