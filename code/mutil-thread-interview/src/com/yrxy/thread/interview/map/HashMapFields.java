package com.yrxy.thread.interview.map;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className HashMapFields
 * @description：
 * @date 2017/7/15 14:55
 */
public class HashMapFields {
    static final int MAXIMUM_CAPACITY = 1 << 30;
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;

    public static void main(String[] args) {
    System.out.println("MAXIMUM_CAPACITY is : "+MAXIMUM_CAPACITY);
    System.out.println("DEFAULT_INITIAL_CAPACITY is : "+DEFAULT_INITIAL_CAPACITY);


    }

}
