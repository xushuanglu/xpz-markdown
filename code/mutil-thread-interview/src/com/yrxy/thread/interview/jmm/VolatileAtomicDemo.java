package com.yrxy.thread.interview.jmm;
/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className VolatileAtomicDemo
 * @description：
 * @date 2017/7/7 14:28
 */
public class VolatileAtomicDemo {
    private static volatile long value = 0;
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            value = 0;
            testAtomic();
        }
    }
    private static class Test1 implements Runnable {
        public void run() {
            long val = 0;
            while (val < 100000) {
                value++;
                val++;
            }
        }
    }
    private static class Test2 implements Runnable {
        public void run() {
            long val = 0;
            while (val < 100000) {
                value++;
                val++;
            }
        }
    }
    private static void testAtomic() throws InterruptedException {
        Thread t1 = new Thread(new Test1());
        t1.start();
        Thread t2 = new Thread(new Test2());
        t2.start();
        t1.join();
        t2.join();
        System.out.println("final val is: " + value);
    }

}
