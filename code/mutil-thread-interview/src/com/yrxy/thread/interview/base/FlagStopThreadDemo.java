package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className FlagStopThreadDemo
 * @description：
 * @date 2017/6/8 17:41
 */
public class FlagStopThreadDemo implements Runnable{
    private  int  ticket = 20;
    private volatile boolean flag = true;

    public void run() {
        synchronized(this) {
        while(flag) {
            ticket--;
            System.out.println(Thread.currentThread().getName() + ", flag is  " + flag + ", 卖了一张票，剩余票数为：" + ticket);
            if (0 == ticket) {
                flag = false;
                System.out.println(Thread.currentThread().getName() + ", flag is  " + flag + ",票卖完了，剩余张数为：" + ticket);
            }
        }
        }
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public static void main(String[] args) throws InterruptedException {
        FlagStopThreadDemo flagStopThread1 = new FlagStopThreadDemo();
        Thread thread1=new Thread(flagStopThread1);
        thread1.setName("Thread-flag1");
        thread1.start();

//        FlagStopThread flagStopThread2 = new FlagStopThread();
//        Thread thread2=new Thread(flagStopThread2);
//        thread2.setName("Thread-flag2");
//        thread2.start();

//        FlagStopThread flagStopThread2 = new FlagStopThread();
//        flagStopThread2.setName("Thread-flag1");
//        flagStopThread2.start();
    }


}
