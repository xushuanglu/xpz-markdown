
void transfer(Entry[] newTable, boolean rehash) {
        int newCapacity = newTable.length;
        for (Entry<K,V> e : table) {
            while(null != e) {
            	//1， 获取旧表的下一个元素
                Entry<K,V> next = e.next;
                if (rehash) {
                    e.hash = null == e.key ? 0 : hash(e.key);
                }
                //i是元素在新数组的位置
                int i = indexFor(e.hash, newCapacity);
                //此处体现了头插法，当前元素的下一个是新数组的头元素
                e.next = newTable[i];
                //将原数组元素加入新数组
                newTable[i] = e;
                e = next;
            }
        }
    }

