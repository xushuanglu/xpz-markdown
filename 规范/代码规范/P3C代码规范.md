### P3C代码规范






#### 关于基本数据类型与包装数据类型的使用标准如下：

 1） 所有的POJO类属性必须使用包装数据类型。
 2） RPC方法的返回值和参数必须使用包装数据类型。
 3） 所有的局部变量推荐使用基本数据类型。 说明：POJO类属性没有初值是提醒使用者在需要使用时，必须自己显式地进行赋值，任何NPE问题，或者入库检查，都由使用者来保证。
            

    public class DemoDO {
        String str;
        Integer a;
    }


![image-20210831152258514](../../images/image-20210831152258514.png)





#### 不允许任何魔法值（即未经定义的常量）直接出现在代码中。 

Negative example:
    //Magic values, except for predefined, are forbidden in coding.
    if (key.equals("Id#taobao_1")) {
            //...
    }





#### 包名统一使用小写，点分隔符之间有且仅有一个自然语义的英语单词。包名统一使用单数形式，但是类名如果有复数含义，类名可以使用复数形式 


    com.alibaba.mpp.util / com.taobao.tddl.domain.dto

![image-20210831152515190](../../images/image-20210831152515190.png)





#### 单个方法的总行数不超过80行。 

> 说明：除注释之外的方法签名、结束右大括号、方法内代码、空行、回车及任何不可见字符的总行数不超过80行。






#### 及时清理不再使用的代码段或配置信息。

> 说明：对于垃圾代码或过时配置，坚决清理干净，避免程序过度臃肿，代码冗余。
> Positive example: For codes which are temporarily removed and likely to be reused, use /// to add a reasonable note.
>  public static void hello() {
> /// Business is stopped temporarily by the owner.
> // Business business = new Business();
> // business.active();
> System.out.println("it's finished");
> }




#### 定义DO/DTO/VO等POJO类时，不要加任何属性默认值。 

    public class DemoDO {
        String str;
        Integer a;
    }



![image-20210831152947074](../../images/image-20210831152947074.png)





#### 所有的抽象方法（包括接口中的方法）必须要用javadoc注释、除了返回值、参数、异常说明外，还必须指出该方法做什么事情，实现什么功能。

说明：如有实现和调用注意事项，请一并说明。
    /**
     * fetch data by rule id
     * 
     * @param ruleId rule id
     * @param page page number
     * @param jsonContext json format context
     * @return Result<XxxxDO>
     */
    Result<XxxxDO> fetchDataByRuleId(Long ruleId, Integer page, String jsonContext);

​        

![image-20210831153122040](../../images/image-20210831153122040.png)





#### 所有的类都必须添加创建者信息。 

> 说明：在设置模板时，注意IDEA的@author为${USER}，而eclipse的@author为${user}，大小写有区别，而日期的设置统一为yyyy/MM/dd的格式。
> /**
>
>    * Demo class
>      *
>      * @author keriezhang
>        * @date 2016/10/31
>          */
>          public class CodeNoteDemo {
>          }





#### 类、类属性、类方法的注释必须使用javadoc规范，

> 使用/**内容*/格式，不得使用//xxx方式和/*xxx*/方式。 说明：在IDE编辑窗口中，javadoc方式会提示相关注释，生成javadoc可以正确输出相应注释；在IDE中，工程调用方法时，不进入方法即可悬浮提示方法、参数、返回值的意义，提高阅读效率。
> /**
>
>    * 
>    * XXX class function description.
>      *
>         */
>        public class XxClass implements Serializable {
>      private static final long serialVersionUID = 113323427779853001L;
>      /**
>       * id
>         */
>         private Long id;
>         /**
>       * title
>         */
>         private String title;

        /**
         * find by id
         * 
         * @param ruleId rule id
         * @param page start from 1
         * @return Result<Xxxx>
         */
        public Result<Xxxx> funcA(Long ruleId, Integer page) {
            return null;
        }
    }


​      


#### 类名使用UpperCamelCase风格，必须遵从驼峰形式，

但以下情形例外：（领域模型的相关命名）DO / BO / DTO / VO / DAO 



返回类型为基本数据类型，return包装数据类型的对象时，自动拆箱有可能产生NPE 
            

    public int method() {
        Integer a = null;
        return a;
    }




#### 除常用方法（如getXxx/isXxx）等外，不要在条件判断中执行复杂的语句，将复杂逻辑判断的结果赋值给一个有意义的布尔变量，以提高可读性。 

> 说明：很多if语句内的逻辑相当复杂，阅读者需要分析条件表达式的最终结果，才能明确什么样的条件执行什么样的语句，那么，如果阅读者分析逻辑表达式错误呢？

Negative example:
    if ((file.open(fileName, "w") != null) && (...) || (...)) {
        // ...
    }



![image-20210831154238376](../../images/image-20210831154238376.png)