package com.yrxy.thread.interview.jmm;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className Reordering
 * @description：
 * @date 2018/7/5 16:56
 */
public class Reordering {
//    static int x = 0, y = 0;
//    static int a = 0, b = 0;

    static int x = 0, y = 0;
    static int a = 0, b = 0;

    public static void main(String[] args) throws InterruptedException {
        for(int  i=0;i<1000000;i++) {
            reOrder();
            if(a==0&&b==0){
                System.out.println("i is: "+i+" , a="+a+", b = "+b);
                break;
            }
        }
    }

    private static void reOrder() throws InterruptedException {
        Thread one = new Thread(new Runnable() {
            public void run() {
                a = 1;
                x = b;
            }
        });

        Thread other = new Thread(new Runnable() {
            public void run() {
                b = 1;
                y = a;
            }
        });
        one.start();
        other.start();
        one.join();
        other.join();
        System.out.println("a= " +a + ", b="+b );
    }
}
