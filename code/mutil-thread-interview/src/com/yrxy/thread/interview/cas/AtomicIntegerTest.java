package com.yrxy.thread.interview.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className AtomicIntegertEST
 * @description：
 * @date 2017/7/3 14:47
 */
public class AtomicIntegerTest {
    static AtomicInteger value = new AtomicInteger(1);

    public static void main(String[] args) {
        boolean isCASSuccess = value.compareAndSet(1,2);
        System.out.println(isCASSuccess);
    }

}
