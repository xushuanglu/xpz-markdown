package com.yrxy.thread.interview.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className FairLockDemo
 * @description：公平锁示例
 * @date 2017/6/3 13:38
 */
public class FairLockDemo {

    private ReentrantLock lock = new ReentrantLock(true);

    public   void testLock(){
        try {
            lock.lock();
            System.out.println(Thread.currentThread().getName() +"获得了锁");
        }finally {
            lock.unlock();
        }
    }
    public static void main(String[] args) {
        FairLockDemo fairLock = new FairLockDemo();
        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName()+"启动");
            fairLock.testLock();
        };
        Thread[] threadArray = new Thread[20];
        for (int i=0; i<20; i++) {
            threadArray[i] = new Thread(runnable);
            threadArray[i].start();
        }
    }



}
