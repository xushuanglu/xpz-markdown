package com.yrxy.thread.interview.threadpool;
/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadPoolStateTest
 * @description：
 * @date 2016/5/20 14:11
 */
public class ThreadPoolStateTest {
    //ctl是线程池中一个非常重要的变量，以它的低29位表示线程池中处于RUNNING状态的线程个数，高3位表示线程池所处的状态
   public static int COUNT_BITS = Integer.SIZE - 3;
   public static int CAPACITY   = (1 << COUNT_BITS) - 1;

    public static void main(String[] args) {
        // <<表示 左移
        int RUNNING    = -1 << COUNT_BITS;
        int SHUTDOWN   =  0 << COUNT_BITS;
        int STOP       =  1 << COUNT_BITS;
        int TIDYING    =  2 << COUNT_BITS;
        int TERMINATED =  3 << COUNT_BITS;
        System.out.println("Integer.SIZE is :"+Integer.SIZE+"  , COUNT_BITS is : "+COUNT_BITS);
        System.out.println("CAPACITY is : "+CAPACITY+" , "+Integer.toBinaryString(CAPACITY));
      //五种状态中SHUTDOWN值等于0，RUNNING值小于0，其他三种状态STOP、TIDYING、TERMINATED值均大于0
        System.out.println("RUNNING    = " + RUNNING + " = " + Integer.toBinaryString(RUNNING));
        System.out.println("SHUTDOWN   = " + SHUTDOWN + " = " + Integer.toBinaryString(SHUTDOWN));
        System.out.println("STOP       = " + STOP + "  = 00" + Integer.toBinaryString(STOP));
        System.out.println("TIDYING    = " + TIDYING + " = 0" + Integer.toBinaryString(TIDYING));
        System.out.println("TERMINATED = " + TERMINATED + " = 0" + Integer.toBinaryString(TERMINATED));
    }
    //CAPACITY低29位全1高3位全0，它与c做与运算得到的就是当前运行的线程个数
    static int workerCountOf(int c)  {
        return c & CAPACITY;
    }

}
