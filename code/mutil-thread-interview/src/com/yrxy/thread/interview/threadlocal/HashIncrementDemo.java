package com.yrxy.thread.interview.threadlocal;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className HashIncrementDemo
 * @description：
 * @date 2017/7/8 18:22
 */
public class HashIncrementDemo {
    //HASH_INCREMENT是斐波那契数,也叫黄金分割数。
    // hash增量为这个数字，作用就是hash分布非常均匀
    private static final int HASH_INCREMENT = 0x61c88647;
    public static void main(String[] args) {
        int hashCode = 0;
        int bucketIndex;
        int length = 16;
        for (int i = 0; i < length; i++) {
            hashCode = HASH_INCREMENT * i + HASH_INCREMENT;
            bucketIndex = hashCode & (length - 1);
            System.out.println(i + " 在桶中的位置是： " + bucketIndex);
        }
    }
}
