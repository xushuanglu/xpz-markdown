# Git极客时间学习

## Git特点

- 最优的存储能力

- 非凡的性能

- 开源的

- 很容易做备份

- 支持离线操作

- 很容易定制工作流程



## Git命令

查看安装git版本

```shell
git --version
```

配置user.name和user.email

```shell
git config --global user.name 'your_name'
git config --global user.email 'your_name@163.com'
```

config的三个作用域

**缺省等同于local**

```
git config --local   只对某个仓库有限
git config --global  global对当前用户所有仓库有效
git config --system  system对系统所有登录的用户有效
```

显示config的配置，加 --list

```
git config --list --local
git config --list --global
git config --list --system
```



创建Git仓库

1、把已有的项目代码纳入Git管理

```
$ cd 项目代码所在的文件夹
$ git init
```

2、新建的项目直接用Git管理

```
$ cd 某个文件夹
$ git init your_project#会在当前路径下创建和项目名称同名的文件夹
$ cd your_project
```

git提交文件

```
$ git add file
```

git提交文件

```
$ git commit -m '文字说明'
```



查看提交日志记录

```
$ git log
```



往仓库里添加文件



git add files -->》 git commit 

工作目录--》暂存区--》版本历史





### 添加工作区里修改的所有文件

```
$ git add -u
```



### 文件重命名

```
$ mv oldfilename newfilename
```



### 把重命名的文件添加进来

```
$ git add 重命名的文件
```



### 删除文件 

```
$ git rm 文件
```



### 一步实现修改文件名

```
$ git mv oldFileName newFileName
```



### 清除暂存区

```
$ git reset --hard
```





## Git查看log



#### 查看最近的4条log

```shell
$ git log -n4 --oneline
6f1ce4d add file
e1a8af4 add test.txt modify readme.txt
e4c0185 Add readme
```





#### 查看本地的分支

```
$ git branch -v
```



#### 查看所有的分支log

```
g$ git log --all
```



#### 查看所有的分支log(图形化)

```
$ git log --all --graph
```



#### 通过生成html页面查看git help log命令

```
$ git help --web log
```



#### 创建分支

```
$ git checkout -b temp
```



#### 切换分支

```
$ git checkout 将要切换到的分支
```



#### 图形化界面命令

```
$ gitk
```





#### 查看文件内容

```
$ git cat-file -p ''
```



#### 查看文件类型

```
$ git cat-file -t ''
```





#### 查看2个log的变更内容

```
$ git diff log1 log2
```



#### 获取远端上面的分支

```
$ git branch -va
```





#### 合并2个不相关的分支

```
$ git merge --allow-unrelated-histories github/master
```





#### 删除分支

```
git branch -D 分支名称
```



#### 怎么修改最新的commit的message

```
git commit --amend
```



#### 怎么修改旧的commit的message

```
git rebase -i commitId
```



#### 怎么把连续的多个commit整理成一个

```
git rebase -i 最新的CommitId
```

```
基于一个commitId修改除了最新commitId其他的数据，前面pick修改为s
```





怎么把间隔的几个commit整理成一个

```

```



#### 比较暂存区和HEAD所包含文件的差异

```
git diff --cached
```



#### 怎么比较工作区和暂存区所含文件的差异

```
git diff
```



#### 如何让暂存区恢复成和HEAD的一样

```
git reset HEAD
```





#### 如何让工作区的文件恢复为和暂存区文件一样

```
git checkout -- 文件名称
```





#### 怎么取消暂存区部分文件的变更

```
git reset HEAD -- 恢复的文件1  恢复的文件2
```





#### 消除最近的几次提交

```shell
git reset --hard commitId
```





#### 看看不同提交的指定文件的差异

```
git diff temp master

git diff temp master -- 文件名
```





#### 正确删除文件的方法

```
 git rm 文件
```



#### 开发中临时加塞了紧急任务怎么处理

```
git stash  临时存放
git stash list  查看临时存放的内容
git stash  apply   恢复之前的文件内容
git stash pop
```



#### 如何指定不需要Git管理的文件

```
vi .gitignore

里面指定过滤提交的文件夹或者文件后缀名

注意斜杠/的情况，有和没有不一样
```





#### 如何将Git仓库备份到本地

```
git clone --bae 本地git项目/.git ya.git


git clone --bae file://本地git项目/.git zhineng.git
```

![image-20200723172507431](C:\Users\xushu\AppData\Roaming\Typora\typora-user-images\image-20200723172507431.png)





## 怎么快速淘到感兴趣的开源项目?

- 例子1

  ```git
  git 最好 资料 in:readme
  ```

- 例子2

  ```shell
  git 最好 资料 in:readme
  ```

  