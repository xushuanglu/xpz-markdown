package com.yrxy.thread.interview.threadlocal;

import java.util.Random;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadLocalDemo
 * @description：
 * @date 2017/7/8 20:44
 */
public class ThreadLocalDemo {

    public static class MyThread implements Runnable {
        private ThreadLocal<Integer> threadlocal = new ThreadLocal<Integer>();
        @Override
        public void run() {
            threadlocal.set(new Random().nextInt(100));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread() + " : " + threadlocal.get());
        }
    }

    public static void main(String[] args) {
        MyThread runnable = new MyThread();
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
    }

}
