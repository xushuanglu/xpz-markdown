package com.yrxy.thread.interview.lock;
import java.util.List;
import java.util.Vector;
/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className BiasedLockDemo
 * @description：
 * @date 2017/5/26 18:13
 */
public class BiasedLockDemo {
    public static List<Integer> vector = new Vector<Integer>();
    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
        int count = 0;
        int startnum = 0;
        while (count < 30000000) {
            vector.add(startnum);
            startnum += 2;
            count++;
        }
        long end = System.currentTimeMillis();
        System.out.println(end - begin);
    }

}
