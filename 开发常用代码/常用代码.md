## 常用开发代码

### 复制类

SystemNotice systemNotice = new SystemNotice();
BeanUtils.copyProperties(requestDTO.getData(),systemNotice);
> 注：第一个参数是前端传过来的值，后面一个参数是新定义的

### 生成主键id
int ret = apkPluginsMapper.insertUseGeneratedKeys(apkPlugins);

### 判断字符串是否为空
StringUtils.isBlank(project)

### json转对象
JSONObject data = JSONObject.parseObject(JSON.toJSONString(resultMap));

### Java8遍历集合
List<JcStepAttachment> attList = jcStepInfoDTO.getAttList();
if(!CollectionUtils.isEmpty(attList)){
    attList.stream().forEach(att ->  jcStepAttachmentMapper.insert(att.setCardId(jcProceduralStep.getCardId())));
}

### 递归遍历数据
    /**
      * 递归步骤列表
      * @param stepList
      * @param parentId
      * @return
      */
     private List<JcStepInfoDTO> getNewStepTree(List<JcStepInfoDTO> stepList, String parentId,
                                                List<JcStepAttachment> jcStepAttachmentList,List<JcStepItem> jcStepItemList,Integer type) {
         return stepList.stream().filter(step -> step.getFid().equals(parentId)).map(step ->
         {
             JcStepInfoDTO jcStepInfoDTO = BeanConvertUtils.copyProperties(step, JcStepInfoDTO.class
             List<JcStepItem> itemList = jcStepItemList.stream().filter(toast -> toast.getStepId().equals(step.getStepId())).collect(Collectors.toList());
             List<JcStepItem> stepItemList = new ArrayList<JcStepItem>();
             List<JcStepItem> stepItemSecondList = new ArrayList<JcStepItem>(
             //app端特殊处理   所有的都存取ch
             if(type == C.N_2 ){
                 itemList.stream().forEach( item ->
                         item.setContent(item.getItemType() == 4 ? item.getItemTitleCh() : item.getContent()));
            
             //查询功能项信息
             if(CollectionUtils.isEmpty(itemList)){
                 jcStepInfoDTO.setJcStepItemList(new ArrayList<JcStepItem>());
             }else{
                 //警示信息提到第一位
                 for(JcStepItem stepItem : itemList){
                     if(stepItem.getItemType() == 5){
                         stepItemList.add(stepItem);
                     }else{
                         stepItemSecondList.add(stepItem);
                     }
                 }
                 stepItemList.addAll(stepItemSecondList);
                 jcStepInfoDTO.setJcStepItemList(stepItemList);
            
             //步骤附件处理
             if(!CollectionUtils.isEmpty(jcStepAttachmentList)){
                 //通过步骤id关联附件
                 List<JcStepAttachment> collect = jcStepAttachmentList.stream().filter(att -> att.getStepId().equals(step.getStepId())).collect(Collectors.toList());
                 jcStepInfoDTO.setAttList(collect);
            
             List<JcStepInfoDTO> childList = getNewStepTree(stepList, jcStepInfoDTO.getStepId(), jcStepAttachmentList, jcStepItemList,type);
             jcStepInfoDTO.setChildren(childList
             return jcStepInfoDTO;
         }).collect(Collectors.toList());


### 名字首字母排序
   
    /**
      *@Author:xyl
      *@Date: 2019/8/24 16:47
      *@Description: 名字首字母排序
      */
     public static Map<String, List<String>>  sort(List<String> list){
         Map<String, List<String>> rtMap = new HashMap<>();
         Map<String, List<String>> result=new LinkedHashMap();
         for (String s : list) {
             char[] arr = s.toCharArray();
             HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
             defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
             defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
             if (arr[0] > 128) {
                 try {
                     String[] temp = PinyinHelper.toHanyuPinyinStringArray(arr[0], defaultFormat);
                     if (temp != null) {
                         if (rtMap.containsKey(String.valueOf(temp[0].charAt(0)))) {
                             rtMap.get(String.valueOf(temp[0].charAt(0))).add(s);
                         } else {
                             List<String> li=new ArrayList<>();
                             li.add(s);
                             rtMap.put(String.valueOf(temp[0].charAt(0)), li);
                         }
                     }
                 } catch (BadHanyuPinyinOutputFormatCombination e) {
                     e.printStackTrace();
                 }
             } else {
                 if (rtMap.containsKey(String.valueOf(arr[0]))) {
                     rtMap.get(String.valueOf(arr[0])).add(s);
                 } else {
                     List<String> li=new ArrayList<>();
                     li.add(s);
                     rtMap.put(String.valueOf(arr[0]), li);
                 }
             }
            result =rtMap.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
 
        }
        return result;
    }
    
        <dependency>
             <groupId>com.belerweb</groupId>
             <artifactId>pinyin4j</artifactId>
             <version>2.5.1</version>
             <scope>test</scope>
         </dependency>
         
### 拼音排序

     // Collator 类是用来执行区分语言环境的 String 比较的，这里选择使用CHINA
        Comparator comparator = Collator.getInstance(java.util.Locale.CHINA);
        String[] arrStrings = { "乔峰", "郭靖", "杨过", "张无忌","韦小宝" };
        // 使根据指定比较器产生的顺序对指定对象数组进行排序。
        Arrays.sort(arrStrings, comparator);
        for (int i = 0; i < arrStrings.length; i++)
            System.out.println(arrStrings[i]);
        }
        
### Mysql根据姓名首字母从上到下正序展示
> SELECT
  	user_name
  FROM
  	tb_user
  ORDER BY
  	CONVERT (user_name USING gbk) COLLATE gbk_chinese_ci ASC
>