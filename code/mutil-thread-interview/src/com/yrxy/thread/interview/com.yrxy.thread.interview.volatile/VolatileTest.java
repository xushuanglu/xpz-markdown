/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className VolatileTest
 * @description：
 * @date 2017/5/22 11:15
 */
public class VolatileTest {

    public volatile int inc = 0;

    public void increase()  {
        inc++;

    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 500; i++) {
//            inc = 0;
            count();
        }

    }

    private static void count() throws InterruptedException {
        final VolatileTest test = new VolatileTest();
        Thread thread;
        long t1 = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            thread=  new Thread() {
                public void run() {
                    for (int j = 0; j < 10000; j++) {

                            test.increase();

                    }
                }

                ;
            };
            thread.start();
            thread.join();

        }

        long t2 = System.currentTimeMillis();
        System.out.println("Counter, "+String.format("结果：%s,耗时(ms)：%s", test.inc, (t2 - t1)));
    }

}
