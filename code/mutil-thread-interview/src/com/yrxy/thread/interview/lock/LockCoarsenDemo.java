package com.yrxy.thread.interview.lock;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className LockCoarsenDemo
 * @description：
 * @date 2017/6/28 19:30
 */
public class LockCoarsenDemo {
    public static void method1() {
        synchronized (LockCoarsenDemo.class) {
            System.out.println("锁粗化之前的逻辑1");
        }
        synchronized (LockCoarsenDemo.class) {
            System.out.println("锁粗化之前的逻辑2");
        }
        System.out.println("锁");
    }
    public static void method2() {
        synchronized (LockCoarsenDemo.class) {
            System.out.println("锁粗化之后的逻辑1");
            System.out.println("锁粗化之后的逻辑2");
        }
        System.out.println("锁");
    }

    public static void main(String[] args) {
        method1();
        method2();
    }

}
