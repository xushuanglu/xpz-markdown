package com.yrxy.thread.interview.map;

import java.util.HashMap;

/**
 * @author huanglaoxie
 * @className HashMapDemo1.log
 * @description：
 * @date 2020/7/23 19:45
 */
public class HashMapDemo1 {
    public static void main(String[] args) {
        HashMap map=new HashMap();
        String star1 = "杨幂";
        String star2 = "梁朝伟";
        String star3 = "刘德华";
        String star4 = "柳岩";
        map.put(star1,110);
        map.put(star2,120);
        map.put(star1,130);
        map.put(star3,140);
        map.put(star4,150);
        int index1=HashAlgDemo.getBucketIndex(HashAlgDemo.hash(star1));
        int index2=HashAlgDemo.getBucketIndex(HashAlgDemo.hash(star2));
        int index3=HashAlgDemo.getBucketIndex(HashAlgDemo.hash(star3));
        int index4=HashAlgDemo.getBucketIndex(HashAlgDemo.hash(star4));
        System.out.println(star1+" ,index is:"+index1+", and its value is:"+map.get(star1));
        System.out.println(star2+" ,index is:"+index2+", and its value is:"+map.get(star2));
        System.out.println(star3+" ,index is:"+index3+", and its value is:"+map.get(star3));
        System.out.println(star4+" ,index is:"+index4+", and its value is:"+map.get(star4));
    }
}
