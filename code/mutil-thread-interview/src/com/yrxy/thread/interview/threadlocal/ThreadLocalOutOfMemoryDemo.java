package com.yrxy.thread.interview.threadlocal;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadLocalOutOfMemoryDemo
 * @description：
 * @date 2017/7/10 15:52
 */
public class ThreadLocalOutOfMemoryDemo {
    final static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(6, 12, 1, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>());
    final static ThreadLocal<Value> localValue = new ThreadLocal<Value>();
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; ++i) {
            poolExecutor.execute(new Runnable() {
                public void run() {
                    localValue.set(new Value());
                    System.out.println("local var is:"+ localValue.get());
                    localValue.remove();
                }
            });
            Thread.sleep(2000);
        }
        System.out.println("pool execute over");
    }
    static class Value {
        private Long[] a = new Long[1024*1024];
    }
}
