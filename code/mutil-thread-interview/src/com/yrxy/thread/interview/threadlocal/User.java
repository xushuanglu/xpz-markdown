package com.yrxy.thread.interview.threadlocal;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className User
 * @description：
 * @date 2017/7/10 17:43
 */
public class User {
    String name;
    int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
