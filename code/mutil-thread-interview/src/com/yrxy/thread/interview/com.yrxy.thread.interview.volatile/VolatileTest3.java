import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className VolatileTest3
 * @description：
 * @date 2017/5/22 11:42
 */
public class VolatileTest3 {

     static volatile int number = 0;

    public static void increase() {

           number++;

    }

    public static void main(String[] args) throws InterruptedException {
        for(int i=0;i<100;i++) {
            number=0;
            count();
        }
    }

    private static void count() throws InterruptedException {
        long t1 = System.currentTimeMillis();
        int threadCount = 10;
        CountDownLatch countDownLatch = new CountDownLatch(threadCount);
        for (int i = 0; i < threadCount; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100000; j++) {
                        increase();
                    }
                } finally {
                    countDownLatch.countDown();
                }
            }).start();
        }
        countDownLatch.await();
        long t2 = System.currentTimeMillis();
        System.out.println("Counter, "+String.format("结果：%s,耗时(ms)：%s", number, (t2 - t1)));
    }


}
