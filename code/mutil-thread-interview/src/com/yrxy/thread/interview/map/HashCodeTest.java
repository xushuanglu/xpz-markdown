package com.yrxy.thread.interview.map;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className HashCodeTest
 * @description：
 * @date 2017/7/14 16:54
 */
public class HashCodeTest {

    public static void main(String[] args) {
        Object obj2 = new Object();
        Object obj1 = new Object();
        System.out.println("obj1哈希值:" + obj1.hashCode());
        System.out.println("obj2哈希值:" + obj2.hashCode());
        System.out.println("obj1和obj2比较==：" + obj1.equals(obj2));
        System.out.println("obj1和obj2比较equals：" + obj1.equals(obj2));
        String str1 = "123";
        String str2 = "123";
        String str3 = new String("123");
        String str4 = new String("123");
        System.out.println("str1哈希值:" + str1.hashCode());
        System.out.println("str2哈希值:" + str2.hashCode());
        System.out.println("str3哈希值:" + str3.hashCode());
        System.out.println("str4哈希值:" + str4.hashCode());
        System.out.println("str1和str2比较==：" + (str1 == str2));
        System.out.println("str1和str3比较==：" + (str1 == str3));
        System.out.println("str4和str3比较==：" + (str4 == str2));
        System.out.println("str1和str2比较equals：" + str1.equals(str2));
        System.out.println("str1和str3比较equals：" + str1.equals(str3));
        System.out.println("str4和str3比较equals：" + str4.equals(str3));
        Integer int1 = new Integer(100);
        Integer int2 = new Integer(100);
        System.out.println("包装类型int1哈希值:" + int1.hashCode());
        System.out.println("包装类型int2哈希值:" + int2.hashCode());
        System.out.println("包装类型int1和int2比较==：" + (int1 == int2));
        System.out.println("包装类型int1和int2比较equal：" + (int1.equals(int2)));
        int a = 5;
        int b = 5;
        System.out.println("a和b比较==：" + (a == b));
    }

}
