package com.yrxy.thread.interview.map;

/**
 * @author huanglaoxie(微信 : yfct - 8888)
 * @className HashMapDemo1.log
 * @description：
 * @date 2017/7/23 15:20
 */
public class HashAlgDemo {
    static final int DEFAULT_INITIAL_CAPACITY =16;
    public static void main(String[] args) {
        String str1 = "黄老邪";
        String str2 = "架构师";
        String str3 = "九阴真经";
        String str4 = "武汉大学";
        System.out.println(str1 + " , and hash is :"+ hash(str1)+", and bucketIndex is:" + getBucketIndex(hash(str1)));
        System.out.println(str2 + " , and hash is :" + hash(str2)+", and bucketIndex is:" + getBucketIndex(hash(str2)));
        System.out.println(str3 + " , and hash is :" + hash(str3)+", and bucketIndex is:" + getBucketIndex(hash(str3)));
        System.out.println(str4 + " , and hash is :" + hash(str4)+", and bucketIndex is:" + getBucketIndex(hash(str4)));
    }
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
    static final int getBucketIndex(int hash) {
      return  (DEFAULT_INITIAL_CAPACITY-1)&hash;
    }
}
