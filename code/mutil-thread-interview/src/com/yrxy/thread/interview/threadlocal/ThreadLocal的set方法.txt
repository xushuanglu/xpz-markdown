
set方法的主要流程：
1、将ThreadLocal变量的值put到当前线程的threadLocals中，ThreadLocal变量及其对应的值会构造成一个ThreadLocalMap.Entry放到threadLocals中。
2、如果出现hash冲突，会基于开放定址法继续寻找下一个空位进行set操作。
3、map的数组结构默认大小为INITIAL_CAPACITY = 16，默认扩容阈值为threshold = INITIAL_CAPACITY * 2 / 3，如果达到扩容标准，则按照成倍扩容。

///////////////////////////////////////////////////////////////
public void set(T value) {
    //获取当前线程
    Thread t = Thread.currentThread();
    //获取当前线程的ThreadLocalMap
    ThreadLocalMap map = getMap(t);
    if (map != null)
        //这里this是指的调用这个方法的threadlocal对象
        //调用set方法
        map.set(this, value);
    else
        //map为null，则初始化一个ThreadLocalMap
        createMap(t, value);
}
private void set(ThreadLocal<?> key, Object value) {
	Entry[] tab = table;
    int len = tab.length;
    //根据hash计算位置
    int i = key.threadLocalHashCode & (len-1);
	//循环，如果第一次没有获取到key相同的，就循环下一个位置
    for (Entry e = tab[i];
         e != null;
         e = tab[i = nextIndex(i, len)]) {
        ThreadLocal<?> k = e.get();//获取key
        if (k == key) {//key相同，直接覆盖
            e.value = value;
            return;
        }
		//若当前槽为过期槽，就清除和占用该过期槽
        if (k == null) {
            replaceStaleEntry(key, value, i);
            return;
        }
    }
	//走到这里说明目标位置是空的，构建元素放到存储数组中
    tab[i] = new Entry(key, value);
    int sz = ++size; //增加元素个数
   // 1. 启发式地扫描并清除过期的Entry。
   //2. 如果没有需要清除的并且需要扩容，则进行扩容
    if (!cleanSomeSlots(i, sz) && sz >= threshold)
        rehash();
}

void createMap(Thread t, T firstValue) {
        t.threadLocals = new ThreadLocalMap(this, firstValue);
}
//构造方法，初始化值
public ThreadLocalMap(java.lang.ThreadLocal<?> firstKey, Object firstValue) {
	    // 初始化table数组
	    table = new Entry[INITIAL_CAPACITY];
	    // 用firstKey的threadLocalHashCode与初始大小16取模得到哈希值
	    int i = firstKey.threadLocalHashCode & (INITIAL_CAPACITY - 1);
	    // 初始化该节点
	    table[i] = new Entry(firstKey, firstValue);
	    // 设置节点表大小为1
	    size = 1;
	    // 扩容阈值
	    setThreshold(INITIAL_CAPACITY);
	}

// 启发式地扫描并清除过期的Entry。
private boolean cleanSomeSlots(int i, int n) {
    boolean removed = false;
    Entry[] tab = table;
    int len = tab.length;
    do {
        // 从位置i的下一个开始搜索
        i = nextIndex(i, len);
        Entry e = tab[i];
        // 如果遇到过期Entry，清除
        if (e != null && e.get() == null) {
            n = len;
            removed = true;
            i = expungeStaleEntry(i);
        }
    } while ( (n >>>= 1) != 0);
    // 只要有过期Entry被移除就会返回true
    return removed;
}
