package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className Interrupt
 * @description：
 * @date 2017/6/8 18:34
 */
public class InterruptStopDemo extends Thread {
    public void run() {
        try {
            for (int i = 0; i < 50000; i++) {
                if (this.isInterrupted()) {
                    System.out.println("i will quit ");
                    throw new InterruptedException();
                }
                System.out.println("i=" + (i + 1));
            }
            System.out.println("~~~~testing~~~");
        } catch (InterruptedException e) {
            System.out.println("occur exception");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            InterruptStopDemo intThread = new InterruptStopDemo();
            intThread.start();
            Thread.sleep(20);
            intThread.interrupt();
        } catch (InterruptedException e) {
            System.out.println("main catch");
            e.printStackTrace();
        }
    }

}
