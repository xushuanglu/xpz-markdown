package com.yrxy.thread.interview.threadpool;

import java.util.concurrent.*;
/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadPoolRejectDemo
 * @description：自定义线程池拒绝策略
 * @date 2016/1/20 16:23
 */
public class ThreadPoolRejectDemo {
    public static class MyTask implements Runnable {

        public void run() {
            System.out.println(System.currentTimeMillis() + "thread id:" + Thread.currentThread().getId());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyTask task = new MyTask();
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>(4);
        ExecutorService es = new ThreadPoolExecutor(5, 5,
                0L, TimeUnit.MILLISECONDS,
                queue,
                Executors.defaultThreadFactory(),
                new RejectedExecutionHandler() {

                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        System.out.println(r.toString() + " is discard");
                        System.out.println(" log the rejectedExecution");
                    }
                });

        for (int i = 0; i < 200; i++) {
            es.submit(task);

        }
    }
}
