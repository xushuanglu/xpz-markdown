import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className VolatileTest
 * @description：
 * @date 2017/5/22 11:15
 */
public class TestAtomic implements Runnable {

    private static AtomicInteger number = new AtomicInteger(0);

    public void run() {
        increase();
    }

    public void increase() {
        number.getAndAdd(1);
    }

    public static void main(String[] args) throws InterruptedException {

        for(int i=0;i<100;i++) {
            number.set(0);
            count();
        }
    }

    private static void count() throws InterruptedException {
        TestAtomic ato = new TestAtomic();
        ExecutorService exec = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < 100000; i++) {
            exec.submit(ato);
        }
        exec.shutdown();

        while(!exec.isTerminated()){
        Thread.sleep(10);
        }


        System.out.println("number : " + number.get());
    }
}
