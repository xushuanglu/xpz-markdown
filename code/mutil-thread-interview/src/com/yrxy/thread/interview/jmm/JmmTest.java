package com.yrxy.thread.interview.jmm;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className JmmTest
 * @description：
 * @date 2017/5/22 16:45
 */
public class JmmTest {

    private static int x = 0, y = 0;
    private static int a = 0, b = 0;

    public static void main(String[] args)
            throws InterruptedException {
        for(int i=0;i<1000;i++) {
            runOutofOrder();
            Thread.sleep(10);
        }
    }

    private static void runOutofOrder() throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                a = 1;
                x = b;
            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                b = 1;
                y = a;
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("x is : " + x + ",  y  is :  " + y );
    }

}
