package com.yrxy.thread.interview.lock;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className LockEliminateDemo
 * @description：
 * @date 2017/6/28 17:17
 */
public class LockEliminateDemo {
    public static void main(String[] args) {
        long s1 = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            appendStr("LockEliminate ", "test");
        }
     System.out.println("花费:" + (System.currentTimeMillis()-s1)+" ms");
    }
    public static String appendStr(String s1, String s2) {
        StringBuffer sb = new StringBuffer();
        sb.append(s1);
        sb.append(s2);
        return sb.toString();
    }
}







