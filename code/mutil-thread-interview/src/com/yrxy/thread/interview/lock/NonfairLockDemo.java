package com.yrxy.thread.interview.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className MyNonfairLock
 * @description：
 * @date 2017/6/3 13:51
 */
public class NonfairLockDemo {

    private ReentrantLock lock = new ReentrantLock(false);

    public  void testLock(){
        try {
            lock.lock();
            System.out.println(Thread.currentThread().getName() +"获得了锁");
        }finally {
            lock.unlock();
        }
    }
    public static void main(String[] args) {
        NonfairLockDemo nonfairLock = new NonfairLockDemo();
        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName()+"启动");
            nonfairLock.testLock();
        };
        Thread[] threadArray = new Thread[10];
        for (int i=0; i<10; i++) {
            threadArray[i] = new Thread(runnable);
            threadArray[i].start();
        }
    }


}
