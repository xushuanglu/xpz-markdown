package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie
 * @className ContextSwitchTest
 * @description：
 * @date 2017/7/7 14:29
 */
public class ContextSwitchTest {
    private static final long count = 100000;
    public static void main(String[] args) throws Exception
    {
        concurrentTest();
        serialTest();
    }

    private static void concurrentTest() throws Exception
    {
        long start = System.currentTimeMillis();
        Thread thread = new Thread(new Runnable(){
            public void run()
            {
                int a = 0;
                for (int i = 0; i < count; i++)
                {
                    a += 5;
                }
            }
        });
        thread.start();
        int b = 0;
        for (long i = 0; i < count; i++)
        {
            b --;
        }
        thread.join();
        long time = System.currentTimeMillis() - start;
        System.out.println("count is : " +count+" , Concurrency：" + time + "ms, b = " + b);
    }

    private static void serialTest()
    {
        long start = System.currentTimeMillis();
        int a = 0;
        for (long i = 0; i < count; i++)
        {
            a += 5;
        }
        int b = 0;
        for (int i = 0; i < count; i++)
        {
            b --;
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("count is : " +count+"  , Serial：" + time + "ms, b = " + b + ", a = " + a);
    }

}
