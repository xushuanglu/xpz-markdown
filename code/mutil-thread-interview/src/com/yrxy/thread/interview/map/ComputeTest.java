package com.yrxy.thread.interview.map;

/**
 * @author huanglaoxie
 * @className ComputeTest
 * @description：
 * @date 2020/7/24 13:27
 */
public class ComputeTest {

    public static void main(String[] args) {
        int i = 8;
        int j = 16;
        System.out.println(i >> 1);
        System.out.println(j << 1);
        System.out.println(Integer.MAX_VALUE);//2147483647
    }

}
