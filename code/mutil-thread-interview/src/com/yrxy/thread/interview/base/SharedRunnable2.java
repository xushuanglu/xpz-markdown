package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className SharedRunnable2
 * @description：Runnable适合于实现资源的共享，而Thread却不能，对比SharedRunnable、SharedThread
 * @date 2017/6/9 15:14
 */
public class SharedRunnable2 implements Runnable {
    int ticket = 20;
    public synchronized void decrease() {
        while (ticket > 0) {
            ticket--;
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (ticket > 0 || ticket == 0) {
                System.out.println(Thread.currentThread().getName() + "卖了一张票，剩余票数为：" + ticket);
            }
        }
    }
    public void run() {
        decrease();
    }
    public static void main(String[] args) {
        SharedRunnable sharedRunnable = new SharedRunnable();
        Thread thread1 = new Thread(sharedRunnable, "窗口1");
        thread1.start();
        Thread thread2 = new Thread(sharedRunnable, "窗口2");
        thread2.start();
        Thread thread3 = new Thread(sharedRunnable, "窗口3");
        thread3.start();
    }
}
