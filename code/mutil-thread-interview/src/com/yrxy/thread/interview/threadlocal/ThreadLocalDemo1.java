package com.yrxy.thread.interview.threadlocal;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className Tools
 * @description：
 * @date 2017/7/9 16:13
 */
public class ThreadLocalDemo1 {
    public static ThreadLocal<String> threadlocal = new ThreadLocal<String>();

    public static class MyThread extends Thread {
        private static AtomicInteger atomicInt = new AtomicInteger();
        public void run() {
            for(int i=0;i<4;i++) {
                threadlocal.set(atomicInt.addAndGet(1) + "");
//                threadlocal.set(null);
                System.out.println("thread name is :" + this.getName() + " , value  is :" + threadlocal.get());
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            }
    }
    public static void main(String[] args) throws Exception {
        Thread th1 = new MyThread();
        Thread th2 = new MyThread();
        Thread th3 = new MyThread();
        th1.setName("Thread 1");
        th2.setName("Thread 2");
        th3.setName("Thread 3");
        th1.start();
        th2.start();
        th3.start();
    }

}
