package com.yrxy.thread.interview.threadpool;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className MaxValueDemo
 * @description：Integer.MAX_VALUE大小是2147483647
 * @date 2017/6/19 16:15
 */
public class MaxValueDemo {

    public static void main(String[] args) {
     System.out.println("Integer.MAX_VALUE  is : "+Integer.MAX_VALUE);
    }

}
