  //Java8的散列值优化函数,也叫扰动函数
  static final int hash(Object key) {
        int h;
        //右位移16位，正好是32bit的一半，自己的高半区和低半区做异或，
        //就是为了混合原始哈希码的高位和低位，以此来加大低位的随机性。
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

