package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className SharedThread
 * @description：Runnable适合于实现资源的共享，而Thread却不能，对比SharedRunnable、SharedThread
 * @date 2017/6/8 13:52
 */
public class SharedThread extends Thread {
    private  int  ticket = 100;
    public void  run() {
        synchronized(this) {
            while (ticket > 0) {
                ticket--;// 如果有票就卖掉1张
                System.out.println(Thread.currentThread().getName() + "卖了一张票，剩余票数为：" + ticket);
            }
        }
    }
    public static void main(String[] args) {
        SharedThread th1=new SharedThread();
        SharedThread th2=new SharedThread();
        SharedThread th3=new SharedThread();
        th1.start();
        th2.start();
        th3.start();
    }
}
