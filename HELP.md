
A
a clearance sale    清仓大贱卖ziyequma
a few            一些，少数几个
a little           少许，一点点
a great/good      deal of    许多，大量
a good many     很 多，相当多的
a great many     许多
a lot            许多，大量
a lot of          许多，大量
a matter of course  理所当然的事
a number of       许多，若干
a series of        一系列的
a case in point     例证，有关的例子
a great deal       许多
a great /good many   很多的
a pair of         一副，一双，一条，一把
a set of          一套，一组
a type of         一种
a wealth of       大量的
according to       根据，按照；符合
above all          首先  最重要的是
above board       在桌面上，公开的，诚实的
absent from        缺席
above sb’s head     超出某人的理解
above one’s breath   高声地
abandon oneself to   沉溺于
according to        根据，按照；据…所说
accuse sb of        指责某人干某事
acquire a habit      养成一种习惯
across the country   遍布全国
act on             奉行，作用于，影响
account for         解释，说明（原因）
accuse sb of sth     指控某人做某事
a couple of         两个，几个
act as             充当
a day off           不工作的一天
add to             增加
add up to          总计为
adhere to          坚持
admit of  …       容纳得下,  有…的余地
a dog’s life        群困潦倒
after all           毕竟
again and again     反复地，一再地
a good/great deal    大量，非常，极其
agree on           同意，赞同
agree with/about/on  同意，赞成，一致
ahead of           在…的前面，事先
ahead of time       提早
all along           一直地，一向
all around          全面的
all at once         突然
all but             差不多，几乎
all day             整天
all ears            全神贯注地听
all for            完全赞成
all in all           归根到底
all of a sudden       突然
all right           行，好的，（健康）良好的
all sorts of         各种各样的
all the more       更加
all the time        一直
all the year round    一年到头
all things considered  通盘来看
all through         始终，一直
all in all           总的来说
all over            到处，遍及
allow for          考虑到，顾及
allow of           容许，容许有…可能
along with         和…一起,陪伴
an entry visa       入境签证
an exit visa        出境签证
among other things  此外（还）
amount to        （数量上）答到，（意义上）等于
and so            所以
and so on          等等
answer for         对…负责
answer to          回答
an appetite for       对…的爱好
anything but         除…以外任何事（物）
angry with/at/for     生气
answer back        回嘴，顶嘴
anxious for/about     焦虑
apart from         且莫说
apply for           申请
apply...to           应用于
approve of          赞成
arm in arm          手挽手
arise from           由…引起
argue against/for      赞成/反对
around the clock      昼夜不停地
around the corner     在拐角处；即将到来
arrive at             到达（某地）
as regards            关于、至于
as a matter of  fact      事实上
as a consequence    因而，结果
at a disadvantage    处于不利地位
as a result of       作为…的结果
as a rule           通常、惯例
as a result of       作为……的结果
as bold as brass     极其胆大妄为
(as) cheap as dirt    偏宜透顶的
as dry as dust      枯燥无味的，另人厌烦的
as far as…be concerned       关于在，至于，就…而言
as good as         与…一样好
as soon as         一…就…
as / so long as     只要，如果，既然
as such           根据某词严格的意义
as sure as death     千真万确
as to             关于，至于
as for            至于，就…方面说
as/ so far as        就…而言，到…程度，至于
as if /though           好像，仿佛
as it happens       碰巧，偶然
as good as           几乎已经；实际已经
as long as         只要，长达…之久
as soon as          一… 就
as usual          照例，像往常一样
as well           同样，也
as well as         除……之外，既……又
ashamed of        对…感到羞耻
ask after          询问，问候
ask about sb       问候某人
ask for            请求，要求
assuming that      假定…
at a discount      打折
at a crisis          在紧急关头
at a distance        有相当距离
at a glance           看一眼，马上
at a loss             不知所措地
at a price            以极高的代价
at a time/at one time    一次/曾经
at all                完全，根本
at all costs          不惜一切代价
at all events /in any event   无论如何
at any rate            无论如何，至少
at any/all cost          论如何
at best / at the best      其量，最好充其量不过……
at breakfast          (正在)进早餐
at church             做礼拜
at close range         在近距离
at (one’s) ease         舒适，无拘无束
at discretion          随意，任意
at first               首先
at first sight          乍一看
at hand              在手边，在附近，即将到来
at heart              内心里
at home             在家；随便，无拘束
at intervals           间歇性地
at large            由于的，未受控制的；整个的，一般的
at last              最后，终于
at (the) least        至少，起码
at leisure           当某人有空时
at one’s leisure      当某人有空时
at length             详细地
at least              至少
at length           最后，终于；详尽地
at last               最后，终于
at liberty          可随意的；有空闲的
at long intervals    间或
at short intervals    常常
at intervals          不时，相隔一定的距离（或时间）
at no time            决不
at (the) most          最多，不超过
at once              立刻
at one blow         一举，一下子
at one’s back        支持某人
at one’s best        处在最佳状态
at one’s wits’ end    智穷计尽
at present          目前，现在
at random         随意地，任意地
at rest            宁静的，安静的，过世了的
at sb.’s disposal;  in sb’s disposal    任某人处理
at sb’s service      听某人吩咐
at sea             茫然
at school          在学校
at the discretion of   听凭处理
at the age of           在…岁时
at the cost of        以……为代价
at the end          结束，完结
at the expense of   以…为代价.由…付费
at the latest         最迟，至迟
at the rate of        以…的速度
at the same time      与此同时
at the sight of      看见
at the mercy of     由…支配
at the moment       此刻
at times            有时；间或
attend to           专注与……；照顾
at war             处于战争中
at work             在工作，活动着
avenge oneself on      对……进行报复
awaken to           使认识到
aware of            意识到
at will               任意地，随意地
to the best of one’s ability     尽力地
leave…about       到处乱放
go about  （着手）做
see about    开始做
go abroad    去国外
without accident       平安无事的
of one’s own accord      自愿的，自动的
in accordance with      根椐
on account of   因为
get sth. across        使人领会
in addition (to)        另外,除…之外
in advance    预先，提前
to one’s advantage  对某人有利
one after another   一个接一个
now and again      不时地，间或
over and over again   一再地
time and (time) again      多次，一再地
get ahead of        超过…
on the air        广播着，播放着
put on airs       摆架子
on (the) alert (for/against)        警戒着,对…保持戒备。
go all out         全力以赴
in all         总共，合计
make allowance(s) for    考虑到
leave sb. alone     别打扰…
let alone         更不用说
get along        进展，相处
in the last (final )analysis    归根到底，总而言之。
one another       相互，彼此
one way or another      以某种方式
in answer to        作为回答，响应
the Antarctic         南极
like anything      非常，十分讯速地
receive with open arms       热烈而高兴地，欢迎
the Atlantic （Ocean）    大西洋
pay attention to       注意
get away from         摆脱；逃避
give sb. away         背弃
right away ,straight away      立即，马上


B
Bachelor of Arts文学学士
Bachelor of Science理学学士
back and forth来回，往复
backwards and forwards来来回回
base sth. on/upon以……为基础，以……为根据
be about to do st.正要做某事
be abundant in,be abundant with有丰富的，有大量的
be against反对，违背
be amazed at对…感到吃惊
be able to能够
be about to be 即将，将要
be absorbed in专心于
be accustomed to习惯于
be acquainted with熟悉
be anything about根本不是
be around来访
be ashamed of为…羞愧
be buried in全神贯注于
be good for对…有用的,有力的
be grateful to sb. for sth.为某事而感激某
beat about the bush绕弯子说话
beat (cudgel , rack) one’s brain绞尽脑汁
beat time打拍子
beat up sb.痛打某人
be bad for对……有害
be badly off生活拮据；群困
be bound to一定
be based on/upon基于
be better off生活良好，生活更佳
be blessed with爱惠，在……方面有福
be child’s play容易之极
be content with对…满足
be capable of能够
be caught in遇到（雨等）
be clever at擅长于
be clever with善于使用
be concerned with关心，挂念，从事于
be combined with与…结合着
be composed of由…组成
be content with沉迷于
be crazy about热衷于，迷恋着
be curious about对…好奇
be destined to注定
be determined to do sth决心做某事
be delivered of（妇女）生下（孩子）
be disappointed with对…感到失望
be dying for; be dying to渴望，巴不得立即
be endowed with天生具有…，以…而告终
be engaged in从事于
be eager to do sth渴望做某事
be of no effect无效
be fond of爱好，喜爱
be familiar with与…熟悉
be known as以…知名，被认为是
be made up of由…组成
be in (the) fashion合乎时尚
be in habit of有…的习惯
be in contact with和…接触，有联系
be in the same boat命运相同
be in trouble处于困境
be familiar to为…所熟悉
be familiar with熟悉，通晓，精通
bend one’s knee to向……跪下，屈服于
be made from由……制造
be made up of由…组成，由…构成
be to blame应受责
be obliged to sb感谢（某人）
be on a diet; go on a diet节食
be on the verge of濒于，濒临
be on strike罢工
be out of (the) fashion 过时
be particular about讲究，挑剔
be pleased to do乐意做（某事）
be satisfied with对…心满意足
be strict with sb对某人严格
be with child怀孕
be up to够…，合格做…，有资格做…；该由…负责，在于…，取决于
be worthy of值得
because of由于，因为
become of发生……情况；……怎么啦
before long不久；一会儿后
Behave yourself！放规矩点！
behind the scenes在幕后
behind the times过时；落在时代后面
behind one’s back背着某人（说坏话）
believe in相信
below (under )one’s breath 低声地
benefit from获益于
because of因为
before long不久以后
bear in mind牢记
begin with以……开始
beneath one’s dignity有失某人身份
beside oneself with极度……
between you and me / between ourselves 咱俩私下说
beyond doubt无疑地，准确地
beyond sb.’s wildest dreams 出乎意料的，超过某人所望的
bit by bit一点一点地
bits and pieces七零八碎的小东西
black and blue（打得）青一块紫一块
black eye（打得）眼圈发黑
board of directors董事会
boarding pass登机牌
book in, book up订（车票，住处等）
both…and…两者都
blow away刮走
blow out吹息
blow up爆炸，充气，大怒
box one’s ears打某人一记耳光
break one’s heart使某人伤心
break away from与……脱离
break down(机器等)坏了；（身体）垮了
break forth突然迸发
break in非法进入；插嘴，打岔
break off中断
break out爆发
break the ice打破僵局
break (beat, cut) the record 破纪录
break through突破
break up打碎，拆散；结束，解散；
bring about实现，使发生
bring back带回来，使恢复
bring forth使产生
bring forward提出，提议，提前
bring into effect实现，实行
bring off使实现，做成
bring on促进，助长，引起
bring out出版，推出，使显露
bring through使（病人）脱险，安全度
bring up抚养，养育，提出
bring/carry/put into effect实行，使生效，实现
bring to a crisis使陷入危及
break away突然离开，强行逃脱
break down损坏，崩溃
break up打碎，散会，终止
break through突破
break in非法闯入，打断，插嘴
break into非法闯入
break into tears突然大哭
break off中断，突然停止
break out爆发，突然发现
break through突围，取得突破性成就
bring about导致，实现
bring …around使……转而同意（某中看法等）
bring back归还；带回；使回想起；使恢复，使苏醒
bring to an end使结束，使停止
bring to light发现，让人知道
bring…to reason使……清醒过来
bear with忍受，容忍
bear in one’s mind记住
be/get angry with sb生某人的气
build on/upon把…建立于，以…为思想（行为）的基础
busy with忙于
build up逐步建立，增进，增强
burn the midnight oil开夜车
burn down烧毁
burn off烧掉
burn out（火自行）熄灭；烧光某物
burst into闯进；突然开始
burst out (doing )爆发
but for 要不是
burst on突然出现，到来
burst out突然起来，大声喊叫
be used to习惯于
burn down烧毁
burn up烧毁
by accident偶然，碰巧
by air坐飞机
by all means当然可以
by and by不久，过一会儿
by and large总的来说
by chance偶然，碰巧
by comparison与…相比较
by contrast with和…相对比
by day/night在白天/晚上
by far…得多，尤其，显然地
by force凭借暴力，强迫地
by means of用，靠
by mistake出于误会
by no means并没有，决不
by oneself单独，独自
by hand用手
by means of通过…手段
by mistake错误的
by reason of由于，因为
by the way顺便说说，顺便提起
by way of途径，通过…的方法
by turns轮流地
feel bad about为……感到难受；抱歉
go bad腐败；变质
go from bad to worse每况愈下
go bankrupt破产
under the banner of以……名义（进行某项工作）
on the basis of根据
full of beans精力充沛的
go to bed去睡觉
in bed在床上；（在）睡觉
make the bed铺床
put to bed安顿……睡觉
to begin with第一；原先本来
on behalf of /on sb.’s behalf为了（……的利益）；代表，替代
fall behind落在后面
leave behind落下；望了带
human being人
You bet当然，一定
had better do…最好……
on better than与……一样；不比……好
few and far between稀少
give birth to生（产）；引起
every bit完全（一样）
to bits成为碎片
in black and white白纸黑字（写下来）
go blind失明
once in a blue难得一次
out of the blue出乎意料地
on board在火车上，在船上
keep body an soul together维持生命
hit the bottle酗酒
keep to the bottle酗酒
over a bottle (of wine )一面喝酒，一面……
from the bottom of one’s heart打心底里；由衷地
have sth. on the brain一心想着某事
earn one’s bread赚钱养家糊口
hold one’s breath（由于害怕）屏住呼吸
out of breath上气不接下气
waste one’s breath白费口舌
in brief简言之
on the brink of…濒临，正要……之际
on business因工作，因上午
go bust破产
one by one一个接着一个地
lay (put ) by储蓄

C
call away叫走
call back在来（访问）；给……回电
call by顺道来防
call for邀约；拿取（某物）；需要
call in召来；请来
call off取消
call on拜访
call on sb. to do sht.号召；呼吁
call to mind（使）想起
call up给……打电话；征召……入伍
can’t/couldn’t help禁不住
cannot help doing sth. 不禁……
catch one’s breath喘息，松一口气
catch up with赶上
come about发生，成为现实
come around/round恢复知觉；（又一度）到来
come from来自，是（某地）人
come across碰到
come into fashion开始风行
come out出版；开花，发芽；结果
come up to及得上，不亚于
come up with想出（计划，答复等），提出
come upon against遭到
connect联结起来
contribute to贡献于，有助于
convince sb. of使某确信
count on/count up指望/总计，
cover up掩盖，掩饰
catch(a)cold伤风；感冒
catch fire着火；燃着
catch sight of突然看见，瞥见
catch hold of拥有,抓住,得到
catch sight （of）望见
catch up with赶上，追上
call back回电话
call for叫（某人）过来，要求
call in叫进来，召来
call off取消
call on/upon访问，拜访，号召，要求
call at停放，拜访某地
call forth引起
call it a day收工，结束
capitalize on利用
care about看重某事，介意
care for照顾，照料，喜欢
carry off拿走，夺走
carry on继续，进行
carry out执行，实行，完成，实现
carry through坚持下去，完成，使度过困难
carry into effect实现，实行，实施
carry away拿走，使失去自制力
carry weight（说话等）有分量，有影响
catch one’s eye引人注目
certain of有把握，确信
change one’s mind改变主意
change sth with sb与某人交换某物
change sth for sth用某物换某物
change into转变，更换
charge with指控
check in（在旅馆，机场等）登记，报到
check up(on)检查，核实
check out结账离去，办妥手续离去
check over核对，检查
cheer up（使）高兴起来，振作起来
clean out打扫干净；清除
clear the air消除误会（猜疑）
clear away把…清除掉，收拾
clear off离开，溜掉
clear out清除，把腾空，赶出
clear up清理，解决，（天）放睛
critical of对…挑剔
consist of/in构成/在于
cool down/off凉快起来，使平静
cannot…enough无论怎样…都不够
compare notes交流经验
come to the point扼要地说
competent in胜任的
combine…with…把…与…结合起来
come about发生，产生
come across偶然遇见，碰上
come along出现，发生，进展
come apart破碎，崩溃
come around/round苏醒，复原，顺便来访
come at攻击，冲向，达到，了解
come by得到，访问，看望
come down to可归结为
come on（表示鼓励，催促等）快，走吧，开始
come out出版，发表，出现，结果是
come through安然度过，经历……仍然活着
come to苏醒，结果是，涉及
come true实现
come up出现，发生，走上前来
come into effect/operation生效/开始运行
come up with提出，提供，想出
compare…to…比拟，比作
concentrate on/upon集中，专心于
connect up连起来，接上
connect with和…有联系，和…有关
consume away消耗掉，憔悴，枯萎凋谢
confident of/in有信心
content oneself with满足于
consistent with一致的
contrary to与相反
contribute to有助于…，促成
convince sb of使某人承认，使某人信服
convenient to便于
convince oneself of充分弄明白
cope with应付，克服
correspond with与…一致，符合，　与…通信
correspond to与…一致，符合，等于，相当于，与…相似
count on/upon指望，依靠
crack down镇压，取缔，扫荡
cross out规划
cut back削减，修剪（树枝）
cut down削减，压缩，砍倒
cut in插进来说，插嘴
cut off切断，隔绝，挡住
cut out删掉，停止
cut short中断，打断
with care留心，注意
put the carry before the horse本末倒置
in any case无论如何
in case万一
in case of万一发生……的时候
objective case宾格
nominative case主格
cassette tape recorder盒式录音机
It rains cats and dogs大雨滂沱
for certain of弄明白，确定
for a change换换环境
in chaos混乱，纷乱
in chorus一齐；齐声
go to the cinema看电影
under the circumstances在那种情况下
under on circumstance无论如何
keep clear of避开
make clear表明；讲清楚
put/turn the clock back把时钟拨回；复旧
work against the clock拼命赶时间
give sb. the cold shoulder冷淡地对待某人
in cold blood残酷无情地
pout/throw cold water on泼冷水；使泄气
in command (of )指挥；控制
in comparison with与……相比较
on condition(that)…在……条件下
in conformity with遵照
for conscience’(s) sake为了问心无愧
have a guilty conscience内疚
make contact with sb. (sth.)与某人（事）联系
on the contrary相反
in control指挥，管理，支配某事
out of control失控
under control控制，控制住
round the corner在拐角处不远；就在眼前
to one’s cost吃了苦头之后才……
of course当然，自然
in the course of在 ……过程中，在……中
under the cover of在……得掩护下



D
dash off急匆匆地写，飞出，急忙离开
day in, day out日复一日
deal in从事于，经营，做…买卖
deal out分配，给予，执行
deal with与…交往（有生意往来），应付，涉及，研究
deliver oneself to向…自首
devote to把…献给，把…专用于
devote oneself to致力于，献身于，专心于
deceive sb into doing sth骗某人做某事
deceive oneself骗自己，误解，想错
decide on对…做出决定
deep down在内心深处，根本上
depart from背离，违反，离开
depend on依靠，依赖
despite all that尽管如此
despite of不管，不顾
devote oneself to献身于，致力于，沉溺于
dry out戒酒，变干，安静
due to因为，由…引起
doubtful about/of怀疑
do one’s best尽力，努力
do away with废除，去掉
dowonders创造奇迹
different from不同
die away逐渐减弱，逐渐模糊
die out逐渐消失，灭绝
die of死于
die off相继死去
dig up掘出
distinguish…from…辨别，把…和…区别开
do away with废除，消灭
do without没有……也行
do sb a favor帮某人个忙
do good/harm to对…有利/有害
doze off打盹，走神
dozens of很多
drag in把…拉（牵涉）进来
drag on拖延，使拖延
draw in收网
draw on运用，利用
draw up起草，制订
dream of梦见，梦想，考虑
dream up凭想象虚构
dress up穿上盛装；装扮，修饰
drop back; drop behind 落后
drop by/in顺便访问一下,偶然到访
drop out退出，退学
drop on sb顺便走访（某人）
drop out退出，（中，小学生）中途退学
drop off睡着；(让…)下车
dwell on详述，长谈，凝视
due to应归功于
（be）a dab (hand) (at sth.)……能手
out of date过时的，陈就的
up to date现代的，最新的
to date迄今
in all one’s born days一生中
of the day当代的，当时的
in depth深入的（地），纵深的（地）
out of one’s depth; beyond one’s depth深不着地；非…所能理解,为…力所不及
go into detail(s)详细叙述
in detail详细地，逐渐地
leave to one’s own device听任…自便
go to the devil完蛋；滚开，见鬼
make a difference区别对待；有关系
split the difference折中，妥协
make a difficulty; make difficulties刁难；提出异议
stand on one’s dignity保持尊严
treat sb. as dirt; treat sb. like a piece of dirt瞧不起某人，视某人如草芥
to the disadvantage of sb.; to sb’s disadvantage对某人不利
with discretion慎重地
dispose of丢掉，处理，解决
keep one’s distance保持距离
keep sb. at a distance冷淡某人，与某人保持距离
distinguish oneself表现杰出，使受人注意
have to do with; be to do with与…有关
go to the dogs潦倒,堕落
love me, love my dog爱物及乌
put on (the) dog摆架子，装腔作势
wake a sleeping dog惹麻烦
work like a dog拼命工作
over and done with完结，了结
What is done can’t be undone木已成舟
（from）door to door从一处直接至另一处，挨门逐户地
out of doors在户外
packed to the doors被挤的水泄不通
on the dot准时地
in doubt不确定的，可怀疑地
no doubt确定地，我确信，我 想；很可能，公认地
like a dream轻而易举地，完美地
run dry流干，被耗尽
give sb. his due公道地对待，给某人应有的评价
(down) in the dump沮丧的，抑郁的
in duplicate一式两份的（地）
in the dust入土的
off duty下了班的
on duty上班的


E
each other互相
either …or…既不…也不…
even if 即使，纵然
ever since 从…起一直到现在
except for 除…之外
ease off减轻（痛苦，紧张状态等）
enjoy oneself享乐，过得快乐
establish oneself in　 定居于，在…落户
establish sb as任命某人担任
expose sth to将某物暴露
equal to等同于
equivalent to相当于
excited about/at为某事而激动
eager for/to do急于做
up to one’s ears in深深卷入，深陷于，忙于
earn one’s living谋生
in earnest认真地
on earth究竟，到底；在世界上，在人间
in at ease局促不安，不自在
eat one’s words承认说错
on the ebb正在衰落
come into effect ;go into effect开始实行，开始生效
have an effect on sth.对…有影响，有效力
in effect实际上，正在实行，有效
to the effect that…大意是，以便
make an effect作出努力
spare no effect不遗余力
either …or…要么…要么…，不是…就是…
in one’s element很适应，得其所
out of one’s element不适应，不得其所
in embryo萌芽的，未发展的，在筹划中的
lay /put /place emphasis on把重点放在
from end to end最终，结果
in the end最终，结果
on end竖着；连续地
make ends meet使收支相抵，量入为出
put an end to结束，终止，废除
end in以……为结束；以……面告终
end up结束，告终
make an enemy树敌
engage in从事于，使忙于
enjoy oneself过的快活，尽情玩
enter for报名参加
even if/though即使，虽然
evening dress夜礼服
evening school夜校
in the event结果，到头来
in the event of倘若，万一
for ever永远
ever since从…以来
ever so非常
every now and then; every now and again间或，不时的
every other 每隔
for example例如
set a good example树立好榜样
except for只要，要不是
to the exclusion of sb.(st)排除（其他的一切）
in excuse of作为…的辩解
make an excuse for为…找一个借口
exert oneself努力，尽力
come into existence出现，成立，产生
make an experiment; carry out an experiment进行实验（试验）
in someone’s eyes在…心目中；在…看来
keep an eye on留神，密切注意，照看
see eye to eye( with sb.)(与某人)想法一致
set eyes on看，望着
up to one’s eye in忙于



高频词

Lesson 1
1、abandon [ə'bændən] vt. 放弃，抛弃
He claimed that his parents had abandoned him他声称父母遗弃了他。
2、absent ['æbsənt] a. 缺席的，不在的
He has been absent from his desk for two weeks他已经有两个星期没有坐在办公桌前了。
【派】 absence n. 缺席，不在场  【反】 present a. 出席的，在场的
【考】 be absent from 缺席
3、absolute ['æbsəluːt] a. 绝对的，完全的
It's not really suited to absolute beginners它并不真正适合从零开始的初学者。
4、absorb [əb'sɔːb] vt. 吸收；吸引
Plants absorb carbon dioxide from the air and moisture from the soil植物从空气中吸收二氧化碳并从土壤中汲取水分。
5、abuse [ə'bjuːs] n. 滥用；虐待，辱骂
I was left shouting abuse as the car sped off  汽车飞驰而去，留下我在那里大声咒骂。
6、academic [ækə'demik] a. 学院的，学术的
Their academic standards are high他们的教学水平很高。
7、accept [ək'sept] v. 接受，认可
Eventually Sam persuaded her to accept an offer of marriage最终萨姆说服她接受了求婚。
8、accident ['æksidənt] n.意外事故，偶然的事【考】by accident 碰巧
She discovered the problem by accident她碰巧发现了这个问题。
9、accompany [ə'kʌmpəni] v. 陪伴，伴随
Ken agreed to accompany me on a trip to Africa肯答应陪我一起去非洲。
10、accomplish [ə'kɔmpliʃ] v. 完成
If we'd all work together, I think we could accomplish our goal只要大家齐心协力，我想我们就能实现目标。
11、account [ə'kaunt] n. 账目；解释；描述 v. 说明，解释
The flight was postponed on account of bad weather. 因为天气恶劣，飞行延期了。
【考】 on account of 因为，由于；account for 对……做出解释
12、accumulate [ə'kjuːmjuleit] v. 积累；堆积
Households accumulate wealth across a broad spectrum of assets家庭在以各种各样的资产形式积累财富。
13、accurate ['ækjurit] a. 准确的，精确的
Police have stressed that this is the most accurate description of the killer to date警方强调这是迄今对凶手最精确的描述。
14、accuse [əkjuːz] vt. 指控，指责，谴责；控诉
Accuse sb of doing sth charge sb with doing sth. 因为某事指控某人，指控某人做了某事。
【考】 accuse sb. of.... 指控某人……  【近】 charge
15、accustomed [ə'kʌstəmd] a. 惯常的，习惯的
It will not take you very long to be accustomed to the way we do it.你不必花很长的时间就能习惯我们做事的方法。
【考】 be accustomed to + n. 习惯于
16、ache [eik] n./v. 疼痛，酸痛
Her head was throbbing and she ached all over她的脑袋嗡嗡作响并且浑身疼痛。
17、achieve [ə'tʃiːv] v. 完成， 达到，实现，获得
There are many who will work hard to achieve these goals有志之士将会共同为实现这些目标而努力。
18、acquaintance [ə'kweintəns] n. 熟人，相识
The proprietor was an old acquaintance of his
业主是他的一位旧相识。
19、acquire [ə'kwaiə] v. 取得，学到
I've never acquired a taste for wine
我一向喝不惯葡萄酒。
20、act [ækt] n. 行为，动作 v. 行动；起作用；演戏
The deaths occurred when police acted to stop widespread looting and vandalism警方采取行动制止猖狂的打砸抢行为时有多人丧生。
21、active ['æktiv] a. 活跃的，积极的；在活动中的
Having an active youngster about the house can be quite wearing.
家里有个好动的小孩会是相当累人的。
22、activity [æk'tiviti] n. 活动You can take part in activities from canoeing to bird watching.
你可以参与从划独木舟到观鸟等各种活动。
23、actual ['æktjuəl] a. 实际的；现实的The segments are filmed using either local actors or the actual people involved这些片断是由当地的演员或真实的当事人参与拍摄的。
24、add [æd] v. 添加；加
Sincere performances and gritty Boston settings add to the film's realism.
真诚的表演和对波士顿这一背景城市的真实刻画增添了电影的现实主义色彩。
【考】 add…to 把……加到……上；add up to 合计达，总数达
25、address [ə'dres] n. 地址，通讯处 n./v. 演讲，致词
Applications should be addressed to: The business affairs editor.
申请表应寄给；商务编辑。
26、adequate ['ædikwit] a. 足够的，恰当的One in four people worldwide are without adequate homes
全世界有1/4的人没有个像样的家。
27、adjust [ə'dʒʌst] v. 调节，调整，使适应We have been preparing our fighters to adjust themselves to civil society
我们一直在培训我们的战士，以使他们适应普通的社会生活。
28、administration [əd mini'streiʃən] n.管理（部门），行政Too much time is spent on administration.
在管理上花费了太多时间。
29、admire [əd'maiə] v. 羡慕，赞赏，钦佩We took time to stop and admire the view.
我们特意驻足欣赏风景。
30、admit [əd'mit] v. 承认；接纳
I am willing to admit that I do make mistakes
我愿意承认我确实会犯错。
31、adopt [ə'dɔpt] v. 采用，采纳；收养
Parliament adopted a resolution calling for the complete withdrawal of troops
议会采纳了要求全部撤军的决议。
32、adult [ə'dʌlt] n. 成人Becoming a father signified that he was now an adult
当了父亲意味着他已是个成年人了。
33、advance [əd'vɑːns] n. 前进，进展 v. 推进，促进；前进Everything has been fixed in advance.
一切都是事先确定好了的。
【考】 in advance 提前，预先
34、advanced [əd'vɑːnst] a. 先进的，高级的Without more training or advanced technical skills, they'll lose their jobs.没有进一步的培训或高级技能的话，他们就会丢掉工作。
35、advantage [əd'vɑːntidʒ] n. 优点；有利条件，好处We can take advantage of new technology more quickly by farming out computer operations把计算机业务分包出去，我们就可以更快地利用新技术。
【考】 take advantage of 利用
36、adventure [əd'ventʃə] n. 冒险，惊险活动
I set off for a new adventure in the United States on the first day of the new year.
新年第一天，我在美国开始了一次新的冒险。
37、advertise ['ædvətaiz] vt. 为……做广告The players can advertise baked beans, but not rugby boots球员可以给烤菜豆做广告，但不能代言橄榄球靴。
38、advise [əd'vaiz] vt. 忠告，劝告；建议
We advised that he should leave early. 我们建 议他早点出发。
【考】 advise 后接 that 从句时须用虚拟语气，即
“（should+） 动 词 原 形 ”；advise sb. to do  sth. 建议某人做某事
39、affair [ə'fɛə] n. 事，事情，事件All their beds were distinctive; Mac's was an iron affair with brass knobs他们的床都各具特色；马克的床是张带球形黄铜柱头的铁家伙。
40、affect [ə'fekt] vt. 影响Nicotine adversely affects the functioning of the heart and arteries
尼古丁会对心脏及动脉功能造成损害。
41、affection [ə'fekʃən] n. 爱，感情She thought of him with affection
她很想念他。
42、afford [ə'fɔːd] vt. 提供；负担得起，买得起My parents can't even afford a new refrigerator
我父母甚至买不起一台新冰箱。
43、afraid [ə'freid] a. 怕的，害怕的
She did not seem at all afraid
她毫无惧色。
44、ago [ə'gəu] ad. 以前，……前He was killed a few days ago in a skiing accident
几天前他在一场滑雪事故中丧生。
45、agree [ə'griː] vi. 同意；赞同
【考】 agree with sb. 同意某人的意见；agree to do sth. 同意做某事
If we agreed all the time it would be a bit boring, wouldn't it?
如果我们总是意见一致，就会有点无趣，对吗？
46、aggressive [ə'gresiv] a. 侵略的，好斗的Some children are much more aggressive than others
一些孩子比其他孩子更好斗。
47、ahead [ə'hed] ad. 前头，在前【考】 ahead of 在……前面，先于Thanks to your help, we accomplished the task ahead of schedule.
亏得你们帮忙，我们才提前完成了任务。
48、aid [eid] n./v. 帮助，援助【考】 first aid 急救
They have already pledged billions of dollars in aid.
他们已经许诺援助几十亿美元。
49、aim [eim] v. 瞄准；致力 n. 目标，目的
He is aiming for the 100 metres world record
他志在打破100米世界纪录。
50、alarm [ə'lɑːm] n. 惊恐，警报（器），闹钟 v. 使惊恐；向……报警The news was greeted with alarm by MPs
得知这个消息，议员们忧心忡忡。
Lesson 2
51、alcohol ['ælkəhɔl] n. 酒精，酒Do either of you smoke cigarettes or drink alcohol?
你们俩有人吸烟或喝酒吗？
52、alike [ə'laik] a. 相同的，相像的We looked very alike.
我们长得很像。
52、alive [ə'laiv] a. 活着的；有活力的
She does not know if he is alive or dead
她不知道他是生是死。
53、all [ɔːl] a. 全部的，所有的 pron. 全部，一切 ad. 完全，都
【考】 after all 毕竟；at all 完全，根本；all over 到处，遍及；in all 总共，共计
54、allow [ə'lau] vt. 准许The children are not allowed to watch violent TV programmes
儿童不准收看含暴力内容的电视节目。
【考】 allow sb. to do sth. 允许某人做某事；allow doing sth. 允许做某事
55、alone [ə'ləun] a. 独自的，单独的 ad. 独自地，单独地
He did not have enough money to have the tire patched up, let alone buy a new one.
他的钱还不够补这个轮胎，更别提买个新的了。
【考】 let alone 不打扰；更别说
56、along [ə'lɔŋ] ad. 向前 prep. 沿着
Newman walked along the street alone
纽曼独自走在街上。
58、although [ɔːl'ðəu] conj. 虽然，即使
Although he is known to only a few, his reputation among them is very great
虽然知道他的人不多，但他在这些人中名声却很响。
59、altogether [ ɔːltə'geðə] ad. 完全，总之，全部
When Artie stopped calling altogether, Julie found a new man
当阿蒂彻底不再上门后，朱莉又找了个男人。
60、always ['ɔːlweiz] ad. 总是，永远Whenever I get into a relationship, I always fall madly in love
我每次谈恋爱都深陷其中，无法自拔。
61、amaze [ə'meiz] vt. 惊奇，惊叹；震惊
He amazed us by his knowledge of Welsh history
他对威尔士的历史知之甚多，让我们惊讶。
62、among [ə'mʌŋ] prep. 在……之中，在……之间
They walked among the crowds in Red Square.
他们在熙熙攘攘的红场散步。
63、ambition [æm'biʃən] n. 雄心，野心
His ambition is to sail round the world
他的梦想是环球航行。
64、ambulance ['æmbjuləns] n. 救护车We got an ambulance and rushed her to hospital
我们叫了一辆救护车，赶紧把她送到了医院。
65、amount [ə'maunt] n. 数量，总额 vi.（to）总计，等于【考】 a large amount of The system wastes a large amount of water.
这套系统浪费了大量的水。
66、amuse [ə'mjuːz] v. 给……以消遣，给……以娱乐
The thought seemed to amuse him
这种想法好像把他逗乐了。
67、analysis [ə'næləsis] n. [ 复数 -ses] 分析，解析
Her criteria defy analysis
她的标准让人搞不明白。
68、analyze ['ænəlaiz] v. 分析，分解We should analyze the cause and effect of this event.
我们应该分析这场事变的因果。
69、ancient ['einʃənt] a. 古代的，古老的They believed ancient Greece and Rome were vital sources of learning.
他们认为古代希腊罗马是知识的重要发源地。
70、anger ['æŋgə] n. 愤怒，生气He cried with anger and frustration
他愤怒而又沮丧得哭了起来。
71、angle ['æŋgl] n. 角，角度，观点The boat is now leaning at a 30 degree angle.
船正以30度角倾斜。
72、angry ['æŋgri] a. 愤怒的，生气的【考】 be angry at/with/about be angry with 后面常接的是人;be angry at后面常接的是事 Please don't be angry with me.请不要生我的气 Don't be angry at what I've done. 别因为我所做的事而生气。
73、announce [ə'nauns] vt. 宣布，宣告He will announce tonight that he is resigning from office
他将于今晚宣布辞职。
74、annoy [ə'nɔi] v. 使烦恼，使不悦，打搅Try making a note of the things which annoy you
试着把烦心事写下来。
75、annual ['ænjuəl] a. 每年的，每年度的
In its annual report, UNICEF says at least 40,000 children die every day.
在其年度报告中，联合国儿童基金会称每天至少有4万名儿童死亡。
76、anxious ['æŋkʃəs] a. 忧虑的，焦急的【考】 be anxious about/for sb./sth. be anxious about为…而担心 be anxious for急切盼望,渴望
77、anyhow ['enihau] ad. 无论如何，不管怎样
I wasn't qualified to apply for the job really but I got it anyhow.
实际上我并不具备申请这份工作的资格，但不管怎样，我还是被录用了。
78、apart [ə'pɑːt] ad. 分离，隔开；相距，相隔【考】 apart from 除了……以外
Apart from Bobby, all the children like music. （除了鲍比，所有的孩子都喜欢音乐。）
79、apologize/-ise [ə'pɔlədʒaiz] vi. 道歉I appologize to my teacher for my late for school.
我因为迟到向老师道歉.
【考】 apologize to sb. 向某人道歉；apologize to sb. for sth./doing sth. 为某事 / 做了某事向
某人道歉
80、apparent [ə'pærənt] a. 明显的， 表面上的I was a bit depressed by our apparent lack of progress
我对我们看似缓慢的进展感到有些沮丧。
81、appear [ə'piə] n. 出现，出场，问世 v. 好像是，仿佛There appears to be increasing support for the leadership to take a more aggressive stance
好像有越来越多的人支持领导层采取更为强势的立场
82、appearance [ə'pirəns] n. 出现；外表，外观
It was the president's second public appearance to date
这是总统到那时为止的第二次公开露面。
83、appetite ['æpitait] n. 食欲，胃口
He has a healthy appetite
他胃口很好。
84、apply [ə'plai] vt. 应用；申请【考】 apply for 申请，请求；apply to 致力于，适用于
85、appoint [ə'pɔint] v. 任命，委托；约定，指定
It made sense to appoint a banker to this job
指派一位银行家做这份工作是明智之举。
86、appreciate [ə'priːʃieit] v. 欣赏；感激【考】 appreciate doing
I appreciate you doing this.
我很感谢你做的这些事情
87、approach [ə'prəutʃ] n. （to） 接 近； 途 径， 方 法vt. 靠近，接近He didn't approach the front door at once
他没有马上走向前门。
88、appropriate [ə'prəupriət] a. 适合的，恰当的
It is appropriate that Irish names dominate the list
名单上大部分是爱尔兰名字，这没什么不合适的
89、approve [ə'pruːv] vt. 批准 vi. 赞成【考】 approve of 赞成……Not everyone approves of the festival
不是所有人都赞成庆祝该节日。
90、approximately [ə'prɔksimitli] ad. 近似地，大约In the course of the 1930s steel production in Britain approximately doubled
在20世纪30年代，英国的钢铁产量几乎翻了一番。
91、area ['eəriə] n. 面积；地区；领域60 years ago half the French population still lived in rural areas.
60年前有一半法国人仍然生活在乡村地区。
92、argue ['ɑːɡjuː] vi. 争辩，争论【考】 argue with sb. over/about sth. 就某事同某人争论He is arguing with his mum.他正在和他的妈妈争论。They argued about this issue yesterday.他们昨天就这个问题发生争论。
93、arise [ə'raiz] vi. 出现，形成【考】 arise from 由……引起
Noisome vapours arise from the mud left in the docks
难闻的蒸汽由船坞中余留的泥浆散发出来。
94、arouse [ə'rauz] vt. 引起，激起，唤起；唤醒
We left in the daytime so as not to arouse suspicion.
我们在白天离开以避免引起怀疑。
95、arrange [ə'reindʒ] vt. 安排，布置
I've arranged to see him on Friday morning
我已安排好星期五上午见他。
96、arrest [ə'rest] n./v. 逮捕，拘捕The sufferer may have to make major changes in his or her life to arrest the disease
患者可能必须对生活习惯作出重大调整以控制病情。
97、arrive [ə'raiv] v. 到来，到达；达成，得出【考】 arrive at+ 小地点； arrive in+ 大地方arrive in Beijing     arrive at school      arrive at the park
98、artificial [ɑːti'fiʃəl] a. 人工的，人造的；不自然的The city is dotted with small lakes, natural and artificial
这个城市小型湖泊星罗棋布，有天然的也有人工的。
99、ashamed [ə'ʃeimd] a. 惭愧的；羞愧的【考】 be ashamed of/to do/that 对……感到羞愧I felt incredibly ashamed of myself for getting so angry
我对自己发那么大的火而深感惭愧。
100、aspect ['æspekt] n. （问题、事物等的）方面
Climate and weather affect every aspect of our lives
气候和天气影响着我们生活的方方面面。
Lesson 3
101、assemble [ə'sembl] v. 集合，集会；装配，组装There wasn't even a convenient place for students to assemble between classes
在课间，学生们连一个方便凑在一起的地方都没有。
102、assembly [ə'sembli] n. 集会，会议；装配
He waited until complete quiet settled on the assembly.
他一直等到集会的人群完全安静下来。
103、assign [ə'sain] v. 分配，分派；选派When I taught, I would assign a topic to children which they would write about
我教课时会给孩子们布置一个题目，让他们就这个题目写点东西。
104、assist [ə'sist] v. 援助，帮助
Julia was assisting him to prepare his speech
朱莉娅正在帮他准备讲演稿。
105、assume [ə'sjuːm] v. 认为；假定；承担
It is a misconception to assume that the two continents are similar
关于这两块大陆相似的假设是一种误解
106、assure [ə'ʃuə] v. 保证，使确信【考】assure sb. of sth. I will ensure that the car arrives by six o'clock.我保证汽车六点钟以前到。
107、astonish [əs'tɔniʃ] vt. 使惊讶【考】be astonished at sth. 对某事感到惊讶My news will astonish you
我的消息会让你感到十分惊讶的。
108、atmosphere ['ætməsfiə] n. 大气；大气层；气氛he Partial Test-Ban Treaty bans nuclear testing in the atmosphere.
《部分禁止核试验条约》禁止在大气层中进行核试验。
109、attach [ə'tætʃ] vt. 系；贴；附加
We attach labels to things before we file them away
存档前，我们先贴上标签。
110、attack [ə'tæk] n. 攻击，抨击 v. 攻击，袭击【考】make an attack on/upon 向…进攻，攻击；The commander disposed his forces so as to make an attack on the enemy.
司令官对他的部队做了部署以便进攻敌军。
111、attain [ə'tein] v. 达到；完成；获得
Jim is halfway to attaining his pilot's licence.
吉姆就快要拿到飞行员执照了。
112、attempt [ə'tempt] n. 努力，尝试 vt. 尝试，试图The only time that we attempted to do something like that was in the city of Philadelphia
只有在费城那次我们曾试着做那样的事
【考】an attempt to 力图，试图；make an attempt at/on 打算，试
113、attend [ə'tend] vt. 出席Thousands of people attended the funeral
数千人参加了葬礼。
114、attention [ə'tenʃən] n. 注意，关心【考】 pay attention to 注意You have my undivided attention
我专心听你所讲。
115、attitude ['ætitjuːd] n. 态度，看法Being unemployed produces negative attitudes to work
失业会产生对工作的消极态度。
116、attract [ə'trækt] vt. 吸引，引起The Cardiff Bay project is attracting many visitors
加的夫海湾项目吸引了众多游客。
117、audience ['ɔːdiəns] n. 听众，观众
The entire audience broke into loud applause
全场观众爆发出热烈的掌声。
118、author ['ɔːθə] n. 作者；作家Haruki Murakami is Japan's best-selling author.
村上春树是日本当红的畅销书作家。
119、automobile ['ɔːtəməbiːl/'ɔːtəu] n. 汽车The parts of an automobile are standardized.
汽车零件是标准化了的。
120、available [ə'veiləbəl] a. 可利用的，可得到的
Since 1978, the amount of money available to buy books has fallen by 17%
自1978年以来，可用于采购图书的经费已经减少了17%。
121、average ['ævəridʒ] a. 平均的；普通的 n. 平均数【考】 on the/an average 平均The fish were counted and an average weight recorded.
数了数鱼的数量，并把平均重量记录了下来。
122、avoid [ə'vɔid] v. 避免，避开【考】 avoid doing sth. 避免做某事I must avoid doing such a thing. 我必须避免做这样的事
123、awake [ə'weik] a. 醒着的【反】 asleep
I don't stay awake at night worrying about that
我并没有因为那件事而担心得彻夜不眠。
124、award [ə'wɔːd] n. 奖品 vt. 授予，奖给【考】 award sb. sth. 奖给某人某物She presented a bravery award to schoolgirl Caroline Tucker.
她向女学生卡罗琳·塔克颁发了英勇奖状。
125、aware [ə'wɛə] a. 意识到的，知道的【考】 be aware of 知道，意识到
Smokers are well aware of the dangers to their own health
吸烟的人都知道吸烟对自身健康的危害。
126、awful ['ɔːful] a. 使人畏惧的，可怕的We met and I thought he was awful
我们见了面，我觉得他很讨人厌。
127、awkward ['ɔːkwəd] a. 笨拙的；尴尬的
I was the first to ask him awkward questions but there'll be harder ones to come
我是第一个向他提出尴尬问题的人，不过还会有人问更难回答的问题。
128、background ['bækɡraund] n. 背景，经历Moulded by his background, he could not escape the traditional role of strict father
受成长环境的影响，他摆脱不了严父的传统角色
129、balance ['bæləns] n. 平衡 v. 使平衡，保持平衡
【考】 keep/lose one’s balance 保持 / 失去平衡
130、bargain ['bɑːgin] n. 特价商品，便宜货 v. 讨价还价【考】 bargain with sb. 与某人讨价还价At this price the wine is a bargain
这种葡萄酒卖这个价真是很便宜。
131、barrier ['bæriə] n. 栅栏；障碍（物）Duties and taxes are the most obvious barrier to free trade.
关税和税收是自由贸易的最大壁垒。
132、base [beis] v. 把……建在，以……为基础 n. 基地，基础There was a cycle path running along this side of the wall, right at its base
在墙的这边，沿着墙根有一条自行车道。
【考】 be based on 以……为基础；on the bases of基于【派】 basic a. 基本的，基础的
133、battle ['bætl] v. 与……作战 n. 战斗，战役
He was appalled to discover members of the board fighting damaging personal battles.
他惊恐地发现董事会成员正在进行你死我活的私人争斗。
134、bear [bɛə]（bore，borne/born） vt. 忍受；生育he can bear off the blow. 她能忍受这次打击
135、beat [biːt]（beat, beaten） v. 敲 打； 跳 动； 打 败n.（音乐）节拍；敲打My wife tried to stop them and they beat her
我妻子想阻止他们，他们就打了她。
136、beauty ['bjuːti] n. 美，美丽；美人
Everyone admired her elegance and her beauty.
人人都羡慕她的优雅和美丽。
137、because [bi'kɔːz] conj. 因为【考】 because of 由于，因为It is mainly because of my fault.
这主要是由于我的过错。
138、behalf [bi'hɑːf] n. 利益；代表【考】 on behalf of/ on one’s behalf 代表某人
She made an emotional public appeal on her son's behalf
她代表儿子动情地发出了公开呼吁。
139、behavio（u）r [bi'heivjə] n. 行为，举止；表现
His behavior was called provocative and antisocial.
他的行为被认为是煽动性的和反社会的。
140、belong [bi'lɔŋ] vi. 属于【考】 belong to 属于
The house had belonged to her family for three or four generations.
这套房子归她们家有三四代人了。
141、bend [bend] v. （使）弯曲
I bent over and kissed her cheek
我俯身亲吻了她的脸颊。
142、benefit ['benifit] v. 有益于；得益于n. 利益，好处
【考】表示“得益于”时常与 from/by 连用【派】 beneficial a. 有益的
All the cells and tissues in the body benefit from the increased intake of oxygen.
体内所有的细胞与组织都从增加的氧气吸入量中受益。
143、bet [bet] n./v. 打赌
Jockeys are forbidden to bet on the outcome of races
职业赛马骑师禁止对赛马结果下注。
144、bind [baind] vt. 捆绑，捆扎
It is the memory and threat of persecution that binds them together.
遭受迫害的记忆和威胁把他们紧紧联系在一起。
145、blame [bleim] vt. 指责；把……归咎于
She seemed to be placing most of the blame on her mother
她好像把大部分责任都推到了母亲身上。

The policy is partly to blame for causing the worst unemployment in Europe.
欧洲出现了最严重的失业情况，这应当部分归咎于这项政策。
【考】 blame ... on 把……归咎于；blame ... for 为……责备某人
146、blow [bləu] v. 吹；吹气；（使）爆炸 n. 打击
A chill wind blew at the top of the hill
山顶寒风呼啸。
147、boast [bəust] v. （of/about）夸口，夸耀
Witnesses said Furci boasted that he took part in killing them
几位证人声称富尔奇曾吹嘘自己参与了对他们的屠杀。
148、boil [bɔil] v. 沸腾，煮沸
I stood in the kitchen, waiting for the water to boil
我站在厨房，等着水烧开。
149、boring ['bɔːriŋ] a. 令人生厌的
Not only are mothers not paid but also most of their boring or difficult work is unnoticed.
母亲们不但得不到报酬，而且她们做的那些乏味艰苦的工作常常都不为人所注意。
【考】 v. -ed 形容词有被动或已完成的意思；v.-ing 形 容 词 有 主 动 或 进 行 的 含 义：
bored—boring；surprised—surprising；interested—interesting； amazed—amazing。
150、bother ['bɔðə] v. 打扰，麻烦【近】 trouble
Lesson 4
151、bottom ['bɔtəm] n. 底，底部
He sat at the bottom of the stairs
他坐在最下面的一级楼梯上。
152、brain [brein] n. 大脑；头脑，智能
Her father died of a brain tumour.
她父亲死于脑瘤。
153、branch [brɑːntʃ] n. 树枝；分支
The local branch of Bank of America is handling the accounts
美国银行在当地的分行正在处理这些账目。
154、brave [breiv] a. 勇敢的He was not brave enough to report the loss of the documents.
他没有勇气报告丢失了文件。
155、break [breik]（broke，broken）v. 撕开，打碎；违反 n. 休息（时间）
Masked robbers broke in and made off with$ 8,000
蒙面强盗闯了进来，抢走了8,000美元。
He was 29 when war broke out
战争爆发时他才29岁
Civil war could come if the country breaks up
如果国家分裂就会爆发内战。
Their car broke down.
他们的车子出故障了。
【考】 break in 闯入；break out 爆发；break up 分手；break down 损坏，瓦解；
break one’s heart使某人心碎
156、breath [breθ] n. 呼吸，气息【考】 out of breath 上气不接下气
By the time I got to the top of the hill, I was quite out of breath.
等我爬到山顶的时候，已经是上气不接下气了。
157、breathe [briːð] v. 呼吸，呼气
He stood there breathing deeply and evenly
他站在那儿均匀地深呼吸。
158、breed [briːd]（bred, bred） v. 养育，繁殖Certain breeds are more dangerous than others.
某些品种比其他品种更危险。
159、brief [briːf] a. 简短的，短暂的【考】 in brief 简要地说She once made a brief appearance on television
她曾经在电视上短暂露面。
160、bright [brait] a. 明亮的；聪明的
She leaned forward, her eyes bright with excitement.
她身体前倾，眼里闪动着兴奋的光芒。
161、brilliant ['briljənt] a. 卓越的，杰出的；极聪明的She had a brilliant mind
她头脑聪明。
162、bring [briŋ]（brought, brought） v. 带来，拿来
The only way they can bring about political change is by putting pressure on the country.
要使该国进行政治变革，唯一的办法就是向其施压。
The government brought in a controversial law under which it could take any land it wanted.
政府推行了一项颇具争议的法令，根据该法令政府可以征用其所需要的任何土地。
She brought up four children
她养大了4个孩子。
【考】 bring about 带来，招致；bring in 引进，引来；bring up 抚养，培养
163、bubble ['bʌbl] n. 泡，水泡，气泡
Ink particles attach themselves to air bubbles and rise to the surface.
墨汁颗粒附着在气泡上，浮到表面上。
164、build [bild] vt. 建筑，建立
I am determined to build on this solid foundation.
我决定在这个坚实的基础之上继续努力。
The regime built up the largest army in Africa
该政权逐渐建立起一支非洲规模最大的军队。
【考】build on/upon 建立在…基础上；build up增强
165、bunch [bʌntʃ] n. 束，捆，串My neighbours are a bunch of busybodies
我的邻居们是一群爱管闲事的人。
166、bundle ['bʌndl] n. 捆，包，束  a bundle of clothes 一包衣物
167、burden ['bəːdn] n. 担子，负担
The developing countries bear the burden of an enormous external debt
发展中国家背负着巨额外债。
168、burn [bəːn]（burnt, burnt） vt. 烧伤；烧
Fires were burning out of control in the center of the city
市中心的火势失去了控制。
Six months after Bud died, the house burned down
巴德死后6个月，这座房子被烧毁。
The satellite re-entered the atmosphere and burned up
卫星重新进入大气层，烧成灰烬。
【考】 burn out 烧 光；burn down 烧 毁；burn up烧尽
169、burst [bəːst]（burst, burst）n./v. 破裂，爆炸；爆发【考】 burst into sth. 突然发生或产生
170、bury ['beri] v. 埋葬；埋藏
They make the charcoal by burying wood in the ground and then slowly burning it.
他们通过把木材埋到地下缓慢燃烧来获取木炭。
171、busy ['bizi] a. 忙的，忙碌的
What is it? I'm busy
什么事？我忙着呢。
be busy doing sth侧重于动作sth是名词哦·do是动词的形式be busy with sth侧重于状态意思都一样,可以互换.He is busy doing home work.=He is busy with homework.
【考】 be busy at/with sth. 忙于某事；be busy doing sth. 忙于做某事
172、calculate ['kælkju leit] v. 计算
From this you can calculate the total mass in the Galaxy
由此你可以算出银河系的总质量。
173、calendar ['kælində] n. 日历，历法There was a calendar on the wall above, with large squares around the dates.
上面的墙上挂着日历，日期上画着大大的方框。
174、call [kɔːl] n. 喊，叫；（一次）电话 vt. 把……叫做 vi. 喊，叫
I'll now call at the vicarage and report to you in due course.
我现在要去教区牧师家拜访，然后适时向你汇报情况。
One of Kenya's leading churchmen has called on the government to resign
肯尼亚一位高级宗教人士要求政府下台。
If we're not around she'll take a message and we'll call you back
如果我们不在，她会带个话儿，我们给您打过去。
He has called off the trip
他已取消了这次行程
【考】 call at 拜访（某地）；call on 拜访（某人）；call on/upon sb. to do sth. 号召某人做某事；
call back 回电话；call off 取消
175、calm [kɑːm] a. 镇静的；平静的 n. 平静；镇静She is usually a calm and diplomatic woman
她通常沉着而老练。
176、camera ['kæmərə] n. 照相机，摄影机Fay was so impressive on camera that a special part was written in for her
费伊很上镜，所以特地为她编写了一个角色。
177、camp [kæmp] n. 野营，营地 v. 设营，宿营We'll make camp on that hill ahead.
我们到前面那座小山上扎营。
178、campaign [kæm'pein] n. 战役；运动
During his election campaign he promised to put the economy back on its feet
他在竞选时许诺将重振经济。
179、campus ['kæmpəs] n.（大学）校园Private automobiles are not allowed on campus.
大学校园不许私人汽车入内。
180、cancel ['kænsəl] v. 取消；删除 cancel ameeting 取消会议
181、cancer ['kænsə] n. 癌
Her mother died of breast cancer
她母亲死于乳腺癌。
182、candidate ['kændidit] n. 候选人；（求职）申请人
The Democratic candidate is still leading in the polls
民主党候选人的得票数依然领先。
183、capable ['keipəbl] a. 有本领的，有能力的
He appeared hardly capable of conducting a coherent conversation
他好像连话都说不清楚。
184、capital ['kæpitl] n. 首都，首府；资本，资金Companies are having difficulty in raising capital
各公司融资困难。
185、captain ['kæptin] n. 船长；队长
Are all your weapons in place, Captain?
上尉，你部所有武器都到位了吗？
186、care [kɛə] n. 照顾；小心 v. 在乎；照顾
I don't care about the matter.
我对这事毫不介意。
They hired a nurse to care for her.
他们雇了个护士来照顾她。
Don't worry yourself about me, I can take care of myself
你别担心我，我能照顾好自己。
【考】 care about 在乎，关心；care for 照顾，喜欢，愿意；take care of 照顾，照料；
takecare 留神，小心   【派】 careful a. 小心的，仔细的；careless a. 马虎的
187、career [kə'riə] n. 生涯，经历；职业
She is now concentrating on a career as a fashion designer
她目前正专注于服装设计师的工作。
188、carry ['kæri] vt. 搬运，运输；携带
The assistant carried on talking
助理继续说下去。
The Social Democrats could still carry out their threat to leave the government
社会民主党人仍有可能将其退出政府的威胁付诸实施。
【考】 carry on 继续下去；carry out 执行，贯彻
189、case [keis] n. 事实，情况 n. 案件，病历
【考】 in any case 总之；in case 假如，以防；in case of 假如，万一
190、casual ['kæʒjuəl] a. 偶然的；非正式的；临时的It's difficult for me to be casual about anything
要我轻松处事很难。
191、catch [kætʃ] vt. 捉住；赶上；染上catch a cold/fire/thief 感冒 / 着火 / 捉贼
【考】 catch on（to）学会；catch up with 追上，赶上
192、cease [siːs] v./n. 停止，中止
At one o'clock the rain had ceased.
一点时，雨已停了。
193、ceremony ['seriməni] n. 典礼，仪式，礼仪
Today's award ceremony took place at the British Embassy in Tokyo.
今天的颁奖典礼在英国驻东京大使馆举行。
194、champion ['tʃæmpjən] n. 优胜者，冠军
Kasparov became world champion.
卡斯帕罗夫成为世界冠军。
195、channel ['tʃænl] n. 海峡，沟渠；频道The government will surely use the diplomatic channels available
政府肯定会利用各种现有的外交渠道。
196、character ['kæriktə] n. 汉字；品格；角色，人物The financial concessions granted to British Aerospace were, he said, of a precarious character
197、characteristic [kæriktə'ristik] a. 特有的，典型的Genes determine the characteristics of every living thing.
基因决定每个生物的特征。
198、charge [tʃɑːdʒ] n. 费用；价钱；负责 v. 管理；控诉；要价
He is in charge of the school work.
他负责学校工作。
【考】 in charge of 负责；in the charge of sb. 由某人负责；charge sb. with sth. 指控某人……罪
199、chase [tʃeis] v./n. 追赶，追求
She chased the thief for 100 yards
她追了窃贼100码远。
200、cheat [tʃiːt] n./v. 欺骗，作弊
Students may be tempted to cheat in order to get into top schools.
为了能进入一流学校，学生们会忍不住想作弊。
Lesson 5
201、check [tʃek] vt. 检查；核对 n. 支票；检查；批改
I'll ring the hotel. I'll tell them we'll check in tomorrow
我来给旅馆打电话，通知他们我们明天入住。
They packed and checked out of the hotel
他们收拾好东西，办理了退房手续。
【考】 check in 报到，登记；check out 查明；结账离开
202、cheer [tʃiə] v. 喝彩，欢呼，使高兴The crowd cheered as Premier Wayne Goss unveiled a lifesize statue of poet Banjo Paterson
韦恩·戈斯州长揭开诗人巴尼奥·佩特森的等身塑像时，人群欢呼起来
203、chew [tʃu] v. 咀嚼
Be certain to eat slowly and chew your food extremely well
一定要慢慢吃，要细细咀嚼。
204、chief [tʃiːf] a. 主要的，首要的 n. 领袖，首领
Financial stress is well established as a chief reason for divorce
经济压力被确认是导致离婚的首要原因。
205、childhood ['tʃaildhud] n. 幼年，童年She had a happy childhood
她曾有过一个幸福的童年。
206、choice [tʃɔis] n. 选择，选择机会
It's available in a choice of colours
有多种颜色可供选择。
207、choose [tʃuːz]（chose, chosen）v. 选择，挑拣
They will be able to choose their own leaders in democratic elections
他们将能够通过民主选举选择自己的领导人。
208、circle ['səːkl] n. 圆，圈；周期，循环
The flag was red, with a large white circle in the center
旗子是红色的，中央有个白色大圆圈。
209、circumstance ['səːkəmstəns] n. 情形，环境，状况
Under no circumstance will I believe you.
无论什么情形之下我都不会相信你。
【考】 under no circumstance 在任何情况下都不；under any circumstance 在任何情况下
210、citizen ['sitizn] n. 公民；市民The life of ordinary citizens began to change.
老百姓的生活开始发生变化
211、claim [kleim] n./v. 要求，主张；声称He claimed that it was all a conspiracy against him
他声称这一切都是一场针对他的阴谋。
212、classical ['klæsikəl] a. 经典的，古典的Fokine did not change the steps of classical ballet; instead he found new ways of using them.
福金没有改变传统芭蕾舞的舞步，而是另辟蹊径对这些舞步加以运用。
213、climate ['klaimit] n. 气候
The economic climate remains uncertain.
经济形势依然不确定。
214、close [kləuz] v. 关，关闭 [kləus] a. 近的，接近的
The factory had to close down for lack of orders.
那家工厂因无人订货而被迫关闭。

I'm very much a family man and need to be close to those I love.
我是个非常恋家的男人，必须生活在所爱的人身旁。
It takes me a long time to drop my guard and get close to people.
我要花上好长时间才能放下戒备心，与人接近。

【考】 close down 关闭，歇业；be close to 紧挨着，在……附近；get close（to）靠近
215、clue [kluː] n. 线索，提示Geneticists in Canada have discovered a clue to the puzzle of why our cells get old and die
加拿大的遗传学家已经发现了解开细胞衰老、死亡之谜的线索。
216、collect [kə'lekt] vt. 收集
Two young girls were collecting firewood
两个小女孩正在拾柴火。
217、combine [kəm'bain] v. 结合，兼有，联合The Church has something to say on how to combine freedom with responsibility
教会想就如何把自由与责任结合起来发表一下意见。
【考】 combine A with B 将 A 与 B 结合在一起
218、come [kʌm] v. 来到，出现；到达
Any possible solution to the Irish question can only come about through dialogue
任何解决爱尔兰问题的可行方案只能通过对话产生。

I came across a group of children playing.
我碰到一群正在玩耍的小孩。
Come on Doreen, let's dance.
来吧，多琳，我们跳舞吧。
The book comes out this week
这本书本周出版。
Several of the members have come up with suggestions of their own
有几位成员提出了自己的建议。
【考】 come about 发生；come across 偶然碰到；come on 请，来吧，快点；come out 出现，
出版，结果是；come up with 想出；提出
219、comfort ['kʌmfət] n. 安逸，舒适；安慰This will enable the audience to sit in comfort while watching the shows
这样能够让观众舒舒服服地坐着看表演。
220、comment ['kɔment] n./v. 评论，解释So far, Mr Cook has not commented on these reports
到目前为止，库克先生仍未就这些报道发表评论。
【考】 make comment on 对……评论
221、commit [kə'mit] v. 犯，干（错事）
I have never committed any crime
我从来没犯过罪。
222、common ['kɔmən] a. 平常的，普通的；共同的
We happened to discover we had a friend in common
我们凑巧发现我们有一个共同的朋友。
【考】 in common 一 样， 同 样；have sth. in common 有共同之处
223、communicate [kə'mjuːnikeit] vi. 通 讯， 交 流 vt.传达；传染
My natural mother has never communicated with me
我的亲生母亲从未和我联系过。
224、community [kə'mjuːniti] n. 社区，社会
He's well liked by people in the community
社区的人都非常喜欢他。
225、companion [kəm'pænjən] n. 同伴，伴侣；朋友
Fred had been her constant companion for the last six years of her life
在她生命的最后6年，弗莱德一直是她忠实的伴侣。
226、company ['kʌmpəni] n. 公司；伙伴；客人Never keep company with dishonest per-sons.
不要与不诚实的人打交道。
【考】 keep company with 与……交往
227、complain [kəm'plein] v. 抱怨；申诉Miners have complained bitterly that the government did not fulfill their promises
【考】 complain that ... 抱怨……；complain to sb.   of/about sth. 向某人抱怨某事
228、complete [kəm'pliːt] vt. 完成 a. 完整的The rebels had taken complete control
叛乱分子完全控制了局面。
229、complex ['kɔmpleks] a. 复杂的    【反】 simple  简单的
I have never had a complex about my height.
我从来没有担心过自己的身高。
230、complicated ['kɔmplikeitid] a. 错综复杂的，难懂的 【近】 complex The situation in Lebanon is very complicated.
黎巴嫩的情况十分复杂。
231、concentrate ['kɔnsentreit] v. 集中（注意力），全神贯注
It was up to him to concentrate on his studies and make something of himself
是否能专心学习并取得一定成就要靠他自己。
【考】 concentrate on/upon 专心于……
232、concept ['kɔnsept] n. 概念
She added that the concept of arranged marriages is misunderstood in the west.
她补充说，西方人对包办婚姻的概念有些误解。
233、concern [kən'səːn] v. 涉及；影响；使关心 n. 关心；忧虑
This aggression is the real issue the world should be concerned about. We want to keep that in focus
这次侵略才是世界应该关注的真正问题。我们想继续保持关注。
【考】 be concerned about（或 for）关心，挂念
234、conclude [kən'kluːd] v. 总结；结束Larry had concluded that he had no choice but to accept Paul's words as the truth
拉里断定自己别无选择，只能当保罗的话是真的。
235、connect [kə'nekt] vt. 连接；联系
You can connect the machine to your hi-fi
你可以把机器连在高保真音响上。
【考】 connect with 与……连接、联系；connect to 连接，相连
236、conscious ['kɔnʃəs] a. 有知觉的；意识到的，自觉的
He was conscious of the faint, musky aroma of after-shave
他注意到了须后水淡淡的麝香味。
【考】 be conscious of/that 意识到
237、consequence ['kɔnsikwəns] n. 结果，后果
Her lawyer said she understood the consequences of her actions and was prepared to go to jail她的律师说她明白自己行为的后果，已有了入狱的心理准备。
【考】 in consequence=as a result 因而，结果；in consequence of=as a result of 由于，因为
238、consider [kən'sidə] v. 考虑，细想；考虑到
The government is being asked to consider a plan to fix the date of the Easter break
人们要求政府考虑确定复活节假期日期的计划。
【考】 consider ... as（把某人）看作……；consider doing sth. 考虑做某事；
consider sb./sth. to do/be 认为某人做某事
239、considerable [kən'sidərəbl] a. 相当的，可观的
To be without Pearce would be a considerable blow
失去皮尔斯将是一个巨大的打击。
240、considerate [kən'sidərit] a. 体贴的，体谅的
I think he's the most charming, most considerate man I've ever known
我觉得他是我所认识的最有魅力并且最为体贴的男士。
241、consist [kən'sist] v. （of）由……组成
Breakfast consisted of porridge served with butter.
早餐是麦片粥配佐餐黄油。
242、constant ['kɔnstənt] a. 经常的；不变的
She suggests that women are under constant pressure to be abnormally thin
她暗示说女性总是处在保持身材异常瘦削的压力之下。
243、contact ['kɔntækt] n./v. 接触，联系，交往Opposition leaders are denying any contact with the government in Kabul
反对派领袖否认与喀布尔政府有任何联系。
【考】 come into/make contact with sb. 取得联系；lose contact with sb. 失去联系
244、contain [kən'tein] vt. 包含，包括；能容纳
The bag contained a Christmas card
这个包里装着一张圣诞卡。
245、contrast ['kɔntræst] n./v. 对比，对照，反差
The two visitors provided a startling contrast in appearance
这两位访客的外表截然不同。
【考】 by/in contrast 对比之下
246、contribute [kən'tribjuːt] v. 贡献，捐献
The three sons also contribute to the family business
这3个儿子也为家族事业做出了贡献。
【考】 contribute sth. to… 向……捐款 / 捐献 / 有益于 / 投稿
【考】 convince + 人 + of + 事，或接 that 从句；
persuade + 人 + to do 或 that 从句。
247、cope [kəup] v. （with）对付，应付【近】 deal; handle
It was amazing how my mother coped with bringing up three children on less than three pounds a week
我妈妈用每周不到3英镑的钱抚养大了3个孩子，简直太不可思议了。
248、count [kaunt] v. 计算，数；有重要意义He was counting slowly under his breath
他轻声地慢慢数着数。
249、cover ['kʌvə] vt. 覆盖 n. 盖子；封底，封面Cover the casserole with a tight-fitting lid
用大小合适的盖子盖住砂锅。
【考】 be covered with 用……覆盖
250、crash [kræʃ] n./v. 碰撞；倒下；坠落His elder son was killed in a car crash a few years ago.
他的长子几年前死于车祸
Lesson 6
251、create [kri'eit] v. 创造，创作，建立We set business free to create more jobs in Britain
我们放开对企业的限制以便在英国创造更多的就业机会。
252、crime [kraim] n. 罪，罪行；罪过He and Lieutenant Cassidy were checking the scene of the crime
他和卡西迪中尉正在勘查犯罪现场
253、criminal ['kriminəl] a. 犯罪的，刑事的 n. 罪犯A group of gunmen attacked a prison and set free nine criminals in Moroto.
一伙持枪歹徒袭击了莫罗托的一所监狱，放走了9名罪犯。
254、criticism ['kriti sizəm] n. 批评，指责；评论
This policy had repeatedly come under strong criticism on Capitol Hill
该政策在美国国会屡遭强烈批评。
255、cruel ['kruːəl] a. 残酷的，残忍的
Children can be so cruel
孩子有时可能会相当残忍
256、culture ['kʌltʃə] n. 文化；修养，教养There is just not enough fun and frivolity in culture today.
当今的文化恰恰是不够轻松有趣。
257、cure [kjuə] n./v. 治愈，矫正An operation finally cured his shin injury
手术最终治好了他胫部的伤。
258、current ['kəːrənt] n.（电、水、气）流，潮流 a. 当前的，流行的Under normal conditions, the ocean currents of the tropical Pacific travel from east to west
在正常情况下，太平洋的热带洋流自东向西流。
259、custom ['kʌstəm] n. 习惯；习俗；[the customs] 海关，关税The custom of lighting the Olympic flame goes back centuries
点燃奥林匹克圣火的习俗可以追溯到几个世纪前。
260、damage ['dæmidʒ] n. /v. 损害，毁坏He maliciously damaged a car with a baseball bat
他用棒球棒恶意损毁了一辆汽车。
261、dare [dɛə] v./v. aux 敢，胆敢
Since he was stuck in a lift a year ago he hasn't dared to get back into one
自从一年前被困在电梯里之后，他就再也没敢乘过电梯。
【考】 dare to do sth. 敢做某事（行为动词）；dare not do sth. 不敢做某事（助动词）
262、deal [diːl] v. 处理，对待 n. 协议；大量deal with a problem 处理问题

I am in a position to save you a good deal of time.
我能够为你节省许多时间。
【考】 deal with 处理；a good deal / a great deal of sth. 许多，大量
263、debt [det] n. 债，负债Three years later, he is still paying off his debts
都3年了，他仍在偿还债务。
264、declare [di'klɛə] v. 宣布；宣称
Speaking outside Ten Downing Street, she declared that she would fight on
她在英国首相官邸外发表讲话时宣称她将继续斗争下去。
265、decrease [diː'kriːs] n./v. 减少，减小
Population growth is decreasing by 1.4% each year
人口增长率正在以每年1.4%的速度下降。
266、deduce [di'djuːs] vt. 推论，推断
Alison had cleverly deduced that I was the author of the letter.
艾莉森已经聪明地推断出那封信就是我写的。
【考】 deduce ... from 从……推导出
267、deed [diːd] n. 行为，事迹
His heroic deeds were celebrated in every corner of India.
他的英勇事迹在印度各地广为传颂。
268、defeat [di'fiːt] n./v. 战胜，挫败The proposal was defeated by just one vote.
该提案仅以一票之差被否决。
269、defend [di'fend] v. 保卫，辩护Every man who could fight was now committed to defend the ridge
每个能够参加战斗的男子现在都决心要保卫这条山脊。
270、degree [di'griː] n. 程度，学位These man-made barriers will ensure a very high degree of protection
这些人造屏障将会确保提供高度的保护。
271、delay [di'lei] v./n. 拖延，延误，延迟For sentimental reasons I wanted to delay my departure until June
因为感情方面的原因，我想把离开的日期延迟至6月。
【考】 without delay 毫不拖延
272、delight [di'lait] n. 快乐，使高兴
Throughout the house, the views are a constant source of surprise and delight
从房子各处看到的景色不断给人以惊奇和欣喜。
273、deliver [di'livə] v. 递送；交付，移交
The Canadians plan to deliver more food to southern Somalia
加拿大计划向索马里南部地区运送更多的食品。
274、demand [di'mɑːnd] n./v. 要求，需求Mr Byers last night demanded an immediate explanation from the Education Secretary
昨晚拜尔斯先生要求教育大臣立刻作出解释。
275、democracy [di'mɔkrəsi] n. 民主，民主制
The new democracies face tough challenges.
这些新兴的民主国家面临着严峻挑战。
276、demonstrate ['demənstreit] v. 证实，表明；展示【派】demonstration n. 证实，表明；展示The study also demonstrated a direct link between obesity and mortality
该研究还表明了肥胖症和死亡率之间存在直接的联系。
277、deny [di'nai] v. 否认，拒绝
She denied both accusations
她对两项指控均予以否认。
278、depend [di'pend] vi. 依靠，依赖
The cooking time needed depends on the size of the potato
所需的烹饪时间取决于土豆的大小。
【考】 depend on/upon 依 靠， 信 赖；It/That depends. 看情况而定。
279、describe [dis'kraib] vt. 描述；形容We asked her to describe what kind of things she did in her spare time
我们请她描述一下她业余时间都在做什么
280、desert ['dezət] n. 沙漠，不毛之地
The vehicles have been modified to suit conditions in the desert.
车辆已改装过以适应沙漠的环境。
281、deserve [di'zəːv] v. 应受，值得Government officials clearly deserve some of the blame as well
政府官员显然也应当承担部分责任。
282、design [di'zain] vt. 设计，策划They wanted to design a machine that was both attractive and practical.
他们想设计一种既美观又实用的机械
【考】 be designed for/to do 专为……而做
283、desire [di'zaiə] vt. 要求；期望 n. 愿望，心愿
I had a strong desire to help and care for people
我非常渴望能够帮助和照顾他人。
【考】desire（sb.）to do sth.希望某人做某事；a desire to do sth. 做某事的欲望
284、despite [di'spait] prep. 不管，尽管
Despite a thorough investigation, no trace of Dr Southwell has been found
尽管进行了彻底的调查，还是没有发现索思韦尔博士的任何踪迹。
284、destination [desti'neiʃən] n. 目的地Spain is still our most popular holiday destination
西班牙仍是我们最喜爱的度假去处。
285、detail ['diːteil] n. 细节；枝节，琐事
We examine the wording in detail before deciding on the final text.
我们彻底仔细地检查了措词后才最终定稿。
【考】 in detail 详细地
286、determine [di'təːmin] vt. 决定，决心
The size of the chicken pieces will determine the cooking time
鸡块的大小将决定烹饪时间的长短。
287、develop [di'veləp] v. （使）发展；培养；开发；冲洗（照片）As children develop, some of the most important things they learn have to do with their sense of self
在成长过程中，孩子学到的一些最重要的东西与他们的自我意识有关。
288、devote [di'vəut] vt. 把……奉献（给）； 把……专用（于）He decided to devote the rest of his life to scientific investigation
他决定将自己的余生献给科学研究事业。
【考】 devote oneself to sth./doing sth.致力于……；献身于……
289、difference ['difərəns] n. 差别，差异
That is the fundamental difference between the two societies
那是这两个社会之间的根本性差异。
【考】 make a/no difference 有 / 没影响
290、different ['difərənt] a. 差异的，不同的
London was different from most European capitals
伦敦同大多数欧洲国家的首都都有所不同。
291、difficulty ['difikəlti] n. 困难

We should help the friends in difficulty
我们应该帮助处于困境的朋友。
【考】 in difficulty 处 境 困 难；have difficulty in doing sth. 在……方面有困难
292、diligent ['dilidʒənt] a. 勤奋的，用功的
Meyers is a diligent and prolific worker
迈耶斯是个勤奋多产的工人。
293、direct [di'rekt] v. 指挥，指导；导演 a. 直的，率直的
They'd come on a direct flight from Athens.
他们是搭乘从雅典直飞这里的航班过来的。
294、direction [di'rekʃən] n. 方向；指导，管理
St Andrews was ten miles in the opposite direction
圣安德鲁斯在相反方向上10英里处。
295、disappear [disə'piə] v. 消失
The black car drove away from them and disappeared
那辆黑色轿车驶离了他们的视线。
296、discourage [dis'kəːridʒ] v. 使失去信心，使泄气
It may be difficult to do at first. Don't let this discourage you.
万事开头难，别因此而灰心。
297、discover [dis'kʌvə] vt. 发现，找到，发觉
She discovered that they'd escaped
她发现他们已经逃跑了。
298、distance ['distəns] n. 距离
I hear the rumble of thunder in the distance.
我听到远处雷声隆隆。
The railway station is at a distance of two miles from our school.
火车站离我们学校两英里路程。
【考】 in the distance 在远处；at a distance of 有相当距离
299、distant ['distənt] a. 在远处的，远隔的The mountains rolled away to a distant horizon.
群山向远处的地平线绵延而去。
300、distinguish [di'stiŋgwiʃ] v. 区别，辨别，辨认出
Could he distinguish right from wrong?
他能分辨是非吗？
Lesson 7
301、distribute [di'stribjut] v. 分配，分发，配给
302、divide [di'vaid] v. 分，划分；分享；分开
303、division [di'viʒən] n. 区分，分开，除法；部门
304、doubt [daut] n. 疑惑，疑问，怀疑 v. 怀疑，不相信
【考】 no doubt 无疑地；in doubt 不能肯定的；without doubt 无疑的
305、draw [drɔː] （drew, drawn） v. 画，绘制；拉
【搭】draw back 退 却， 撤 回；draw up 草 拟，制定
306、dress [dres] n. 女装，连衣裙 vt. 给……穿衣服；打扮
【考】 dress up 穿上盛装，打扮；be/get dressed in穿着……衣服
307、drop [drɔp] n. 滴，水滴；下降 v. 落下；扔下；中断
【考】 drop in 顺便拜访；drop out 退出，退学
308、economy [i'kɔnəmi] n. 经济
309、educate ['edjukeit] vt. 教育，培养
310、effect [i'fekt] n. 效果；作用
【考】 have an effect on 对 …… 有 影 响 / 作 用；come into effect 生效，起作用
311、effort ['efət] n. 努力，艰难的尝试【考】 make an effort to do sth. 努力做某事
312、elect [i'lekt] vt. 选举，推选【派】 elector n. 选民；election n. 选举
313、element ['elimənt] n. 元素，成分
314、embarrass [im'bærəs] vt. 使窘迫，使困扰
315、emotion [i'məuʃən] n. 情感，情绪
316、emphasis ['emfəsis] n. 强调
317、emphasize ['emfəsaiz] v. 强调
318、enable [in'eibl] v. 使能够
319、encourage [in'kʌridʒ] v. 鼓励，助长，促进
320、endless ['endlis] a. 无限的，无穷的
321、enemy ['enimi] n. 敌人，仇敌
322、energy ['enədʒi] n. 精力，气力，能量
323、engage [in'geidʒ] v. 雇用，使订婚；占用
324、ensure [in'ʃuə] v. 保证，担保
325、entertain [entə'tein] v. 招待，使欢乐
326、entire [in'taiə] a. 完全的，完整的
327、entrance ['entrəns] n. 入口，门口；进入
328、entry ['entri] n. 进入，入场
329、environment [in'vaiərənmənt] n. 环境，周围状况，自然环境
330、envy ['envi] n./v. 妒忌，羡慕
331、equal ['iːkwəl] a. 同等的，相等的，匹敌者
332、equip [i'kwip] v. 装备，设备【派】 equipment n. 装备，器材
333、error ['erə] n. 错误，过失
334、fact [fækt] n. 事实；真相【考】 in fact 实际上，事实上；as a matter of fact事实上
335、factor ['fæktə] n. 因素，要素
336、fail [feil] v. 失败；不及格【考】 fail to do sth. 没能做成某事；fail in sth. 在某事物中失败
337、fair [fɛə] n. 集市；展览会 a. 公平的；（肤色）白皙的
338、faith [feiθ] n. 信任；信仰，信念
339、fall [fɔːl] （fell, fallen） v. 下降；摔倒
【考】 fall asleep 入睡；fall behind 落后；fall down跌倒，摔倒；fall in love with 爱上
340、familiar [fə'miljə] a. 熟悉的
【考】 be familiar with sb. / sth. 通晓或者熟悉；sth. be familiar to sb. 为……所熟知
341、fashion ['fæʃən] n. 样子，方式；时尚，风气
342、fault [fɔːlt] n. 缺点，毛病    【考】 find fault （with）挑错，批评
343、fear [fiər] n. 害怕，恐惧
【考】 for fear of 生 怕， 唯 恐；for fear that 唯恐……发生；in fear of 为……而担心
344、feed [fiːd] （fed，fed） v. 喂养，向……提供
【考】 feed on 以……为食，以吃……为生；feed with 用……喂；be fed up with 使对……厌
烦，使厌倦
345、field [fiːld] n. 田地；领域
346、figure ['fiɡə] v. 计算；认为 n. 数字；人物【考】 figure out 理解，想明白，计算出
347、find [faind]（found, found） v. 找到，发现，认为
【考】 find sb. doing sth. 看见某人正在做某事；find sb. do sth. 看到某人做某事的过程；
find out 查出，发现
348、fit [fit] v. 适合 a. 健康的；合适的【考】 be fit for 适合，胜任  【近】 suit；match
349、follow ['fɔləu] vt. 跟随；仿照；听得懂；遵循【反】 lead v. 引领【考】 as follows 如下
350、forbid [fə'bid]（forbade，forbidden） vt. 禁止，不许
【考】 forbid sb. to do 禁止某人做某事；forbid doing 禁止做某事【近】 forbid 和 prohibit
Lesson 8
351、forget [fə'ɡet]（forgot, forgotten） vt. 忘记
【考】 forget to do 忘记去做某事；forget doing 忘记曾做过了某事
352、forgive [fə'ɡiv]（forgave；forgiven） v. 宽恕，饶恕
【考】 forgive sth.;           forgive sb. for doing sth.
353、form [fɔːm] n. 形状，形式；表格 v. 形成，养成
354、formal ['fɔːməl] a. 正式的，礼仪上的
355、former ['fɔːmə] a. 在前的，以前的；前者的
356、forth [fɔːθ] ad. 向前，向外
【考】 back and forth 来回的，往复的
357、fortune ['fɔːtʃən] n. 命运，好运；财产，财富【考】 make a fortune 发财
358、fortunate ['fɔːtʃənit] a. 幸运的
359、fresh [freʃ] a. 新鲜的；清新的；水（淡）的
360、frighten ['fraitən] v. 吓唬，使惊恐
361、fuel ['fjuəl] n. 燃料
362、fulfil（l） [ful'fil] v. 完成，履行
363、function ['fʌŋkʃən] n. 职责；功能；作用 v. 运行，起作用
364、gain [ɡein] n./vt. 挣得；赢得；获得
365、gaze [ɡeiz] vi./n. 凝视，注视
366、general ['dʒenərəl] a. 大 体 的， 笼 统 的， 总 的    n. 将军
【考】 in general 基 本 上， 大 体 上， 总 之；
generally speaking （常做插入语）总体来说，一般来说
367、generation [dʒenə'reiʃən] n. 代，世代；产生，发生
368、generous ['dʒenərəs] a. 慷慨的，宽厚的
369、genius ['dʒinjəs] n. 天才
370、gentle ['dʒentl] a. 和蔼的，有礼貌的
371、genuine ['dʒenjuin] a. 真正的，真诚的
372、gesture ['dʒestʃə] n. 姿势，手势，姿态；表示
373、giant ['dʒaiənt] n. 巨人 a. 巨大的
374、gift [ɡift] n. 礼物；天赋，才能【考】 have a gift for 对……有天赋
375、glory ['ɡlɔri] n. 光荣，荣誉
376、goal [gəul] n. 终点，目的
377、graduate ['ɡrædjueit] v. 毕业 n. 毕业生
378、grant [ɡrɑːnt] v. 同意，准予；授予【考】 take sth. for granted 认为……是理所当然的
379、greet [ɡriːt] vt. 问候；向……致敬
380、ground [ɡraund] n. 地面，土地；运动场【考】 on the grounds of 以……为理由
381、group [ɡruːp] n. 群，团体；集团
382、grow [ɡrəu]（grew, grown） v. 生长，种植；发育；渐渐变得
【考】 grow up 长大成人；grow into 长大成为，变得【派】 growth n. 生长
383、guard [ɡɑːd] v. 警戒，看守 n. 防护装置；看守员
384、guess [ɡes] v./n. 猜
385、guide [ɡaid] n. 向导，导游；指导者 vt. 指导，引导【派】 guidance n. 引导，指导
386、guilt [ɡilt] n. 犯罪，过失；内疚【派】 guilty a. 有罪的；负疚的
387、habit ['hæbit] n. 习惯，习性，脾性
【考】 form/get the habit of 养 成 …… 习 惯；get into the habit of 沾染了……恶习；
grow out of the habit 改掉了……习惯
388、half [hɑːf] n. 一半 a. 一半的
389、halt [hɔːlt] n./v. 停止，终止
390、handle ['hændl] n. 手柄 v. 对付，处理；管理
391、happen ['hæpən] v. 发生；碰巧
【考】 happen to do sth. 碰巧做某事；happen on/upon 巧遇，偶然发现；
it happens that ... 碰巧，恰巧
392、happiness ['hæpinis] n. 幸福，快乐，愉快
393、harvest [hɑːvist] n. 收成，收获 vt. 收割
394、hatred ['heitrid] n. 憎恶
395、healthy ['helθi] a. 健康的
396、hear [hiə] v. 听见，听说；听取
【考】 hear of 听到，听说；hear from 收到来信【近】 listen（强调过程）
397、heat [hit] v. 加热，升温 n. 热
398、help [help] n./v. 帮助
【派】 helpful a. 有帮助的，有益的；helpless a.无助的
【考】 help oneself to sth.（用餐时）随便吃点，自便；help sb. with 帮助某人……；
help sb.（to）do sth. 帮 助 某 人 做 某 事；with the help of 在……帮助下；
can’t help（doing）忍不住做某事
399、hesitate ['heziteit] v. 犹豫，踌躇
400、historic [his'tɔːrik] a. 历史上有重大意义的           【辨】historical
Lesson 9
401、hold [həuld]（held, held） vt. 拿着；举行；容纳
【考】 hold back 阻碍，阻止；hold on（打电话时）别挂断，等一等；hold up 推迟，耽误
402、holy ['həuli] a. 神圣的，圣洁的
403、hope [həup] n./v. 希望
【考】 hope for 希望，盼望；in the hope of … 怀有……希望【辨】wish
404、host [həust] n. 主人 v. 主办
405、human ['hjuːmən] a. 人的，人类的
406、hunt n./v. 打猎，狩猎；寻找，搜索【考】 hunt for 寻找
407、hurry ['hʌri] n./v. 赶忙，匆忙，慌忙
【考】 hurry up（使）赶快迅速完成；in a hurry匆忙
408、hurt [həːt] vt. 使受伤；伤害    【考】 get hurt 受伤
409、idea [ai'diə] n. 主意；想法；计划【考】 have no idea 不知道
410、ideal [ai'diəl] a. 理想的，称心的
【例】 He’s the ideal husband for her. 他是她理想的丈夫。
411、identify [ai'dentifai] v. 识别；认出
412、idle ['aidl] a. 闲着的，空虚；懒惰的
413、ignore [iɡ'nɔː] vt. 不顾，不理；忽视
414、image ['imidʒ] n. 像，肖像，图像；形象
415、imagine [i'mædʒin] vt. 设想，猜想【派】 imagination n. 想象，想象力
【考】 imagine + 宾语 + 补语（doing； to be 短语）想象……
416、imitate ['imiteit] v. 模仿，仿效【派】 imitation n. 模仿，仿效
417、immediate [i'miːdjət] a. 立即的，最接近的
418、impact ['impækt] n. 冲击，影响
419、imply [im'plai] v. 暗示
420、implication [impli'keiʃən] n. 含义，暗指
421、import ['impɔrt] v. 进口，输入 n. 进口物资
422、important [im'pɔːtənt] a. 重要的，重大的
【派】 importance n. 重要，重要性 【考】 be of great importance to sb./sth. 对 …… 极其重要
423、impress [im'pres] vt. 给……以深刻印象
【派】 impression n. 印象；impressive a. 给人印象深刻的
【考】 impress on 使铭记，使印象深刻；impress sb. with sth. 因为某事使某人印象深刻
424、improve [im'pruːv] v. 改进，更新；变得更好【派】 improvement n. 改进，改进措施
425、include [in'kluːd] vt. 包括，包含【近】 contain
426、income ['inkʌm] n. 收入，所得
427、increase [in'kriːs] v./n. 增加，增长，增进
428、independent [indi'pendənt] a. 独立的，自主的
【派】 independently ad. 独立地，自主地；inde-pendence n. 独立，自立，自主
【考】 be independent of sb./sth.
429、indicate ['indikeit] vt. 指点，指示；表明
430、individual [indi'vidjuəl] n. 个人，个体 a. 个人的，个别的
431、industry ['indəstri] n. 工业；行业【派】 industrial a. 工业的，产业的
432、infect [in'fekt] v. 传染
433、inferior [in'fiəriə] a. 次的，低劣的【反】 superior a. 较好的，较多的
【考】 be inferior to 低于，劣于
434、influence ['influəns] vt. 影响，对……有作用 n.影响
【考】 have influence on ... 对……有影响
435、inform [in'fɔːm] v. 通知，告诉；报告
【考】 inform sb. of/about sth. 通知某人某事
436、information [infə'meiʃən] n. 信息，资料，情报
437、inherit [in'herit] v. 继承
438、injure ['indʒə] v. 伤害【派】 injury n. 伤   【近】 hurt v. 伤害
439、innocent ['inəsnt] a. 清白的；单纯的【派】 innocence n. 无辜；纯真
440、insist [in'sist] v. （on, upon）坚持；坚决认为
【考】 insist 用法与 suggest 相似。insist 作“坚持”说时，要用陈述语气；作“坚决主张；
坚决要求”讲时，用虚拟语气。
441、instant ['instənt] a. 立刻的，瞬间的
442、instead [in'sted] ad. 代替；反而；而不是
【考】 instead of 代替，而不是
443、instruction [in'strʌkʃən] n. 说明，须知；教导
444、instrument ['instrumənt] n. 乐器；工具，器械
445、insult ['insʌlt] n. 侮辱，凌辱
446、intelligence [in'telidʒəns] n. 智力，理解力；情报【派】 intelligent a. 聪明的； 有才智的
447、intend [in'tend] v. 想要，打算；意指，意思是
【考】 intend + 名词 / 动名词 / 动词不定式 /that从句；
it is intended that（接宾语从句）企图，意图是
448、interfere [intə'fiə] vi. 干涉，干扰，妨碍
449、interrupt [intə'rʌpt] v. 打断；打扰
450、interview ['intəvjuː] v/n. 采访，访谈
Lesson 10
451、introduce [intrə'djuːs] v. 介绍，提出，引进【考】 introduce oneself 作自我介绍
452、invent [in'vent] vt. 发明，创造；编造
453、invest [in'vest] v. 投资
454、invite [in'vait] vt. 邀请【考】 invite sb. to 邀请某人去……
455、involve [in'vɔlv] vt. 包括；使卷入，涉及【考】 be involved in 与……有关；被卷入
456、jealous ['dʒeləs] a. 妒忌的
457、join [dʒɔin] vi. 连接，接合 vt. 加入，参加【考】 join + sb. 或组织；join in + 活动
458、joint [dʒɔint] n. 关节 a. 接合处，联合的
459、journey ['dʒəːni] n. 旅行，旅程    【近】 tour; trip
460、judge [dʒʌdʒ] n. 法官，裁判员 v. 断定，裁决；审判【考】 judge by/from 以……来判断
461、justice ['dʒʌstis] n. 正义，公正，公平
462、keen [kiːn] a. 锋利的，尖锐的；热心的
463、keep [kiːp] v. （使）保持，（使）继续；阻止；遵守；保有
464、【考】 keep from 阻止，克制；keep on 继续，保持；
keep up （使）继续下去，（使）不停止；keep up with 跟上
465、knowledge ['nɔlidʒ] n. 知识，学识；知道，了解
467、labo（u）r ['leibə] n. 工作，劳动；劳力
468、lack [læk] v./n. 缺少，缺乏
【考】 lack 是及物动词，无需加入介词；名词lack 后面要加 of。
469、language ['læŋɡwidʒ] n. 语言
470、latter ['lætə] ad. 后面的，后者
471、laughter ['lɑːftə] n. 笑声
472、launch [lɔːntʃ] v. 发射，发起，开始
473、largely ['lɑːdʒli] ad. 大部分，基本上
474、lay [lei]（laid, laid） v. 放，搁，下（蛋），铺设
【考】 lay out 摆 开， 展 示；lay off 解 雇；lay aside 搁置，积蓄
475、lazy ['leizi] a. 懒惰的
476、lead [lid] v. 领导，带领；导致，通向 n. 铅
【考】 lead to 导致，导向
477、leader ['liːdə] n. 领袖
478、leading ['liːdiŋ] a. 领导的；最主要的
479、leak [liːk] n./v. 泄露，漏出
480、lean [liːn] v. 倾斜；屈身，倚
481、leap [liːp] v. 跳跃，跳过
482、learned ['ləːnid] a. 有学问的
483、least [liːst] a. 最小的，最少的 ad. 最少，最小【考】 at least 至少
484、leave [liːv]（left, left） v. 离开，离去；遗忘
leave for 动身去；leave behind留下，遗留；leave ... alone不管，不理会；leave out 省去，遗漏
485、legal ['liːɡəl] a. 法律（上）的，合法的【派】 illegal a. 违法的
486、leisure ['liːʒə] n. 空闲；悠闲
487、length [leŋθ] n. 长，长度
488、level ['levl] n. 水平，标准，水准
489、liberate ['libəreit] vt. 解放，使获自由
490、lie 1  [lai]（lied, lied, lying） n./v. 说谎
491、lie 2  [lai]（lay, lain, lying）v. 躺，平放，处于，位于，在于（in）
492、lift [lift] vt. 举起，抬起 n. 电梯；搭便车
493、likely ['laikli] a. 可能的，有希望的 ad. 大概，多半【考】 be likely to do sth./that
494、likewise ['laikwaiz] ad. 同样，也
495、limit ['limit] vt. 限制，限定 n. 界限；限制
496、link [liŋk] v. 连接；联系 n. 联系；关系；环节
497、literature ['litərətʃə] n. 文学，文学作品
498、live [liv] vi. 生活，生存 [laiv] a. 活的，直播的
【考】live on/by 靠…生活，以…为食；live up to 实践，不辜负
499、loan [ləun] n. 贷款；借出 v. 借出
500、local ['ləukəl] a. 当地的，地方性的
Lesson 11
501、lonely ['ləunli] a. 孤独的，荒凉的
502、look [luk] v. 看，注视；好像，显得 n. 脸色，外表
【考】 look out 当心，小心；look after 照顾；look like 看起来像；
look around 四处看看；look on 看待，以为； look forward to 盼望，期待；
look into 调查，观察；look up 查找，寻找，查出；look down upon 蔑视，看不起
503、loose [luːs] a. 松的，宽的，松散的
504、lose [luːz] v. 失去，丢失【考】lose one‘s way 迷 路；lose oneself in 沉 湎于……
505、loss [lɔs] n. 丧失，损失；失败【考】 at a loss 茫然，困惑
506、loyal ['lɔiəl] a. 忠诚的，忠贞的【考】 be loyal to sb./sth.
507、luck [lʌk] n. 运气；好运，幸运【考】 bad luck 倒霉；out of luck 运气不好
508、luxury ['lʌkʃəri] n. 奢华，奢侈，奢侈品
509、machinery [mə'ʃiːnəri] n. （总称）机械，机器
510、magic ['mædʒik] n. 魔术，魔法 a. 魔术的；有魔力的
【例】 This diet tea has a magic effect. 这种减肥茶有着神奇的功效。//magic show 魔术表演
511、main [mein] a. 主要的，总的 n. 总管道，干线
512、maintain [mein'tein] v. 保养，维修；维持，保持；坚持
513、major ['meidʒə] a. 较大的，主要的          【反】 minor
514、majority [mə'dʒɔːriti] n. 大多数
【考】 the majority are/is ... 大多数……；be in the majority 占多数【反】 minority
515、make [meik]（made, made） v. 做；制作；使得
【考】 be made of 由……制成（看得出原材料）；be made from 由……制成（看不出原材料）；
be made in 在……制作；make fun of 取笑，开玩笑；make sure of 确信，确定；
make up 拼凑，编造，赔偿；make sb. do sth. 迫使某人做某事
516、manage ['mænidʒ] v. 管理；设法对付
【辨】manage to do 和 try to do：manage to do sth.设法去做成（强调结果）；
try to do 尽力（去）做（强调动作）。
517、mankind [mæn'kaind] n. 人类
518、manner ['mænə] n. 方式，方法；态度，举止；（pl.）礼貌，规矩
519、manual ['mænjuəl] a. 用手的，体力的
521、mark [mɑːk] n./v. 记号，痕迹，分数，作标记
522、mass [mæs] n. 团，堆，民众，大量
523、master ['mɑːstə] n. 主人 v. 掌握
524、match [mætʃ] v. 使相配，使成对 n. 竞赛；火柴
525、material [mə'tiəriəl] n. 材料，原料 a. 物质的，有形的
526、matter ['mætə] n. 要紧事；问题 vi. 要紧，有重大关系
【考】 as a matter of fact（=in fact） 事 实 上；no matter how/what 不管怎样
527、maximum ['mæksiməm] n. 最大量 a. 最大的，最多的
528、mean [miːn，min]（meant, meant） vt. 意思是；打算，意味着
【考】 mean to do 打 算 做 某 事；mean doing 意味着
529、means [miːnz] n. 方法，手段，工具
【考】 by means of 用，凭借；by all means 尽一切办法；by no means 绝不
530、measure ['meʒər] v. 测量 n. 尺寸，标准；措施，办法【考】 take measures 采取措施
531、medical ['medikəl] a. 医学的，医疗的
532、meet [miːt]（met, met） v. 碰到，见；满足
533、memory ['meməri] n. 记忆，纪念
【考】 in memory of sb./to the memory of sb. 作为对某人的纪念
534、mend [mend] v. 修补，修理，缝补
535、mental ['mentl] a. 精神的；脑力的
536、mention ['menʃən] v. 提到；说起
【考】 don’t mention it 不客气，不用谢； not to mention 更不用说；更不必说
537、mercy ['məːsi] n. 怜悯，宽恕【考】 at the mercy of 任凭，摆布
538、mere [miə] a. 纯粹的，仅仅的
539、method ['meθəd] n. 方法
540、mind [maind] n. 头，头脑；想法 v. 介意
【考】 make up one’s mind 下决心；    keep in mind记 住；
slip one’s mind 忘 记； 想 不 起；      mind doing sth. 介意做某事
541、minus ['mainəs] a. 减的，负的 prep. 减
542、miracle ['mirəkəl] n. 奇迹，令人惊奇的人（或事）
543、miserable ['mizərəbl] a. 悲惨的
544、misunderstand ['misʌndə'stænd] v. 误解，误会，曲解
545、mix [miks] n. 混合，混合物 v. 混合，搞混
546、mixture ['mikstʃə] n. 混合（物），混杂
547、model ['mɔdəl] n. 原型；样式；模特
548、modest ['mɔdist] a. 端庄的，朴素的；谦虚的，谦逊的
549、moment ['məumənt] n. 片刻，瞬间；时机
550、monitor ['mɔnitə] n. 监视器；（学校的）班长 v.监听，监视；检测
Lesson 12
551、move [muːv] v. 移动，搬家；运行；使……感动
【考】 move away 搬走，移开；离开；move into/in 迁入新居；移入；move on 继续前进
552、multiply ['mʌltiplai] v. 乘，增加，繁殖
553、murder ['məːdə] n./v. 谋杀，凶杀
554、mutual ['mjutʃuəl] a. 相互的，共同的
555、mystery ['mistəri] n. 神秘，神秘的事物
556、name [neim] a. 名字；名声 v. 命名，取名【考】 name after 以……名字命名
557、native ['neitiv] n. 土著，当地人 a. 本国的，土生的
558、nature ['neitʃə] n. 大自然；天性
559、necessary ['nesə səri] a. 必须的，必要的，必然的
560、necessity [ni'sesiti] 必要，（近切）需要，必需品
561、negative ['neɡətiv] a. 消极的；否定的
562、neglect [nig'lekt] n./v. 疏忽，忽视，忽略
563、nerve [nəːv] n. 神经，胆量
564、nervous ['nəːvəs] a. 神经的，紧张不安的
565、normal ['nɔːməl] a. 正常的
566、notice ['nəutis] v. 注意，通知，留心 n. 注意，布告，通知
【考】 take no notice of sb./sth. 不理会……
567、obey [ə'bei] v. 服从，听由【反】 disobey
568、object ['ɔbdʒikt] n. 物，物体；宾语
【例】 A “black hole” is an object made by pure energy. 黑 洞 是一种由纯能量组成的物质。
569、objective [əb'dʒektiv] n. 目标 a. 客观的
570、observe [əb'zəːv] v. 观察，观测；发现；遵守；庆祝【近】 notice
571、obstacle ['ɔbstəkl] n. 障碍，干扰【考】 an obstacle to… 对……的障碍
572、obtain [əb'tein] vt. 获得，得到【考】 obtain sth. from sb./sth. 从……获得……
573、obvious ['ɔbviəs] a. 明显的，显而易见的
574、occasion [ə'keiʒən] n. 场合，时节，时刻
.575、occupy ['ɔkjupai] v. 占用，占；占领；占据（时间）
576、occur [ə'kəː] vi. 发生，出现，存在
【考】 occur to sb. 某人想起……；It occurs to sb.that ... 念头浮现于某人的脑海
577、offend [ə'fend] v. 冒犯，使不快
578、offer [ɔfə] v. 提供，提议
579、omit [əu'mit] v. 省略，忽略
580、operate ['ɔpəreit] vt. 操作 vi. 运转；动手术；起作用
581、opinion [ə'pinjən] n. 看法，见解【考】 in one’s opinion 在某人看来
582、opportunity [ɔpə'tjuːniti] n. 机会【近】 chance
583、oppose [ə'pəuz] v. 反对
584、opposite ['ɔpəzit] a. 对面的；相反的 n. 相反，对立，对立面
585、optional ['ɔpʃənəl] a. 可以任选的
586、order ['ɔːdə] n. 顺序；秩序 v. 命令；定购；点菜
【考】 in order to do sth. 目的在于做某事；out of order 无次序的；有毛病的，出故障的；
in order 按顺序
587、ordinary ['ɔːdinəri] a. 平常的，普通的
588、organize ['ɔːɡənaiz] vt. 组织，安排
589、origin ['ɔːridʒin] n. 起源，来源，由来
590、output ['autput] n. 产出；产量
591、outstanding [aut'stændiŋ] a. 杰出的，优秀的
592、overcome [əuvə'kʌm] v. 战胜；压倒；克服
593、overlook [əuvə'luk] v. 俯瞰，忽略，宽容
594、overnight ['əuvə'nait] ad. 一夜间；突然
595、owe [əu] v. 欠，应向……支付，得到感激
596、own [əun] v. 拥有 a. 自己的
597、owner ['əunə] n. 物主，所有人
598、pace [peis] n. 步子，步速
599、pack [pæk] v. 包装，挤满 n. （一）包，（一）群
600、participate [pɑː'tisipeit] v.（in）参加，参与；分享【近】 join
【考】 participate in sth. with sb. 同某人参与某事
Lesson 13
601、passive ['pæsiv] a. 被动的，消极的
602、patient ['peiʃ（ə）nt] n. 病人，患者 a. 有耐心的
603、pattern ['pætən] n. 模式，式样；图案，花样
604、perfect ['pəːfikt] a. 完美的，极好的
605、perform [pə'fɔːm] v. 表演；做，完成；行动
606、period ['piəriəd] n. 时期，时代，一段时间
607、permanent ['pəːmənənt] a. 永久的，持久的
608、permit [pə'mit] v. 允许，许可
【考】 permit sb. sth. 允许；permit doing sth. 允许做某事；
permit sb. to do sth. 允许某人做某事
609、personal ['pəːsənl] a. 个人的，私人的；本人的，亲身的
610、persuade [pə'sweid] v. 说服，劝说
【考】 persuade sb. to do sth.=persuade sb. into doing sth. 说服某人做某事
611、philosopher [fi'lɔsəfə] n. 哲人【派】 philosophy n. 哲学
612、physical ['fizikəl] a. 身体的；物理的【派】 physicist n. 物理学家
613、pick [pik] vt. 挑选，选择；拾起；学会（语言）
【考】 pick up 捡起；乘载；学会；pick out 挑出，选出；辨别出
614、pioneer [paiə'niə] n. 先驱者，开拓者
615、place [pleis] vt. 放置，安置，安排 n. 地方，处境，职务
【考】 take place 发生，出现；take the place of 代替， 取 代；in place of 代 替；
out of place不得其所的，不适当的
616、point [pɔint] v. 指 n. 尖端；点；观点；意义
【考】 point at 指向；point out 指出，指明；come to the point 说到要点，扼要地说
617、poison ['pɔizn] n. 毒物，毒药 v. 放毒，毒害
618、polite [pə'lait] a. 有礼貌的，客气的
619、politics ['pɔlitiks] n. 政治；政治学
620、pollute [pə'luːt] v. 污染
621、population [ pɔpju'leiʃən] n. 人口
622、possess [pə'zes] vt. 持有，拥有
【考】 in possession of 拥 有；in the possession of（某物）为（某人）所有
623、postpone [pəust'pəun] vt. 推迟，延期【近】 delay v. 延缓；使延期
624、potential [pə'tenʃəl] a. 潜在的；可能的 n. 潜力
625、poverty ['pɔvəti] n. 贫困，贫穷
626、powerful ['pauəful] a. 强有力的
627、practical ['præktikəl] a. 实际的，实用的
628、practice ['præktis] n. 习惯，常规，惯例
【考】 in practice 在 实 践 中， 实 际 上；out of practice 久不练习，荒疏
629、praise [preiz] n./vt. 赞扬，表扬
630、pray [prei] v. 祈祷，祈求
631、predict [pri'dikt] vt. 预言，预测
632、prefer [pri'fəː] v. 更喜欢，宁可，宁愿
【考】 （1）prefer+ 名词 / 动名词 / 代词，意为“宁愿，更喜欢”；
（2）prefer to do，意为“宁愿做……”；（3）prefer ... to 意为“喜欢……而不喜欢”
633、preserve [pri'zəːv] vt. 保护，维护；保存
634、deserve [di'zəːv] vt. 应受，该得
635、present ['prez(ə)nt] a. 出席的；目前的 n. 目前，现在；礼物 [pri'zent] v. 提出，出示
【派】 presence n. 出席，在场   【反】 absence n. 缺席【考】 at present 目前，现在
636、press [pres] v. 压，挤，按 n. 报刊，出版界
637、pressure ['preʃə(r)] n. 压力，紧张
638、prevent [pri'vent] v. 阻止，防止
【考】 prevent/stop/keep sb. from doing 阻止某人做某事
639、previous ['priːvjəs] a. 先的，前的，以前的 a. （to）在……之前……
640、primary ['praiməri] a. 首要的，基本的；最初的
641、principle ['prinsəpl] n. 原理，原则；准则【考】 on principle 根据原则
642、prior ['praiə] a. 在前的，优先的     【考】 prior to prep. 在……之前
643、private ['praivit] a. 私人的
644、privilege ['privilidʒ] n. 优惠，特权 v. 给予特权
645、prize [praiz] n. 奖金，奖品
646、procedure [prə'siːdʒə] n. 程序，手续
647、proceed [prə'siːd] v. 继续进行
648、process [prə'ses] n. 过程，历程v. 加工，处理
649、profit ['prɔfit] n. 利润，盈余；利益
650、progress ['prəugres] n. 前进，发展；前进 vi. 前进，进行
【考】 in progress 在进行中，在发展中；make progress （in）（在……方面）取得进步
Lesson 14
651、prohibit [prə'hibit] vt. 禁止；阻止【考】 prohibit sb. from doing sth. 禁止某人做某事
652、promise ['prɔmis] v. 承诺，答应 n. 承诺，诺言
【考】 promise to do sth. 承 诺 做 某 事；make a promise 做出承诺；
keep one’s promise 信守（兑现）诺言
653、promote [prə'məut] vt. 提升，晋升；促进
654、proof [pruːf] n. 证明，证据
655、property ['prɔpəti] n. 财产，资产，所有物
656、proportion [prə'pɔːʃən] n. 部分，比例，比重
657、propose [prə'pəuz] v. 提议，提名；求婚【派】 proposal n. 提议，建议；求婚
【考】 propose to do sth./doing sth./that ...提议……
658、prospect ['prɔspekt] n. 展望，前景
659、protect [prə'tekt] vt. 保护【派】 protection n. 保护
【考】 protect ... from/against 保护……不受到损害或侵犯
660、proud [praud] a. 自豪的，骄傲的【考】 be proud of 骄傲，自豪
661、prove [pruːv] vt. 证明，证实
662、provide [prə'vaid] v. 提供
【考】 provide sb. with sth. 为 某 人 提 供 某 物；provide sth. for sb. 向某人提供某物
663、provided [prə'vaidid] conj. 只要，假如
664、public ['pʌblik] a. 公共的；公开的 n. 公众
665、publish ['pʌbliʃ] v. 出版，公开，发行
666、pull [pul] v./n. 拉，拖，牵【考】 pull on 穿上，戴上；pull out 拔出，抽出
667、punish ['pʌniʃ] v. 惩罚，处罚
668、purchase ['pətʃəs] vt. 买，购买
669、pure [pjuə] a. 纯的；完全的，十足的
670、purpose ['pəːpəs] n. 目的，意图
【考】 on purpose 故 意 地；for （the） purpose of为……的目的；为的是
671、pursue [pə'sjuː] vt. 追赶；追求；从事
672、put [put]（put, put） vt. 放；穿（衣）；提出 vi. 放
【考】 put off 推迟，延期；put on 穿上；put away 放好；put through 接通电话；put up with
忍受；put out 熄灭；put up 搭起，张贴
673、qualify ['kwɔlifai] v. 取得资格
674、quality ['kwɔliti ] n. 质量；性质；品质
675、quantity ['kwɔntiti] n. 量，数量
【考】 a large quantity of+ 名词（谓语动词的单复数由名词决定）；
large quantities of+ 名词（谓语动词一律用复数形式）。
676、quarter ['kwɔːtə] n. 四分之一；一刻钟
677、quit [kwit] v. 离开，停止，辞职
678、raise [reiz] vt. 使升高；抚养，饲养
679、range [rendʒ] vi. （在一定范围内）变化，变动 n.一系列；范围
【考】 range from ... to ... 在……范围内变化；in/within range 在射程内
680、rank [ræŋk] n. 排；等级 v. 评价；把……评级
681、rapid ['ræpid] a. 快的，急速的
682、rare ['rɛə] a. 罕见的，稀有的
683、rate [reit] n. 速率，比率
684、rather ['rɑːðə] ad. 宁可，宁愿；更确切地说
【考】 would rather do ... than do ... 宁愿……也不愿……；rather than 宁愿；不顾
685、reach [riːtʃ] vt. 伸手（或脚等）够到；到达
【考】 reach for 伸手去拿；摸（某物）；out of reach 够不着
686、react [ri'ækt] v. （作出）反应
687、recall [ri'kɔl] v. 回想，想起
688、recognize ['rekəgnaiz] v. 认出，识别；承认
689、recommend [rekə'mend] v. 劝告，建议；介绍，推荐
700、recover [ri'kʌvə] v. 恢复，痊愈
Lesson 15
701、refer [ri'fəː] v. （to）提到，指；参考，查阅；提交，上呈
702、reform [ri'fɔːm] n./v. 改革；革新
703、refuse [ri'fjuːz] v. 拒绝，不接受，不同意
704、regard [ri'ɡɑːd] vt. 看作 n. [ pl.] 问候
【考】 with regard to 关于；regard ... as 把……当作……
705、regardless [ri'ɡɑːdlis] a. （of）不管/706、register ['redʒistə] v. 登记，注册
706、regret [ri'ɡret] n./v. 遗憾，懊悔，抱歉
【考】 regret doing 后悔做了某事；regret to do 遗憾做某事
707、regular ['reɡjulə] a. 规则的；定期的；经常的
708、reject [ri'dʒekt] vt. 抛弃；拒绝
【近】 refuse
709、relate [ri'leit] v. 联系，关联
710、relative ['relətiv] n. 亲戚 a. 相对的，比较的
711、relax [ri'læks] v. 放松，松弛
712、release [ri'liːs] n./v. 释放，解放
713、relieve [ri'liːv] v. 缓解，消除，减少
714、religion [ri'lidʒən] n. 宗教，信仰
715、religious [ri'lidʒəs] a. 宗教的，信教的，虔诚的
716、reluctant [ri'lʌktənt] a. 不愿的，勉强的
717、rely [ri'lai] vi. 依靠，依赖；信赖，信任
【考】 rely on/upon 依赖于【派】 reliable a. 可靠的，可信赖的【近】 depend
718、remain [ri'mein] v. 剩下；保持【考】 remain to do 尚待
719、remark [ri'mɑːk] n./v. 评论，谈论
720、remarkable [ri'mɑːkəbl] a. 显著的，非凡的
721、remind [ri'maind] v. （of）提醒，使想起
722、remove [ri'muːv] v. 消除；脱掉，搬
723、repair [ri'pɛə] n./v. 修理，修复
724、replace [ri'pleis] v. 替换，取代
725、represent [riːpri'zent] vt. 代表
726、request [ri'kwest] n./v. 请求，要求【考】 request sb. to to sth. 要求某人做某事
727、require [ri'kwaiə] vt. 需求，要求
【考】 require（sth.）of sb.；require doing/to bedone 需要做
728、rescue ['reskjuː] n./v. 营救，救援
729、research [ri'səːtʃ] n./v. 研究，调查
730、resemble [ri'zembl] vt. （不用于被动语态）像，类似
731、resident ['rezidənt] n. 居民
732、resign [ri'zain] v. 辞去，辞职【近】 quit
733、resist [ri'zist] v. 抵抗，反抗；忍住，抵制【反】 resistant a. 抵抗的；resistance n. 抵抗
734、resource [riˈsɔːs] n. 资源
735、respect [ri'spekt] n./v. 尊重，尊敬
736、respective [ris'pektiv] a. 各自的，各个的
737、respond [ris'pɔnd] v. 反应；回答【派】 response n. 反应；回答
738、responsibility [rispɔnsə'biliti] n. 责任，负责；职责，义务
【考】 take（on/over）the responsibility for/ofdoing sth. 负起对（做）某事的责任
739、responsible [ri'spɔnsəbl] a. 应负责的，有责任的
【考】 be responsible for sb./sth. 对……负责
740、restless ['restlis] a. 不安的，坐立不安的
741、restrict [ris'trikt] v. 限制，约束
742、result [ri'zʌlt] n. 结果，效果；成绩 vi. 导致
【考】 as a result 由于，因此；result in 结果是；导致；result from 起因于
743、retire [ri'taiə] v. 退休，退出，退役
744、return [ri'təːn] vi. 返回，回来 n./v. 归还；回报
【考】 in return（for） 作 为 …… 的 交 换；return sth. to sb. 把某物归还给某人
745、reveal [ri'viːl] v. 提示，揭露，展现
746、review [ri'vjuː] n./v. 复习，回顾；评论
747、revolt [ri'vəult] v. 反抗，起义
748、revolution [revə'luːʃən] n. 革命；大改变
749、reward [ri'wɔːd] n. 酬谢，报酬，奖金 v. 酬谢，报答，奖酬
750、rid [rid]（rid, rid） v. 使摆脱，使去掉【考】 get rid of 摆脱，除去
Lesson 16
751、rise [raiz] n./v. 上升，上涨
752、risk [risk] n. 风险 v. 冒风险
753、rough [rʌf] a. 粗糙的；粗鲁的，粗暴的
754、route [ruːt] n. 路线，航线
755、routine [ruː'tiːn] a. 例行公事；日常工作
756、royal ['rɔiəl] a. 皇家的
757、rude [ruːd] a. 粗鲁的，不礼貌的
758、ruin ['ruin] v. （使）毁灭，（使）破坏； n. [pl.] 废墟；遗迹
759、rule [ruːl] n. 规则，条例，统治 v. 统治，支配
760、rumo（u）r ['ruːmə] n. 传闻，谣言
761、run [rʌn] v. 奔，跑；经营，运转
【考】 run away 出 逃；run into 偶 然 遇 到；run down 撞倒；run out 用完
762、rush [rʌʃ] n./v. 冲，奔
763、sack [sæk] n. 袋，麻袋；解雇
764、sacrifice ['sækrifais] n./v. 牺牲
765、sail [seil] n./v. 航行 n. 帆
767、sake [seik] n. 缘故，理由【考】 for the sake of 为了
768、sample ['sæmpl] n. 样品，标本
769、satisfy ['sætisfai] v. 使……满意，满足【考】 be satisfied with 对……感到满意
770、scale [skeil] n. 大小，规模
771、scarce [skɛəs] a. 稀少的，缺乏的
772、scare [skɛə] vt. 恐吓，使害怕
773、scenery ['siːnəri] n. 风景，景色
774、schedule ['skedʒjul] n. 日程安排表
【考】 ahead of schedule 提前；on schedule 按时间表，准时
775、scold [skəuld] v. 训斥，责骂
776、score [skɔː] n. 得分，分数
777、seal [siːl] n. 印；封条 v. 盖章；封
778、search [səːtʃ] n./v. 搜寻，搜查，搜索
【考】 search sb./sth. 搜查某人、某地；search for寻找；in search of 寻找
779、second ['sekənd] n. 秒 num. 第二 a. 第二的
780、secretary ['sekrətri] n. 秘书
781、section [sekʃən] n. 章节，部分，地区
782、security [si'kjuːriti] n. 安全，保护，保障
783、see [siː] v. 看见；理解，明白
【考】 see off 给……送行；see through 看穿，识破；see to 负责，照料
784、seek [siːk]（sought, sought） vt. 试图；探寻
【派】 seeker n. 寻找者【考】 seek after/for 寻找；seek to do 试图做某事
785、seem [siːm] vi. 好像，似乎
【考】 seem to do/be 看起来似乎……；it seems/seemed that 觉得似乎……
786、seize [siːz] vt. 抓住（时机等），捉住；夺取，占领
787、seldom ['seldəm] ad. 不常，很少【考】 seldom 放在句首时，句子倒装。
788、selfish ['selfiʃ] a. 自私的，利己的
789、select [si'lekt] v. 选择，挑选，选拔
790、send [send] v. 寄；派遣
【考】 send away 解雇；派遣；send for 召唤，派人去请；send out 派遣；发送
791、senior ['siːnjə] a. 年长的，资格老的，地位高的
792、sense [sens] n. 感觉，意识；意思
【考】 make sense 有道理；有意义；in a sense 从某种意义上
793、sensible ['sensəbl] a. 懂事的，明智的；意思到的
【考】 be sensible of 知晓，明白
794、sensitive ['sensitiv] a. 有感觉的，敏感的
【考】 be sensitive to 对……敏感， 易感受……
795、separate ['sepəreit] a. 分开的，各自的 v. 分开，隔离，分别
【考】 separate ... from ... 使……和……分离
796、serious ['siəriəs] a. 认真的，严肃的；严重的【考】 be serious about sth. 对某事是认真的
797、serve [səːv] v. 为……服务，服役；供应【考】 serve sb. with sth. 为某人提供某物
798、service ['səːvis] n. 服务【考】 be of service 效劳，有帮助
799、set [set] v. 放，置；使开始；调整【考】 set aside 留出；set about doing 开始做某事；
set out to do 动向，出发，开始；set up 建立，设立
800、settle ['setl] v. 解决；安家，定居【考】 settle down 定居
Lesson 17
801、several ['sevərəl] a. 若干，数个
802、severe [si'viə] a. 严重的；严厉的
803、shallow ['ʃæləu] a. 浅的，肤浅的【反】 deep
804、shame [ʃeim] n. 羞愧，耻辱
805、hape [ʃeip] n. 形状；外形；身材【考】 in shape 健康；out of shape 身体状况不佳
806、hare [ʃɛə] v. 分享，分配 n. 一份，份额【考】 share with sb. 与某人分享
807、harp [ʃɑːp] a. 锋利的；尖的
808、elter ['ʃeltə] n. 掩蔽；隐蔽处【考】 take shelter from 躲避
809、hift [ʃift] n./v. 转移，移动，转变
810、ine [ʃain] (shone, shone) v. （使）发光，照耀 n. 光泽，光
811、ck [ʃɔk] vt. 使震惊 n. 震动，震惊
812、hoot [ʃuːt]（shot, shot） v.射击，发射，射中
813、hortage ['ʃɔːtidʒ] n. 不足，缺少
814、tcoming ['ʃɔːt kʌmiŋ] n. 短处，缺点
815、how [ʃəu]（showed, shown） vt. 展示；演出；说明n. 演出，展览，展示
【真】 on show 展览；show ... around 引领……参观；show up 出席；露面；show off 炫耀
816、ink [ʃriŋk] v. 起皱，收缩；退缩
817、t [ʃʌt] v. 关，关闭【考】 shut down 停工，关闭；shut up 住口，关闭
818、 [ʃai] a. 害羞的；易受惊的，胆怯的
819、ht [sait] n. 情景，风景；看见；视力
【考】 at（the）sight of ... 一 看 见 …… 就；lose sight 失 明；out of sight 看 不 见；in sight
被见到，在望；catch sight of 发现，突然看见
820、n [sain] n. 符号，标志；手势 vt. 签名；示意
821、nal ['signəl] n. 信号，暗号
822、nificance [sig'nifikəns] n. 重要性；意义
823、ilar ['similə] a. 类似的，相似的
824、ple ['simpl] a. 简单的，朴素的
825、gle ['siŋɡəl] a. 单个的；唯一的，独身的
826、k [siŋk]（sank, sunk） v. 下落，下沉
827、uation [sitju'eiʃən] n. 情形，境遇，形势【考】 in ... situation 在……状况下
828、skill [skil] n. 技能；技巧；熟练；能力
【考】 social/learning skills 社交能力 / 学习能力；have skill in 有……技能
829、ght [slait] a. 轻微的；微小的
830、p [slip] v. 滑（动）；溜【考】 slip one’s mind/memory 忘了
831、w [sləu] a. 慢的 v. 放慢，减速
832、t [smɑːt] a. 聪明的，伶俐的【近】 clever
833、ll [smel] n. 气味；嗅觉 vt. 闻到
834、oth [smuːð] a. 平滑的；平稳的
835、e [səul] a. 单独的，唯一的
836、emn ['sɔləm] a. 庄严的，严肃，隆重的
837、id ['sɔlid] a. 固体的，结实的
838、ve [sɔlv] v. 解决，解答【派】 solution n. 解答；溶解
839、ewhat ['sʌmhwɔt] ad. 稍微，有点儿
840、e [sɔː] a. 疼痛的，痛心的；痛处，疮口
841、row ['sɔrəu] n. 悲伤，悲哀
842、t [sɔːt] n. 种类，类别
843、rce [sɔːs] n. 源，源泉；来源
844、ce [speis] n. 太空；空间
845、re [spɛə] a. 空闲的，空暇的；多余的【考】 in one’s spare time 在某人的业余时间
846、ial ['speʃəl] a. 特别的，特殊的，专门的
847、cific [spə'sifik] a. 特殊的，具体的，明确的
848、d [spiːd] n. 速度
849、nd [spend] vt. （spent, spent）花费（金钱、时间等），度过
【考】 spend time/money on sth./in doing sth. 花时间 / 金钱做……
850、ll [spil]（spilt, spilt） v. 溢出，流出
Lesson 18
851、it ['spirit] n. 精神；心灵；灵魂
852、pite [spait] n. 恶意，怨恨【考】 in spite of 不顾，不管
853、plit [split] v. 劈开；（使）裂开
854、 [spɔil] v. 搞糟，损坏；宠坏，溺爱
855、sponsor ['spɔnsə] n. 发起人，主办者 v. 发起，主办
856、spot [spɔt] n. 斑点；地点，场所 vt. 弄脏；发现【考】 on the spot 当场，在现场
857、spread [spred]（spread, spread） v. 延伸，展开；传播
858、square [skwɛə] n. 广场；正方（形）
859、stable ['steibl] a. 稳定的，稳固的
860、taff [stɑːf] n. 全体职工，行政人员
861、take [steik] n. 赌注；利害关系【考】 at stake 胜败关头
862、tale [steil] a. 陈旧的，陈腐的
863、tand [stænd] v. 站立；经受，忍受
【考】 stand up 站起，竖起；stand by 站在旁边；stands for 代表，象征
864、sandard ['stændəd] n. 标准，规格
865、stare [stɛə] v. 盯，凝视
866、starve [stɑːv] v. 挨饿，饿死
867、state [steit] n. 国家，州；情况，状态 v. 声明，
868、status ['steitəs] n. 身份，地位
869、steady ['stedi] a. 稳定的；平稳的
870、steal [stiːl]（stole, stolen） vt. 偷，窃取
871、step [step] n. 脚步，台阶，梯级
【考】 step by step 一步一步地；take steps 采取步骤
872、stick [stik] （stuck, stuck） v. 粘贴，粘住；坚持；伸 n.木棍
【考】 stick to sth. 忠于；坚持做某事；stick withsb. 紧跟着某人；stick out 突出，伸出
873、stimulate ['stimjuleit] v. 刺激；鼓励，鼓舞
874、straight [streit] a. 直的；坦率的 ad. 直接地
875、strengthen ['streŋθən] v. 加强，巩固
876、stress [stres] n. 压力；重要性 v. 强调，着重
877、strict [strikt] a. 严格的，严厉的，精确的【考】 be strict with sb. 对某人要求严格
878、strike [straik]（struck, struck） v. 打，击；使突然想起 n. 打击；罢工
879、structure ['strʌktʃə] n. 结构，构造
880、struggle ['strʌgl] n./v. 斗争，奋斗；努力
881、stuff [stʌf] n. 材料，原料，东西
882、stupid ['stjuːpid] a. 愚蠢的，迟钝的
883、style [stail] n. 风格，时髦，时尚
【考】 in style 时兴的，流行的；out of style 过时的，不流行的
884、subject ['sʌbdʒikt] n. 主题；学科；主语
885、submit [səb'mit] v. 呈送，提交；屈服，服从
886、subsequent ['sʌbsikwənt] a. 随后的，后来的
887、succeed [sək'siːd] vi. 成功【反】 fail v. 失败【考】 succeed in doing sth. 成功做某事
888、succession [sək'seʃən] n. 连续，系列
889、suffer ['sʌfə] vt. 遭受，经历 vi. 受苦；患病
890、sufficient [sə'fiʃənt] a. 充分的，足够的
891、suggest [sə'dʒest] vt. 建议，提议；表明，说明
【派】 suggestion n. 建议，意见【考】 suggest doing/that ...（should）建议做某事
892、summary ['sʌməri] n. 摘要，概要【考】 in summary 总的说来
893、superior [sjuː'piəriə] a. 较好的；优越的
894、supply [sə'plai] v./n. 供给；供应【近】 provide
895、support [sə'pɔt] n. 支持，支柱 vt. 维持，支持
【考】 in support of 支持；support sb. in sth. 支持某人做某事
896、suppose [sə'pəuz] vt. 推想，假设 vi. 料想【考】 be supposed to do sth. 应当做某事
897、supreme [sə'priːm] a. 最高的
898、sure [ʃuə] a. 确信的；一定的，必然的【考】 be sure of 确信
899、surface ['səːfis] n. 面，表面；外表，外观
900、surprise [sə'praiz] n. 惊讶，惊奇 v. 使诧异，使惊奇
【考】 in surprise 惊 讶 地； 吃 惊 地；to one’s surprise 使人惊讶的是……
Lesson 19
901、surround [sə'raund] vt. 围绕，包围
902、survey [səː'vei] n./v. 审察；测量；调查
903、survive [sə'vaiv] v. 生存，幸存
904、suspect [səspekt] n. 犯 罪 嫌 疑 人； 怀 疑 对 象[səs'pekt] vt. 怀疑，猜想
【近】 doubt【考】 suspect sb. of （doing）sth. 怀疑某人（做了）某事
905、swear [swɛə] v. （swore，sworn）宣誓，发誓；诅咒，骂人
906、sweep [swiːp] v. 扫，打扫
907、swell [swel]（swelled，swollen） v./n. 膨胀，增大，隆起
908、swing [swiŋ] n./v. 摇摆，摇荡
909、symbol ['simbəl] n. 标志，符号；象征【近】 sign 和 mark
910、sympathy ['simpəθi] n. 同情（心），赞同，同感
911、system ['sistəm] n. 系统；制度，体制
912、take [teik]（took, taken） v. 拿，带；花费；接受；认为
【考】 take care 当心；take off 脱下，起飞；take out 取出；take it easy 不紧张，别急；
take in 接收；take over 接管；take up 占用
913、talent [tælənt] n. 天资，才能；人才【考】 a talent/talents for 有……才能
914、target ['tɑːɡit] n. 靶子；目标【近】 aim; goal
915、task [tɑːsk] n. 任务，工作
916、taste [teist] n. 味道，品尝 v. 品尝，尝味【派】 tasteless a. 无滋味的；tasty a. 味道好的
917、tear [tiə] n. 眼泪 v. [tɛə] （tore, torn）撕开【考】 in tears 流着泪，含着泪，在哭着
918、technology [tek'nɔlədʒi] n. 技术，工艺【派】 technological a. 技术的，工艺的
919、temperature ['tempəritʃə] n. 温度
920、temporary ['tempərəri] a. 临时的，暂时的
921、tend [tend] vi. 易于；趋向，倾向（to）vt. 照料，照看
922、tension ['tenʃən] n. 紧张
923、think [θiŋk]（thought, thought） v. 想；认为
【考】 think up 想 出；think about 考 虑；think of想到，想起；think over 仔细考虑
924、thorough ['θʌrə] a. 彻底的；仔细周到的
925、threat [θret] n. 威胁，危险迹象
926、threaten ['θretn] v. 威胁，恫吓
927、thrive [θraiv] v. 兴旺，繁荣
928、tight [tait] a. 紧的，拉紧的
929、tiny ['taini] a. 微波的，细小的
930、title ['taitl] n. 书名，题目；头衔，称号
931、topic ['tɔpik] n. 论题，题目
932、total ['təutl] a. 全体的，总的；完全的 n. 合计，总数【考】 in total 总共
933、touch [tʌtʃ] n. 接触 v. 触，碰，摸
【考】 keep in touch with 与某人保持联系；lose touch with 和……失去联系
934、tough [tʌf] a. 吃苦耐劳的；强硬的；棘手的，难解的
935、trace [treis] n. 痕迹，踪迹
936、tradition [trə'diʃən] n. 传统，惯例
937、traffic ['træfik] n. 交通
938、train [trein] v. 训练，培养 n. 火车
939、transform [træns'fɔrm ] v. 转变，变形，变革
940、translate [trænz'leit] vt. 翻译【派】 translation n. 翻译
【考】 translate ... into ... 把……翻译成……
941、transport [træns'pɔːt] v. 运送，运输
942、trap [træp] n. 陷阱 vt. 使陷入困境
943、travel ['trævl] v/n. 旅行
944、treasure ['treʒə] v. 珍爱，珍视 n. 财富，宝藏
945、treat [triːt] vt. 对待，看待；治疗【考】 treat ... as ... 把……当成……
946、trend [trend] n. 趋势，倾向；时尚
947、try [trai] v. 试，努力；审理 n. 尝试【派】 trial n. 审讯，试验
【考】 try one’s best to do 尽最大努力去做某事；try on 试穿；try out 试用
948、trick [trik] n. 诡计，花招 v. 哄骗，欺骗【考】 play a trick on sb. 诈骗某人；开某人玩笑
949、trip [trip] n. 旅行，远足
950、trouble ['trʌbl] v. 困扰，使烦恼，麻烦 n. 烦恼，困难【派】 troublesome a. 令人烦恼的
【考】 in trouble 处 于 困 境， 在 困 难 中；have trouble（in）doing sth. 做某事有困难
Lesson 20
951、trust [trʌst] n. 信任，信托 vt. 相信，信任，信赖【考】 trust in 相信
952、truth [truːθ] n. 真实，真相；真理
953、turn [tərn] vt. 旋转 vi. 绕；翻动，变化 n. 次序，旋转
【考】 turn away 将拒之门外          turn on 打开            turn off 关上
turn up（音量）调大            turn down（音量）调小         turn ... into 把……变成
turn out 结果是，最终成为      turn over 翻转；移交            in turn 依次，轮流
954、type [taip] n. 类型，品种，样式 v. 打字
955、typical ['tipikəli] a. 有代表性的，典型的
956、uncertain [ʌn'səːtn] a. 不确定的
957、undergo [ʌndə'gəu] v. 遭受，经历
958、underline [ʌndə'lain] vt. 在……下面画线；强调，加强
959、understand [ʌndə'stænd]（understood, understood）v. 理解；懂；了解
960、undertake [ʌndə'teik] v. 接受，承担
961、uneasy [ʌn'iːzi] a. 局促的；不安的
962、unexpected ['ʌnik'spektid] a. 想不到的，意外的
963、union ['juːnjən] n. 结合；统一；工会，联盟
964、universe ['juːnivəːs] n. 宇宙，万物
965、unless [ən'les] conj. 如果不，除非
966、unload [ʌn'ləud] vt. 卸；卸货
967、upset [ʌp'set] n. 混乱 v. 扰乱 a. 烦乱的，不高兴的
968、use [juːz] vt. 使用，用 [juːs] n. 用法，用途
【派】 useful a. 有用的；useless a. 无用的
【考】 use up 用尽，耗光； make use of 使用，利用；be used to do sth. 被用来做某事
969、used [juːzd] vi. （to）惯常 a. 习惯于……的；用过的，旧的
【考】 used to do sth. 过 去 经 常 做 某 事；get/be used to doing sth. 习惯于做某事
970、vacant ['veikənt] a. 空着的；空缺的；空虚的【反】 occupied a. 已占用的；无空闲的；在使用
971、vacation [və'keiʃən] n. 休假，假期【近】 holiday
972、vain [vein] a. 自负的，徒劳的，无效的【考】 in vain 徒劳，无效
973、value ['vælju] n. 价值，益处 v. 评价；重视
【考】 be of value 有价值；of no value 没价值的
974、vanish ['væniʃ] v. 消失，消散【近】 disappear vi. 消失，不见
975、vanity ['væniti] n. 虚荣心，浮华
976、various ['vɛəriəs] a. 各种各样的，不同的【近】 different
977、vary ['vɛəri] v. 改变，变化；有差异
978、vast [vɑːst] a. 巨大的，广阔的；大量的
979、vehicle ['viːikl] n. 车辆，交通工具
980、venture ['ventʃə] n./v. 冒险
981、victim ['viktim] n. 牺牲品，受害者
982、victory ['viktəri] n. 胜利
983、view [vjuː] v. 看，考虑 n. 风景；观点，见解【考】 in view of 鉴于，考虑到
984、violent ['vaiələnt] a. 暴力的，猛烈的
985、vision ['viʒən] n. 视觉，视力
986、visit ['vizit] n./v. 拜访，访问；参观【考】 pay a visit to 参观，访问
987、vital ['vaitl] a. 至关重要的
988、vivid ['vivid] a. 生动的，鲜艳的，鲜明的
989、vocation [vəu'keiʃən] n. 行业，职业
【派】 vocational a. 行业的，职业的
990、volume ['vɔljum] n. （一）卷，（一）册；体积，容积；音量
991、volunteer [vɔlən'tiə] v. 自愿效劳 n. 志愿者【派】 voluntary a. 自愿的
992、vote [vəut] v. 选举，投票
993、voyage [vɔidʒ] n. 航海，航程
994、wake [weik] vi. 醒来 vt. 唤醒【考】 wake up 醒过来；叫醒
995、wander ['wɔndə] vi. 漫游，游荡，漫步，流浪
996、warn [wɔrn] v. 警告，注意，通知
【考】 warn sb. of sth. 警告某人某事；warn sb.against doing sth. 警告某人不要做某事
997、waste [weist] n. 废物；浪费 v. 浪费【考】 waste sth. on sth.
998、wave [weiv] v. 挥手，挥动；波动 n. （热，光，声等的）波，波浪
999、way [wei] n. 路，路线；方式，手段
【考】 on one’s way to 在去……的途中；in a way在某种程度上；by the way 顺便说；
lose one’s way 迷路
1000、wealth [welθ] n. 财富，财产
1001、weapon ['wepən] n. 武器，兵器
1002、weep [wiːp] v. 哭泣，流泪
1003、weigh [wei] vt. 称……重量；掂量【派】 weight n. 重量，体重
1004、welfare ['welfɛə] n. 福利（事业）
1005、wide [waid] a. 宽阔的 ad. 完全地，充分地【反】 narrow
1006、widespread ['waidspred] a. 分布广的，普遍的
1007、wild [waild] a. 野生的；荒芜的；野蛮的
1008、wipe [waip] n./v. 擦，抹
1009、wise [waiz] a. 有智慧的，聪明的
1010、wish [wiʃ] v. 希望；但愿；祝福 n. 希望；心愿
【考】 当 wish 后的宾语从句中表示与现在事实相反的愿望时，从句常用虚拟语气。
1011、withdraw [wið'drɔː]（withdrew, withdrawn） v.  收回；提取；撤退
1012、witness ['witnis] n. 目击者，见证人v. 目击，目睹
1013、wonder ['wʌndə] v. 对……疑惑；想要知道n. 惊讶；奇迹
【考】 It’s no wonder （that 从 句 ）=No wonder ...难怪……
1014、word [wəːd] n. 单词；言语；诺言
【考】 in a word 总而言之；in other words 也就是说；keep one’s word 守信
1015、worldwide ['wəːldwaid] a./ad. 全世界的（地）
1016、worship ['wəːʃip] v. 崇拜 n. 崇拜，礼拜（仪式）
1017、worth [wəːθ] a. 值……钱，值得的 n. 价值
【考】 worth 只能做表语，后接名词或动名词的主动形式。
1018、worthwhile ['wəːθ'hwail] a. 值得（做）的
1019、wound [wuːnd] n. 创伤，伤口 v. 受伤，伤害
1020、wrap [ræp] v. 卷，包，缠绕
1021、wreck [rek] n. 失事，海难；沉船，残骸v.（船等）失事，遇难
1022、yield [jiːld] v. 生产，出产；让步，屈服   n. 产量，收获量
附录一:英语不规则动词记忆表
1.AAA
动词原形	过去式	过去分词
cost[kɔst]	cost[kɔst]	cost[kɔst]	花费
cut[kʌt]	cut[kʌt]	cut[kʌt]	割，切
hurt[hə:t]	hurt[hə:t]	hurt[hə:t]	受伤
hit[hit]	hit[hit]	hit[hit]	打，撞
let[let]	let[let]	let[let]	让
put[put]	put[put]	put[put]	放下
read[ri:d]	read[ri:d]	read[ri:d]	读
set[set]	set[set]	set[set]	安排，安置
spread[spred]	spread[spred]	spread[spred]	展开，传播，涂
spit[spit]	spit/spat[spit]	spit/spat[spit]	吐痰,
shut[ʃʌt]	shut[ʃʌt]	shut[ʃʌt]	关上, 闭起,停止营业
2.AAB
动词原形        过去式       过去分词
beat[bi:t]       beat[bi:t]      beaten['bi:tn]    打败
3.ABA
动词原形	过去式	过去分词
become	became	become	变
come[kʌm]	came[keim]	come[kʌm]	来
run[rʌn]	ran[ræn]	run[rʌn]	跑
4.ABB
（1）在动词原形后加一个辅音字母d, t或ed构成过去式或过去分词。
动词原形	过去式	过去分词
burn[bə:n]	burnt[bə:nt]	burnt[bə:nt]	燃烧
deal[di:l]	dealt[delt]	dealt[delt]	解决
dream	dreamed/dreamt	dreamed/dreamt[dremt]	做梦
hear[hiə]	heard[hə:d]	heard[hə:d]	听见
hang['hæŋ]	hanged/ hung[hʌŋ]	hanged/ hung	绞死，悬挂
learn[lə:n]	learned/learnt[lə:nt]	learned/learnt[lə:nt]	学习
light['lait]	lit/lighted[lit]	lit/lighted[lit]	点燃, 照亮
mean[mi:n]	meant[ment]	meant[ment]	意思
prove	proved	proven/proved['pru:vən]	证明, 证实,试验
shine[ʃain]	shone/shined	shone/shined[ʃəun, ʃɔn]	使照耀使发光
show[ʃəu]	showed	showed/shown['ʃəun]	展示, 给...看
smell[smel]	smelled/smelt	smelled/smelt[smelt]	闻, 嗅
speed	sped/speeded	sped/speeded[sped]	加速
spell[spel]	spelled/spelt	spelled/spelt[spelt]	拼写
wake[weik]	waked/woke	waked/woken['wəukən]	醒来叫醒,激发
（2）把动词原形的最后一个辅音字母“d”改为“t” 构成过去式或过去分词。
动词原形	过去式	过去分词
build[bild]	built[bilt]	built[bilt]	建筑
lend[lend]	lent[lent]	lent[lent]	借给
rebuild[,ri:'bild]	rebuilt[,ri:'bilt]	rebuilt[,ri:'bilt]	改建, 重建
send[send]	sent[sent]	sent[sent]	送
spend[spend]	spent[spent]	spent[spent]	花费
（3）原形→ought →ought
动词原形	过去式	过去分词
bring[briŋ]	brought[brɔ:t]	brought[brɔ:t]	带来
buy[bai]	bought[bɔ:t]	bought[bɔ:t]	买
fight[fait]	fought[fɔ:t]	fought[fɔ:t]	打架
think[θiŋk]	thought[θɔ:t]	thought[θɔ:t]	思考,想
(4) 原形→aught →aught
动词原形	过去式	过去分词
catch[kætʃ]	caught[kɔ:t]	caught[kɔ:t]	捉,抓
teach[ti:tʃ]	taught[tɔ:t]	taught[tɔ:t]	教
（5）变其中一个元音字母
动词原形	过去式	过去分词
dig[diɡ]	dug[dʌɡ]	dug[dʌɡ]	掘(土),挖(洞、沟等)
feed[fi:d]	fed[fed]	fed[fed]	喂
find[faind]	found	found	发现，找到
get[ɡet]	got[ɡɔt]	got/gotten	得到
hold[həuld]	held[held]	held[held]	拥有，握住，支持
lead[li:d]	led[led]	led[led]	引导, 带领, 领导
meet[mi:t]	met[met]	met[met]	遇见
sit[sit]	sat[sæt]	sat[sæt]	坐
shoot[ʃu:t]	shot[ʃɔt]	shot[ʃɔt]	射击
spit[spit]	spit/spat[spæt]	spit/spat[spæt]	吐痰,
stick[stik]	stuck[stʌk]	stuck[stʌk]	插进, 刺入, 粘住,
win[win]	won[wʌn]	won[wʌn]	赢
（6）原形→□lt/pt/ft→□lt/pt/ft
动词原形	过去式	过去分词
feel['fi:l]	felt[felt]	felt[felt]	感到
keep[ki:p]	kept[kept]	kept[kept]	保持
leave[li:v]	left[left]	left[left]	离开
sleep[sli:p]	slept[slept]	slept[slept]	睡觉
sweep[swi:p]	swept[swept]	swept[swept]	扫
(7)其它
动词原形	过去式	过去分词
lay[lei]	laid[leid]	laid[leid]	下蛋, 放置
pay[pei]	paid[peid]	paid[peid]	付
say[sei]	said[sed]	said[sed]	说
stand[stænd]	stood[stud]	stood[stud]	站
understand	understood	understood	明白
lose[lu:z]	lost[lɔst, lɔ:st]	lost[lɔst, lɔ:st]	失去
have[həv]	had[hæd, həd, əd]	had[hæd, həd, əd]	有
make[meik]	made[meid]	made[meid]	制造
sell[sel]	sold[səuld]	sold[səuld]	卖
tell[tel]	told[təuld]	told[təuld]	告诉
retell[,ri:'tel]	retold[,ri:'təuld]	retold[,ri:'təuld]	重讲，重复，复述
5.ABC（1）原形→过去式→原形+(e)n
动词原形	过去式	过去分词
blow[bləu]	blew[blu:]	blown	吹
drive[ draiv ]	drove[drəuv]	driven[drivən]	驾驶
draw[drɔ:]	drew[dru:]	drawn[drɔ:n]	画画
eat[i:t]	ate[et,eit]	eaten['i:tən]	吃
fall[fɔ:l]	Fell[fel]	fallen['fɔ:lən]	落下
give[ɡiv]	gave[ɡeiv]	given['ɡivən]	给
grow[ɡrəu]	grew[ɡru:]	grown[ɡrəun]	生长
forgive[fə'ɡiv]	forgot[fə'ɡɔt]	forgiven	原谅, 饶恕
know[nəu]	knew[nju:, nu:]	known	知道
mistake[mi'steik]	mistook[mi'stuk]	mistooken	弄错; 误解,
overeat['əuvə'i:t]	overate	overeaten	(使)吃过量
prove[pru:v]	proved	proven/proved	证明,证实, 试验
take[teik]	took[tuk]	taken	拿
throw[θrəu]	threw[θru:]	thrown	抛，扔
ride[raid]	rode[rəud]	ridden['ridən]	骑
see[si:]	saw[sɔ:]	seen[si:n]	看见
show[ʃəu]	showed	showed/shown	展示
write[rait]	wrote[rəut]	written['ritən]	写
（2）原形→过去式→过去式+(e)n
动词原形	过去式	过去分词
break[breik]	broke[brəuk]	broken['brəukən]	打破
choose[tʃu:z]	chose[tʃəuz]	chosen['tʃəuzən]	选择
get[ɡet]	got[ɡɔt]	got/gotten	得到
hide[haid]	hid[hid]	hidden['hidən]	隐藏
forget[fə'ɡet]	forgot[fə'ɡɔt]	forgotten[fə'ɡɔtn]	忘记
freeze[fri:z]	froze[frəuz]	frozen['frəuzn]	冷冻,结冰
speak[spi:k]	spoke[spəuk]	spoken['spəukən]	说
steal[sti:l]	stole	stolen['stəulən]	偷
(3) 变单词在重读音节中的元音字母“i”分别为“a”（过去式）和“u”（过去分词）。                              
动词原形	过去式	过去分词	[i→a →u]   
begin[bi'ɡin]	began[bi'ɡæn]	begun[bi'ɡʌn]	开始
drink[driŋk]	drank[dræŋk]	drunk[drʌŋk]	喝
sing[siŋ]	sang[sæŋ]	sung[sʌŋ]	唱
sink[siŋk]	sank[sæŋk]	sunk[sʌŋk]	下沉, 沉没
swim[swim]	swam[swæm]	swum[swʌm]	游泳
ring[riŋ]	rang[ræŋ]	rung[rʌŋ]	打电话
（4）其它
动词原形	过去式	过去分词
be(am, is, are)	was/ were	been	是
bear[bεə]	bore[bɔ:]	born/borne[bɔ:n]	负担, 忍受
do[du:, du:]	did[did]	done[dʌn]	做
fly[flai]	flew[flu:]	flown[fləun]	飞
go[ɡəu]	went[went]	gone[ɡɔn]	去
lie[lai]	lay[lei]	lain[lein]	躺
wear[wεə]	wore[wɔ:]	worn[wɔ:n]	穿
