package com.yrxy.thread.interview.threadpool;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadPoolDemo3
 * @description：线程池的主要处理流程(直接提交队列)
 * @date 2017/5/25 19:31
 */
public class ThreadPoolDemo3  implements  Runnable {

    public static void main(String[] args) {
        //SynchronousQueue没有容量,是无缓冲等待队列,是一个不存储元素的阻塞队列
        SynchronousQueue<Runnable> queue = new SynchronousQueue<Runnable>();
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, queue);
        for (int i = 0; i < 10000; i++)
        {
            threadPool.execute(
                    new Thread(new ThreadPoolDemo1(), "Thread".concat(i + "")));
            System.out.println("线程池中当前的线程数： " +threadPool.getPoolSize()+",核心线程数："+ threadPool.getCorePoolSize()+",最大线程数："+threadPool.getMaximumPoolSize());
            System.out.println(" SynchronousQueue 队列大小: "+queue.size());
            if (queue.size() > 0)
            {
//                System.out.println("队列中阻塞的线程数" + queue.size());
                System.out.println("队列有线程了，队列中的线程数：" + queue.size()+", 线程池中执行任务的线程数："+threadPool.getActiveCount());
            }
        }
        threadPool.shutdown();
    }

    @Override
    public void run() {
        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
