package com.yrxy.thread.interview.base;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className FutureTaskDemo
 * @description：
 * @date 2017/6/8 10:52
 */
public class FutureTaskDemo implements Callable<Object> {
    private String name;
    public FutureTaskDemo(String name){
       this.name=name;
    }

    public Object call() throws Exception {
        String hello="hello , "+name;
        return hello;
    }

    public static void main(String[] args) {
        FutureTaskDemo task = new FutureTaskDemo("huanglaoxie");
        FutureTask<Object> futureTask = new FutureTask<>(task);
        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            String result = (String) futureTask.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
