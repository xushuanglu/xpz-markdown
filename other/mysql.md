## mysql工作中常用函数
### 保留2位小数

```mysql
SELECT FORMAT(12562.6655,2);
```



### groub by 统计

```mysql
SELECT
      user_id as userId,
      create_user as createUser,
      (SELECT username FROM jc_work_file WHERE user_id = userId ORDER BY create_time desc LIMIT 1) username,
      count(1) as workCardeComplete,
      ifnull(SUM(template_step_num),0) as totalStepNum,
      ifnull(SUM(complete_num),0) as totalCompleteNum,
      FORMAT(ifnull((ifnull(SUM(complete_num),0) * 100 / ifnull(SUM(template_step_num),0)),0),2) completion,
      (SELECT create_time FROM jc_work_file WHERE user_id = userId ORDER BY create_time asc LIMIT 1) createTime
    FROM
      jc_work_file
    WHERE
      del_flag = 0
      AND app_id = #{appId}
      GROUP BY user_id,create_user ORDER BY workCardeComplete DESC,completion DESC,createTime asc
```

### MySQL 大批量插入，如何过滤掉重复数据？

#### sql初始化语句：

```mysql
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `student` (`id`, `name`, `age`) VALUES ('1', 'cat', '12');
INSERT INTO `student` (`id`, `name`, `age`) VALUES ('2', 'dog', '13');
INSERT INTO `student` (`id`, `name`, `age`) VALUES ('3', 'camel', '25');
INSERT INTO `student` (`id`, `name`, `age`) VALUES ('4', 'cat', '32');
INSERT INTO `student` (`id`, `name`, `age`) VALUES ('5', 'dog', '42');
​```mysql
```



#### 1、先看看哪些数据重复了

```mysql
SELECT
	NAME,
	count(1)
FROM
	student
GROUP BY
	NAME
HAVING
	count(1) > 1;

​```mysql
```



#### 2、删除全部重复数据，一条不留

> 直接删除会报错

```mysql
DELETE 
FROM
 student 
WHERE
 NAME IN (
 SELECT NAME 
 FROM
  student 
 GROUP BY
 NAME 
HAVING
 count( 1 ) > 1)
```

报错：1093 - You can't specify target table 'student' for update in FROM clause, Time: 0.016000s

**原因是：更新这个表的同时又查询了这个表，查询这个表的同时又去更新了这个表，可以理解为死锁。mysql不支持这种更新查询同一张表的操作**

> 解决办法：把要更新的几列数据查询出来做为一个第三方表，然后筛选更新。

```mysql
DELETE 
FROM
 student 
WHERE
 NAME IN (
 SELECT
  t.NAME 
FROM
 ( SELECT NAME FROM student GROUP BY NAME HAVING count( 1 ) > 1 ) t)

```

#### 3、删除表中删除重复数据，仅保留一条
在删除之前，我们可以先查一下，我们要删除的重复数据是啥样的

SELECT
 * FROM
 student 
WHERE
 id NOT IN (
 SELECT
 t.id 
    FROM
 ( SELECT MIN( id ) AS id FROM student GROUP BY `name` ) t 
 )

啥意思呢，就是先通过name分组，查出id最小的数据，这些数据就是我们要留下的火种，那么再查询出id不在这里面的，就是我们要删除的重复数据。另外，关注Java知音公众号，回复“后端面试”，送你一份面试题宝典！

开始删除重复数据，仅留一条
很简单，刚才的select换成delete即可

```mysql
DELETE 
FROM
 student 
WHERE
 id NOT IN (
 SELECT
  t.id 
 FROM
 ( SELECT MIN( id ) AS id FROM student GROUP BY `name` ) t 
 )
```

90万+的表执行起来超级快。


#### 4、查询单个字段有逗号的sql

```mysql
SELECT
    nickname,
	username
FROM
	`user`
WHERE
	concat(',', username, ',') LIKE concat('%,','admin', ',%');

```


**查询结果**

| nickname | username  |
| -------- | --------- |
| 许许     | admin,123 |



#### 5、Mysql的列修改成行并显示数据的简单实现



这篇文章主要介绍了Mysql的列修改成行并显示数据的简单实现,本文给大家介绍的非常详细，具有参考借鉴价值，需要的朋友可以参考下



![img](https://img.jbzj.com/file_images/article/201610/2016102410160410.png)

创建测试表：

```mysql
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
`year` int(11) DEFAULT NULL,
`month` int(11) DEFAULT NULL,
`amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

插入数据：

```mysql
INSERT INTO `test` VALUES ('1991', '1', '1.1');
INSERT INTO `test` VALUES ('1991', '2', '1.2');
INSERT INTO `test` VALUES ('1991', '3', '1.3');
INSERT INTO `test` VALUES ('1991', '4', '1.4');
INSERT INTO `test` VALUES ('1992', '1', '2.1');
INSERT INTO `test` VALUES ('1992', '2', '2.2');
INSERT INTO `test` VALUES ('1992', '3', '2.3');
INSERT INTO `test` VALUES ('1992', '4', '2.3');
```

看到题目要求，仔细想想可以：

利用SUM(IF()) 生成列 + WITH ROLLUP 生成汇总行,并利用 IFNULL将汇总行标题显示为 Total_num

实现

SQL代码块如下：

```mysql
select year,
sum(if(month=1,amount,0)) as "M1",
sum(if(month=2,amount,0)) as "M2",
sum(if(month=3,amount,0)) as "M3",
sum(if(month=4,amount,0)) as "M4"
from test
GROUP by year;
```

效果如下：

![img](https://img.jbzj.com/file_images/article/201610/2016102410160511.png)





#### 6、mysql行数据转为列数据



现有如下表：

![img](../images/1061826-20190517205038432-1188031895.png)

 

 

需要统计手机所有售卖价格，显示为如下表:

![img](../images/1061826-20190517204816721-1743031368.png)

需要使用group_concat对price进行处理,并且去重重复价格

sql如下：

```mysql
select type,group_concat(DISTINCT(price) Separator ",") as price from table
```



#### 7、mysql如何给某张表的id字段设置整数自动加1

> 删除原来的列，再新建一个自增的列



#### 8、Mysql如何清空数据库的所有表数据

1.先查询出库中的所有表，“db”是数据库名称

SELECT CONCAT('truncate table ',TABLE_NAME,';') AS a FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'db' ;

2.得到所有表后，复制，粘贴，运行，见下图

![img](..\images\759305-20151130122712312-2088594238.png)

***\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\**\***


The server time zone value '???ú±ê×??±??' is unrecognized or represents more than one time zone.
其中是有乱码。

解决的方法是，在连接mysql的url后面加上了一句话。

原来的url:jdbc:mysql://localhost:3306/stock

修改后的是：jdbc:mysql://localhost:3306/stock?serverTimezone=GMT

修改好，至少这个问题解决了。