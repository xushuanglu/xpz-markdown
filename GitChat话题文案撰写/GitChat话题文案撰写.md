## GitChat话题文案撰写



### 分布式存储FastDFS



       存储不论是在我们平时电脑场景使用，同时在项目开发上也是必不可少的技术。随着每个行业的挑战，越来越多的公司都会考虑使用分布式架构来支撑平时的开发工作，分布式架构的引入给我们带来了很多便捷。考虑到文件存储的灵活性问题，我们在存储文件的时候也可能会选择分布式文件存储，而FastDFS就是一款分布式存储框架，类似的技术还有OSS、MInio等技术。

在本场Chat中您可以很好的去理解分布式存储技术：

1、FastDFS基础使用，Linux环境部署
2、FastDFS系统架构和功能原理的理解
3、FastDFS集群和配置优化
4、FastDFS的应用实战


### 缓存redis


### 持久层框架MyBatis

     在我们项目开发过程中持久层是必不可少的，从最开始的 JDBC，到后来的 Hibernate 每时每刻都在更新。再到后来的 MyBatis 框架。MyBatis 框架也是目前使用最多的一种，当然还有 MyBatis Plus。这篇文章主要讲解 MyBatis 相关内容，希望小伙伴们有所收获。

在本场 Chat 中，会讲到以下内容:

1、自定义持久性框架
2、MyBatis 相关概念
3、MyBatis 基础使用
4、MyBatis 配置文件
5、MyBatis 复杂映射开发
6、MyBatis 注解开发
7、MyBatis 缓存
8、MyBatis 插件
9、MyBatis 架构原理
10、MyBatis 设计模式