/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className VolatileTest4
 * @description：
 * @date 2017/5/22 15:31
 */
public class VolatileTest4 {

    private static volatile long _longVal = 0;

    public static void main(String[] args) throws InterruptedException {

        for(int i=0;i<200;i++) {
            Thread t1 = new Thread(new LoopVolatile());
            t1.start();

            Thread t2 = new Thread(new LoopVolatile2());
            t2.start();

            while (t1.isAlive() || t2.isAlive()) {
                Thread.sleep(10);
            }

            System.out.println("final val is: " + _longVal);
        }
    }

    private static class LoopVolatile implements Runnable {
        public void run() {
            long val = 0;
            while (val < 10000000L) {
                _longVal++;
                val++;
            }
        }
    }


    private static class LoopVolatile2 implements Runnable {
        public void run() {
            long val = 0;
            while (val < 10000000L) {
                _longVal++;
                val++;
            }
        }
    }

}
