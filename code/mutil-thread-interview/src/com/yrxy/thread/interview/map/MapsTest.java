package com.yrxy.thread.interview.map;

import java.util.*;

/**
 * @author huanglaoxie
 * @className MapsTest
 * @description：
 * @date 2017/7/26 18:54
 */
public class MapsTest {
    public static void main(String[] args) {
        testHashMap();
        System.out.println("===========");
        testLinkedHashMap();
        System.out.println("===========");
        testTreeMap();
    }
    private static void testHashMap() {
        Map<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("name1", "黄老邪11");
        hashMap.put("name2", "黄老邪12");
        hashMap.put("name3", "黄老邪13");
        Set<Map.Entry<String, String>> set = hashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            System.out.println("HashMap ,  key:" + key + ",value:" + value);
        }
    }

    private static void testLinkedHashMap() {
        Map<String, String> linkedHashMap = new LinkedHashMap<>(16, 0.75f, true);
        linkedHashMap.put("name1", "黄老邪21");
        linkedHashMap.put("name2", "黄老邪22");
        linkedHashMap.put("name3", "黄老邪23");
        System.out.println("开始时顺序：");
        Set<Map.Entry<String, String>> set = linkedHashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            System.out.println("LinkedHashMap, key:" + key + ",value:" + value);
        }
        System.out.println("通过get方法，导致key为name1对应的Entry到表尾");
        linkedHashMap.get("name1");
        Set<Map.Entry<String, String>> set2 = linkedHashMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry entry = iterator2.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            System.out.println("LinkedHashMap, key:" + key + ",value:" + value);
        }
    }

     static void testTreeMap() {
        TreeMap<String,String> map = new TreeMap<String,String>(new xbComparator());
        map.put("name1", "黄老邪31");
        map.put("name2", "黄老邪32");
        map.put("name3", "黄老邪33");
        Set<String> keys = map.keySet();
        Iterator<String> iter = keys.iterator();
        while(iter.hasNext())
        {
            String key = iter.next();
            System.out.println("TreeMap , key is:  "+key+" ,value: "+map.get(key));
        }
    }
    static class xbComparator implements Comparator
    {
        public int compare(Object o1,Object o2)
        {
            String i1=(String)o1;
            String i2=(String)o2;
            return i1.compareTo(i2);
        }
    }
}
