package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className RunnableTest
 * @description：
 * @date 2017/5/25 10:05
 */
public class RunnableDemo implements Runnable {

    public static void main(String[] args) {
        for(int i=0;i<10;i++){
            RunnableDemo runnableTest=new RunnableDemo();
            Thread th1=new Thread(runnableTest);
            th1.setName("thread "+i);
            th1.start();
        }
    }

    public void run() {
        System.out.println(Thread.currentThread().getName()+"  ~~~ hello ,runnable ~~~~");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
