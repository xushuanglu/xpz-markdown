package com.yrxy.thread.interview.jmm;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className AtmoicLongDemo
 * @description：
 * @date 2017/7/7 14:36
 */
public class AtmoicLongDemo {
    private static AtomicLong value = new AtomicLong();
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            value.set(0);
            testAtomic();
        }
    }
    private static class Test1 implements Runnable {
        public void run() {
            long val = 0;
            while (val < 100000) {
                value.incrementAndGet();
                val++;
            }
        }
    }
    private static class Test2 implements Runnable {
        public void run() {
            long val = 0;
            while (val < 100000) {
                value.incrementAndGet();
                val++;
            }
        }
    }

    private static void testAtomic() throws InterruptedException {
        Thread t1 = new Thread(new Test1());
        t1.start();
        Thread t2 = new Thread(new Test2());
        t2.start();
        t1.join();
        t2.join();
        System.out.println("final val is: " + value);
    }

}
