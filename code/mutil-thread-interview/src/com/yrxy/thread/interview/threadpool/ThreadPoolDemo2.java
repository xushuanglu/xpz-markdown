package com.yrxy.thread.interview.threadpool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author huhuanglaoxie(微信:yfct-8888)
 * @className ThreadPoolDemo2
 * @description：线程池的主要处理流程(非阻塞队列)
 * @date 2016/1/19 16:40
 */
public class ThreadPoolDemo2 implements Runnable {

    public static void main(String[] args) {
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, queue);
        for (int i = 0; i < 10000; i++)
        {
            threadPool.execute(
                    new Thread(new ThreadPoolDemo1(), "Thread".concat(i + "")));
            if (queue.size() > 0)
            {
//                System.out.println("队列中阻塞的线程数" + queue.size());
                System.out.println("队列有线程了，队列中的线程数：" + queue.size()+", 线程池中执行任务的线程数："+threadPool.getActiveCount());
            }
            System.out.println("线程池中当前的线程数： " +threadPool.getPoolSize()+",核心线程数："+ threadPool.getCorePoolSize()+",最大线程数："+threadPool.getMaximumPoolSize());
        }
        threadPool.shutdown();
    }

    @Override
    public void run() {
        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
