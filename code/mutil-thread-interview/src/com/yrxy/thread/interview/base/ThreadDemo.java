package com.yrxy.thread.interview.base;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadTest
 * @description：
 * @date 2017/5/25 9:58
 */
public class ThreadDemo extends Thread {

    public static void main(String[] args) {

        for(int i=0;i<10;i++){
            ThreadDemo threadDemo=new ThreadDemo();
            threadDemo.setName("thread "+i);
            threadDemo.start();
        }
    }

    public void run(){
     System.out.println(Thread.currentThread().getName()+ "   ~~~~~~ hello ,thread ~~~~~");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

