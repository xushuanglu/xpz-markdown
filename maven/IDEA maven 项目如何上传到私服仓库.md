# [IDEA maven 项目如何上传到私服仓库](https://www.cnblogs.com/yuxiaole/p/9771613.html)

> 前言：idea maven 发布版本到私服（快照和正式版）。我有个项目（ jar 包源码），其他 maven 项目能直接引入依赖就最好了，所以必须将这个 jar 包源码发布到 maven 私服仓库里去。这里记录一下。

## 一、发布版本

```
<version>1.0-SNAPSHOT</version>
```

在 pom 文件中添加如下代码：



```xml
    <!--发布到私服: 设置 version 后，选择 maven 的 deploy 命令-->
    <distributionManagement>
        <repository>
            <id>nexus</id>
            <name>nexus Repository RELEASES</name>
            <url>http://127.0.0.1:8081/nexus/content/repositories/releases/</url>
        </repository>
        <snapshotRepository>
            <id>nexus</id>
            <name>nexus Repository SNAPSHOTS</name>
            <url>http://127.0.0.1:8081/nexus/content/repositories/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>
```



然后，idea maven 的 选择设置好的 setting.xml 文件

![image-20210923094624610](../images/20210923094647.png)

 

最后，在 idea maven 中选择 deploy 命令

![img](../images/20210923094757.png)

 

参考文档：https://www.cnblogs.com/rwxwsblog/p/6029636.html