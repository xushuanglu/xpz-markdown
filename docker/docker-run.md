## docker常用软件安装

### mysql
- docker search mysql
- docker pull mysql:5.7.30
- docker run --network host -e MYSQL_ROOT_PASSWORD=root -d --rm mysql:5.7.30
- docker images | grep mysql
- docker ps -a
- docker start 337f3b206173fbd7e97169a6a8281ff3cfbd7e4007bc9d7fd6a05d63e3843ed6
- docker ps -s
- docker exec -it  337f3b206173fbd7e97169a6a8281ff3cfbd7e4007bc9d7fd6a05d63e3843ed6 bash
- mysql -uroot -p -h localhost
- 输入mysql数据库密码

> 本地连接mysql数据库有可能连接不上
>[root@localhost yum.repos.d]# vi  /etc/sysconfig/iptables
 [root@localhost yum.repos.d]# service  iptables restart
 Redirecting to /bin/systemctl restart iptables.service
 Failed to restart iptables.service: Unit not found.
 [root@localhost yum.repos.d]# 
 [root@localhost yum.repos.d]# 
 [root@localhost yum.repos.d]# 
 [root@localhost yum.repos.d]# systemctl restart iptables
 Failed to restart iptables.service: Unit not found.
 [root@localhost yum.repos.d]# systemctl start firewalld.service   //开启防火墙
 [root@localhost yum.repos.d]# systemctl stop firewalld.service    //关闭防火墙
 [root@localhost yum.repos.d]# 
>
>
- 解决方案：

网页地址：https://blog.csdn.net/qq_37655695/article/details/78042824

主要的有两种方法，改表法和授权法，下面将分别介绍。

1、登陆mysql

[java] view plain copy
mysql -u root -p  
2、改表法：修改mysql库的user表，将host项，从localhost改为%。%这里表示的是允许任意host访问，如果只允许某一个ip访问，则可改为相应的ip，比如可以将localhost改为192.168.1.123，这表示只允许局域网的192.168.1.123这个ip远程访问mysql。

[java] view plain copy
mysql> USE MYSQL;   
mysql> UPDATE USER SET host = '%' WHERE user = 'root';   
3、授权法：

[java] view plain copy
mysql> USE MYSQL;  
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION //赋予任何主机访问以及修改所有数据的权限   
例如，你想root用户使用root从任何主机连接到mysql服务器的话。  
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;   
如果你想允许用户root从ip为192.168.1.123的主机连接到mysql服务器，并使用root作为密码   
GRANT ALL PRIVILEGES ON *.* TO 'root'@'192.168.1.123'IDENTIFIED BY 'root' WITH GRANT OPTION;   
mysql> FLUSH PRIVILEGES //修改生效  
最后：

防火墙开放3306端口

1、打开防火墙配置文件
vi  /etc/sysconfig/iptables  

2、增加下面
> -A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT  
3、更改后的防火墙配置文件


[java] view plain copy
# Firewall configuration written by system-config-firewall  
# Manual customization of this file is not recommended.  
*filter  
:INPUT ACCEPT [0:0]  
:FORWARD ACCEPT [0:0]  
:OUTPUT ACCEPT [0:0]  
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT  
-A INPUT -p icmp -j ACCEPT  
-A INPUT -i lo -j ACCEPT  
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT  
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT  
-A INPUT -j REJECT --reject-with icmp-host-prohibited  
-A FORWARD -j REJECT --reject-with icmp-host-prohibited  
  
COMMIT  
4、保存后重启防火墙

service  iptables restart  （操作系统高得话这个命令不适合就用）  systemctl restart iptables

########################################################################################################

在CentOS 7或RHEL 7或Fedora中防火墙由firewalld来管理

添加

firewall-cmd --zone=public --add-port=80/tcp --permanent （–permanent永久生效，没有此参数重启后失效）

firewall-cmd --zone=public --add-port=1000-2000/tcp --permanent
重新载入

firewall-cmd --reload
查看

firewall-cmd --zone=public --query-port=80/tcp
删除

firewall-cmd --zone=public --remove-port=80/tcp --permanent
开启防火墙

systemctl start firewalld.service
关闭防火墙

systemctl stop firewalld.service
查看运行状态

firewall-cmd --state //running 表示运行


### redis
