
### 数据库

        <!-- MySQL -->
		<dependency>
           <groupId>mysql</groupId>
                     			<artifactId>mysql-connector-java</artifactId>
                     			<scope>runtime</scope>
                     		</dependency>
                     		
                     		<!-- PostgreSQL -->
                            		<dependency>
                            			<groupId>org.postgresql</groupId>
                            			<artifactId>postgresql</artifactId>
                            			<scope>runtime</scope>
                            		</dependency>
                            		<!-- H2 DB -->
                            		<dependency>
                            			<groupId>com.h2database</groupId>
                            			<artifactId>h2</artifactId>
                            			<scope>runtime</scope>
                            		</dependency>

                            		<!--分页插件-->
                                            <!--tk mybatis-->
                                            <dependency>
                                                <groupId>tk.mybatis</groupId>
                                                <artifactId>mapper</artifactId>
                                                <version>${tk.mybatis.version}</version>
                                            </dependency>
                                            <dependency>
                                                <groupId>tk.mybatis</groupId>
                                                <artifactId>mapper-spring-boot-starter</artifactId>
                                                <version>1.1.4</version>
                                            </dependency>
                                            <!--tk mybatis-->
                                            
                                            
mqtt消息
	<dependency>
			<groupId>cn.xyliang</groupId>
			<artifactId>integration-mqtt-starter</artifactId>
			<version>0.0.2</version>
		</dependency>