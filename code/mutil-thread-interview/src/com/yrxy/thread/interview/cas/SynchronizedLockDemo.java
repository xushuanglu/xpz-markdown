package com.yrxy.thread.interview.cas;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className SynchronizedLockDemo
 * @description：
 * @date 2017/7/2 17:19
 */
public class SynchronizedLockDemo {
    private boolean locked = false;
    public synchronized boolean lock() {
        if(!locked) {
            locked = true;
            return true;
        }
        return false;
    }

}
