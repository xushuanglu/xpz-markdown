package com.yrxy.thread.interview.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author huanglaoxie(微信:yfct-8888)
 * @className ThreadPoolDemo1
 * @description：线程池的主要处理流程(阻塞队列)
 * @date 2016/1/19 16:23
 */
public class ThreadPoolDemo1 implements Runnable{

    public static void main(String[] args) {
        ArrayBlockingQueue<Runnable> queue=new ArrayBlockingQueue<Runnable>(12);
//        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>(12);
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, queue);
        for (int i = 0; i < 30; i++)
        {
            threadPool.execute(
                    new Thread(new ThreadPoolDemo1(), "Thread".concat(i + "")));
//            System.out.println("线程池中活跃的线程数： " +threadPool.getPoolSize()+",核心线程数："+ threadPool.getCorePoolSize()+",最大线程数："+threadPool.getMaximumPoolSize());
            if (queue.size() > 0)
            {
                System.out.println("阻塞队列有线程了，队列中阻塞的线程数：" + queue.size()+", 线程池中执行任务的线程数："+threadPool.getActiveCount());
            }
            System.out.println(" 线程池中当前的线程数：" +threadPool.getPoolSize());

        }
        threadPool.shutdown();
    }

    @Override
    public void run() {
        try
        {
            Thread.sleep(100);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }



}
